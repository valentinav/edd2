package CAPA_DOMINIO;

public class NODO<Tipo> {
	 Tipo valor;
		
	private NODO<Tipo> siguiente;
	
	public NODO() {
		
	}
	public NODO(Tipo pValor) {
		this.valor=pValor;
	}
	public NODO<Tipo> getSiguiente() {
		return siguiente;
	}
	
	public void setSiguiente(NODO<Tipo> siguiente) {
		this.siguiente = siguiente;
	}
	public Tipo getValor() {
		return valor;
	}
	public void setValor(Tipo valor) {
		this.valor = valor;
	}

}
