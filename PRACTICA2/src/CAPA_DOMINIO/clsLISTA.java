package CAPA_DOMINIO;

public class clsLISTA<Tipo> {
	
	private NODO<Tipo> cabeza;
	private int tamanio;
	public clsLISTA() {
		// TODO Auto-generated constructor stub
	}
	public clsLISTA(String rutaFICHERO)
	{
		 
	}
	public boolean listaVacia()
	{
		if(cabeza == null)
		{
			return true;
		}
		return false;
	}
	public void AgregarElementoAlFinal(Tipo parElemento)
	{
		NODO<Tipo> varNuevoNodo= new NODO<Tipo>(parElemento);	
		if(listaVacia())
		{
			this.cabeza=varNuevoNodo;
		}
		NODO<Tipo> varNodoAuxiliar=this.cabeza;
		while(varNodoAuxiliar.getSiguiente()!=null)
		{
			varNodoAuxiliar= varNodoAuxiliar.getSiguiente();
		}
		varNodoAuxiliar.setSiguiente(varNuevoNodo);
		this.tamanio++;
	}
	public void agregarAlInicio(Tipo valor){
            NODO<Tipo> nuevo = new NODO<Tipo>(valor);
	        nuevo.setValor(valor);
	        if (listaVacia()) {
            cabeza = nuevo;
	        } 
	            nuevo.setSiguiente(cabeza);
	            cabeza = nuevo;
	        
	        tamanio++;
	    }
    public void insertarPorReferencia(Tipo referencia, int valor){
    	    NODO<Tipo> nuevo = new NODO<Tipo>();	       
	        if (!listaVacia()) {
	            if (buscar(referencia)) {
	                NODO<Tipo> aux = cabeza;
	                while (aux.getValor() != referencia) {
	                    aux = aux.getSiguiente();
	                }
	                NODO<Tipo> siguiente = aux.getSiguiente();
	                aux.setSiguiente(nuevo);
	                nuevo.setSiguiente(siguiente);
	                tamanio++;
	            }
	        }
	    }
	public void insertarPorPosicion(int posicion, Tipo valor){
	    	    if(posicion>=0 && posicion<=tamanio){
	            NODO<Tipo> nuevo = new NODO<Tipo>();
	            nuevo.setValor(valor);
	            if(posicion == 0){
	                nuevo.setSiguiente(cabeza);
	                cabeza = nuevo;
	            }
	            else{
	                if(posicion == tamanio){
	                    NODO<Tipo> aux = cabeza;
	                    while(aux.getSiguiente() != null){
	                        aux = aux.getSiguiente();
	                    }
	                    aux.setSiguiente(nuevo);              
	                }
	                else{
	                    NODO<Tipo> aux = cabeza;
	                    for (int i = 0; i < (posicion-1); i++) {
	                        aux = aux.getSiguiente();
	                    }
	                    NODO<Tipo> siguiente = aux.getSiguiente();
	                    aux.setSiguiente(nuevo);
	                    nuevo.setSiguiente(siguiente);
	                }
	            }
	            tamanio++;
	        }
	    }  
	public boolean buscar(Tipo referencia){
	        NODO<Tipo> aux = cabeza;
	        boolean encontrado = false;
	        while(aux != null && encontrado != true){
	            if (referencia == aux.getValor()){
	                encontrado = true;
	            }
	            	        }
	        return encontrado;
	    }
}
