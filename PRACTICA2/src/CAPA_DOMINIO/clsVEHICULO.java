package CAPA_DOMINIO;

public class clsVEHICULO {
	  private int codigo;
	  private String tipo;
	  private String marca;
	  private String linea;
	  private int modelo;
	  private int cantidad;
	  private String motor;
	  private int precio;
	  public clsVEHICULO() {
		
	  }
	  public clsVEHICULO(int pCodigo,String pTipo, String pMarca, String pLinea, int pModelo, int pCantidad, String pMotor, int pPrecio) {
		  this.codigo=pCodigo;
			this.tipo=pTipo;
			this.marca=pMarca;
			this.linea=pLinea;
			this.modelo=pModelo;
			this.cantidad=pCantidad;
			this.motor=pMotor;
			this.precio= pPrecio;
	  }
	  
		public int getCodigo() {
			return codigo;
		}
		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}
		public String getTipo() {
			return tipo;
		}
		public void setTipo(String tipo) {
			this.tipo = tipo;
		}
		public String getMarca() {
			return marca;
		}
		public void setMarca(String marca) {
			this.marca = marca;
		}
		public String getLinea() {
			return linea;
		}
		public void setLinea(String linea) {
			this.linea = linea;
		}
		public int getModelo() {
			return modelo;
		}
		public void setModelo(int modelo) {
			this.modelo = modelo;
		}
		public int getCantidad() {
			return cantidad;
		}
		public void setCantidad(int cantidad) {
			this.cantidad = cantidad;
		}
		public String getMotor() {
			return motor;
		}
		public void setMotor(String motor) {
			this.motor = motor;
		}
		public int getPrecio() {
			return precio;
		}
		public void setPrecio(int precio) {
			this.precio = precio;
		}
		  
}
