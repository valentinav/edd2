package Presentacion;

import Logica_Programa.ArbolAVL;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 * @author Fabio Salazar
 */

public class Cliente {
    static ArbolAVL arbAVL = new ArbolAVL();
    
    public static void main(String[] args) throws IOException{
        
        int opcion = -1, dato;
    do {
            try {
                opcion = menu();
                System.out.println();
                switch (opcion) {
                    case 1:
                        try {arbAVL.leerArchivo();} //Intenta cargar el archivo y a la vez generando el posible arbol
                        catch (IOException ex) {System.out.print("Archivo no existe - " + ex.getMessage());}
                        break;
                    case 2:
                        if (!arbAVL.esVacio()) arbAVL.imprimir(arbAVL.getRaiz(), 0);
                        else JOptionPane.showMessageDialog(null, "El árbol está vacio","¡Cuidado!",JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 3:
                        dato = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar el elemento", "Agregando elemento", JOptionPane.QUESTION_MESSAGE));
                        arbAVL.insertar(dato);
                        break;
                    case 4:
                        if (!arbAVL.esVacio()) {
                            System.out.println("El recorrido del arbol en Preorden es: ");
                            arbAVL.preorden(arbAVL.getRaiz());
                        } else JOptionPane.showMessageDialog(null, "El arbol esta vacio", "¡Cuidado!", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 5:
                        if (!arbAVL.esVacio()) {
                            System.out.println("\nEl recorrido del arbol en Inorden es: ");
                            arbAVL.inorden(arbAVL.getRaiz());
                        } else JOptionPane.showMessageDialog(null, "El arbol esta vacio", "¡Cuidado!", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 6:
                        if (!arbAVL.esVacio()) {
                            System.out.println("\nEl recorrido del arbol en Postorden es: ");
                            arbAVL.postorden(arbAVL.getRaiz());
                        } else JOptionPane.showMessageDialog(null, "El arbol esta vacio", "¡Cuidado!", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 7:
                        if (!arbAVL.esVacio()) {
                            dato = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar el dato a buscar ", "Buscando dato", JOptionPane.QUESTION_MESSAGE));
                            if (arbAVL.buscarNodo(arbAVL.getRaiz(), dato) == null) JOptionPane.showMessageDialog(null, "El nodo " + dato + " no existe en el arbol ", "¡Nodo no Encontrado!", JOptionPane.INFORMATION_MESSAGE);
                            else System.out.println("Nodo Encotrado ");
                            System.out.println(arbAVL.buscarNodo(arbAVL.getRaiz(), dato));
                        } else JOptionPane.showMessageDialog(null, "El arbol esta vacio ", "¡Cuidado!", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    /*case 8:
                        if (!arbAVL.esVacio()) {
                            dato = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar el dato a eliminar ", "Eliminando dato", JOptionPane.QUESTION_MESSAGE));
                            if (arbAVL.eliminarNodo(dato) == false) JOptionPane.showMessageDialog(null, "El nodo " + dato + " no se encuentra en el arbol ", "¡Nodo no Encontrado!", JOptionPane.INFORMATION_MESSAGE);
                            else JOptionPane.showMessageDialog(null, "El nodo " + dato + " ha sido eliminado del arbol ", "¡Nodo Eliminado!", JOptionPane.INFORMATION_MESSAGE);
                            System.out.println(arbAVL.eliminarNodo(dato));
                        } else JOptionPane.showMessageDialog(null, "El arbol esta vacio ", "¡Cuidado!", JOptionPane.INFORMATION_MESSAGE);
                        break;*/
                    case 0:
                        JOptionPane.showMessageDialog(null, "Aplicacion Finalizada", "Fin", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion Incorrecta", "¡Cuidado!", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (NumberFormatException n) {
                JOptionPane.showMessageDialog(null, "Error " + n.getMessage());
            }
        } while (opcion != 0);
    }
    
    public static int menu(){
        int opcion = Integer.parseInt(JOptionPane.showInputDialog(null,
                          "1.  Cargar el árbol\n"
                        + "2.  Imprimir el árbol\n"
                        + "3.  Agregar un Nodo\n"
                        + "4.  Recorrido Preorden\n"
                        + "5.  Recorrido Inorden\n"
                        + "6.  Recorrido Postorden\n"
                        + "7.  Buscar Nodo especifico\n"
                        //+ "8.  Eliminar Nodo\n"
                        + "0.  Salir\n"
                        + "Elige una opcion: ", "Arboles AVL", JOptionPane.QUESTION_MESSAGE));
        return opcion;
    }    
}
