package Logica_Programa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Fabio Salazar
 */

public class Archivo {
    BufferedWriter archivoEscritura = null;
    BufferedReader archivoLectura = null;

    public void abrirArchivo(String nombre, boolean escritura) throws IOException {
	if(escritura)
            setArchivoEscritura(new BufferedWriter(new FileWriter(nombre)));
	else
            setArchivoLectura(new BufferedReader(new FileReader(nombre)));	
    }
    public void cerrarArchivo() throws IOException{
        if(getArchivoEscritura() != null)
            getArchivoEscritura().close();
        if(getArchivoLectura() != null)
            getArchivoLectura().close();
    }
    public int contarLineas(String nombre) throws IOException {
        abrirArchivo(nombre, false);
        int lineas = 0;
        while(puedeLeer()){
            leerArchivo();
            lineas ++; 
        }
        cerrarArchivo();
	return lineas;
    } 
    public void limpiarArchivo() throws IOException{
        getArchivoEscritura().write("");
    }
    public String leerArchivo() throws IOException{
        return getArchivoLectura().readLine();
    }
    public void escribirArchivo(String datos) throws IOException {
        getArchivoEscritura().write(datos);
    }
    public boolean puedeLeer() throws IOException{
	return getArchivoLectura().ready();
    }

    public BufferedWriter getArchivoEscritura() {
        return archivoEscritura;
    }
    public BufferedReader getArchivoLectura() {
        return archivoLectura;
    }

    public void setArchivoEscritura(BufferedWriter archivoEscritura) {
        this.archivoEscritura = archivoEscritura;
    }
    public void setArchivoLectura(BufferedReader archivoLectura) {
        this.archivoLectura = archivoLectura;
    }
    
}
