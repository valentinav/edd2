package Logica_Programa;

import java.io.IOException;

/**
 * @author Fabio Salazar
 */

public class ArbolAVL {
    
    private Nodo raiz;

    public ArbolAVL() { raiz = null; }
    public Nodo getRaiz() { return raiz; }
    public void setRaiz(Nodo raiz) { this.raiz = raiz; }
    
    //Metodo que indica si el arbol esta vacio
    public boolean esVacio(){
        return raiz == null;
    }
    
    //Buscar un Nodo
    public Nodo buscar(Nodo arbol,int dato){
        if (raiz == null) 
            return null;
        else if (arbol.getDato() == dato) 
            return buscar(arbol.getHijoDer(), dato);
        else
            return buscar(arbol.getHijoIzq(), dato);
    }
    
    //Obtener factor de equilibrio
    public int obtenerFactorEquilibrio(Nodo arbol){
        if (arbol == null) return -1;
        else  return arbol.getFacEquilibrio();
    }
    
    //Rotaciones simples
    public Nodo rotacionIzquierda(Nodo arbol){
        Nodo aux = arbol.getHijoIzq();
        arbol.setHijoIzq(aux.getHijoDer());
        aux.setHijoDer(arbol);
        arbol.facEquilibrio = (Math.max(obtenerFactorEquilibrio(arbol.getHijoIzq()), obtenerFactorEquilibrio(arbol.getHijoDer()))) + 1;
        aux.facEquilibrio = (Math.max(obtenerFactorEquilibrio(aux.getHijoIzq()), obtenerFactorEquilibrio(aux.getHijoDer()))) + 1;
        return aux;
    }
    public Nodo rotacionDerecha(Nodo arbol){
        Nodo aux = arbol.getHijoDer();
        arbol.setHijoDer(aux.getHijoIzq());
        aux.setHijoIzq(arbol);
        arbol.facEquilibrio = (Math.max(obtenerFactorEquilibrio(arbol.getHijoIzq()), obtenerFactorEquilibrio(arbol.getHijoDer()))) + 1;
        aux.facEquilibrio = (Math.max(obtenerFactorEquilibrio(aux.getHijoIzq()), obtenerFactorEquilibrio(aux.getHijoDer()))) + 1;
        return aux;
    }
    
    //Rotaciones Dobles
    public Nodo rotacionDobleIzquierda(Nodo arbol){
        Nodo aux;
        arbol.setHijoIzq(rotacionDerecha(arbol.getHijoIzq()));
        aux = rotacionIzquierda(arbol);
        return aux;
    }
    public Nodo rotacionDobleDerecha(Nodo arbol){
        Nodo aux;
        arbol.setHijoDer(rotacionIzquierda(arbol.getHijoDer()));
        aux = rotacionDerecha(arbol);
        return aux;
    }
    
    //Insertar Nodo
    public Nodo insertar(Nodo arbol, Nodo subArb){
        Nodo nuevoPadre = subArb;
        if (arbol.getDato() < subArb.getDato()) {
            if (subArb.getHijoIzq() == null) 
                subArb.setHijoIzq(arbol);
            else {
                subArb.setHijoIzq(insertar(arbol, subArb.getHijoIzq()));
                if ((obtenerFactorEquilibrio(subArb.getHijoIzq()) - obtenerFactorEquilibrio(subArb.getHijoDer())) == 2) {
                    if (arbol.getDato() < subArb.getHijoIzq().getDato()) 
                        nuevoPadre = rotacionIzquierda(subArb);
                    else 
                        nuevoPadre = rotacionDobleIzquierda(subArb);
                }
            }
        }else if (arbol.getDato() > subArb.getDato()) {
                if (subArb.getHijoDer() == null)  subArb.setHijoDer(arbol);
                else {
                    subArb.setHijoDer(insertar(arbol, subArb.getHijoDer()));
                    if ((obtenerFactorEquilibrio(subArb.getHijoDer()) - obtenerFactorEquilibrio(subArb.getHijoIzq())) == 2){
                        if (arbol.getDato() > subArb.getHijoDer().getDato()) 
                            nuevoPadre = rotacionDerecha(subArb);
                        else 
                            nuevoPadre = rotacionDobleDerecha(subArb);
                    }
            }
        }else  System.out.println("¡¡Este elemento yá se encuentra en el arbol!!");
        //Actualizar altura(Factor Equilibrio)
        if (subArb.getHijoIzq() == null && subArb.getHijoDer() != null) 
            subArb.setFacEquilibrio(subArb.getHijoDer().getFacEquilibrio() + 1);
        else if(subArb.getHijoDer() == null && subArb.getHijoIzq() != null)
                subArb.setFacEquilibrio(subArb.getHijoIzq().getFacEquilibrio() + 1);
        else  subArb.facEquilibrio = Math.max(obtenerFactorEquilibrio(subArb.getHijoIzq()), obtenerFactorEquilibrio(subArb.getHijoDer())) + 1;
        return nuevoPadre;
    }
    
    //Metodo para insertar
    public void insertar(int dato){
        Nodo aux = new Nodo(dato);
        if (getRaiz() == null)  setRaiz(aux);
        else  setRaiz(insertar(aux, getRaiz()));
    }
    
        //Recorrido Preorden
    public void preorden(Nodo arbol){
        if (arbol != null) {
            System.out.print(arbol.getDato() + " ");
            preorden(arbol.getHijoIzq());
            preorden(arbol.getHijoDer());
        }  
    }    
    //Recorrido Inorden
    public void inorden(Nodo arbol){
        if (arbol != null) {
           inorden(arbol.getHijoIzq());
           System.out.print(arbol.getDato() + " ");
           inorden(arbol.getHijoDer());
        }  
    }
    //Recorrido Postorden
    public void postorden(Nodo arbol){
        if (arbol != null) {
           postorden(arbol.getHijoIzq());
           postorden(arbol.getHijoDer());
           System.out.print(arbol.getDato() + " ");
        }  
    }
    
    //Metodo para imprimir los nodos con su respectivo nivel
    public void imprimir(Nodo arbol, int nivel){
        if (arbol != null) {
            imprimir(arbol.getHijoIzq(),nivel + 1);
            imprimir(arbol.getHijoDer(),nivel + 1);
            System.out.println("Nivel: " + nivel + " - Dato: " + arbol.getDato());
        }
    }
    
    //Metodo para Buscar un nodo especifico
    public Nodo buscarNodo(Nodo arbol, int dato){
        while(arbol.getDato() != dato){
            if (dato < arbol.getDato()) arbol = arbol.getHijoIzq();
            else arbol = arbol.getHijoDer();
            
            if (arbol == null) return null;
        }
        return arbol;
    }
    
    //Metodo para cargar el archivo que contiene el arbol
    public void leerArchivo() throws IOException{
        Archivo archivo = new Archivo();
        archivo.abrirArchivo("ArbolAVL.txt", false);
        while(archivo.puedeLeer()){
            String linea = archivo.leerArchivo();
            String[] arbol = linea.split(" ");
        for(int i = 0; i < arbol.length; i++)
            insertar(Integer.parseInt(arbol[i]));
        }
    archivo.cerrarArchivo();
    System.out.println("Archivo cargado.");
    }
    
    //Metodo para Eliminar un nodo del arbol
    public boolean eliminarNodo(int dato) {
        Nodo subArb = raiz;
        Nodo aux = raiz;
        Nodo padre = raiz;
        boolean esHijoIzq = true;
        
        while(aux.getDato() != dato){
            padre = aux;
            if (dato < aux.getDato()) aux = aux.getHijoIzq();
            else{
                esHijoIzq = false;
                aux = aux.getHijoDer();
            }
            if (aux == null) return false;
        }
        //Nodo Hoja
        if (aux.getHijoIzq() == null && aux.getHijoDer() == null) {
            if (aux.equals(raiz)) raiz = null;
            else {
                if (esHijoIzq) padre.setHijoIzq(null);
                else padre.setHijoDer(null);
                if ((obtenerFactorEquilibrio(subArb.getHijoIzq()) - obtenerFactorEquilibrio(subArb.getHijoDer())) == 2) {
                    if (aux.getDato() < subArb.getHijoIzq().getDato()) 
                        padre = rotacionIzquierda(subArb);
                    else 
                        padre = rotacionDobleIzquierda(subArb);
                }
            }
        }else if (aux.getHijoDer().equals(null)) {
                 if (aux.equals(raiz)) raiz = aux.getHijoIzq();
            else 
                if (esHijoIzq) padre.setHijoIzq(aux.getHijoIzq());
                else padre.setHijoDer(aux.getHijoIzq());
        }else if (aux.getHijoIzq() == null) {
                 if (aux.equals(raiz)) raiz = aux.getHijoDer();
            else 
                if (esHijoIzq) padre.setHijoIzq(aux.getHijoDer());
                else padre.setHijoDer(aux.getHijoDer());
        }else{
            Nodo reemplazo = obtenerNodoReemplazo(aux);
            if (aux.equals(raiz)) raiz = reemplazo;
            else if (esHijoIzq)
                    padre.setHijoIzq(reemplazo);
                else
                    padre.setHijoDer(reemplazo);
            reemplazo.setHijoIzq(aux.getHijoIzq());
            }
        return true;
    }
    
    //Metodo de Reemplazo del nodo a Eliminar
    public Nodo obtenerNodoReemplazo(Nodo nodoReemp){
        Nodo reemplazarPadre = nodoReemp;
        Nodo reemplazo = nodoReemp;
        Nodo aux = nodoReemp.getHijoDer();
        while(aux != null){
            reemplazarPadre = reemplazo;
            reemplazo = aux;
            aux = aux.getHijoIzq();
        }
        if (reemplazo != nodoReemp.getHijoDer()) {
            reemplazarPadre.setHijoIzq(reemplazo.getHijoDer());
            reemplazo.setHijoDer(nodoReemp.getHijoDer());
        }
        System.out.println("Nodo reemplazo " + reemplazo);
        return reemplazo;
    }
}
