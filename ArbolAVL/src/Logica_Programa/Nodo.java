package Logica_Programa;

/**
 * @author Fabio Salazar
 */

public class Nodo {
    int dato;
    int facEquilibrio;
    Nodo HijoIzq;
    Nodo HijoDer;

    public Nodo(int dato) {
        this.dato = dato;
        this.facEquilibrio = 0;
        this.HijoIzq = null;
        this.HijoDer = null;
    }
    
    //Metodos Accesores
    public int getFacEquilibrio() {
        return facEquilibrio;
    }
    public int getDato() {
        return dato;
    }
    public Nodo getHijoIzq() {
        return HijoIzq;
    }
    public Nodo getHijoDer() {
        return HijoDer;
    }
    
    //Metodos Mutadores
    public void setDato(int dato) {
        this.dato = dato;
    }
    public void setFacEquilibrio(int facEquilibrio) {
        this.facEquilibrio = facEquilibrio;
    }
    public void setHijoIzq(Nodo HijoIzq) {
        this.HijoIzq = HijoIzq;
    }
    public void setHijoDer(Nodo HijoDer) {
        this.HijoDer = HijoDer;
    }

    public String toString(){
        return "Su dato es: " + dato;
    } 
    
    /*public Nodo insertarNodo(Nodo n, Integer ele){
        if(n == null)
            return new Nodo(ele);
        else if(ele < n.getDato())
            n.setHijoIzq(insertarNodo(n.getHijoIzq(), ele));
        else if(ele > n.getDato())
            n.setHijoDer(insertarNodo(n.getHijoDer(), ele));
        return n;
    }*/
}
