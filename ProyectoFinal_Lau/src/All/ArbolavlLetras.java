package All;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;


public class ArbolavlLetras { 

    private NodoavlLetras raiz;
    int band1 = 0, band2 = 0;
    int altura, nivel;

    public NodoavlLetras getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoavlLetras raiz) {
        this.raiz = raiz;
    }

    public ArbolavlLetras() {
        this.raiz = null;
    }

    public void insertar(String elemento) {
        raiz = insertar(raiz, elemento);
    }

    private void buscar(NodoavlLetras arbol, String dato) {

        if (arbol == null) {
            band1 = -1;
        } else if (dato.equals(arbol.getElemento())) {
            band1 = 1;
        } else if (dato.compareTo(arbol.getElemento())<0) {
            band2 = 1;
            buscar(arbol.getIzq(), dato);
        } else if (dato.compareTo(arbol.getElemento())>0) {
            band2 = 2;
            buscar(arbol.getDer(), dato);
        }

    }

    public boolean buscar(String dato) throws IOException {

        buscar(raiz, dato);

        if (band1 == 1) {
            if (band2 == 1) {
                return true;

            } else if (band2 == 2) {
                return true;
            }
        }
        return false;
    }

    private Object elementosNodo(NodoavlLetras arbol) {
        if (arbol == null) {
            return null;
        } else {
            return arbol.getElemento();
        }
    }

    public static int factorEquilibrio(NodoavlLetras arbol) {
        if (arbol == null) {
            return -1;
        } else {
            return arbol.factorEquilibrio;
        }
    }

    private static int maxFB(int alturaIzquierdo, int alturaDerecho) {
        if (alturaIzquierdo > alturaDerecho) {
            return alturaIzquierdo;
        } else {
            return alturaDerecho;
        }
    }

    private NodoavlLetras insertar(NodoavlLetras arbol, String elemento) {
        if (arbol == null) {
            arbol = new NodoavlLetras(elemento, null, null);
        } else if (elemento.compareTo(arbol.getElemento())<0) {
            arbol.setIzq(insertar(arbol.getIzq(), elemento));
            if (factorEquilibrio(arbol.getIzq()) - factorEquilibrio(arbol.getDer()) == 2) {
                if (elemento.compareTo(arbol.getIzq().getElemento()) < 0) {
                    arbol = RotacionSimpleIzq(arbol);
                } else {
                    arbol = RotacionDobleIzq_Der(arbol);
                }
            }
        } else if (elemento.compareTo(arbol.getElemento()) > 0) {
            arbol.setDer(insertar(arbol.getDer(), elemento));
            if (factorEquilibrio(arbol.getDer())
                    - factorEquilibrio(arbol.getIzq()) == 2) {
                if (elemento.compareTo(arbol.getDer().getElemento()) > 0) {
                    arbol = RotacionSimpleDer(arbol);
                } else {
                    arbol = RotacionDobleDer_Izq(arbol);
                }
            }
        } else
     ;
        arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.getIzq()), factorEquilibrio(arbol.getDer())) + 1;
        return arbol;
    }

    private static NodoavlLetras RotacionSimpleIzq(NodoavlLetras arbol) {
        NodoavlLetras subArbol = arbol.getIzq();
        arbol.setIzq(subArbol.getDer());
        subArbol.setDer(arbol);
        arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.getIzq()), factorEquilibrio(arbol.getDer())) + 1;
        subArbol.factorEquilibrio = maxFB(
                factorEquilibrio(subArbol.getIzq()), arbol.factorEquilibrio) + 1;
        return subArbol;
    }

    private static NodoavlLetras RotacionSimpleDer(NodoavlLetras subArbol) {
        NodoavlLetras arbol = subArbol.getDer();
        subArbol.setDer(arbol.getIzq());
        arbol.setIzq(subArbol);
        subArbol.factorEquilibrio = maxFB(factorEquilibrio(subArbol.getIzq()), factorEquilibrio(subArbol.getDer())) + 1;
        arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.getDer()), subArbol.factorEquilibrio) + 1;
        return arbol;
    }

    private static NodoavlLetras RotacionDobleIzq_Der(NodoavlLetras arbol) {
        arbol.setIzq(RotacionSimpleDer(arbol.getIzq()));
        return RotacionSimpleIzq(arbol);
    }

    private static NodoavlLetras RotacionDobleDer_Izq(NodoavlLetras subArbol) {
        subArbol.setDer(RotacionSimpleIzq(subArbol.getDer()));
        return RotacionSimpleDer(subArbol);
    }

    public void imprimirArbolNiveles(NodoavlLetras arbol, int nivel) {
        if (arbol != null) {
            imprimirArbolNiveles(arbol.getIzq(), nivel + 1);
            System.out.println("" + nivel + "\t" + arbol.getElemento());
            imprimirArbolNiveles(arbol.getDer(), nivel + 1);
        }
    }

    public boolean eliminarNodo(NodoavlLetras arbol, String elementoEliminar) {
        NodoavlLetras padre = raiz;
        try {
            boolean hijoIzquierdo = true;
            while (arbol.getElemento().compareTo(elementoEliminar) != 0) {
                padre = arbol;
                if (elementoEliminar.compareTo(arbol.getElemento()) < 0) {

                    hijoIzquierdo = true;
                    arbol = arbol.getIzq();
                } else {
                    hijoIzquierdo = false;
                    arbol = arbol.getDer();
                }
                if (arbol == null) {
                    return false;
                }
            }
            if (arbol.getIzq() == null && arbol.getDer() == null) {
                if (arbol == raiz) {
                    raiz = null;
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(null);
                } else {
                    padre.setDer(null);
                }
            } else if (arbol.getDer() == null) {
                if (arbol == raiz) {
                    raiz = arbol.getIzq();
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(arbol.getIzq());
                } else {
                    padre.setDer(arbol.getIzq());
                }
            } else if (arbol.getIzq() == null) {
                if (arbol == raiz) {
                    raiz = arbol.getDer();
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(arbol.getDer());
                } else {
                    padre.setDer(arbol.getDer());
                }
            } else {
                NodoavlLetras descendiente = obtenerDescendiente(arbol);
                if (arbol == raiz) {
                    raiz = descendiente;
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(descendiente);
                } else {
                    padre.setDer(descendiente);
                }
                descendiente.setIzq(arbol.getIzq());
            }
            return true;
        } catch (Exception E) {
            
        }
    return true;
    }

    private NodoavlLetras obtenerDescendiente(NodoavlLetras subArbol) {
        NodoavlLetras descendientePadre = subArbol;
        NodoavlLetras descendiente = subArbol;
        NodoavlLetras arbol = subArbol.getDer();

        while (arbol != null) {
            descendientePadre = descendiente;
            descendiente = arbol;
            arbol = arbol.getIzq();
        }
        if (descendiente != subArbol.getDer()) {
            descendientePadre.setIzq(descendiente.getDer());
            descendiente.setDer(subArbol.getDer());

        }
        return descendiente;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int n) {
        this.altura = n;
    }

    public void altura(NodoavlLetras arbol, int nivel) {
        if (arbol != null) {
            if (altura < nivel) {
                setAltura(nivel);

            }
            altura(arbol.getIzq(), nivel + 1);
            altura(arbol.getDer(), nivel + 1);
        }
    }
    

    public int altura() {
        altura = 0;
        altura(this.getRaiz(), 1);
        return getAltura();
    }
    
    String[] niveles;
    
    public ArrayList NivelavLetras() {
        niveles = new String[altura + 1];
        ArrayList l = new ArrayList();
        NivelavLetras(raiz, 0);
        for (int i = 0; i < niveles.length; i++) {
            l.add(niveles[i] + " ");
            //System.out.println(niveles[i] + " ");
        }
        return l;
    }

    private void NivelavLetras(NodoavlLetras pivote, int nivel2) {
        try {
            if (pivote != null) {
                niveles[nivel2] = pivote.getElemento() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
                NivelavLetras(pivote.getDer(), nivel2 + 1);
                NivelavLetras(pivote.getIzq(), nivel2 + 1);
            }
        } catch (Exception E) {

        }
    }
    
    
    public ArrayList PorNivelesAv() {
        ArrayList l = new ArrayList();
        PorNivelesAv(raiz, 1, l);
        return l;
    }

    private void PorNivelesAv(NodoavlLetras reco, int nivel, ArrayList l) {
        if (reco != null) {
            PorNivelesAv(reco.getIzq(), nivel + 1, l);
            l.add(reco.getElemento() + " Nivel: (" + nivel + ") ");
            PorNivelesAv(reco.getDer(), nivel + 1, l);
        }
    }
}
