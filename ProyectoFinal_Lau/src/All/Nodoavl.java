package All;


public class Nodoavl 
 {
  private int elemento;
  private Nodoavl izq;
  private Nodoavl der;
  protected int factorEquilibrio;
  private int posx;
  private int posy;
  private int Hoja;
  int rotacion;
  private int Grado;
  
  Nodoavl AsociacionABB;//este es el apuntador para asociar el Nodo del Arbl AVL "noticia" con el Arbol de "complementos" ABB
    ////////////////////VARIABLES DE INFORMACION/////////////////////////
    String Titulo,Descripcion,Path;
    
  public Nodoavl(int elemento, Nodoavl izq, Nodoavl der )
  {
   this.elemento = elemento;
   this.izq = izq;
   this.der = der;
   factorEquilibrio = 0;
  }

    public int getHoja() {
        return Hoja;
    }

    public void setHoja(int Hoja) {
        this.Hoja = Hoja;
    }
    public void setGrado(int Grado)
    {
        this.Grado = Grado;
    }
    public int getGrado()
    {
        return Grado ;
    }
  
  public Nodoavl(int elemento) 
  {
   this (elemento, null, null );
  }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public Nodoavl getIzq() {
        return izq;
    }

    public void setIzq(Nodoavl izq) {
        this.izq = izq;
    }

    public Nodoavl getDer() {
        return der;
    }

    public void setDer(Nodoavl der) {
        this.der = der;
    }

    public int getElemento() {
        return elemento;
    }

    public void setElemento(int elemento) {
        this.elemento = elemento;
    }
    
    


        
}
