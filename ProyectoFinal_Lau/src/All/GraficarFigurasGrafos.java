package All;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javafx.scene.shape.Circle;
import logica_NegocioGND.NodoGrafoND;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lenovo
 */
public class GraficarFigurasGrafos {

    public void DibujarPunto(Graphics2D a, Punto p) {
       int posx=p.getPosx();
       int posy=p.getPosy();
       String dato=p.getDato();   
        a.drawOval(posx, posy, 30, 30);
        a.drawString(dato, posx+10, posy+20);
    }

    public void DibujarAdyacencia(Graphics2D a, Punto p1,Punto p2,int peso) {
        int posx1=p1.getPosx();
        int posy1=p1.getPosy();
        int posx2=p2.getPosx();
        int posy2=p2.getPosy();
        a.drawLine(posx2, posy2, posx1, posy1);
        a.drawString(Integer.toString(peso), posx1+50, posy1+50);
    }

   /* public void DibujarPunto(Graphics2D a, NodoGrafoND n) {
        if (n != null) {
            

            a.drawRect(n.getPosx(), n.getPosy(), 20, 20);
            a.drawString(Integer.toString(n.getVertice()), n.getPosx() + 4, n.getPosy() + 14);
            if (n.getIzq() != null) {

                a.drawLine(n.getPosx(), n.getPosy() + 10, n.getIzq().getPosx() + 20, n.getIzq().getPosy());
            }
            if (n.getDer() != null) {

                a.drawLine(n.getPosx() + 20, n.getPosy() + 10, n.getDer().getPosx(), n.getDer().getPosy());
            }

            
        }

    }*/

    public void DibujarRaiz(Graphics2D a, Nodo n, int x, int y) {
        a.drawRect(500, 50, 30, 30);
        a.drawString(Integer.toString(n.getElemento()), x + 10, y + 10);

    }

    public void GenerarPosiciones(Nodo n) {
        if (n != null) {
            if (n.getIzq() != null) {
                n.getIzq().setPosx(n.getPosx() - 80);
                n.getIzq().setPosy(n.getPosy() + 30);

            }
            if (n.getDer() != null) {
                n.getDer().setPosx(n.getPosx() + 80);
                n.getDer().setPosy(n.getPosy() + 30);

            }

            GenerarPosiciones(n.getIzq());
            GenerarPosiciones(n.getDer());
        }

    }

    public void GenerarPosiciones(Nodoavl n) {
        if (n != null) {
            if (n.getIzq() != null) {
                //ArrayList<Integer> C = new ArrayList();
                //ArrayList<Integer> D = new ArrayList();
                //PasarVector(arbol.getRaiz(),C,D);
                //boolean b = BuscarPosicion(n.getPosx()-80,n.getPosy()+80,C,D);
                n.getIzq().setPosx(n.getPosx() - 170);
                n.getIzq().setPosy(n.getPosy() + 60);

            }
            if (n.getDer() != null) {
                n.getDer().setPosx(n.getPosx() + 170);
                n.getDer().setPosy(n.getPosy() + 60);

            }

            GenerarPosiciones(n.getIzq());
            GenerarPosiciones(n.getDer());
        }

    }

    public void Resetpos(Nodoavl n) {
        if (n != null) {
            n.setPosx(0);
            n.setPosy(0);
            Resetpos(n.getIzq());
            Resetpos(n.getDer());
        }
    }

    public void Dibujar(Graphics2D a, Nodoavl n) {
        if (n != null) {
            Dibujar(a, n.getIzq());

            a.drawRect(n.getPosx(), n.getPosy(), 20, 20);
            a.drawString(Integer.toString(n.getElemento()), n.getPosx() + 4, n.getPosy() + 14);

            if (n.getIzq() != null) {

                a.drawLine(n.getPosx(), n.getPosy() + 10, n.getIzq().getPosx() + 20, n.getIzq().getPosy());
            }
            if (n.getDer() != null) {

                a.drawLine(n.getPosx() + 20, n.getPosy() + 10, n.getDer().getPosx(), n.getDer().getPosy());
            }

            Dibujar(a, n.getDer());
        }

    }

    public void pulirposizq(Nodoavl arbol) {
        while (arbol != null) {
            if (arbol.getDer() != null) {
                arbol.getDer().setPosx(arbol.getDer().getPosx() - 40);
                pulirposder(arbol.getDer());
                pulirposizq(arbol.getDer());
            }
            arbol = arbol.getIzq();
        }
    }

    public void pulirposder(Nodoavl arbol) {
        while (arbol != null) {
            if (arbol.getIzq() != null) {
                arbol.getIzq().setPosx(arbol.getIzq().getPosx() + 40);
                pulirposizq(arbol.getIzq());
                pulirposder(arbol.getIzq());
            }
            arbol = arbol.getDer();
        }
    }

    public void GradoNodos(Nodo arbol) {
        if (arbol != null) {
            if (arbol.getDer() != null) {
                if (arbol.izquierda != null) {
                    arbol.setGrado(2);
                } else {
                    arbol.setGrado(1);
                }
            } else if (arbol.izquierda != null) {
                arbol.setGrado(1);
            }
            GradoNodos(arbol.izquierda);
            GradoNodos(arbol.getDer());
        }
    }
    
    public void GradoNodosL(NodoLetras arbol) {
        if (arbol != null) {
            if (arbol.getDer() != null) {
                if (arbol.izquierda != null) {
                    arbol.setGrado(2);
                } else {
                    arbol.setGrado(1);
                }
            } else if (arbol.izquierda != null) {
                arbol.setGrado(1);
            }
            GradoNodosL(arbol.izquierda);
            GradoNodosL(arbol.getDer());
        }
    }

    public void GradoNodosAVL(Nodoavl arbol) {
        if (arbol != null) {
            if (arbol.getDer() != null) {
                if (arbol.getIzq() != null) {
                    arbol.setGrado(2);
                } else {
                    arbol.setGrado(1);
                }
            } else if (arbol.getIzq() != null) {
                arbol.setGrado(1);
            }
            GradoNodosAVL(arbol.getIzq());
            GradoNodosAVL(arbol.getDer());
        }
    }

    public void NumeroNodos(Nodo arbol, ArrayList<Nodo> num) {
        if (arbol != null) {
            num.add(arbol);
            NumeroNodos(arbol.izquierda, num);
            NumeroNodos(arbol.derecha, num);
        }
    }

    public void NumeroNodosAv(Nodoavl arbol, ArrayList<Nodoavl> numavl) {
        if (arbol != null) {
            numavl.add(arbol);
            NumeroNodosAv(arbol.getIzq(), numavl);
            NumeroNodosAv(arbol.getDer(), numavl);
        }
    }

    public void NumeroNodosAvL(NodoavlLetras arbol, ArrayList<NodoavlLetras> num) {
        if (arbol != null) {
            num.add(arbol);
            NumeroNodosAvL(arbol.getIzq(), num);
            NumeroNodosAvL(arbol.getDer(), num);
        }
    }
    
    public void NumeroNodosL(NodoLetras arbol, ArrayList<NodoLetras> num) {
        if (arbol != null) {
            num.add(arbol);
            NumeroNodosL(arbol.izquierda, num);
            NumeroNodosL(arbol.derecha, num);
        }
    }

    public void Resetpos(Nodo n) {
        if (n != null) {
            n.setPosx(0);
            n.setPosy(0);
            Resetpos(n.izquierda);
            Resetpos(n.derecha);
        }
    }

    public int NumeroNodosAVL(Nodoavl arbol) {
        if (arbol == null) {
            return 0;
        } else {
            return 1 + NumeroNodosAVL(arbol.getIzq()) + NumeroNodosAVL(arbol.getDer());
        }
    }

    public void ImprimirNivel(Nodo arbol, int nivel, int n) {
        if (arbol != null) {
            ImprimirNivel(arbol.getIzq(), nivel + 1, n);
            if (nivel == n) {
                System.out.println(arbol.getElemento());
            }
            ImprimirNivel(arbol.getDer(), nivel + 1, n);
        }
    }

    ///LETRAS
    public void Dibujar(Graphics2D a, NodoLetras n) {
        if (n != null) {
            Dibujar(a, n.getIzq());

            a.drawRect(n.getPosx(), n.getPosy(), 20, 20);
            a.drawString((n.getElemento()), n.getPosx() + 4, n.getPosy() + 14);
            if (n.getIzq() != null) {

                a.drawLine(n.getPosx(), n.getPosy() + 10, n.getIzq().getPosx() + 20, n.getIzq().getPosy());
            }
            if (n.getDer() != null) {

                a.drawLine(n.getPosx() + 20, n.getPosy() + 10, n.getDer().getPosx(), n.getDer().getPosy());
            }

            Dibujar(a, n.getDer());
        }

    }

    public void DibujarRaiz(Graphics2D a, NodoLetras n, int x, int y) {
        a.drawRect(500, 50, 30, 30);
        a.drawString((n.getElemento()), x + 10, y + 10);

    }

    public void GenerarPosiciones(NodoLetras n) {
        if (n != null) {
            if (n.getIzq() != null) {
                n.getIzq().setPosx(n.getPosx() - 80);
                n.getIzq().setPosy(n.getPosy() + 30);

            }
            if (n.getDer() != null) {
                n.getDer().setPosx(n.getPosx() + 80);
                n.getDer().setPosy(n.getPosy() + 30);

            }

            GenerarPosiciones(n.getIzq());
            GenerarPosiciones(n.getDer());
        }
    }
    
    
    public void Resetpos(NodoLetras n) {
        if (n != null) {
            n.setPosx(0);
            n.setPosy(0);
            Resetpos(n.izquierda);
            Resetpos(n.derecha);
        }
    }

    //AVL LETRAS
    
    public void Dibujar(Graphics2D a, NodoavlLetras n) {
        if (n != null) {
            Dibujar(a, n.getIzq());

            a.drawRect(n.getPosx(), n.getPosy(), 20, 20);
            a.drawString((n.getElemento()), n.getPosx() + 4, n.getPosy() + 14);
            if (n.getIzq() != null) {

                a.drawLine(n.getPosx(), n.getPosy() + 10, n.getIzq().getPosx() + 20, n.getIzq().getPosy());
            }
            if (n.getDer() != null) {

                a.drawLine(n.getPosx() + 20, n.getPosy() + 10, n.getDer().getPosx(), n.getDer().getPosy());
            }

            Dibujar(a, n.getDer());
        }

    }

    public void DibujarRaiz(Graphics2D a, NodoavlLetras n, int x, int y) {
        a.drawRect(500, 50, 30, 30);
        a.drawString((n.getElemento()), x + 10, y + 10);

    }

    public void GenerarPosiciones(NodoavlLetras n) {
        if (n != null) {
            if (n.getIzq() != null) {
                n.getIzq().setPosx(n.getPosx() - 80);
                n.getIzq().setPosy(n.getPosy() + 30);

            }
            if (n.getDer() != null) {
                n.getDer().setPosx(n.getPosx() + 80);
                n.getDer().setPosy(n.getPosy() + 30);

            }

            GenerarPosiciones(n.getIzq());
            GenerarPosiciones(n.getDer());
        }
    }
    
    public void Resetpos(NodoavlLetras n) {
        if (n != null) {
            n.setPosx(0);
            n.setPosy(0);
            Resetpos(n.getIzq());
            Resetpos(n.getDer());
        }
    } 
    

}
