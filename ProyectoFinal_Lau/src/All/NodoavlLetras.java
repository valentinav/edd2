package All;


public class NodoavlLetras 
 {
  private String elemento;
  private NodoavlLetras izq;
  private NodoavlLetras der;
  protected int factorEquilibrio;
  private int posx;
  private int posy;
  private int Hoja;
  int rotacion;
  private int Grado;
  
  NodoavlLetras AsociacionABB;//este es el apuntador para asociar el Nodo del Arbl AVL "noticia" con el Arbol de "complementos" ABB
    ////////////////////VARIABLES DE INFORMACION/////////////////////////
    String Titulo,Descripcion,Path;
    
  public NodoavlLetras(String elemento, NodoavlLetras izq, NodoavlLetras der )
  {
   this.elemento = elemento;
   this.izq = izq;
   this.der = der;
   factorEquilibrio = 0;
  }

    public int getHoja() {
        return Hoja;
    }

    public void setHoja(int Hoja) {
        this.Hoja = Hoja;
    }
    public void setGrado(int Grado)
    {
        this.Grado = Grado;
    }
    public int getGrado()
    {
        return Grado ;
    }
  
  public NodoavlLetras(String elemento) 
  {
   this (elemento, null, null );
  }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public NodoavlLetras getIzq() {
        return izq;
    }

    public void setIzq(NodoavlLetras izq) {
        this.izq = izq;
    }

    public NodoavlLetras getDer() {
        return der;
    }

    public void setDer(NodoavlLetras der) {
        this.der = der;
    }

    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }
    
    


        
}
