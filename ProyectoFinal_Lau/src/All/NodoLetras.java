package All;

//Programa elaborado por Laura Buitrón 
public class NodoLetras {

    String elemento;
    NodoLetras izquierda;
    NodoLetras derecha;
    private int grado;
    private int hoja;
    private int posx;
    private int posy;

    public NodoLetras(String elemento, NodoLetras izq, NodoLetras der) {
        this.elemento = elemento;
        izquierda = null;
        derecha = null;

    }

    public NodoLetras(String Dato) {
        this.elemento = Dato;
        this.izquierda = null;
        this.derecha = null;
        this.grado = 0;
        this.posx = 0;
        this.posy = 0;
    }

    public NodoLetras getIzq() {
        return izquierda;
    }

    public void setIzq(NodoLetras izq) {
        this.izquierda = izq;
    }

    public NodoLetras getDer() {
        return derecha;
    }

    public void setDer(NodoLetras der) {
        this.derecha = der;
    }

    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }

    public int getHoja() {
        return hoja;
    }

    public void setHoja(int hoja) {
        this.hoja = hoja;
    }

    public int getGrado() {
        return grado;
    }

    public void setGrado(int grado) {
        this.grado = grado;
    }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public int BuscarPos(int x, int y, int bandera, NodoLetras arbol) {
        if (arbol != null) {
            if ((arbol.getPosx() == x)) {
                return bandera = 1;
            }
            BuscarPos(x, y, bandera, arbol.getIzq());
            BuscarPos(x, y, bandera, arbol.getDer());
        }
        return 0;
    }

    public NodoLetras insertarNodo(NodoLetras n, String elemento, ArbolLetras A) {
        if (n == null) {
            return new NodoLetras(elemento);
        } else if (elemento.compareTo(n.getElemento()) <0 ) {
            n.setIzq(insertarNodo(n.getIzq(), elemento, A));

        } else if (elemento.compareTo(n.getElemento())>0) {
            n.setDer(insertarNodo(n.getDer(), elemento, A));
        }
        return n;
    }

}
