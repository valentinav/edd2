package All;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;


public class Arbolavl { 

    private Nodoavl raiz;
    int band1 = 0, band2 = 0;
    int altura, nivel;

    public Nodoavl getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodoavl raiz) {
        this.raiz = raiz;
    }

    public Arbolavl() {
        this.raiz = null;
    }

    public void insertar(int elemento) {
        raiz = insertar(raiz, elemento);
    }

    private void buscar(Nodoavl arbol, int dato) {

        if (arbol == null) {
            band1 = -1;
        } else if (dato == arbol.getElemento()) {
            band1 = 1;
        } else if (dato < arbol.getElemento()) {
            band2 = 1;
            buscar(arbol.getIzq(), dato);
        } else if (dato > arbol.getElemento()) {
            band2 = 2;
            buscar(arbol.getDer(), dato);
        }

    }

    public boolean buscar(int dato) throws IOException {

        buscar(raiz, dato);

        if (band1 == 1) {
            if (band2 == 1) {
                return true;

            } else if (band2 == 2) {
                return true;
            }
        }
        return false;
    }

    private Object elementosNodo(Nodoavl arbol) {
        if (arbol == null) {
            return null;
        } else {
            return arbol.getElemento();
        }
    }

    public static int factorEquilibrio(Nodoavl arbol) {
        if (arbol == null) {
            return -1;
        } else {
            return arbol.factorEquilibrio;
        }
    }

    private static int maxFB(int alturaIzquierdo, int alturaDerecho) {
        if (alturaIzquierdo > alturaDerecho) {
            return alturaIzquierdo;
        } else {
            return alturaDerecho;
        }
    }

    private Nodoavl insertar(Nodoavl arbol, int elemento) {
        if (arbol == null) {
            arbol = new Nodoavl(elemento, null, null);
        } else if (elemento < arbol.getElemento()) {
            arbol.setIzq(insertar(arbol.getIzq(), elemento));
            if (factorEquilibrio(arbol.getIzq()) - factorEquilibrio(arbol.getDer()) == 2) {
                if (elemento < arbol.getIzq().getElemento()) {
                    arbol = RotacionSimpleIzq(arbol);
                } else {
                    arbol = RotacionDobleIzq_Der(arbol);
                }
            }
        } else if (elemento > arbol.getElemento()) {
            arbol.setDer(insertar(arbol.getDer(), elemento));
            if (factorEquilibrio(arbol.getDer())
                    - factorEquilibrio(arbol.getIzq()) == 2) {
                if (elemento > arbol.getDer().getElemento()) {
                    arbol = RotacionSimpleDer(arbol);
                } else {
                    arbol = RotacionDobleDer_Izq(arbol);
                }
            }
        } else
     ;
        arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.getIzq()), factorEquilibrio(arbol.getDer())) + 1;
        return arbol;
    }

    private static Nodoavl RotacionSimpleIzq(Nodoavl arbol) {
        Nodoavl subArbol = arbol.getIzq();
        arbol.setIzq(subArbol.getDer());
        subArbol.setDer(arbol);
        arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.getIzq()), factorEquilibrio(arbol.getDer())) + 1;
        subArbol.factorEquilibrio = maxFB(
                factorEquilibrio(subArbol.getIzq()), arbol.factorEquilibrio) + 1;
        return subArbol;
    }

    private static Nodoavl RotacionSimpleDer(Nodoavl subArbol) {
        Nodoavl arbol = subArbol.getDer();
        subArbol.setDer(arbol.getIzq());
        arbol.setIzq(subArbol);
        subArbol.factorEquilibrio = maxFB(factorEquilibrio(subArbol.getIzq()), factorEquilibrio(subArbol.getDer())) + 1;
        arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.getDer()), subArbol.factorEquilibrio) + 1;
        return arbol;
    }

    private static Nodoavl RotacionDobleIzq_Der(Nodoavl arbol) {
        arbol.setIzq(RotacionSimpleDer(arbol.getIzq()));
        return RotacionSimpleIzq(arbol);
    }

    private static Nodoavl RotacionDobleDer_Izq(Nodoavl subArbol) {
        subArbol.setDer(RotacionSimpleIzq(subArbol.getDer()));
        return RotacionSimpleDer(subArbol);
    }

    public void imprimirArbolNiveles(Nodoavl arbol, int nivel) {
        if (arbol != null) {
            imprimirArbolNiveles(arbol.getIzq(), nivel + 1);
            System.out.println("" + nivel + "\t" + arbol.getElemento());
            imprimirArbolNiveles(arbol.getDer(), nivel + 1);
        }
    }

    public boolean eliminarNodo(Nodoavl arbol, int elementoEliminar) {
        Nodoavl padre = raiz;
        try {
            boolean hijoIzquierdo = true;
            while (arbol.getElemento() != elementoEliminar) {
                padre = arbol;
                if (elementoEliminar < arbol.getElemento()) {

                    hijoIzquierdo = true;
                    arbol = arbol.getIzq();
                } else {
                    hijoIzquierdo = false;
                    arbol = arbol.getDer();
                }
                if (arbol == null) {
                    return false;
                }
            }
            if (arbol.getIzq() == null && arbol.getDer() == null) {
                if (arbol == raiz) {
                    raiz = null;
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(null);
                } else {
                    padre.setDer(null);
                }
            } else if (arbol.getDer() == null) {
                if (arbol == raiz) {
                    raiz = arbol.getIzq();
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(arbol.getIzq());
                } else {
                    padre.setDer(arbol.getIzq());
                }
            } else if (arbol.getIzq() == null) {
                if (arbol == raiz) {
                    raiz = arbol.getDer();
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(arbol.getDer());
                } else {
                    padre.setDer(arbol.getDer());
                }
            } else {
                Nodoavl descendiente = obtenerDescendiente(arbol);
                if (arbol == raiz) {
                    raiz = descendiente;
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(descendiente);
                } else {
                    padre.setDer(descendiente);
                }
                descendiente.setIzq(arbol.getIzq());
            }
            return true;
        } catch (Exception E) {
            
        }
    return true;
    }

    private Nodoavl obtenerDescendiente(Nodoavl subArbol) {
        Nodoavl descendientePadre = subArbol;
        Nodoavl descendiente = subArbol;
        Nodoavl arbol = subArbol.getDer();

        while (arbol != null) {
            descendientePadre = descendiente;
            descendiente = arbol;
            arbol = arbol.getIzq();
        }
        if (descendiente != subArbol.getDer()) {
            descendientePadre.setIzq(descendiente.getDer());
            descendiente.setDer(subArbol.getDer());

        }
        return descendiente;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int n) {
        this.altura = n;
    }

    public void altura(Nodoavl arbol, int nivel) {
        if (arbol != null) {
            if (altura < nivel) {
                setAltura(nivel);

            }
            altura(arbol.getIzq(), nivel + 1);
            altura(arbol.getDer(), nivel + 1);
        }
    }
    

    public int altura() {
        altura = 0;
        altura(this.getRaiz(), 1);
        return getAltura();
    }
    
    String[] niveles;
    
    public ArrayList Nivelav() {
        niveles = new String[altura + 1];
        ArrayList l = new ArrayList();
        Nivelav(raiz, 0);
        for (int i = 0; i < niveles.length; i++) {
            l.add(niveles[i] + " ");
            //System.out.println(niveles[i] + " ");
        }
        return l;
    }

    private void Nivelav(Nodoavl pivote, int nivel2) {
        try {
            if (pivote != null) {
                niveles[nivel2] = pivote.getElemento() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
                Nivelav(pivote.getDer(), nivel2 + 1);
                Nivelav(pivote.getIzq(), nivel2 + 1);
            }
        } catch (Exception E) {

        }
    }
    
    
    public ArrayList PorNivelesAv() {
        ArrayList l = new ArrayList();
        PorNivelesAv(raiz, 1, l);
        return l;
    }

    private void PorNivelesAv(Nodoavl reco, int nivel, ArrayList l) {
        if (reco != null) {
            PorNivelesAv(reco.getIzq(), nivel + 1, l);
            l.add(reco.getElemento() + " Nivel: (" + nivel + ") ");
            PorNivelesAv(reco.getDer(), nivel + 1, l);
        }
    }
}
