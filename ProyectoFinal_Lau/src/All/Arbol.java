package All;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import javax.swing.JPanel;

public class Arbol {

    private Nodo raiz;
    int band1 = 0, band2 = 0;
    int altura, hoja;

    public Arbol() {
        this.raiz = null;
    }

    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    public void insertar(int elemento) {
        if (this.raiz == null) {
            this.raiz = new Nodo(elemento);
        } else {
            raiz = raiz.insertarNodo(raiz, elemento, this);
        }
    }

    private void buscar(Nodo arbol, int dato) {

        if (arbol == null) {
            band1 = -1;
        } else if (dato == arbol.getElemento()) {
            band1 = 1;
        } else if (dato < arbol.getElemento()) {
            band2 = 1;
            buscar(arbol.getIzq(), dato);
        } else if (dato > arbol.getElemento()) {
            band2 = 2;
            buscar(arbol.getDer(), dato);
        }

    }

    public boolean buscar(int dato) throws IOException {

        buscar(raiz, dato);

        if (band1 == 1) {
            if (band2 == 1) {
                return true;

            } else if (band2 == 2) {
                return true;
            }
        }
        return false;
    }

    public boolean eliminarNodo(Nodo arbol, int elementoEliminar) {
        Nodo padre = raiz;
        try {

            boolean hijoIzquierdo = true;
            while (arbol.getElemento() != elementoEliminar) {
                padre = arbol;
                if (elementoEliminar < arbol.getElemento()) {

                    hijoIzquierdo = true;
                    arbol = arbol.getIzq();
                } else {
                    hijoIzquierdo = false;
                    arbol = arbol.getDer();
                }
                if (arbol == null) {
                    return false;
                }
            }
            if (arbol.getIzq() == null && arbol.getDer() == null) {
                if (arbol == raiz) {
                    raiz = null;
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(null);
                } else {
                    padre.setDer(null);
                }
            } else if (arbol.getDer() == null) {
                if (arbol == raiz) {
                    raiz = arbol.getIzq();
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(arbol.getIzq());
                } else {
                    padre.setDer(arbol.getIzq());
                }
            } else if (arbol.getIzq() == null) {
                if (arbol == raiz) {
                    raiz = arbol.getDer();
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(arbol.getDer());
                } else {
                    padre.setDer(arbol.getDer());
                }
            } else {
                Nodo descendiente = obtenerDescendiente(arbol);
                if (arbol == raiz) {
                    raiz = descendiente;
                } else if (hijoIzquierdo == true) {
                    padre.setIzq(descendiente);
                } else {
                    padre.setDer(descendiente);
                }
                descendiente.setIzq(arbol.getIzq());
            }
            return true;
        } catch (Exception E) {

        }
        return true;
    }

    private Nodo obtenerDescendiente(Nodo subArbol) {
        Nodo descendientePadre = subArbol;
        Nodo descendiente = subArbol;
        Nodo arbol = subArbol.getDer();

        while (arbol != null) {
            descendientePadre = descendiente;
            descendiente = arbol;
            arbol = arbol.getIzq();
        }
        if (descendiente != subArbol.getDer()) {
            descendientePadre.setIzq(descendiente.getDer());
            descendiente.setIzq(subArbol.getDer());
        }
        return descendiente;
    }

    public ArrayList PorNiveles() {
        ArrayList l = new ArrayList();
        PorNiveles(raiz, 1, l);
        return l;
    }

    private void PorNiveles(Nodo reco, int nivel, ArrayList l) {
        if (reco != null) {
            PorNiveles(reco.getIzq(), nivel + 1, l);
            l.add(reco.getElemento() + " Nivel: (" + nivel + ") ");
            PorNiveles(reco.getDer(), nivel + 1, l);
        }
    }
    
    String[] niveles;

    public int getAltura() {
        return altura;
    }

    public void setAltura(int n) {
        this.altura = n;
    }

    public void altura(Nodo arbol, int nivel) {
        if (arbol != null) {
            if (altura < nivel) {
                setAltura(nivel);

            }
            altura(arbol.getIzq(), nivel + 1);
            altura(arbol.getDer(), nivel + 1);
        }
    }

    public int altura() {
        altura = 0;
        altura(this.getRaiz(), 1);
        return getAltura();
    }

    public ArrayList Nivel() {
        niveles = new String[altura + 1];
        ArrayList l = new ArrayList();
        Nivel(raiz, 0);
        for (int i = 0; i < niveles.length; i++) {
            l.add(niveles[i] + " ");
            //System.out.println(niveles[i] + " ");
        }
        return l;
    }

    private void Nivel(Nodo pivote, int nivel2) {
        try {
            if (pivote != null) {
                niveles[nivel2] = pivote.getElemento() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
                Nivel(pivote.getDer(), nivel2 + 1);
                Nivel(pivote.getIzq(), nivel2 + 1);
            }
        } catch (Exception E) {

        }
    }
    
    

    
    /*public static String ExternoIzq(Nodo arbol) throws IOException {
        String info = "";
        if (arbol != null) {
            info += ("" + arbol.getElemento());
            ExternoIzq(arbol.getIzq());
        }
        return info;
    }

    public String ExternoIzq() throws IOException {

        //System.out.println(""+raiz);
        return ExternoIzq(raiz.getIzq());

    }

    public String ExternoDer(Nodo arbol) throws IOException {

        String info = "";
        if (arbol != null) {
            info+=("" + arbol.getElemento());
            ExternoDer(arbol.getDer());
            
        }
        return info;

    }

    public String ExternoDer() throws IOException {

        //System.out.println(""+raiz);
        return ExternoDer(raiz.getDer());

    }

    public static String ExternoIzqDer(Nodo arbol) throws IOException {
        String info = "";
        if (arbol.getIzq() != null) {
            ExternoIzqDer(arbol.getDer());
            info +=("" + arbol.getDer().getElemento());
        }
        return info;
    }

    public String ExternoIzqDer() throws IOException {

        //System.out.println(""+raiz);
        return ExternoIzqDer(raiz.getIzq());

    }

    public String ExternoDerIzq(Nodo arbol) throws IOException {
        String info = "";
        if (arbol != null) {

            ExternoIzqDer(arbol.getDer());
            info+=("" + arbol.getIzq().getElemento());

        }
        return info;
    }

    public String ExternoDerIzq() throws IOException {

        //System.out.println(""+raiz);
        
        return ExternoDerIzq(raiz.getDer());

    }*/

}
