package All;

//Programa elaborado por Laura Buitrón 
public class Nodo {

    int elemento;
    Nodo izquierda;
    Nodo derecha;
    private int grado;
    private int hoja;
    private int posx;
    private int posy;

    public Nodo(int elemento, Nodo izq, Nodo der) {
        this.elemento = elemento;
        izquierda = null;
        derecha = null;

    }

    public Nodo(int Dato) {
        this.elemento = Dato;
        this.izquierda = null;
        this.derecha = null;
        this.grado = 0;
        this.posx = 0;
        this.posy = 0;
    }

    public Nodo getIzq() {
        return izquierda;
    }

    public void setIzq(Nodo izq) {
        this.izquierda = izq;
    }

    public Nodo getDer() {
        return derecha;
    }

    public void setDer(Nodo der) {
        this.derecha = der;
    }

    public int getElemento() {
        return elemento;
    }

    public void setElemento(int elemento) {
        this.elemento = elemento;
    }

    public int getHoja() {
        return hoja;
    }

    public void setHoja(int hoja) {
        this.hoja = hoja;
    }

    public int getGrado() {
        return grado;
    }

    public void setGrado(int grado) {
        this.grado = grado;
    }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public int BuscarPos(int x, int y, int bandera, Nodo arbol) {
        if (arbol != null) {
            if ((arbol.getPosx() == x)) {
                return bandera = 1;
            }
            BuscarPos(x, y, bandera, arbol.getIzq());
            BuscarPos(x, y, bandera, arbol.getDer());
        }
        return 0;
    }

    public Nodo insertarNodo(Nodo n, int elemento, Arbol A) {
        if (n == null) {
            return new Nodo(elemento);
        } else if (elemento < n.getElemento()) {
            n.setIzq(insertarNodo(n.getIzq(), elemento, A));

        } else if (elemento > n.getElemento()) {
            n.setDer(insertarNodo(n.getDer(), elemento, A));
        }
        return n;
    }

}
