/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package All;

/**
 *
 * @author USUARIO
 */
public class Punto {
    private int posx;
    private int posy;
    private String dato;

    public Punto() {
    }
    
    public Punto(String dato,int posx, int posy){
        this.dato=dato;
        this.posx=posx;
        this.posy=posy;
    }
    
    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }
    
    
    
}
