package logica_NegocioGD;

/**
 *
 * @author Lau
 * @param <T>
 */
public class NodoLista<T> {

    T valor = null;
    NodoLista siguiente = null;

    public NodoLista(T valor) {
        setValor(valor);
    }

    public T getValor() {
        return valor;
    }

    private void setValor(T valor) {
        this.valor = valor;
    }

    public NodoLista<T> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoLista<T> siguiente) {
        this.siguiente = siguiente;
    }

    public NodoLista agregarNodo(NodoLista nodol, T valor) {
        if (nodol == null) {
            return new NodoLista<>(valor);
        } else {
            nodol.setSiguiente(agregarNodo(nodol.getSiguiente(), valor));
        }
        return nodol;
    }
    
    public NodoLista agregarNodoAdy(NodoLista nodol, T valor, int peso) {
        if (nodol == null) {
            return new NodoLista<>(valor);
        } else {
            nodol.setSiguiente(agregarNodoAdy(nodol.getSiguiente(), valor, peso));
        }
        return nodol;
    }

    public NodoLista<T> eliminarNodo(NodoLista<T> nodol, int posicion, int i) {
        if (i == posicion) {
            return nodol.getSiguiente();
        } else {
            nodol.setSiguiente(eliminarNodo(nodol.getSiguiente(), posicion, i + 1));
        }
        return nodol;
    }

    /*public NodoLista<T> modificarNodo(NodoLista<T> nodol, NodoLista<T> nuevo, T dato) {
        if (nodol == null) {
            return nodol;
        } else if (nodol.getValor() == dato) {
            nuevo.setSiguiente(nodol.getSiguiente());
            return nuevo;
        } else {
            nodol.setSiguiente(modificarNodo(nodol.getSiguiente(), nuevo, dato));
        }
        return nodol;
    }*/
    
    public NodoLista<T> modificarNodo(NodoLista<T> n, NodoLista<T> nuevo, int pos, int i) {
        if(i == pos){
            nuevo.setSiguiente(n.getSiguiente());
            return nuevo;
        }
        else
            n.setSiguiente(modificarNodo(n.getSiguiente(), nuevo, pos, i + 1));
        return n;
    }
    public NodoLista<T> modificarNodo(NodoLista<T> n, NodoLista<T> nuevo, T ele) {
        if(n == null)
            return n;
        else if(n.getValor().equals(ele)){
            nuevo.setSiguiente(n.getSiguiente());
            return nuevo;
        }
        else
            n.setSiguiente(modificarNodo(n.getSiguiente(), nuevo, ele));
        return n;
    } 

    public NodoLista<T> obtenerNodo(NodoLista<T> nodo, int pos, int i) {
        if (i == pos) {
            return nodo;
        }
        return obtenerNodo(nodo.getSiguiente(), pos, i + 1);
    }

    public NodoLista<T> obtenerNodo(NodoLista<T> nodol, T dato) {
        if (nodol == null || nodol.getValor().equals(dato)) {
            return nodol;
        }
        return obtenerNodo(nodol.getSiguiente(), dato);
    }

    public int indiceNodo(NodoLista<T> n, T ele, int i) {
        if(n == null)
            return -1;
        if(n.getValor().equals(ele))
            return i;
        return indiceNodo(n.getSiguiente(), ele, i + 1);
    }
}
