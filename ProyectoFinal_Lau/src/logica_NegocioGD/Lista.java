package logica_NegocioGD;

/**
 *
 * @author Lau
 * @param <T>
 */
public class Lista<T> {

    NodoLista<T> cabeza = null;
    int tamano = 0;

    public NodoLista<T> getFrente() {
        return cabeza;
    }

    public void setFrente(NodoLista<T> cabeza) {
        this.cabeza = cabeza;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }
    
    public void agregar(T dato) {
        if (vacio()) {
            setFrente(new NodoLista<>(dato));
        } else {
            setFrente(getFrente().agregarNodo(getFrente(), dato));
        }
        setTamano(getTamano() + 1);
    }
    
    public void agregarNodo(T dato, int peso) {
        if (vacio()) {
            setFrente(new NodoLista<>(dato));
        } else {
            setFrente(getFrente().agregarNodo(getFrente(), dato));
        }
        setTamano(getTamano() + 1);
    }

    public void eliminar(int pos) {
        if (!vacio() && pos < getTamano()) {
            setFrente(getFrente().eliminarNodo(getFrente(), pos, 0));
            setTamano(getTamano() - 1);
        }
    }
    
    

    public void modificar(T ele, int pos) {
        if (!vacio()) {
            setFrente(getFrente().modificarNodo(getFrente(), new NodoLista<>(ele), pos , 0));
        }
    }

    public NodoLista obtener(int pos, int posinicial) {
        if (!vacio() && pos < getTamano()) {
            return getFrente().obtenerNodo(getFrente(), pos, posinicial);
        }
        return null;
    }

    public NodoLista obtener(T ele) {
        if (!vacio()) {
            return getFrente().obtenerNodo(getFrente(), ele);
        }
        return null;
    }

    public boolean vacio() {
        return getTamano() == 0;
    }
    
    public int indice2(T ele) {
        if (!vacio())
            return getFrente().indiceNodo(getFrente(), ele, 0);
        return -1;
    }
 
}
