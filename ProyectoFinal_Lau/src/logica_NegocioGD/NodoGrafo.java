package logica_NegocioGD;


/**
 *
 * @author Lau
 * @param <T>
 */
 public class NodoGrafo<T> {

    T vertice = null;
    Lista<T> lNodos = null;
    private int Peso = 0;
    private int indice = 1;

    public NodoGrafo(T vertice) {
        setVertice(vertice);
        setLNodos(new Lista<>());
    }

    public void agregarNodo(T valor) {
        getLNodos().agregar(valor);
    }
    
    public void agregarNodo(T valor, int peso) {
        getLNodos().agregarNodo(valor,peso);
    }

    public Lista<T> getLNodos() {
        return lNodos;
    }

    public void setLNodos(Lista<T> lNodos) {
        this.lNodos = lNodos;
    }

    public T getVertice() {
        return vertice;
    }

    private void setVertice(T vertice) {
        this.vertice = vertice;
    }

    public int getPeso() {
        return Peso;
    }

    public void setPeso(int Peso) {
        this.Peso = Peso;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

}
