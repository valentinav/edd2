package logica_NegocioGD;

import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JOptionPane;
import static logica_NegocioGD.Operaciones.getGrafo;
import logica_NegocioGD.NodoGrafo;

/**
 *
 * @author Lau
 * @param <T>
 */
public class Grafo<T> {

    Lista List = new Lista();
    public Lista<NodoGrafo> lVertices = null;

    public Lista<NodoGrafo> getListaVertices() {
        return lVertices;
    }

    private void setListaVertices(Lista<NodoGrafo> lVertices) {
        this.lVertices = lVertices;
    }

    public Grafo() {
        setListaVertices(new Lista<>());
    }

    public void agregarVertices(T vertice) {
        if (Buscarv(vertice) == null) //vertice=new NodoGrafo(vertice);
        //getListaVertices().agregar(vertice);
        {
            getListaVertices().agregar(new NodoGrafo(vertice));
        }

    }

    public void agregarVertices2(T vertice) {
        if (Buscarv(vertice) == null) //vertice=new NodoGrafo(vertice);
        //getListaVertices().agregar(vertice);
        {
            getListaVertices().agregar(new NodoGrafo(vertice));
        }
    }

    public NodoGrafo<T> Buscarv(T vertice) {
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            NodoGrafo nodo = (NodoGrafo) getListaVertices().obtener(i, 0).getValor();
            if (nodo != null) {
                if (nodo.getVertice().equals(vertice)) {
                    return nodo;
                }
            }
        }
        return null;
    }

    public void Agregaradyacentes(T vert1, T vert2, int peso) {
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            NodoGrafo v1 = (NodoGrafo) getListaVertices().obtener(i, 0).getValor();
            NodoGrafo v2 = (NodoGrafo) Buscarv(vert2);
            if (v1.getVertice().equals(vert1)) {
                int indice = 1;
                for (int j = 0; j < v1.getLNodos().getTamano(); j++) {
                    NodoGrafo adyacencia = (NodoGrafo) v1.getLNodos().obtener(j, 0).getValor();
                    if (adyacencia.getVertice().equals(vert2)) {
                        indice++;
                    }
                }
                NodoGrafo aux = new NodoGrafo(v2.getVertice());
                aux.setLNodos(v2.getLNodos());
                aux.setPeso(peso);
                aux.setIndice(indice);
                v1.getLNodos().agregar(aux);
                break;
            }
        }

    }

    public void imprimirAdyacentes(T vertice) {
        NodoGrafo nodo = Buscarv(vertice);
        if (nodo != null) {
            boolean bandera = false;
            System.out.println(" y sus vertices adyacentes son...  ");
            for (int i = 0; i < nodo.getLNodos().getTamano(); i++) {
                NodoGrafo aux = (NodoGrafo) nodo.getLNodos().obtener(i, 0).getValor();
                System.out.println(vertice + " -- " + aux.getPeso() + " -> " + aux.getVertice() + "");
                bandera = true;
            }
            if (bandera == false) {
                System.out.println("\nCero, el vertice no tiene vertices adyacentes");
            }
        } else {
            System.out.println("\n El vertice no existe...");
        }
    }

    public boolean buscarVertice(T vertice) {
        return Buscarv(vertice) != null;
    }

    static Scanner v = new Scanner(System.in);

    public String ModificarPonderacion(T vertice1, T vertice2) {
        String info = "";
            for (int i = 0; i < getListaVertices().getTamano(); i++) {
                NodoGrafo v1 = (NodoGrafo) getListaVertices().obtener(i, 0).getValor();
                NodoGrafo v2 = (NodoGrafo) Buscarv(vertice2);
                NodoGrafo v3 = (NodoGrafo) Buscarv(vertice1);
                if (v2 != null && v3 != null) {
                    if (v1.getVertice().equals(vertice1)) {
                        int indice = 0;
                        for (int j = 0; j < v1.getLNodos().getTamano(); j++) {
                            NodoGrafo adyacencia = (NodoGrafo) v1.getLNodos().obtener(j, 0).getValor();
                            if (adyacencia.getVertice().equals(vertice2)) {
                                indice++;
                            }
                        }
                        if (indice != 0) {
                            int camino, peso;
                            info+=("Existen " + indice + " caminos entre " + vertice1 + " y " + vertice2 + " .... Cual quieres modificar? ");
                            camino = Integer.parseInt(JOptionPane.showInputDialog("Digite vertice 1 para modificar: "));
                            peso= Integer.parseInt(JOptionPane.showInputDialog("Digite el nuevo peso "));
                            for (int h = 0; h < v1.getLNodos().getTamano(); h++) {
                                NodoGrafo ind = (NodoGrafo) v1.getLNodos().obtener(h, 0).getValor();
                                if (ind.getVertice().equals(vertice2)) {
                                    if (ind.getIndice() == camino) {
                                        ind.setPeso(peso);
                                        info+=("La ponderación se modificó");
                                    } else if (camino > indice) {
                                        info+=("La ponderación no se pudo modificar, debes escoger un camino existente");
                                        break;
                                    }
                                }
                            }
                            break;

                        } else {
                            info+=("Los vertices no son adyacentes... !");
                            break;
                        }
                    }
                    return info;
                    
                } else {
                    info+=("Uno de los 2 vertices no existe...!");
                    break;
                }
            }
            return info;
        
    }

    public int obtenerIndice(T vertice) {
        int i = 0;
        for (int j = 0; j < getListaVertices().getTamano(); i++, j++) {
            NodoGrafo nodo = (NodoGrafo) getListaVertices().obtener(j, 0).getValor();
            if (nodo.getVertice().equals(vertice))
                return i;
        }
        return -1;
    }
    
    public int[][] matrizAdyacente() {
        int[][] matriz = new int[getListaVertices().getTamano()][getListaVertices().getTamano()];
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            NodoGrafo<T> nodo1 = (NodoGrafo<T>) getListaVertices().obtener(i, 0).getValor();
            for (int j = 0; j < getListaVertices().getTamano(); j++) {
                NodoLista<T> nodo2 = nodo1.getLNodos().obtener(j, 0);
                if (nodo2 != null) {
                    NodoGrafo nodo3 = (NodoGrafo) nodo2.getValor();
                    int indice = obtenerIndice((T) nodo3.getVertice());       
                    if (indice != -1)
                        matriz[i][indice] = 1;
                }
            }
        }
        return matriz;
    }
    
    

   /* public int[][] MatrizCuadrada() {
        int[][] Matriz = MatrizAdyacente();
        int[][] matrizcuadrada = new int[getListaVertices().getTamano()][getListaVertices().getTamano()];
        for (int i = 0; i < Matriz.length; i++) {
            for (int j = 0; j < Matriz.length; j++) {
                for (int h = 0; h < Matriz.length; h++) {
                    matrizcuadrada[i][j] += Matriz[i][h] * Matriz[h][j];
                }
            }
        }
        return matrizcuadrada;

    }
*/
    public int BuscarIndice(NodoGrafo nodo, T vertice) {
        int cont = 0;
        for (int i = 0; i < nodo.getLNodos().getTamano(); i++, cont++) {
            NodoGrafo nodox = (NodoGrafo) nodo.getLNodos().obtener(i, 0).getValor();
            if (nodox != null) {
                if (nodox.getVertice().equals(vertice)) {
                    return cont;
                }
            }
        }
        return -1;
    }

    public static String GradoTodosVert() {
        String info = "";
        int cont = 0;
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
            NodoGrafo nodo1 = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += ("\nEl grado de salida de " + nodo1.getVertice() + " es: ");
            for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                NodoGrafo nodo2 = (NodoGrafo) nodo1.getLNodos().obtener(j, 0).getValor();
                cont++;
            }
            info += cont;
            cont = 0;

        }
        return info;
    }


    public static String ImprimirVertices() {
        String info = "";
        info += ("V = {");
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
            NodoGrafo nodo1 = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += (nodo1.getVertice() + ",");
        }
        info += ("}");
        return info;
    }

    public static String OrdenGrafo() {
        String info = "";
        int cont = 0;
        info += ("\nE:{");
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
            NodoGrafo nodo1 = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            cont++;
            for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                info += ("(" + nodo1.getVertice() + ",");
                NodoGrafo nodo2 = (NodoGrafo) nodo1.getLNodos().obtener(j, 0).getValor();
                info += (nodo2.getVertice() + "),");
            }
            info += ("");
        }
        info += ("}");
        info += ("\nOrden: " + cont);
        return info;

    }

    public String GradoEntrada(T vertice1) {
        String info = "";
        int cont = 0;
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
            NodoGrafo v1 = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            NodoGrafo v3 = (NodoGrafo) Buscarv(vertice1);
            if (v1 != null && v3 != null) {
                for (int m = 0; m < getGrafo().getListaVertices().getTamano(); m++) {
                    NodoGrafo aux = (NodoGrafo) getGrafo().getListaVertices().obtener(m, 0).getValor();
                    if (vertice1.equals(v1.getVertice())) {
                        for (int j = 0; j < aux.getLNodos().getTamano(); j++) {
                            NodoGrafo adyacencia = (NodoGrafo) aux.getLNodos().obtener(j, 0).getValor();
                            
                            if (adyacencia.getVertice().equals(vertice1)) {
                                info +=("Hola");
                                cont = cont + 1;
                            }
                        }
                    }
                }

            }
        }
        return info+=("El grado de entrada de " + vertice1 + " es: " + cont);
    }

    public void EliminarVertice(String vertice) {
        if (getGrafo() != null) {

            System.out.println("" + lVertices.getFrente().getValor().vertice);
            if (vertice.equals(lVertices.getFrente().getValor().vertice)) {
                lVertices.setFrente(lVertices.cabeza.getSiguiente());
                System.out.println("El vertice ha sido eliminado");
            } else {
                NodoLista<NodoGrafo> aux = lVertices.getFrente();
                while (aux != null) {
                    if (aux.getSiguiente().getValor().vertice == vertice) {
                        break;
                    }
                    aux = aux.getSiguiente();
                }
                NodoLista<NodoGrafo> prox = aux.getSiguiente().getSiguiente();
                aux.setSiguiente(prox);
                System.out.println("El vertice ha sido eliminado");
            }

        } else {
            System.out.println("No existe un grafo... crea uno ");
        }
    }

}
