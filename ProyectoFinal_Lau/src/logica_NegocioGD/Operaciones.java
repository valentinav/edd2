package logica_NegocioGD;

import java.util.InputMismatchException;

/**
 *
 * @author Lau
 */
public class Operaciones {

    static Grafo<String> grafo = null;

    public static Grafo<String> getGrafo() {
        return grafo;
    }

    public static void setGrafo(Grafo<String> grafo) {
        Operaciones.grafo = grafo;
    }

    //1.
    public static void crearGrafo() {
        if (getGrafo() == null) {
            setGrafo(new Grafo<>());
        }
    }

    //2.
    public static void agregarVertices(String vertice) {
        if (getGrafo() != null) {
            getGrafo().agregarVertices(vertice);
        } else {
            System.out.println("");
        }
    }

    //3.
    public static boolean buscarVertice(String vertice) {
        if (getGrafo() != null) {
            if (getGrafo().buscarVertice(vertice)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //4.
    public static void Agregaradyacentes(String v1, String v2, int peso) {
        getGrafo().Agregaradyacentes(v1, v2, peso);

    }

    public static void agregarVertices2(String vertice) {
        if (getGrafo() != null) {
            getGrafo().agregarVertices2(vertice);
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }
    }

    public static String ModificarPonderacion(String v1, String v2) {
        String info="";
        return info+= getGrafo().ModificarPonderacion(v1, v2);
      
    }

    //5.
    public static void imprimirAdyacentes(String vertice) {
        if (getGrafo() != null) {
            if (getGrafo().buscarVertice(vertice)) {
                System.out.print("\n" + vertice + ", SI existe en el grafo");
                getGrafo().imprimirAdyacentes(vertice);
            } else {
                System.out.println("\nEl elemento '" + vertice + "' NO existe en el grafo..");
            }
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }
    }

    public static String imprimirGrafo() {
        String info = "";
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
            NodoGrafo nodo1 = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += (nodo1.getVertice() + " -> ");
            for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                NodoGrafo nodo2 = (NodoGrafo) nodo1.getLNodos().obtener(j, 0).getValor();
                info += (nodo2.getVertice() + " : " + nodo2.getPeso() + " | ");
            }
            info += ("\n ");
        }
        return info;
    }

    public static String ExpresarGrafo() {
        String info = "";
        return info += "" + Grafo.ImprimirVertices() + Grafo.OrdenGrafo();
    }

    public static String ImprimirVertices() {

        String info = "";
        return info += "" + Grafo.ImprimirVertices();
    }

    public static String GradoTodosVertice() {
        String info = "";
        return info += Grafo.GradoTodosVert();

    }

    public static void EliminarVer(String vertice) {

        if (getGrafo() != null) {
            getGrafo().EliminarVertice(vertice);
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }

    }
    
    public static String matrizAdyacente() {
        String info = "\t";
        int[][] matriz  = getGrafo().matrizAdyacente();
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++){
            NodoGrafo nodo = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += nodo.getVertice() + "\t";
        }
        info += "\n\n\r";
        for (int i = 0; i < matriz.length; i++) {
            NodoGrafo nodo = (NodoGrafo) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += nodo.getVertice() + "\t";
            for (int j = 0; j < matriz[i].length; j++)
                info += matriz[i][j] + "\t";
            info += "\n\r";
        }
        return info;
    }
}
