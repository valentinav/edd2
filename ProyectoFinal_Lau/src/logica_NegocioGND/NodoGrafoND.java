package logica_NegocioGND;

/**
 *
 * @author Lau
 * @param <T>
 */
public class NodoGrafoND<T> {

    T vertice = null;
    private int peso = 0;
    ListaND<T> lNodos = null;


    public NodoGrafoND(T vertice) {
        setVertice(vertice);
        setLNodos(new ListaND<>());
 
    }

    public void agregarNodo(T valor) {
        getLNodos().agregar(valor);
    }

    public ListaND<T> getLNodos() {
        return lNodos;
    }

    private void setLNodos(ListaND<T> lNodos) {
        this.lNodos = lNodos;
    }

    public T getVertice() {
        return vertice;
    }

    private void setVertice(T vertice) {
        this.vertice = vertice;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    
}
