package logica_NegocioGND;

import java.util.InputMismatchException;
import logica_NegocioGND.NodoGrafoND;
import logica_NegocioGND.NodoListaND;
import static logica_NegocioGND.OperacionesND.getGrafo;

/**
 *
 * @author Lau
 * @param <T>
 */
public class GrafoND<T> {

    ListaND ListaOrdenada = new ListaND();

    public ListaND<NodoGrafoND> lVertices = null;

    public ListaND<NodoGrafoND> getListaVertices() {
        return lVertices;
    }

    private void setListaVertices(ListaND<NodoGrafoND> lVertices) {
        this.lVertices = lVertices;
    }

    public GrafoND() {
        setListaVertices(new ListaND<>());
    }

    public void agregarVertices(T vertice) {
        if (Buscarv(vertice) == null) //vertice=new NodoGrafo(vertice);
        //getListaVertices().agregar(vertice);
        {
            getListaVertices().agregar(new NodoGrafoND(vertice));
        } else {
        }
    }

    public void agregarVertices2(T vertice) {
        if (Buscarv(vertice) == null) //vertice=new NodoGrafo(vertice);
        //getListaVertices().agregar(vertice);
        {
            getListaVertices().agregar(new NodoGrafoND(vertice));
        }
    }

    public NodoGrafoND<T> Buscarv(T vertice) {
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            NodoGrafoND nodo = (NodoGrafoND) getListaVertices().obtener(i, 0).getValor();
            if (nodo != null) {
                if (nodo.getVertice().equals(vertice)) {
                    return nodo;
                }
            }
        }
        return null;
    }

    public boolean Agregaradyacentes(T vert1, T vert2, int peso) {
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            NodoGrafoND v1 = (NodoGrafoND) getListaVertices().obtener(i, 0).getValor();
            NodoGrafoND v2 = (NodoGrafoND) Buscarv(vert2);
            if (v1.getVertice().equals(vert1) && v1.getLNodos().obtener(v2) == null) {
                v2.setPeso(peso);
                v1.getLNodos().agregar(v2);
                Agregaradyacentes(vert2, vert1, peso);
                return true;
            }else if (v1.getVertice() != (vert1) && v1.getLNodos().obtener(vert2) != null){
                return false;
            }
        }
        return false;
    }

    public  String imprimirAdyacentes(T vertice) {
        String info ="";
        NodoGrafoND nodo = Buscarv(vertice);
        if (nodo != null) {
            boolean bandera = false;
            info+=("\nLos vertices adyacentes de " + vertice + " son...  ");
            for (int i = 0; i < nodo.getLNodos().getTamano(); i++) {
                NodoGrafoND aux = (NodoGrafoND) nodo.getLNodos().obtener(i, 0).getValor();
                info+=(vertice + " -- " + aux.getPeso() + " -> " + aux.getVertice());
                bandera = true;
            }
            if (bandera == false) {
                info+=("Cero, el vertice no tiene vertices adyacentes");
            }
        } else {
            info+=("\n El vertice no existe...");
        }
        return info;
    }

    public void imprimirOrdenamiento() {

        Ordenarv();
        for (int i = 0; i < ListaOrdenada.getTamano(); i++) {
            NodoGrafoND aux = (NodoGrafoND) ListaOrdenada.obtener(i, 0).getValor();
            System.out.println(aux.getVertice());
        }
        //}
    }

    public boolean buscarVertice(T vertice) {
        return Buscarv(vertice) != null;
    }

    public void Ordenarv() {
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            T a = (T) getListaVertices().obtener(i, 0).getValor();
            for (int j = 0; j < getListaVertices().getTamano(); i++) {
                T b = (T) getListaVertices().obtener(j, 0).getValor();
                if (a.hashCode() > b.hashCode()) {
                    //if((Integer.parseInt(getListaVertices().obtener(i, 0).getValor().toString()) > Integer.parseInt(getListaVertices().obtener(j, 0).getValor().toString()))){
                    String Posi = (String) getListaVertices().obtener(i, 0).getValor();
                    String Posj = (String) getListaVertices().obtener(j, 0).getValor();
                    String aux = Posi;
                    Posi = Posj;
                    Posj = aux;
                    ListaOrdenada.agregar(Posj);
                }
                ListaOrdenada.agregar(2);
            }
        }
        for (int i = 0; i < ListaOrdenada.getTamano(); i++) {
            NodoGrafoND aux = (NodoGrafoND) ListaOrdenada.obtener(i, 0).getValor();
            System.out.println("a" + aux.getVertice());
        }
    }

    public int[][] matrizCaminos() {
        int[][] matriz = matrizAdyacente(), matrizCuadrada = new int[getListaVertices().getTamano()][getListaVertices().getTamano()];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                for (int k = 0; k < matriz[j].length; k++) {
                    matrizCuadrada[i][j] += matriz[i][k] * matriz[k][j];
                }
            }
        }
        return matrizCuadrada;
    }

    public int[][] matrizAdyacente() {
        int[][] matriz = new int[getListaVertices().getTamano()][getListaVertices().getTamano()];
        for (int i = 0; i < getListaVertices().getTamano(); i++) {
            NodoGrafoND<T> nodo1 = (NodoGrafoND) getListaVertices().obtener(i, 0).getValor();
            for (int j = 0; j < getListaVertices().getTamano(); j++) {
                NodoListaND<T> nodo2 = nodo1.getLNodos().obtener(j, 0);
                if (nodo2 != null) {
                    NodoGrafoND nodo3 = (NodoGrafoND) nodo2.getValor();
                    int indice = getListaVertices().indice(nodo3);
                    if (indice != -1) {
                        matriz[i][indice] = 1;
                    }
                }
            }
        }
        return matriz;
    }

    public void GradoVertice(T vertice) {
        NodoGrafoND nodo = Buscarv(vertice);
        if (nodo != null) {
            boolean bandera = false;
            int cont = 0;
            System.out.print("El grado de " + vertice + " es: ");
            for (int i = 0; i < nodo.getLNodos().getTamano(); i++) {
                NodoGrafoND aux = (NodoGrafoND) nodo.getLNodos().obtener(i, 0).getValor();
                cont++;
                bandera = true;
            }

            if (bandera) {
                System.out.println("" + cont);
            } else {
                System.out.println(" " + cont);
            }
            cont = 0;
        } else {
            System.out.println("El vertice no existe");
        }

    }

    public static String GradoTodosVert() {
        String info = "";
        int cont = 0;
            for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
                NodoGrafoND nodo1 = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
                info+=("El grado de " + nodo1.getVertice() + " es: ");
                for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                    NodoGrafoND nodo2 = (NodoGrafoND) nodo1.getLNodos().obtener(j, 0).getValor();
                    cont++;
                }
                info+=("" + cont+"\n");
                cont = 0;
                
            }
            return info;
    }
    
    public static String ImprimirVertices() {
        String info = "";
        if (getGrafo() != null) {
            info+=("V = {");
            for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
                NodoGrafoND nodo1 = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
                info+=(""+nodo1.getVertice()+",");
            }
            info+=("}");
        } else {
            info+= "No existe un grafo... crea uno ";
        }
        return info;
    }
    
    public static String OrdenGrafo() {
        String info = "";
        int cont = 0;
        info += "\nE:{";
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
            NodoGrafoND nodo1 = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
            cont++;
            for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                info += "(" + nodo1.getVertice() + ",";
                NodoGrafoND nodo2 = (NodoGrafoND) nodo1.getLNodos().obtener(j, 0).getValor();
                info += nodo2.getVertice() + "),";
            }
            info+= "";
        }
        info += "}";
        info += "\nOrden: " + cont;
        return info;
    }

    public static int ContarGrupos() {
        int cont = 0;
        if (getGrafo() != null) {
            for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
                NodoGrafoND nodo1 = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
                for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                    NodoGrafoND nodo2 = (NodoGrafoND) nodo1.getLNodos().obtener(j, 0).getValor();
                    cont++;
                }
                System.out.print("");
            }
        }
        return cont;
    }

    public static int ContarVertices() {
        int cont = 0;
        if (getGrafo() != null) {
            for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
                NodoGrafoND nodo1 = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
                cont++;
            }
        } else {
            System.out.println("No existe un grafo... crea uno ");
        }
        return cont;
    }

    public static String Formula() {
        String info = "";
        int n = ContarVertices();
        int enlaces = n * (n - 1) / 2;
        int resultado = ContarGrupos() / 2;
        if (resultado == enlaces) {

            info+=("El grafo es completo");
        } else {
            info+=("El grafo no es completo");
        }
        return info;
    }

    public void EliminarVertice(String vertice) {
        if (getGrafo() != null) {

            System.out.println("" + lVertices.getFrente().getValor().vertice);
            if (vertice.equals(lVertices.getFrente().getValor().vertice)) {
                lVertices.setFrente(lVertices.cabeza.getSiguiente());
                System.out.println("El vertice ha sido eliminado");
            } else {
                NodoListaND<NodoGrafoND> aux = lVertices.getFrente();
                while (aux != null) {
                    if (aux.getSiguiente().getValor().vertice == vertice) {
                        break;
                    }
                    aux = aux.getSiguiente();
                }
                NodoListaND<NodoGrafoND> prox = aux.getSiguiente().getSiguiente();
                aux.setSiguiente(prox);
                System.out.println("El vertice ha sido eliminado");
            }

        } else {
            System.out.println("No existe un grafo... crea uno ");
        }
    }

}
