package logica_NegocioGND;

import java.util.InputMismatchException;

/**
 *
 * @author Lau
 */
public class OperacionesND {

    static GrafoND<String> grafo = null;

    public static GrafoND<String> getGrafo() {
        return grafo;
    }

    public static void setGrafo(GrafoND<String> grafo) {
        OperacionesND.grafo = grafo;
    }

    //1.
    public static void crearGrafo() {
        if (getGrafo() == null) {
            setGrafo(new GrafoND<>());
        }
    }

    //2.
    public static void agregarVertices(String vertice) {
        if (getGrafo() != null) {
            getGrafo().agregarVertices(vertice);
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }
    }

    //3.
    public static boolean buscarVertice(String vertice) {
        if (getGrafo() != null) {
            if (getGrafo().buscarVertice(vertice)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //4.
    public static boolean Agregaradyacentes(String v1, String v2, int peso) {
        return getGrafo().Agregaradyacentes(v1, v2, peso);

    }

    public static void agregarVertices2(String vertice) {
        if (getGrafo() != null) {
            getGrafo().agregarVertices2(vertice);
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }
    }

    //5.
    public static void imprimirAdyacentes(String vertice) {
        String info = "";
        info += "" + getGrafo().imprimirAdyacentes(vertice);

    }

    public static void imprimirOrdenamiento() {
        if (getGrafo() != null) {
            getGrafo().Ordenarv();
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }
    }

    public static String imprimirGrafo() {
        String info = "";
        
            for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++) {
                NodoGrafoND nodo1 = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
                info+=(nodo1.getVertice() + " -> ");
                for (int j = 0; j < nodo1.getLNodos().getTamano(); j++) {
                    NodoGrafoND nodo2 = (NodoGrafoND) nodo1.getLNodos().obtener(j, 0).getValor();
                    info+=(nodo2.getVertice() + " : " + nodo2.getPeso());
                }
                info+=("\n");
            }
            return info;
    }

    public static void matrizCaminos() {
        if (getGrafo() != null) {
            int[][] matriz = getGrafo().matrizCaminos();
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[0].length; j++) {
                    System.out.print(matriz[i][j] + "\t");
                }
                System.out.println("");
            }
        }
    }

    public static String matrizAdyacente() {
        String info = "\t";
        int[][] matriz  = getGrafo().matrizAdyacente();
        for (int i = 0; i < getGrafo().getListaVertices().getTamano(); i++){
            NodoGrafoND nodo = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += nodo.getVertice() + "\t";
        }
        info += "\n\n\r";
        for (int i = 0; i < matriz.length; i++) {
            NodoGrafoND nodo = (NodoGrafoND) getGrafo().getListaVertices().obtener(i, 0).getValor();
            info += nodo.getVertice() + "\t";
            for (int j = 0; j < matriz[i].length; j++)
                info += matriz[i][j] + "\t";
            info += "\n\r";
        }
        return info;
    }
    
    public static void ImprimirmatrizCuadrada() {
        if (getGrafo() != null) {
            int[][] matriz = getGrafo().matrizCaminos();
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[0].length; j++) {
                    System.out.print(matriz[i][j] + "\t");
                }
                System.out.println("");
            }
        }
    }

    public static String GradoVertices() {
        String info = "";
        return info += "" + GrafoND.GradoTodosVert();
    }

    public static String ExpresarGrafo() {
        String info = "";
        return info += "" + GrafoND.ImprimirVertices() + GrafoND.OrdenGrafo();
    }

    public static String ImprimirVertices() {
        String info = "";
        return info += "" + GrafoND.ImprimirVertices();

    }

    public static String VerificarGrafo() {
        String info = "";
        return info+= getGrafo().Formula();
    }

    public static void EliminarVer(String vertice) {

        if (getGrafo() != null) {
            getGrafo().EliminarVertice(vertice);
        } else {
            System.out.println("\nPara realizar esta operación debes crear un grafo");
        }

    }

}
