package logica_NegocioGND;

/**
 *
 * @author Lau
 * @param <T>
 */
public class ListaND<T> {

    NodoListaND<T> cabeza = null;
    int tamano = 0;

    public NodoListaND<T> getFrente() {
        return cabeza;
    }

    public void setFrente(NodoListaND<T> cabeza) {
        this.cabeza = cabeza;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }
    
    public void agregar(T dato) {
        if (vacio()) {
            setFrente(new NodoListaND<>(dato));
        } else {
            setFrente(getFrente().agregarNodo(getFrente(), dato));
        }
        setTamano(getTamano() + 1);
    }

    public void eliminar(int pos) {
        if (!vacio() && pos < getTamano()) {
            setFrente(getFrente().eliminarNodo(getFrente(), pos, 0));
            setTamano(getTamano() - 1);
        }
    }

    public void modificar(T ele, int pos) {
        if (!vacio()) {
            setFrente(getFrente().modificarNodo(getFrente(), new NodoListaND<>(ele), pos , 0));
        }
    }

    public NodoListaND obtener(int pos, int posinicial) {
        if (!vacio() && pos < getTamano()) {
            return getFrente().obtenerNodo(getFrente(), pos, posinicial);
        }
        return null;
    }

    public NodoListaND obtener(T ele) {
        if (!vacio()) {
            return getFrente().obtenerNodo(getFrente(), ele);
        }
        return null;
    }

    public boolean vacio() {
        return getTamano() == 0;
    }
    
    public int indice(T ele) {
        if (!vacio())
            return getFrente().indiceNodo(getFrente(), ele, 0);
        return -1;
    }
    
}
