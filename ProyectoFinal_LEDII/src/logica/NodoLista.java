package logica;

public class NodoLista<T> {
	// Atributos
	private T valor;
	private NodoLista<T> siguiente;
	//getters y setters
	public T getValor() {
		return valor;
	}
	public void setValor(T valor) {
		this.valor = valor;
	}
	public NodoLista<T> getSiguiente() {
		return siguiente;
	}
	public void setSiguiente(NodoLista<T> siguiente) {
		this.siguiente = siguiente;
	}	
	public NodoLista<T> obtenerNodoEnPosicion(NodoLista<T> n, int pos, int i) {
        if(i == pos)  return n;
        return obtenerNodoEnPosicion(n.getSiguiente(), pos, i + 1);
    }
}