package logica;

public class NodoGrafo<T> {
	// Atributos
	private Lista <T> listaNodos;
	private T elemento;
	/**
	 * valor de peso q toma la arista 
	 */
	private int peso;
	/**
	 * cantidad de aristas antes de ingresar la nueva
	 */
	private int cantidad;
	
	/**
	 * creacion de un vertice
	 * @param elemento
	 */
	public NodoGrafo(T elemento) {
		this.listaNodos = new Lista<T>();
		this.elemento = elemento;
		this.peso=-1;
	}
	/**
	 * creacion de un vertice adyacente
	 * @param elemento
	 * @param peso
	 * @param id
	 */
	public NodoGrafo(T elemento,int peso,int id) {
		this.listaNodos = new Lista<T>();
		this.elemento = elemento;
		this.peso=peso;
		this.setCantidad(id);
	}
	public NodoGrafo(T elemento,int id) {
		this.listaNodos = new Lista<T>();
		this.elemento = elemento;
		this.setCantidad(id);
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
	public Lista<T> getListaNodos() {
		return listaNodos;
	}
	public boolean hayListaNodos() {
		if (listaNodos.esVacia())
			return true;
		return false;
	}
	public void setListaNodos(T valor) {
		this.listaNodos.agregar(valor);
	}
	public T getElemento() {
		return elemento;
	}
	public void setElemento(T elemento) {
		this.elemento = elemento;
	}
}
