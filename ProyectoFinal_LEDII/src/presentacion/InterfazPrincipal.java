package presentacion;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.View;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
//import com.jgoodies.forms.layout.FormLayout;
//import com.jgoodies.forms.layout.ColumnSpec;
//import com.jgoodies.forms.layout.FormSpecs;
//import com.jgoodies.forms.layout.RowSpec;
//import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class InterfazPrincipal {

	private JFrame frmPresentacion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazPrincipal window = new InterfazPrincipal();
					window.frmPresentacion.setVisible(true);

					window.frmPresentacion.setLocationRelativeTo(null);;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPresentacion = new JFrame();
		frmPresentacion.setTitle("PRESENTACION");
		frmPresentacion.setBounds(100, 100, 450, 491);
		frmPresentacion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPresentacion.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 434, 452);
		frmPresentacion.getContentPane().add(panel);
		
		JLabel lblEstructuraDeDatos = new JLabel("Estructura de Datos II");
		lblEstructuraDeDatos.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblJanethCristinaCifuentes = new JLabel("Janeth Cristina Cifuentes Manrique");
		lblJanethCristinaCifuentes.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblTrabajoFinal = new JLabel("Trabajo Final:");
		lblTrabajoFinal.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblProgramaDeIngenieria = new JLabel("Programa de Ingenieria de Sistemas");
		lblProgramaDeIngenieria.setFont(new Font("Arial", Font.PLAIN, 14));
		
		
		JLabel lblUnivercidadDelCauca = new JLabel("Univercidad del Cauca");
		lblUnivercidadDelCauca.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JButton btnProyectoFinal = new JButton("Proyecto Final");
		btnProyectoFinal.setIcon(new ImageIcon(InterfazPrincipal.class.getResource("/javax/swing/plaf/basic/icons/JavaCup16.png")));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addGap(103)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblProgramaDeIngenieria, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(8)
							.addComponent(lblJanethCristinaCifuentes, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGap(102))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(133)
					.addComponent(lblUnivercidadDelCauca, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(159))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addGap(151)
					.addComponent(lblEstructuraDeDatos, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(147))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addGap(174)
					.addComponent(lblTrabajoFinal, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(172))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(259, Short.MAX_VALUE)
					.addComponent(btnProyectoFinal, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(48)
					.addComponent(lblUnivercidadDelCauca)
					.addGap(60)
					.addComponent(lblProgramaDeIngenieria, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
					.addGap(42)
					.addComponent(lblTrabajoFinal, GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblEstructuraDeDatos, GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE)
					.addGap(96)
					.addComponent(lblJanethCristinaCifuentes, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
					.addGap(31)
					.addComponent(btnProyectoFinal)
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		btnProyectoFinal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Principal abrir =new Principal();
				abrir.main(null);
			}
		});
	}
}
