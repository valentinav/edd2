package presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Panel;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import logica.Grafo;

import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interfaz extends JFrame {

	private JFrame frmProyectofinal;
	private JPanel contentPane;
	private JTextField textField;
	private JScrollPane scrollPane;
	
	private ButtonGroup bt1= new ButtonGroup();
	private ButtonGroup bt2= new ButtonGroup();
	private ButtonGroup bt3= new ButtonGroup();
	private ButtonGroup bt4= new ButtonGroup();
	
	//creacion de grafos
    private Grafo<Integer> GrafoDirigido =new Grafo();
	private Grafo<String> GrafoDirigidoCad =new Grafo();
	private Grafo<Integer> GrafoNoDirigido =new Grafo();
	private Grafo<String> GrafoNoDirigidoCad =new Grafo();
		
	// vareables controladoras de tipo de seleccio rbt
	private boolean SelectDirigido=false;
	private boolean SelectNoDirigido=false;
	private boolean SelectSi=false;
	private boolean SelectNo=false;
	private boolean SelectLetras=false;
	private boolean SelectNumeros=false;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz frame = new Interfaz();
					frame.setVisible(true);
					//frame.frmProyectofinal.setLocationRelativeTo(null);;
					//frame.frmProyectofinal.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interfaz() {
		
		
		
		//:____________________________________________
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 778, 449);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lbGrafo = new JLabel("Grafo");
		lbGrafo.setBounds(10, 11, 46, 14);
		panel.add(lbGrafo);
		
		JLabel lbPonderado = new JLabel("Ponderado");
		lbPonderado.setBounds(73, 11, 66, 14);
		panel.add(lbPonderado);
		
		JLabel lbFormato = new JLabel("Formato");
		lbFormato.setBounds(149, 11, 46, 14);
		panel.add(lbFormato);
		
		
		JRadioButton rdbtnDirigido = new JRadioButton("Dirigido");
		bt1.add(rdbtnDirigido);
		//rdbtnDirigido.setSelected(true);
		rdbtnDirigido.setBounds(6, 28, 73, 23);
		panel.add(rdbtnDirigido);
		
		JRadioButton rdbtnNoDirigido = new JRadioButton("No Dirigido");
		bt1.add(rdbtnNoDirigido);
		//rdbtnNoDirigido.setSelected(true);
		rdbtnNoDirigido.setBounds(6, 54, 77, 23);
		panel.add(rdbtnNoDirigido);
		
		JRadioButton rdbtnNo = new JRadioButton("No");
		bt2.add(rdbtnNo);
		rdbtnNo.setBounds(81, 28, 46, 23);
		panel.add(rdbtnNo);
		
		JRadioButton rdbtnSi = new JRadioButton("Si");
		bt2.add(rdbtnSi);
		rdbtnSi.setBounds(81, 54, 39, 23);
		panel.add(rdbtnSi);
		
		JRadioButton rdbtnLetras = new JRadioButton("Letras");
		bt3.add(rdbtnLetras);
		//rdbtnLetras.setSelected(true);
		rdbtnLetras.setBounds(138, 28, 55, 23);
		panel.add(rdbtnLetras);
		
		JRadioButton rdbtnNumeros = new JRadioButton("Numeros");
		bt3.add(rdbtnNumeros);
		rdbtnNumeros.setBounds(138, 54, 77, 23);
		panel.add(rdbtnNumeros);
		
		/*bt1.add(rdbtnDirigido);
		bt1.add(rdbtnNoDirigido);
		
		bt2.add(rdbtnNo);
		bt2.add(rdbtnSi);
		
		bt3.add(rdbtnLetras);
		bt3.add(rdbtnNumeros);
		*/
		JPanel panelDibujoG = new JPanel();
		panelDibujoG.setLayout(null);
		panelDibujoG.setToolTipText("Zona de Dibujo");
		panelDibujoG.setForeground(Color.BLACK);
		panelDibujoG.setBorder(new LineBorder(SystemColor.activeCaptionBorder, 1, true));
		panelDibujoG.setBounds(10, 78,604, 199);
		panel.add(panelDibujoG);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Seleccione", "Insertar un vertice", "Eliminar un vertice", "Insertar una arista", "Eliminar arista", "Editar coste de arista"}));
		comboBox.setBounds(492, 28, 122, 23);
		panel.add(comboBox);
		
		JLabel lbFuncionalidades = new JLabel("Funcionalidades");
		lbFuncionalidades.setBounds(491, 7, 77, 14);
		panel.add(lbFuncionalidades);
		
		JButton btnDijkstra = new JButton("Dijkstra");
		btnDijkstra.setBounds(624, 7, 118, 23);
		panel.add(btnDijkstra);
		
		JButton btnMAdyacencias = new JButton("M-Adyacencias");
		btnMAdyacencias.setBounds(624, 47, 118, 23);
		panel.add(btnMAdyacencias);
		
		JButton btnInformscion = new JButton("Informscion");
		btnInformscion.setBounds(624, 91, 118, 23);
		panel.add(btnInformscion);
		
		JButton btnIr = new JButton("Ir");
		btnIr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnIr.setBounds(436, 28, 46, 23);
		panel.add(btnIr);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(624, 135, 118, 23);
		panel.add(btnLimpiar);
		
		textField = new JTextField();
		textField.setBounds(10, 192, 284, 48);
		panel.add(textField);
		
		scrollPane = new  JScrollPane(textField, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 288, 604, 101);
		panel.add(scrollPane);
	}
}
