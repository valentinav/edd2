package presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JScrollPane;

import java.awt.CardLayout;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Graphics;

import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import logica.*;
import Servicios.*;
import Servicios.*;

import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.ScrollPaneConstants;

public class Principal {

	private JFrame frmProyectofinal;
	private JTextField textUsuarioG1;
	private JTextField textUsuarioG2;
	//creacion de grupos para radioButton
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private final ButtonGroup buttonGroup_4 = new ButtonGroup();
	private final ButtonGroup buttonGroup_5 = new ButtonGroup();
	private JTextArea textImprecionG;
	
	//creacion de grafos
	private Grafo<Integer> GrafoDirigido =new Grafo();
	private Grafo<String> GrafoDirigidoCad =new Grafo();
	private Grafo<Integer> GrafoNoDirigido =new Grafo();
	private Grafo<String> GrafoNoDirigidoCad =new Grafo();
	//creacion de bipartitos
	private int cant_V=0;
	private int cant_U=0;
	private String v[];
	private String u[];
	private Grafo<String> bipartito;
	private String Informacion;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmProyectofinal.setLocationRelativeTo(null);;
					window.frmProyectofinal.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmProyectofinal = new JFrame();
		frmProyectofinal.setTitle("ProyectoFinal");
		frmProyectofinal.setBounds(100, 100, 913, 720);
		frmProyectofinal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmProyectofinal.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		frmProyectofinal.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelGrafo = new JPanel();
		tabbedPane.addTab("Grafos", null, panelGrafo, null);
		panelGrafo.setLayout(null);
		
		JPanel panelDibujoG = new JPanel();
		panelDibujoG.setLayout(null);
		panelDibujoG.setToolTipText("Zona de Dibujo");
		panelDibujoG.setForeground(Color.BLACK);
		panelDibujoG.setBorder(new LineBorder(SystemColor.activeCaptionBorder, 1, true));
		panelDibujoG.setBounds(10, 195,447, 272);
		panelGrafo.add(panelDibujoG);
		
		JPanel panelUsuarioGrafo = new JPanel();
		panelUsuarioGrafo.setBorder(new BevelBorder(BevelBorder.LOWERED, UIManager.getColor("Button.shadow"), SystemColor.inactiveCaption, SystemColor.inactiveCaptionBorder, SystemColor.activeCaption));
		panelUsuarioGrafo.setBounds(0, 0,892, 187);
		panelGrafo.add(panelUsuarioGrafo);
		panelUsuarioGrafo.setLayout(null);
		
		JPanel panelGRAFO = new JPanel();
		panelGRAFO.setLayout(null);
		panelGRAFO.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "GRAFO:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelGRAFO.setBounds(10, 11, 107, 79);
		panelUsuarioGrafo.add(panelGRAFO);
		
		JRadioButton NoDirigido = new JRadioButton("No Dirigido");
		buttonGroup_3.add(NoDirigido);
		NoDirigido.setBounds(6, 49, 88, 23);
		panelGRAFO.add(NoDirigido);
		
		JRadioButton Dirigido = new JRadioButton("Dirigido");
		buttonGroup_3.add(Dirigido);
		Dirigido.setBounds(6, 17, 71, 23);
		panelGRAFO.add(Dirigido);
		
		JPanel panelFORMATO = new JPanel();
		panelFORMATO.setLayout(null);
		panelFORMATO.setBorder(new TitledBorder(null, "TIPO:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelFORMATO.setBounds(262, 11, 107, 79);
		panelUsuarioGrafo.add(panelFORMATO);
		
		JRadioButton NumerosG = new JRadioButton("Numeros");
		buttonGroup_4.add(NumerosG);
		NumerosG.setBounds(6, 19, 95, 23);
		panelFORMATO.add(NumerosG);
		
		JRadioButton LetrasG = new JRadioButton("Letras");
		buttonGroup_4.add(LetrasG);
		LetrasG.setBounds(6, 45, 95, 23);
		panelFORMATO.add(LetrasG);
		
		JComboBox Vertice_o_Arista = new JComboBox();
		Vertice_o_Arista.setModel(new DefaultComboBoxModel(new String[] {"Vertice", "Arista"}));
		Vertice_o_Arista.setBounds(574, 158, 119, 20);
		panelUsuarioGrafo.add(Vertice_o_Arista);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(SystemColor.activeCaptionBorder, 1, true));
		panel_5.setBounds(391, 11, 314, 136);
		panelUsuarioGrafo.add(panel_5);
		panel_5.setLayout(null);
		
		textUsuarioG1 = new JTextField();
		textUsuarioG1.setBounds(65, 29, 86, 20);
		panel_5.add(textUsuarioG1);
		textUsuarioG1.setColumns(10);
		
		textUsuarioG2 = new JTextField();
		textUsuarioG2.setEnabled(false);
		textUsuarioG2.setBounds(65, 74, 86, 20);
		panel_5.add(textUsuarioG2);
		textUsuarioG2.setColumns(10);
		
		JLabel lblVertice = new JLabel("Vertice:");
		lblVertice.setBounds(9, 21, 46, 14);
		panel_5.add(lblVertice);
		
		JLabel lblVertice_1 = new JLabel("Vertice2: ");
		lblVertice_1.setBounds(10, 67, 46, 14);
		panel_5.add(lblVertice_1);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(UIManager.getBorder("TextArea.border"));
		panel_6.setBounds(194, 11, 110, 88);
		panel_5.add(panel_6);
		panel_6.setLayout(null);
		
		JRadioButton Inserta = new JRadioButton("Inserta");
		Inserta.setSelected(true);
		buttonGroup_2.add(Inserta);
		Inserta.setBounds(0, 7, 109, 23);
		panel_6.add(Inserta);
		
		JRadioButton Elimina = new JRadioButton("Elimina");
		buttonGroup_2.add(Elimina);
		Elimina.setBounds(0, 33, 109, 23);
		panel_6.add(Elimina);
		
		JRadioButton Edita = new JRadioButton("Edita");
		Edita.setEnabled(false);
		buttonGroup_2.add(Edita);
		Edita.setBounds(0, 60, 109, 23);
		panel_6.add(Edita);
		
		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(65, 105, 139, 20);
		panel_5.add(btnAceptar);
		
		JButton btnInformacionG = new JButton("Informacion");
		btnInformacionG.setEnabled(false);
		btnInformacionG.setBounds(736, 94, 129, 23);
		panelUsuarioGrafo.add(btnInformacionG);
		
		JButton btnGenerarArchivoG = new JButton("Gernerar Archivo");
		btnGenerarArchivoG.setEnabled(false);
		btnGenerarArchivoG.setBounds(728, 128, 154, 23);
		panelUsuarioGrafo.add(btnGenerarArchivoG);
		
		JButton btnLimpiarG = new JButton("Limpiar");
		btnLimpiarG.setToolTipText("Limpiar Grafo");
		btnLimpiarG.setBounds(736, 67, 129, 23);
		panelUsuarioGrafo.add(btnLimpiarG);
		
		JButton Desactiva = new JButton("");
		Desactiva.setBounds(478, 155, 43, 23);
		panelUsuarioGrafo.add(Desactiva);
		Desactiva.setVisible(false);
		
		JButton Dibujar = new JButton("");
		Dibujar.setBounds(391, 155, 43, 23);
		panelUsuarioGrafo.add(Dibujar);
		
		JRadioButton GBipartito = new JRadioButton("G. Bipartito");
		buttonGroup_3.add(GBipartito);
		GBipartito.setBounds(746, 158, 109, 22);
		panelUsuarioGrafo.add(GBipartito);
		
		JPanel panelPONDERADO = new JPanel();
		panelPONDERADO.setBorder(new TitledBorder(null, "PONDERADO", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelPONDERADO.setBounds(132, 11, 107, 79);
		panelUsuarioGrafo.add(panelPONDERADO);
		panelPONDERADO.setLayout(null);
		
		JRadioButton Si = new JRadioButton("Si");
		buttonGroup_5.add(Si);
		Si.setBounds(6, 17, 84, 23);
		panelPONDERADO.add(Si);
		
		JRadioButton No = new JRadioButton("No");
		buttonGroup_5.add(No);
		No.setBounds(6, 49, 84, 23);
		panelPONDERADO.add(No);
		
		JButton Dijsktra = new JButton("Dijsktra");
		Dijsktra.setBounds(736, 7, 129, 23);
		panelUsuarioGrafo.add(Dijsktra);
		
		JButton M_adyacencias = new JButton("M-Adyacencias");
		M_adyacencias.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try
				{
					{
						textImprecionG.setText("");
						if(Dirigido.isSelected())
						{
							if(LetrasG.isSelected()){Informacion = "Matriz Adyacencia: \n" + GrafoDirigidoCad.matriz();}
							if(NumerosG.isSelected()){Informacion = "Matriz Adyacencia: \n" + GrafoDirigido.matriz();}
						}if(NoDirigido.isSelected())
						{
							if(LetrasG.isSelected()){Informacion = "Matriz Adyacencia: \n" + GrafoNoDirigidoCad.matriz();}
							if(NumerosG.isSelected()){Informacion = "Matriz Adyacencia: \n" + GrafoNoDirigido.matriz();}
						}
						textImprecionG.setText(Informacion);
					}
				}catch(Exception e){ JOptionPane.showMessageDialog(null, e.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);}
			}
		});
		M_adyacencias.setBounds(736, 37, 129, 23);
		panelUsuarioGrafo.add(M_adyacencias);
		
		JPanel panelZIG = new JPanel();
		panelZIG.setBounds(10, 478, 872, 164);
		panelGrafo.add(panelZIG);
		panelZIG.setLayout(null);
		panelZIG.setBorder(UIManager.getBorder("CheckBox.border"));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 0, 867, 165);
		panelZIG.add(scrollPane_1);
		
		textImprecionG = new JTextArea();
		scrollPane_1.setViewportView(textImprecionG);
		textImprecionG.setEditable(true);
		
		JPanel panelRecubrimientoG = new JPanel();
		panelRecubrimientoG.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelRecubrimientoG.setBounds(467, 195, 415, 272);
		panelGrafo.add(panelRecubrimientoG);
		panelRecubrimientoG.setLayout(null);
		Dibujar.setVisible(false);

		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionG.setText("");
				if (Vertice_o_Arista.getSelectedIndex()==0)//el primer item es de vertice
				{//TODO
					if(Elimina.isSelected())//eliminar Vertice
					{
						if(Dirigido.isSelected()){
							if(Si.isSelected()){							
								if(NumerosG.isSelected())
								{
									if(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoDirigido.eliminar(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{
									if(GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
									{
										GrafoDirigidoCad.eliminar((textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}}
							else if(No.isSelected()){
								
								if(NumerosG.isSelected())
								{
									if(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoDirigido.eliminar(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{
									if(GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
									{
										GrafoDirigidoCad.eliminar((textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}
							}
						}else 
						if(NoDirigido.isSelected())
						{
							if(Si.isSelected()){							
								if(NumerosG.isSelected())
								{
									if(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoNoDirigido.eliminar(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{
									if(GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
									{
										GrafoNoDirigidoCad.eliminar((textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}
							}
							else if(No.isSelected()){
								
								if(NumerosG.isSelected())
								{
									if(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoNoDirigido.eliminar(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{
									if(GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
									{
										GrafoNoDirigidoCad.eliminar((textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}
														}
						}
						else if(GBipartito.isSelected())
						{
							JOptionPane.showMessageDialog(null, "No Se puede eliminar un vertice por que es un Grafo ("+
									cant_V+" X "+cant_U+")","Error",
									JOptionPane.ERROR_MESSAGE);
						}
					}//TODO
					if(Inserta.isSelected())//insetrar vertice
					{ try{
						if(Dirigido.isSelected())
						{
							if(Si.isSelected()){							
								if(NumerosG.isSelected())
								{
									if(!GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoDirigido.agregarVertices(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{
									if(textopermitido(textUsuarioG1.getText()))throw (new InvalidException());
									{
										if(!GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										{
											GrafoDirigidoCad.agregarVertices((textUsuarioG1.getText()));
											JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
										}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
												JOptionPane.ERROR_MESSAGE);}
									}
								}
							}
							else if(No.isSelected()){
								
								if(NumerosG.isSelected())
								{
									if(!GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoDirigido.agregarVertices(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{	if(textopermitido(textUsuarioG1.getText()))throw (new InvalidException());
									{

									if(!GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
									{
										GrafoDirigidoCad.agregarVertices((textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
									}
								}
							
							}
						}else
						if (NoDirigido.isSelected())
						{
							if(Si.isSelected()){							
								if(NumerosG.isSelected())
								{
									if(!GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoNoDirigido.agregarVertices(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{	if(textopermitido(textUsuarioG1.getText()))throw (new InvalidException());
									{
									if(!GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										{
											GrafoNoDirigidoCad.agregarVertices((textUsuarioG1.getText()));
											JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
										}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",JOptionPane.ERROR_MESSAGE);}
									}
								}
							}
							else if(No.isSelected()){
								
								if(NumerosG.isSelected())
								{
									if(!GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
									{
										GrafoNoDirigido.agregarVertices(Integer.parseInt(textUsuarioG1.getText()));
										JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
									}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
											JOptionPane.ERROR_MESSAGE);}
								}else 
								if (LetrasG.isSelected())
								{
									if(textopermitido(textUsuarioG1.getText()))throw (new InvalidException());
									{
										if(!GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										{
											GrafoNoDirigidoCad.agregarVertices((textUsuarioG1.getText()));
											JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
										}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",JOptionPane.ERROR_MESSAGE);}
									}
								}
														}
						}else if(GBipartito.isSelected())
						{
							JOptionPane.showMessageDialog(null, "No Se puede Agregar otro vertice por que Usted escogio un Grafo ("+
									cant_V+" X "+cant_U+")","Error",
									JOptionPane.ERROR_MESSAGE);
						}
						
						}	catch(InvalidException ex){ JOptionPane.showMessageDialog(null, ex.getMessage(), "Error",JOptionPane.ERROR_MESSAGE);
						}	catch (NumberFormatException e2) {JOptionPane.showMessageDialog(null, "Ingrese numeros enteros", "Error",JOptionPane.ERROR_MESSAGE);
						}
					}
				}if (Vertice_o_Arista.getSelectedIndex()==1)//el segundo item es de arista
				{//TODO
					if(Edita.isSelected())//edita peso de arista
					{
						if(Dirigido.isSelected())
						{
							if(Si.isSelected()){							
								if(NumerosG.isSelected())
								{
									int vertice1=Integer.parseInt(textUsuarioG1.getText());
									int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
									if((GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
											&&(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
									{
										String []mensaje=(GrafoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
										if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
										{	
											int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
										            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
										            mensaje, mensaje[0]);
		
											try{
												int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
												GrafoDirigido.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
												mensaje=(GrafoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
												JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
														+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
														JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
												
											}catch(Exception E)
											{
												JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
											E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
											}
										}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
												"Error",JOptionPane.ERROR_MESSAGE);}
									}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
								
								}else 
								if (LetrasG.isSelected())
								{
									String vertice1=(textUsuarioG1.getText());
									String vertice2=(textUsuarioG2.getText());
									if((GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
											&&(GrafoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
									{
										String []mensaje=(GrafoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
										if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
										{	
											int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
										            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
										            mensaje, mensaje[0]);
		
											try{
												int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
												GrafoDirigidoCad.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
												mensaje=(GrafoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
												JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
														+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
														JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
												
											}catch(Exception E)
											{
												JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
											E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
											}
										}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
												"Error",JOptionPane.ERROR_MESSAGE);}
									}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
								
								}
}
							else if(No.isSelected())
							{	JOptionPane.showMessageDialog(null, "El grafo seleccionado es no ponderado","Opci�n invalida",JOptionPane.WARNING_MESSAGE);}
						}else
						if(NoDirigido.isSelected())
						{
							if(Si.isSelected()){							
								if(NumerosG.isSelected())
								{
									int vertice1=Integer.parseInt(textUsuarioG1.getText());
									int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
									if((GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
											&&(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
									{
										String []mensaje=(GrafoNoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
										if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
										{	
											int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
										            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
										            mensaje, mensaje[0]);
		
											try{
												int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
												GrafoNoDirigido.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
												mensaje=(GrafoNoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
												JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
														+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
														JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
												
											}catch(Exception E)
											{
												JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
											E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
											}
										}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
												"Error",JOptionPane.ERROR_MESSAGE);}
									}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
								
								}else 
								if (LetrasG.isSelected())
								{
									String vertice1=(textUsuarioG1.getText());
									String vertice2=(textUsuarioG2.getText());
									if((GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
											&&(GrafoNoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
									{
										String []mensaje=(GrafoNoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
										if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
										{	
											int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
										            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
										            mensaje, mensaje[0]);
		
											try{
												int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
												GrafoNoDirigidoCad.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
												mensaje=(GrafoNoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
												JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
														+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
														JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
												
											}catch(Exception E)
											{
												JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
											E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
											}
										}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
												"Error",JOptionPane.ERROR_MESSAGE);}
									}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
								
								}
}
							else if(No.isSelected()){ JOptionPane.showMessageDialog(null, "El grafo seleccionado es no ponderado","Opci�n invalida",JOptionPane.WARNING_MESSAGE);}
						}else if(GBipartito.isSelected())
						{
							JOptionPane.showMessageDialog(null,"Error: No puede editar su peso por que es un Grafo No Ponderado",
									"Error",JOptionPane.ERROR_MESSAGE);
						}
					}//TODO
					String mensaje="";
					if(Inserta.isSelected())//inserta arista
					{
						if(Dirigido.isSelected())
						{
							if(Si.isSelected()){							

								if(NumerosG.isSelected())
								{
									int vertice1=Integer.parseInt(textUsuarioG1.getText());
									int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
									if((GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
											&&(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
									{
										try{
											int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
											int val =GrafoDirigido.agregarAristaDirigido(vertice1, vertice2,peso);
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}else 
								if (LetrasG.isSelected())
								{
									if((GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
											&&(GrafoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
									{
										try{
											int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
											int val =GrafoDirigidoCad.agregarAristaDirigido((textUsuarioG1.getText()), (textUsuarioG2.getText()),peso);
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}
							
							}
							else if(No.isSelected()){
								

								if(NumerosG.isSelected())
								{
									int vertice1=Integer.parseInt(textUsuarioG1.getText());
									int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
									if((GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
											&&(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
									{
										try{
											int val =GrafoDirigido.agregarAristaDirigidoNP(vertice1, vertice2);
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}else 
								if (LetrasG.isSelected())
								{
									if((GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
											&&(GrafoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
									{
										try{
											int val =GrafoDirigidoCad.agregarAristaDirigidoNP((textUsuarioG1.getText()), (textUsuarioG2.getText()));
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}
							
														}
						}else
						if (NoDirigido.isSelected())
						{
							if(Si.isSelected()){							

								if(NumerosG.isSelected())
								{
									int vertice1=Integer.parseInt(textUsuarioG1.getText());
									int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
									if((GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
											&&(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
									{
										try{
											int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
											int val =GrafoNoDirigido.agregarAristaNoDirigido(vertice1, vertice2,peso);
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}else 
								if (LetrasG.isSelected())
								{
									if((GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
											&&(GrafoNoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
									{
										try{
											int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
											int val =GrafoNoDirigidoCad.agregarAristaNoDirigido((textUsuarioG1.getText()), (textUsuarioG2.getText()),peso);
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}
							}
							else if(No.isSelected()){
								
								if(NumerosG.isSelected())
								{
									int vertice1=Integer.parseInt(textUsuarioG1.getText());
									int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
									if((GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
											&&(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
									{
										try{
											int val =GrafoNoDirigido.agregarAristaNoDirigidoNP(vertice1, vertice2);
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}else 
								if (LetrasG.isSelected())
								{
									if((GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
											&&(GrafoNoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
									{
										try{
											int val =GrafoNoDirigidoCad.agregarAristaNoDirigidoNP((textUsuarioG1.getText()), (textUsuarioG2.getText()));
											if(val==0)
												JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
										}
									}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
								}
							
							}
						}else if(GBipartito.isSelected())
						{
							if((bipartito.verificarVertices((textUsuarioG1.getText())))
									&&(bipartito.verificarVertices((textUsuarioG2.getText()))))
							{
								try{
									
					//				int val =bipartito.agregarAristaNoDirigido((textUsuarioG1.getText()), (textUsuarioG2.getText()));
					//				if(val==0)
										JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
								}catch(Exception E)
								{
									JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
								}
							}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
						}
					}
				}
				
				Desactiva.doClick();
			}
			
		});
		btnInformacionG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionG.setText("");
				if(Dirigido.isSelected())
				{
					 if (LetrasG.isSelected())
					 {
						 String []vertices=GrafoDirigidoCad.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoDirigidoCad.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoDirigidoCad.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoDirigidoCad.mostrarAristasEntreVertices((vertice1),(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoDirigidoCad.grado((num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoDirigidoCad.grafoCompleto());
						 //TODO;
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE COMPLEMENTO :\n "+GrafoDirigidoCad.ComplementoGrafoDir());						 
					 
						 
					 }else if (NumerosG.isSelected())
					 {
						 String []vertices=GrafoDirigido.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoDirigido.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoDirigido.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoDirigido.mostrarAristasEntreVertices(Integer.parseInt(vertice1),
										 Integer.parseInt(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoDirigido.grado(Integer.parseInt(num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoDirigido.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE COMPLEMENTO :\n "+GrafoDirigido.ComplementoGrafoDir());						 
					 }
				}else if(NoDirigido.isSelected())
				{
					if (LetrasG.isSelected())
					 {
						 String []vertices=GrafoNoDirigidoCad.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoNoDirigidoCad.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoNoDirigidoCad.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoNoDirigidoCad.mostrarAristasEntreVertices((vertice1),(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoNoDirigidoCad.grado((num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoNoDirigidoCad.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE COMPLEMENTO :\n "+
						 GrafoNoDirigidoCad.ComplementoGrafoNoDir());						 
					 
						 
					 }else if (NumerosG.isSelected())
					 {
						 String []vertices=GrafoNoDirigido.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoNoDirigido.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoNoDirigido.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){ 
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoNoDirigido.mostrarAristasEntreVertices(Integer.parseInt(vertice1),
										 Integer.parseInt(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoNoDirigido.grado(Integer.parseInt(num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoNoDirigido.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE COMPLEMENTO :\n "+GrafoNoDirigido.ComplementoGrafoNoDir());						 
					 }
				}else if(GBipartito.isSelected())
				{
					 String []vertices=bipartito.imprimirVertices().trim().split(" ");
					 
					 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
					 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
					 textImprecionG.setText(textImprecionG.getText()+"  V ={"+bipartito.imprimirVertices()+"}\n");
					 textImprecionG.setText(textImprecionG.getText()+"  E ={"+bipartito.verticesFormalmento()+"}\n");
					 
					 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
					 for (String vertice1: vertices){
						 for (String vertice2: vertices){
							 String defecto="   No hay vertices asociados a este valor ".trim();
							 String resivido=bipartito.mostrarAristasEntreVertices((vertice1),(vertice2)).trim();
							 if(!defecto.equals(resivido))
							 {
								 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
								 String []Aristas=resivido.trim().split("\n");	
								 for(String arista:Aristas)
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
								 }
							 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
							
						 }
					 }
					 
					 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
					 for (String num: vertices){
						 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+bipartito.grado((num)));
					 }
					 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+bipartito.grafoCompleto());
					 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE ADYACENCIA :\n "+
							 bipartito.imprimeMatrizAdyacencia());
					
				}
			}
		});
		btnLimpiarG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionG.setText("");
				int resp;
				if(NoDirigido.isSelected())
				{
					 if(NumerosG.isSelected())
					 {
						 resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo No Dirigido<Numeros>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Grafo Borrado .");

								 GrafoNoDirigido=new Grafo();
								panelDibujoG.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
						   }
					 }else if(LetrasG.isSelected())
					 {
						 resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo No Dirigido<Letras>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Grafo Borrado .");
								 GrafoNoDirigidoCad=new Grafo();
								panelDibujoG.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
						   }
					 }
				}else
				if(Dirigido.isSelected())
				{
					if(NumerosG.isSelected())
					 {
						resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo Dirigido<Numeros>?");
						if (JOptionPane.OK_OPTION == resp){
							JOptionPane.showMessageDialog(null, "Grafo Borrado .");

							 GrafoDirigido=new Grafo();
							panelDibujoG.repaint();
					    }else if(JOptionPane.CANCEL_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
					   }else if(JOptionPane.NO_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
					   }
					 }else if(LetrasG.isSelected())
					 {
						 resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo Dirigido<Letras>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Grafo Borrado .");
								 GrafoDirigidoCad=new Grafo();
								panelDibujoG.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
						   }
					 }
				}else if(GBipartito.isSelected()){JOptionPane.showMessageDialog(null, "Usted Usted Podra Borrar el Grafo Bipartico seleccionando "
						+ "de nuevo a �G.Bipartito� ");}
				Desactiva.doClick();
			}
		});
		btnGenerarArchivoG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomArch="", tipo1 ="",tipo2="";
				if(Dirigido.isSelected()){
					if(NumerosG.isSelected()){
						nomArch="Dirigido.txt";
						tipo1= "numeros";
						tipo2="Dirigido";
					}else
					if(LetrasG.isSelected()){
						nomArch="Dirigidocad.txt";	
						tipo2="Dirigido";
						tipo1="letras";
					}
				}else if(NoDirigido.isSelected())
				{
					if(NumerosG.isSelected()){
						nomArch="NoDirigido.txt";
						tipo1= "numeros";
						tipo2="No Dirigido";
					}else
					if(LetrasG.isSelected()){
						nomArch="NoDirigidocad.txt";
						tipo2="No Dirigido";
						tipo1="letras";
					}
				}else if(GBipartito.isSelected())
				{
					nomArch="bipartito.txt";
					tipo1= cant_V+","+cant_U;
					tipo2="Bipartito";
				}
				
				if(!textImprecionG.getText().equals(""))
				{
					int resp=JOptionPane.showConfirmDialog(null,"Desea generar el Archivo de Grafo " +tipo2+"<"+tipo1+"> con la informacion en pantalla?");
					if (JOptionPane.OK_OPTION == resp){
						JOptionPane.showMessageDialog(null, "se ha guardado en el archivo en : "+nomArch,"informacion",JOptionPane.INFORMATION_MESSAGE);
						
				    	//Archivo miArch =new Archivo("entrada.txt",nomArch);
				    	String info=(textImprecionG.getText());
				    	//System.out.println(info);
				    	//miArch.ingresar(info);
				    	//miArch.cerrar();
				    	
				    }else if(JOptionPane.CANCEL_OPTION==resp)
				    {
				    	JOptionPane.showMessageDialog(null, "Usted cancelo guardar en el Archivo");
				   }else if(JOptionPane.NO_OPTION==resp)
				    {
				    	JOptionPane.showMessageDialog(null, "Usted No guardo en el archivo");
				   }
				}else{
					JOptionPane.showMessageDialog(null, "No se pudo guardar nada  Reacarge la Informacion"
							,"Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
//modificacion del estado enable desde el combobox
				Vertice_o_Arista.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
				if(GBipartito.isSelected())
				{
					textUsuarioG1.setText("V_");
					textUsuarioG2.setText("U_");
				}
				Desactiva.doClick();			
				}	
				});
				Si.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Desactiva.doClick();
						textImprecionG.setText("");
						textUsuarioG1.setText("");
						textUsuarioG2.setText("");

					}
				});
				No.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Desactiva.doClick();
						textImprecionG.setText("");
						textUsuarioG1.setText("");
						textUsuarioG2.setText("");

					}
				});
				Dirigido.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Desactiva.doClick();
						textImprecionG.setText("");
						textUsuarioG1.setText("");
						textUsuarioG2.setText("");
		
					}
				});		
				NoDirigido.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Desactiva.doClick();
						textImprecionG.setText("");
						textUsuarioG1.setText("");
						textUsuarioG2.setText("");
					}
				});
				NumerosG.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if(GBipartito.isSelected())
						{
							NumerosG.setSelected(false);
							LetrasG.setSelected(true);
							JOptionPane.showMessageDialog(null, "No Es Posible este Cambio De Tipo En El Grafo Bipartito","Error", JOptionPane.CANCEL_OPTION);
						}else{
							Desactiva.doClick();
							textImprecionG.setText("");
							textUsuarioG1.setText("");
							textUsuarioG2.setText("");
						}
					}
				});
				LetrasG.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Desactiva.doClick();
						textImprecionG.setText("");
						textUsuarioG1.setText("");
						textUsuarioG2.setText("");				
					}
				});
				
				Desactiva.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						panelDibujoG.repaint();
						Dibujar.doClick();
						if (Vertice_o_Arista.getSelectedIndex()==0)
						{
							if(Dirigido.isSelected()){
								if(Si.isSelected()){

									if(NumerosG.isSelected()){
										if(GrafoDirigido.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
											
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}else
									if(LetrasG.isSelected()){
										if(GrafoDirigidoCad.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}
								
								}else if(No.isSelected()){

									if(NumerosG.isSelected()){
										if(GrafoDirigido.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
											
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}else
									if(LetrasG.isSelected()){
										if(GrafoDirigidoCad.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}
								
								}
							}else if(NoDirigido.isSelected())
							{
								if(Si.isSelected()){

									if(NumerosG.isSelected()){
										if(GrafoNoDirigido.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
											
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}else
									if(LetrasG.isSelected()){
										if(GrafoNoDirigidoCad.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}
									
								
								}else if(No.isSelected()){

									if(NumerosG.isSelected()){
										if(GrafoNoDirigido.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
											
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}else
									if(LetrasG.isSelected()){
										if(GrafoNoDirigidoCad.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(true);
											Edita.setEnabled(false);
											textUsuarioG2.setEnabled(false);
										}
									}
									
								}
							}else if (GBipartito.isSelected())
							{
								if(bipartito.cantidadVertices()==0)
								{
									btnGenerarArchivoG.setEnabled(false);
									btnInformacionG.setEnabled(false);
									Elimina.setEnabled(true);
									Edita.setEnabled(false);
									textUsuarioG2.setEnabled(false);
								}else{
									btnGenerarArchivoG.setEnabled(true);
									btnInformacionG.setEnabled(true);
									Elimina.setEnabled(true);
									Edita.setEnabled(false);
									textUsuarioG2.setEnabled(false);
								}
							}
						}
						if (Vertice_o_Arista.getSelectedIndex()==1)
						{
							if(Dirigido.isSelected()){
								if(Si.isSelected()){
									if(NumerosG.isSelected()){
										if(GrafoDirigido.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
											
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
										}
									}else
									if(LetrasG.isSelected()){
										if(GrafoDirigidoCad.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
										}
									}
								}else if(No.isSelected()){

									if(NumerosG.isSelected()){
										if(GrafoDirigido.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
											
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
										}
									}else
									if(LetrasG.isSelected()){
										if(GrafoDirigidoCad.cantidadVertices()==0)
										{
											btnGenerarArchivoG.setEnabled(false);
											btnInformacionG.setEnabled(false);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
										}else{
											btnGenerarArchivoG.setEnabled(true);
											btnInformacionG.setEnabled(true);
											Elimina.setEnabled(false);
											Edita.setEnabled(true);
											textUsuarioG2.setEnabled(true);
										}
									}
								
								}
							}else if(NoDirigido.isSelected())
							{if(Si.isSelected()){

								if(NumerosG.isSelected()){
									if(GrafoNoDirigido.cantidadVertices()==0)
									{
										btnGenerarArchivoG.setEnabled(false);
										btnInformacionG.setEnabled(false);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
										
									}else{
										btnGenerarArchivoG.setEnabled(true);
										btnInformacionG.setEnabled(true);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
									}
								}else
								if(LetrasG.isSelected()){
									if(GrafoNoDirigidoCad.cantidadVertices()==0)
									{
										btnGenerarArchivoG.setEnabled(false);
										btnInformacionG.setEnabled(false);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
									}else{
										btnGenerarArchivoG.setEnabled(true);
										btnInformacionG.setEnabled(true);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
									}
								}	
							
							}else if(No.isSelected()){

								if(NumerosG.isSelected()){
									if(GrafoNoDirigido.cantidadVertices()==0)
									{
										btnGenerarArchivoG.setEnabled(false);
										btnInformacionG.setEnabled(false);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
										
									}else{
										btnGenerarArchivoG.setEnabled(true);
										btnInformacionG.setEnabled(true);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
									}
								}else
								if(LetrasG.isSelected()){
									if(GrafoNoDirigidoCad.cantidadVertices()==0)
									{
										btnGenerarArchivoG.setEnabled(false);
										btnInformacionG.setEnabled(false);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
									}else{
										btnGenerarArchivoG.setEnabled(true);
										btnInformacionG.setEnabled(true);
										Elimina.setEnabled(false);
										Edita.setEnabled(true);
										textUsuarioG2.setEnabled(true);
									}
								}
								
							
							}}else if (GBipartito.isSelected())
							{
								if(bipartito.cantidadVertices()==0)
								{
									btnGenerarArchivoG.setEnabled(false);
									btnInformacionG.setEnabled(false);
									Elimina.setEnabled(false);
									Edita.setEnabled(true);
									textUsuarioG2.setEnabled(true);
								}else{
									btnGenerarArchivoG.setEnabled(true);
									btnInformacionG.setEnabled(true);
									Elimina.setEnabled(false);
									Edita.setEnabled(true);
									textUsuarioG2.setEnabled(true);
								}
							}
						
						}
					}
				});
				
				Dibujar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						JOptionPane.showMessageDialog(null, "Se graficara en la zona de impresion", "Graficar",JOptionPane.WARNING_MESSAGE);
						Graphics k=panelDibujoG.getGraphics();
						Dibujo t=new Figura();
						int cantV=0;//CANTIDAD VERTICES
						//TODO
						if(Dirigido.isSelected()){
							if(Si.isSelected()){
								if(NumerosG.isSelected()){
									if(GrafoDirigido.cantidadVertices()!=0)
									{
										String[] vertices= GrafoDirigido.imprimirVertices().trim().split(" ");
										cantV=GrafoDirigido.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoDirigido.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoDirigido.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoDirigido.verificarVertices(Integer.parseInt(inicioFin[0])))&&
														(GrafoDirigido.verificarVertices(Integer.parseInt(inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
											/*
										for(int i =0;i<aristasG.length;i++)
										{
											t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
										}*/
									}
								}else
								if(LetrasG.isSelected())
								{
									if(GrafoDirigidoCad.cantidadVertices()!=0)
									{
										String[] vertices= GrafoDirigidoCad.imprimirVertices().trim().split(" ");
										cantV=GrafoDirigidoCad.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoDirigidoCad.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoDirigidoCad.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoDirigidoCad.verificarVertices((inicioFin[0])))&&
														(GrafoDirigidoCad.verificarVertices((inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
									}
								}
//------------------------�����������������������������������������������������������������������������������������������������------------------------------------------------------------------------------------------------------------------------------------------------------------------------								
							}else if(No.isSelected()){

								if(NumerosG.isSelected()){
									if(GrafoDirigido.cantidadVertices()!=0)
									{
										String[] vertices= GrafoDirigido.imprimirVertices().trim().split(" ");
										cantV=GrafoDirigido.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoDirigido.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoDirigido.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoDirigido.verificarVertices(Integer.parseInt(inicioFin[0])))&&
														(GrafoDirigido.verificarVertices(Integer.parseInt(inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
											/*
										for(int i =0;i<aristasG.length;i++)
										{
											t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
										}*/
									}
								}else
								if(LetrasG.isSelected())
								{
									if(GrafoDirigidoCad.cantidadVertices()!=0)
									{
										String[] vertices= GrafoDirigidoCad.imprimirVertices().trim().split(" ");
										cantV=GrafoDirigidoCad.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoDirigidoCad.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoDirigidoCad.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoDirigidoCad.verificarVertices((inicioFin[0])))&&
														(GrafoDirigidoCad.verificarVertices((inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
									}
								}			
							}
						}
						//TODO====================================================================================================================================================================
						else if(NoDirigido.isSelected())
						{
							if(Si.isSelected()){
								if(NumerosG.isSelected()){
									if(GrafoNoDirigido.cantidadVertices()!=0)
									{
										String[] vertices= GrafoNoDirigido.imprimirVertices().trim().split(" ");
										cantV=GrafoNoDirigido.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoNoDirigido.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoNoDirigido.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoNoDirigido.verificarVertices(Integer.parseInt(inicioFin[0])))&&
														(GrafoNoDirigido.verificarVertices(Integer.parseInt(inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
									}
								}else
								if(LetrasG.isSelected())
								{
									if(GrafoNoDirigidoCad.cantidadVertices()!=0)
									{
										String[] vertices= GrafoNoDirigidoCad.imprimirVertices().trim().split(" ");
										cantV=GrafoNoDirigidoCad.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoNoDirigidoCad.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoNoDirigidoCad.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoNoDirigidoCad.verificarVertices((inicioFin[0])))&&
														(GrafoNoDirigidoCad.verificarVertices((inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
									}
								}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
							}else if(No.isSelected()){
								if(NumerosG.isSelected()){
									if(GrafoNoDirigido.cantidadVertices()!=0)
									{
										String[] vertices= GrafoNoDirigido.imprimirVertices().trim().split(" ");
										cantV=GrafoNoDirigido.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoNoDirigido.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoNoDirigido.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoNoDirigido.verificarVertices(Integer.parseInt(inicioFin[0])))&&
														(GrafoNoDirigido.verificarVertices(Integer.parseInt(inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
									}
								}else
								if(LetrasG.isSelected())
								{
									if(GrafoNoDirigidoCad.cantidadVertices()!=0)
									{
										String[] vertices= GrafoNoDirigidoCad.imprimirVertices().trim().split(" ");
										cantV=GrafoNoDirigidoCad.cantidadVertices();
										String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
										int cantMP=calcularTamMatriz(cantV);
										String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
										boolean salirSegundoBucle=false;
										int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
										for (int i=0 ; i<cantMP;i++)
										{
											for (int j=0 ; j<cantMP;j++)
											{
												int datosLlenos=i*cantMP+(j+1);
												if (datosLlenos>cantV)
												{
													salirSegundoBucle=true;
													break;
												}else
												{									
													DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
													DatosDibujo[datoIngresado][1]=String.valueOf(i);
													DatosDibujo[datoIngresado][2]=String.valueOf(j);
													datoIngresado++;
												}
											}
											if(salirSegundoBucle)
												break;
										}
										int acumulador=0;
										int disparador=-1;
										for(int i =0;i<cantV;i++)
										{
											
											if(DatosDibujo[i][0]!=null)
											{
												
												int x=Integer.parseInt(DatosDibujo[i][1]);
												int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												if(x!=disparador){acumulador=0;}
												if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
												if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
												 
												y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
												DatosDibujo[i][2]=y+"";//este es opcional 
												//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
												t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
											}
										}
										String[]aristasG;
										if(!GrafoNoDirigidoCad.verticesFormalmento().equals(""))
										{
											String cadenaAristas=GrafoNoDirigidoCad.verticesFormalmento().trim().replace("(", "").replace(")", " ");
											//System.out.println(""+cadenaAristas);
											aristasG=cadenaAristas.split(" ");
											
											for(String arista:aristasG)
											{
												String[]inicioFin=arista.split(",");
												if((GrafoNoDirigidoCad.verificarVertices((inicioFin[0])))&&
														(GrafoNoDirigidoCad.verificarVertices((inicioFin[0]))))
												{
													String[] inicio=posicion(DatosDibujo,inicioFin[0]);
													String[] fin=posicion(DatosDibujo,inicioFin[1]);
													//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
													t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
															Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
												}
											}
										}
									}
								}
	
							}
						}
					}
					//TODO
					public int calcularTamMatriz(int vertices)
					{
						int i;
						for( i =0; i<214748364; i++)
						{
							int tam=0;
							tam=i*i;
							if(tam>=vertices)
								break;
						}
						return (i);
					}
					public String[] posicion(String[][]datos, String buscando)
					{
						String[]poscicion=new String[2];
						for(int i =0;i<datos.length;i++)
						{
							if(datos[i][0].equals(buscando))
							{
								poscicion[0]=datos[i][1];
								poscicion[1]=datos[i][2];
								break;
							}					
						}
						return poscicion;
					}
				});
				
	}
	private boolean textopermitido(String texto)
	{
		char x = texto.charAt(0);
		boolean permitido =((int)x>=32&&(int)x<=96)||((int)x>=123&&(int)x<=255);
		return permitido;
	}

}
