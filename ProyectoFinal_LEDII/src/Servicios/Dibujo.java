package Servicios;

import java.awt.Color;
import java.awt.Graphics;
/**
 * 
 * @author Janeth Cristina Cifuentes Manrique
 *
 *esta solo contiene la declaracion de metodos que se implementara en la clase figura del paquete logica
 */
public interface Dibujo {

	  Color colorAleatorio(Graphics g );
	  void linea(Graphics g,String palabra, int x1, int y1, int x2, int y2 );
	  void grafico(Graphics g,String palabra,int x, int y, String color);
	  void prueba(Graphics g, int x1, int y1, int x2, int y2 );
	
}
