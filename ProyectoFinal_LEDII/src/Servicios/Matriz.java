package Servicios;

/**
 * Esta clase almacena una matriz  que se podra mostrar llamando el metodo
 *  y realiza algunas operaciones declaradas
 * @author Usuario
 *
 */
public class Matriz {
	private int[][] miMatriz;
	private int tamanio;
/**
 * inicializar la matriz
 * @param miMatriz matriz que almacenara el objeto
 */
	public Matriz(int[][] miMatriz) {
		this.miMatriz = miMatriz;
		this.tamanio = this.miMatriz.length;
	}
// getter y setters
	public int[][] getMiMatriz() {
		return miMatriz;
	}

	public void setMiMatriz(int[][] miMatriz) {
		this.miMatriz = miMatriz;
	}

	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}
//operaciones
	/**
	 * multiplicacion de matriz por ella misma
	 */
	public int[][] multiplicacionPorEllaMisma() {
		int[][] matrizResultante = new int[this.tamanio][this.tamanio];
		int val = 0, i = 0, multiplicado = 0, acumulador = 0;

		for (int x = 0; x < matrizResultante.length; x++) {
			for (int y = 0; y < matrizResultante[x].length; y++) {
				for (int z = 0; z < miMatriz.length; z++) {
					matrizResultante[x][y] += miMatriz[x][z] * miMatriz[z][y];
				}
			}
		}
		return (matrizResultante);
	}

//impreciones en pantalla
	/**
	 * imprime la matriz guardada en el objeto Matriz que se creo
	 */
	public String imprimirMatriz() {
		return imprimirMatriz(this.miMatriz);
	}

	/**
	 * imprime cualquier matriz enviada (o si es sin paratros  se hara: {@link #imprimirMatriz()} )
	 * 
	 * @param matriz
	 */
	public String imprimirMatriz(int[][] matriz) {
		int tam = matriz.length;
		String impresion = "";
		for (int i = 0; i < tam; i++) {
			impresion += "\n";
			for (int j = 0; j < tam; j++) {
				impresion += matriz[i][j] + " ";
			}
		}
		return impresion;
	}
	/**
	 *llena con ceros la matriz
	 * 
	 * @param matriz
	 */
	public void llenarConCeros () {
		int tam = miMatriz.length;
		for (int i = 0; i < tam; i++) {
			
			for (int j = 0; j < tam; j++) {
				miMatriz[i][j]=0;
			}
		}
	}
	/**
	 *modificar valor
	 * 
	 * 
	 */
	public void setPosicion  (int i , int j, int valor ) {
		
				miMatriz[i][j]=valor;
				
	}
	/**
	 *ver valor
	 * 
	 * 
	 */
	public int gerPosicion  (int i , int j) {
		
			return	miMatriz[i][j];
				
	}
}
