package Interfaz_Arboles;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.AbstractAction;

import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import Archivo.Archivo;
import Logica_Negocio.Arbol_ABB;
import Logica_Negocio.Arbol_AVL;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Dibujo extends javax.swing.JFrame {
	private JTextArea jTar_Informacion;
	private JComboBox jCbox_Recorridos;
	private JButton Btn_abrirArchivo;
	private JButton jBtn_Generar;
	private JButton Btn_Insertar;
	private JButton Btn_Informacion;
	private JTextField TFD_Datos;
	private JLabel Lbl_Recoriidos;
	private JButton Btn_Limpiar;
	private JButton Btn_Eliminar;
	private static JRadioButton jRbtn_ABB;
	private static JRadioButton jRbtn_AVL;
	private ButtonGroup buttonGroup1;
	private JDesktopPane jDesktopPane1;
	private AbstractAction abstractAction2;
	private AbstractAction abstractAction1;
	private JButton jButton2;
	private JButton jButton1;

	private JScrollPane scrll_Datos ;
	private static ButtonGroup grupo= new ButtonGroup();

	private Arbol_ABB abb = new Arbol_ABB();
	private Arbol_AVL avl = new Arbol_AVL();
	
	private String informacion;
	private String nombreArchivo ;
	private Archivo arch = new Archivo();
	//
	
	//
	private int mostrar =0;
	//
	private int Altura;
	private int numero_Nodos;
	private ArrayList<Integer> sub_Izq = new ArrayList<Integer>();
	private ArrayList<Integer> sub_Der = new ArrayList<Integer>();
	private ArrayList<String> Grado_cNodo = new ArrayList<String>();
	private ArrayList<Integer> nodosInteriores = new ArrayList<Integer>();
	private ArrayList<String> hijos_cNodo = new ArrayList<String>();
	private ArrayList<String> hermanos_cNodo = new ArrayList<String>();
	private ArrayList<String> elem_Nivel = new ArrayList<String>();
	private ArrayList<String> cont_elem = new ArrayList<String>();
	private ArrayList<Integer> nodos_Hoja = new ArrayList<Integer>();
	

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Dibujo inst = new Dibujo();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
				
				grupo.add(jRbtn_AVL);
				grupo.add(jRbtn_ABB);
				
			}
		});
	}
	
	public Dibujo() {
		super("Arboles Binarios De Busqueda");
		initGUI();
	}
	
	private void initGUI() {
		try {
			
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				
				jRbtn_AVL = new JRadioButton("Arbol AVL");
				getContentPane().add(jRbtn_AVL);
				jRbtn_AVL.setBounds(12, 439, 84, 18);
			}
			{
				jRbtn_ABB = new JRadioButton("Arbol ABB");
				getContentPane().add(jRbtn_ABB);
				jRbtn_ABB.setBounds(12, 407, 84, 18);
			}
			{
				ComboBoxModel jCbox_RecorridosModel = 
					new DefaultComboBoxModel(
							new String[] { "","Preorden", "Inorden","Postorden" });
				jCbox_Recorridos = new JComboBox();
				getContentPane().add(jCbox_Recorridos);
				jCbox_Recorridos.setModel(jCbox_RecorridosModel);
				jCbox_Recorridos.setBounds(640, 406, 138, 21);
				jCbox_Recorridos.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if (jRbtn_ABB.isSelected() || jRbtn_AVL.isSelected()) {
							if (jRbtn_ABB.isSelected()) {
								if (abb.getRaiz() != null) {
									if (jCbox_Recorridos.getSelectedItem().toString().compareTo("Preorden") == 0) {
										ArrayList<Integer> aux = new ArrayList();
										aux = abb.imprimir_preorden();
										String valores = "Recorrido Preorden: ";
										for (int i = 0; i < aux.size(); i++) {
											valores = (valores + String.valueOf(aux.get(i)) + ", ");
										}
										abb.limpiarArrays();
										jTar_Informacion.setText("");
										jTar_Informacion.setText(valores);
									}
									if (jCbox_Recorridos.getSelectedItem().toString().compareTo("Inorden") == 0) {
										ArrayList<Integer> aux = new ArrayList();
										aux = abb.imprimir_inorden();
										String valores = "Recorrido Inorden: ";
										for (int i = 0; i < aux.size(); i++) {
											valores = (valores + String.valueOf(aux.get(i)) + ", ");
										}
										abb.limpiarArrays();
										jTar_Informacion.setText("");
										jTar_Informacion.setText(valores);

									}
									if (jCbox_Recorridos.getSelectedItem().toString().compareTo("Postorden") == 0) {
										ArrayList<Integer> aux = new ArrayList();
										aux = abb.imprimir_postorden();
										String valores = "Recorrido Postorden: ";
										for (int i = 0; i < aux.size(); i++) {
											valores = (valores + String.valueOf(aux.get(i)) + ", ");
										}
										abb.limpiarArrays();
										jTar_Informacion.setText("");
										jTar_Informacion.setText(valores);
									}
								} else
									JOptionPane.showMessageDialog(null,"Aun no Existen Elementos\nEn el Arbol ABB","Estado del Programa",JOptionPane.INFORMATION_MESSAGE);
							}
							if (jRbtn_AVL.isSelected()) {
								if (avl.getRaiz() != null) {
									if (jCbox_Recorridos.getSelectedItem().toString().compareTo("Preorden") == 0) {
										ArrayList<Integer> aux = new ArrayList();
										aux = avl.imprimir_preorden();
										String valores = "Recorrido Preorden: ";
										for (int i = 0; i < aux.size(); i++) {
											valores = (valores + String.valueOf(aux.get(i)) + ", ");
										}
										avl.limpiarArrays();
										jTar_Informacion.setText("");
										jTar_Informacion.setText(valores);
									}
									if (jCbox_Recorridos.getSelectedItem().toString().compareTo("Inorden") == 0) {
										ArrayList<Integer> aux = new ArrayList();
										aux = avl.imprimir_inorden();
										String valores = "Recorrido Inorden: ";
										for (int i = 0; i < aux.size(); i++) {
											valores = (valores + String.valueOf(aux.get(i)) + ", ");
										}
										avl.limpiarArrays();
										jTar_Informacion.setText("");
										jTar_Informacion.setText(valores);

									}
									if (jCbox_Recorridos.getSelectedItem().toString().compareTo("Postorden") == 0) {
										ArrayList<Integer> aux = new ArrayList();
										aux = avl.imprimir_postorden();
										String valores = "Recorrido Postorden: ";
										for (int i = 0; i < aux.size(); i++) {
											valores = (valores + String.valueOf(aux.get(i)) + ", ");
										}
										avl.limpiarArrays();
										jTar_Informacion.setText("");
										jTar_Informacion.setText(valores);
									}
								} else
									JOptionPane.showMessageDialog(null,"Aun no eExisten Elementos \nEn el Arbol AVL","Estado del Programa",JOptionPane.INFORMATION_MESSAGE);
							}
						} else
							JOptionPane.showMessageDialog(null,"Aun no ha Seleccionado Ningun Arbol","Estado del Programa",JOptionPane.INFORMATION_MESSAGE);

					}
				});
			}
			{
				jBtn_Generar = new JButton();
				getContentPane().add(jBtn_Generar);
				jBtn_Generar.setText("Generar Archivo");
				jBtn_Generar.setBounds(462, 438, 144, 21);
				jBtn_Generar.setEnabled(false);
				jBtn_Generar.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						
						String nombre = JOptionPane.showInputDialog(null,"Digite el nombre del archivo\n�Sin extension!","Pedir nombre",JOptionPane.INFORMATION_MESSAGE);
						nombreArchivo = nombre;
						Btn_abrirArchivo.setEnabled(true);
						if(jRbtn_ABB.isSelected()){
							ArrayList<Integer> aux = new ArrayList();
							aux = abb.imprimir_preorden();
							String valores = "Recorrido Preorden: ";
							for (int i = 0; i < aux.size(); i++) {
								valores = (valores + String.valueOf(aux.get(i)) + ", ");
							}
							abb.limpiarArrays();
							informacion = informacion + "\n"+valores;
							
							aux = abb.imprimir_inorden();
							valores = "Recorrido Inorden: ";
							for (int i = 0; i < aux.size(); i++) {
								valores = (valores + String.valueOf(aux.get(i)) + ", ");
							}
							abb.limpiarArrays();
							informacion = informacion + "\n"+valores;
							
							aux = abb.imprimir_postorden();
							valores = "Recorrido Postorden: ";
							for (int i = 0; i < aux.size(); i++) {
								valores = (valores + String.valueOf(aux.get(i)) + ", ");
							}
							abb.limpiarArrays();
							informacion = informacion + "\n"+valores;
							//escritura en el archivo
							arch.escribirArchivo(nombreArchivo, informacion);
						}
						
						
						
						
						
						
						
						if(jRbtn_AVL.isSelected()){
							ArrayList<Integer> aux = new ArrayList();
							aux = avl.imprimir_preorden();
							String valores = "Recorrido Preorden: ";
							for (int i = 0; i < aux.size(); i++) {
								valores = (valores + String.valueOf(aux.get(i)) + ", ");
							}
							avl.limpiarArrays();
							informacion = informacion + "\n"+valores;
							
							aux = avl.imprimir_inorden();
							valores = "Recorrido Inorden: ";
							for (int i = 0; i < aux.size(); i++) {
								valores = (valores + String.valueOf(aux.get(i)) + ", ");
							}
							avl.limpiarArrays();
							informacion = informacion + "\n"+valores;
							
							aux = avl.imprimir_postorden();
							valores = "Recorrido Postorden: ";
							for (int i = 0; i < aux.size(); i++) {
								valores = (valores + String.valueOf(aux.get(i)) + ", ");
							}
							avl.limpiarArrays();
							informacion = informacion + "\n"+valores;
							//escritura en el archivo
							arch.escribirArchivo(nombreArchivo, informacion);
						}
						
					}
				});
			}
			
		   

			{
				Btn_Insertar = new JButton();
				getContentPane().add(Btn_Insertar);
				Btn_Insertar.setText("Insertar");
				Btn_Insertar.setBounds(217, 406, 112, 21);
				Btn_Insertar.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						if(jRbtn_ABB.isSelected() || jRbtn_AVL.isSelected()){
						if(!TFD_Datos.getText().equals("")){
							
							String valor = TFD_Datos.getText();
							Boolean invariante = Numerico(valor);
							if(invariante){
								
								if (jRbtn_AVL.isSelected()) {
									//
	
									avl.insertar(Integer.parseInt(valor));
									TFD_Datos.setText("");
									JOptionPane.showMessageDialog(null, valor+"  Agregado en el Arbol AVL", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
									jRbtn_ABB.setEnabled(false);
									Btn_Informacion.setEnabled(true);
									jBtn_Generar.setEnabled(true);
									
									repaint();
									if(mostrar==0)
									{
										jDesktopPane1.setVisible(true);
										AVL_Grafico dg=new AVL_Grafico(avl);
										dg.setIconifiable(false);
										dg.setResizable(false);
										dg.setClosable(false);
										//dg.setResizable(true);
									
										dg.setLocation(1, 1);
									
										dg.setSize(500, 400);
										Dimension dlgSize = dg.getPreferredSize(); 
					    				Dimension pantalla = getSize(); 
					    				Dimension ventana = dg.getSize() ; 
					    				
					    				jDesktopPane1.add(dg);
					    				
					    		
					    				dg.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
					    				

									mostrar=1;
									}
								}
								
								
								if (jRbtn_ABB.isSelected()) {
					
									
									abb.insertar(Integer.parseInt(valor));
									TFD_Datos.setText("");
									JOptionPane.showMessageDialog(null, valor+"  Agregado en el Arbol ABB", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
									jRbtn_AVL.setEnabled(false);
									Btn_Informacion.setEnabled(true);
									jBtn_Generar.setEnabled(true);
						
									repaint();
									
									if(mostrar==0)
									{
										jDesktopPane1.setVisible(true);
										ABB_Grafico dg=new ABB_Grafico(abb);
						
										//dg.setResizable(true);
										
										dg.setLocation(1, 1);
									
										dg.setSize(500, 400);
										Dimension dlgSize = dg.getPreferredSize(); 
					    				Dimension pantalla = getSize(); 
					    				Dimension ventana = dg.getSize() ; 
					    				
					    				jDesktopPane1.add(dg);
					    			
					    				
					    				dg.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
					    				

									mostrar=1;
									}
								}
								
								
							}
							else{
								JOptionPane.showMessageDialog(null, "Debe ingresar datos validos", "Error lectura", JOptionPane.ERROR_MESSAGE);
								TFD_Datos.setText("");
							}
						}
						else
							JOptionPane.showMessageDialog(null,"debe ingresar algun valor","Estado del Programa",JOptionPane.INFORMATION_MESSAGE);
						
						}
						else{
							JOptionPane.showMessageDialog(null, "Debe Seleccionar un Arbol", "Estado del Programa", JOptionPane.ERROR_MESSAGE);
							TFD_Datos.setText("");
						}
						}
				});
			}
			{
				Btn_Eliminar = new JButton();
				getContentPane().add(Btn_Eliminar);
				Btn_Eliminar.setText("Eliminar");
				Btn_Eliminar.setBounds(217, 438, 112, 21);
				Btn_Eliminar.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						if(jRbtn_ABB.isSelected() || jRbtn_AVL.isSelected()){
						if(!TFD_Datos.getText().equals("")){
							String valor = TFD_Datos.getText();
							Boolean invariante = Numerico(valor);
							if(invariante){
								//Eliminar del arbol
								if (jRbtn_AVL.isSelected()) {
									boolean estado1=avl.eliminarNodo(Integer.parseInt(valor));
									//avl.eliminarNodo(Integer.parseInt(valor));
									TFD_Datos.setText("");
									//JOptionPane.showMessageDialog(null, valor+"  Eliminado Exitosamente del Arbol AVL", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
									if(estado1==true)
									{
									
										JOptionPane.showMessageDialog(null,"El elemento "+valor+" se elimino correctamente");
										repaint();
									}
									else
									{
										JOptionPane.showMessageDialog(null,"El elemento "+valor+" no se encuentra en el arbol");
								
								
									}
									jRbtn_ABB.setEnabled(false);
									
								}
								
								
								if (jRbtn_ABB.isSelected()) {
									//abb.eliminarNodo(Integer.parseInt(valor));
									TFD_Datos.setText("");
									//JOptionPane.showMessageDialog(null, valor+"  Eliminado Exitosamente del Arbol ABB", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
									boolean estado=abb.eliminarNodo(Integer.parseInt(valor));
								
									if(estado==true)
									{
									
										JOptionPane.showMessageDialog(null,"El elemento "+valor+" se elimino correctamente");
										repaint();
									}
									else
									{
										JOptionPane.showMessageDialog(null,"El elemento "+valor+" no se encuentra en el arbol");
								
								
									}
									
									jRbtn_AVL.setEnabled(false);
									
								}
							}
							else{
								JOptionPane.showMessageDialog(null, "Debe ingresar datos validos", "Error lectura", JOptionPane.ERROR_MESSAGE);
								TFD_Datos.setText("");
							}
						}
						else
							JOptionPane.showMessageDialog(null,"debe ingresar algun valor", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
					}else{
							JOptionPane.showMessageDialog(null, "Debe Seleccionar un Arbol", "Estado del Programa", JOptionPane.ERROR_MESSAGE);
							TFD_Datos.setText("");
						
					}}
				});
			}
			{
				Btn_Informacion = new JButton();
				getContentPane().add(Btn_Informacion);
				Btn_Informacion.setText("Informacion");
				Btn_Informacion.setBounds(346, 438, 105, 21);
				Btn_Informacion.setEnabled(false);
				Btn_Informacion.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(abb.raiz != null){
						if(jRbtn_ABB.isSelected()){
							informacion = "***** Informacion del arbol ABB *****\n"
									+ "\n Raiz : " + abb.getRaiz().elemento 
									+ "\n Altura: " + abb.profundidad()
									+ "\n Numero de Nodos: " + abb.numero_nodos()
									+"\n\n SubArbol Derecho: \n";
							
							sub_Izq = abb.subArbolDerecho(abb.raiz.der) ;
							sub_Der = abb.subArbolIzquierdo(abb.raiz.izq);
							
							for (int i = 0; i < sub_Izq.size(); i++) {
								informacion = informacion + sub_Izq.get(i)+", ";
							}
							
							informacion = informacion + "\n subArbol Izquierdo: \n";
							for (int i = 0; i < sub_Der.size(); i++) {
								informacion = informacion + sub_Der.get(i)+", ";
							}
							
							informacion = informacion + "\n\n Valor del nodo y su respectivo grado:\n";
							Grado_cNodo = abb.gradoCadaNodo();
							for (int j = 0; j < Grado_cNodo.size(); j++) {
								informacion= informacion + Grado_cNodo.get(j)+"\n";
							}
							
							informacion = informacion + "\n Nodos interiores del arbol: \n";
							nodosInteriores = abb.retornarNodosInterirores();
							for (int i = 0; i < nodosInteriores.size(); i++) {
								informacion = informacion + nodosInteriores.get(i)+", ";
							}
							
							informacion = informacion + "\n\n Valor del nodo y sus respectivos hijos: \n\n";
							hijos_cNodo = abb.hijosCadaNodo();
							for (int j = 0; j < hijos_cNodo.size(); j++) {
								informacion = informacion + hijos_cNodo.get(j)+"\n";
							}
							
							informacion = informacion + "\n\n Valor del nodo y su respectivo hermano: \n\n";
							hermanos_cNodo = abb.hnosCadaNodo();
							for (int j = 0; j < hermanos_cNodo.size(); j++) {
								informacion = informacion + hermanos_cNodo.get(j)+"\n";
							}
							
							
							
							informacion = informacion + "\n\n Nivel     Elemento: \n";
							elem_Nivel = abb.imprimirNiveles();
							cont_elem = abb.contadorNiveles();
							for (int i = 0; i < cont_elem.size(); i++) {
								informacion = informacion + cont_elem.get(i)+"\n";
							}
							informacion = informacion + "\n";
							for (int i = 0; i < elem_Nivel.size(); i++) {
								informacion = informacion + elem_Nivel.get(i)+"\n";
							}							
							
							informacion = informacion + "\nLos nodos hoja del arbol son: \n";
							nodos_Hoja = abb.NodosHoja();
							for (int j2 = 0; j2 < nodos_Hoja.size() ; j2++) {
								informacion = informacion + nodos_Hoja.get(j2)+", ";
							}
							
							jTar_Informacion.setText(informacion);
							  abb.sacarBasura();
							
						}}
						if(avl.raiz != null){
						if(jRbtn_AVL.isSelected()){
							informacion = "***** Informacion del arbol AVL *****\n"
								+ "\n Raiz : " + avl.getRaiz().elemento 
								+ "\n Altura: " + avl.profundidad()
								+ "\n Numero de Nodos: " + avl.numero_nodos()
								+"\n\n SubArbol Derecho: \n";
						
						sub_Izq = avl.subArbolDerecho(avl.raiz.der) ;
						sub_Der = avl.subArbolIzquierdo(avl.raiz.izq);
						
						for (int i = 0; i < sub_Izq.size(); i++) {
							informacion = informacion + sub_Izq.get(i)+", ";
						}
						
						informacion = informacion + "\n subArbol Izquierdo: \n";
						for (int i = 0; i < sub_Der.size(); i++) {
							informacion = informacion + sub_Der.get(i)+", ";
						}
						
						informacion = informacion + "\n\n Valor del nodo y su respectivo grado:\n";
						Grado_cNodo = avl.gradoCadaNodo();
						for (int j = 0; j < Grado_cNodo.size(); j++) {
							informacion= informacion + Grado_cNodo.get(j)+"\n";
						}
						
						informacion = informacion + "\n Nodos interiores del arbol: \n";
						nodosInteriores = avl.retornarNodosInterirores();
						for (int i = 0; i < nodosInteriores.size(); i++) {
							informacion = informacion + nodosInteriores.get(i)+", ";
						}
						
						informacion = informacion + "\n\n Valor del nodo y sus respectivos hijos: \n\n";
						hijos_cNodo = avl.hijosCadaNodo();
						for (int j = 0; j < hijos_cNodo.size(); j++) {
							informacion = informacion + hijos_cNodo.get(j)+"\n";
						}
						
						informacion = informacion + "\n\n Valor del nodo y su respectivo hermano: \n\n";
						hermanos_cNodo = avl.hnosCadaNodo();
						for (int j = 0; j < hermanos_cNodo.size(); j++) {
							informacion = informacion + hermanos_cNodo.get(j)+"\n";
						}
						
						informacion = informacion + "\n\n Nivel     Elemento: \n";
						elem_Nivel = avl.imprimirNiveles();
						cont_elem = avl.contadorNiveles();
						for (int i = 0; i < cont_elem.size(); i++) {
							informacion = informacion + cont_elem.get(i)+"\n";
						}
						informacion = informacion + "\n";
						for (int i = 0; i < elem_Nivel.size(); i++) {
							informacion = informacion + elem_Nivel.get(i)+"\n";
						}							
						
						informacion = informacion + "\nLos nodos hoja del arbol son: \n";
						nodos_Hoja = avl.NodosHoja();
						for (int j2 = 0; j2 < nodos_Hoja.size() ; j2++) {
							informacion = informacion + nodos_Hoja.get(j2)+", ";
						}
						jTar_Informacion.setText(informacion);
						 avl.sacarBasura();
						}}
						
					}
				});
			}
			{
				Btn_Limpiar = new JButton();
				getContentPane().add(Btn_Limpiar);
				Btn_Limpiar.setText("Limpiar ");
				Btn_Limpiar.setBounds(344, 406, 107, 21);
				Btn_Limpiar.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						if(jRbtn_ABB.isSelected() || jRbtn_AVL.isSelected()){
						if(jRbtn_ABB.isSelected()){
							if(abb.getRaiz() != null){
								int opc = JOptionPane.showConfirmDialog(null, "�En realidad desea eliminar el arbol?");
								if(JOptionPane.OK_OPTION == opc){
									abb.eliminarArbol();
									JOptionPane.showMessageDialog(null, "El Arbol ABB ha sido Eliminado de la \nMemoria del Programa", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
									jRbtn_ABB.setEnabled(true);
									jRbtn_AVL.setEnabled(true);
									jRbtn_ABB.setSelected(false);
									jRbtn_AVL.setSelected(false);
									Btn_Informacion.setEnabled(false);
									Btn_abrirArchivo.setEnabled(false);
									jBtn_Generar.setEnabled(false);
									jTar_Informacion.setText("");
									
									jDesktopPane1.setVisible(false);
									
									mostrar=0;
								
								}
							}else{
							JOptionPane.showMessageDialog(null, "No Existen Elementos para Eliminar\nEn el Arbol ABB", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
							}
						
							
						}
						if(jRbtn_AVL.isSelected()){
							if(avl.getRaiz() != null){
								int opc = JOptionPane.showConfirmDialog(null, "�En realidad desea eliminar el arbol?");
								if(JOptionPane.OK_OPTION == opc){
									//eliminar arbol
									avl.eliminarArbol();
									JOptionPane.showMessageDialog(null, "El Arbol AVL ha sido Eliminado de la \nMemoria del Programa", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
									jRbtn_ABB.setEnabled(true);
									jRbtn_AVL.setEnabled(true);
									jRbtn_ABB.setSelected(false);
									jRbtn_AVL.setSelected(false);
									Btn_Informacion.setEnabled(false);
									Btn_abrirArchivo.setEnabled(false);
									jBtn_Generar.setEnabled(false);
									jTar_Informacion.setText("");
									
									jDesktopPane1.setVisible(false);
									
									mostrar=0;
								
								}
							}else{
								JOptionPane.showMessageDialog(null, "No Existen Elementos para Eliminar\nEn el Arbol AVL", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
								}
						}
					}else
						JOptionPane.showMessageDialog(null, "Aun no ha Creado ningun Arbol", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
				}});
			}
			{
				Lbl_Recoriidos = new JLabel();
				getContentPane().add(Lbl_Recoriidos);
				Lbl_Recoriidos.setText("Recorridos: ");
				Lbl_Recoriidos.setBounds(640, 433, 85, 20);
			}
			{
				TFD_Datos = new JTextField();
				getContentPane().add(TFD_Datos);
				TFD_Datos.setBounds(111, 418, 76, 26);
			}
			{
				Btn_abrirArchivo = new JButton();
				getContentPane().add(Btn_abrirArchivo);
				Btn_abrirArchivo.setText("Abrir Archivo");
				Btn_abrirArchivo.setBounds(462, 406, 144, 21);
				Btn_abrirArchivo.setEnabled(false);
				Btn_abrirArchivo.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						if(nombreArchivo != ""){
							abrirArchivo(nombreArchivo+".txt");
						}
						
					}
				});
			}
			{
				jTar_Informacion = new JTextArea();
				getContentPane().add(jTar_Informacion);
				jTar_Informacion.setBounds(520, 12, 283, 374);
				scrll_Datos = new JScrollPane(jTar_Informacion);
				getContentPane().add(scrll_Datos);
				scrll_Datos.setBounds(520, 12, 283, 374);
				jTar_Informacion.setEditable(false);
				jTar_Informacion.setPreferredSize(new java.awt.Dimension(275, 286));
			}
			{
				jButton1 = new JButton();
				getContentPane().add(jButton1);
				getContentPane().add(getJDesktopPane1());
				jButton1.setText("Mostrar_ABB");
				jButton1.setBounds(217, 372, 112, 23);
				jButton1.setAction(getAbstractAction1());
			}
		/*	{
				jButton2 = new JButton();
				getContentPane().add(jButton2);
				jButton2.setText("Mostrar_AVL");
				jButton2.setBounds(356, 364, 106, 23);
				jButton2.setAction(getAbstractAction2());
			}*/
			pack();
			this.setSize(827, 505);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	private  boolean Numerico(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	
	public static void abrirArchivo(String nombre){

		try {
		    File file = new File(nombre);
		    Desktop.getDesktop().open(file);
		} catch(Exception e) {
		    e.printStackTrace();
		}
	}
	
	private AbstractAction getAbstractAction1() {
		if(abstractAction1 == null) {
			abstractAction1 = new AbstractAction("Mostrar_Arbol", null) {
				public void actionPerformed(ActionEvent evt) {
					if (jRbtn_ABB.isSelected()) {
						jDesktopPane1.setVisible(true);
					ABB_Grafico dg=new ABB_Grafico(abb);
					dg.setResizable(true);
					///**
					//int x = jDesktopPane1.getWidth()/2;
					//int y = jDesktopPane1.getHeight()/2;
					dg.setLocation(1, 1);
					//dg.setBounds(12, 76, 494, 282);
					//**
					dg.setSize(500, 400);
					Dimension dlgSize = dg.getPreferredSize(); 
    				Dimension pantalla = getSize(); 
    				Dimension ventana = dg.getSize() ; 
    				//dg.setLocation((pantalla.width - dg.WIDTH ) / 2 , 
    					//	(pantalla.height - dg.HEIGHT) / 2); 
    			//	dg.setLocationRelativeTo(null) ; // CENTRA EL SEGUNDO FRAME EN LA PANTA 
    				//dg.pack() ; 
    				//dg.setResizable(true) ; // PERMITE REDIMENSIONARLO O AGRANDARL 
    				jDesktopPane1.add(dg);
    				
    				
    				dg.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
    				

    
					}
					if (jRbtn_AVL.isSelected()){
						
						jDesktopPane1.setVisible(true);
						AVL_Grafico dg2=new AVL_Grafico(avl);
						
						
						dg2.setResizable(true);
						///**
						//int x = jDesktopPane1.getWidth()/2;
						//int y = jDesktopPane1.getHeight()/2;
						dg2.setLocation(1, 1);
						//dg.setBounds(12, 76, 494, 282);
						//**
						dg2.setSize(500, 400);
						Dimension dlgSize = dg2.getPreferredSize(); 
	    				Dimension pantalla = getSize(); 
	    				Dimension ventana = dg2.getSize() ; 
	    				//dg.setLocation((pantalla.width - dg.WIDTH ) / 2 , 
	    					//	(pantalla.height - dg.HEIGHT) / 2); 
	    			//	dg.setLocationRelativeTo(null) ; // CENTRA EL SEGUNDO FRAME EN LA PANTA 
	    				//dg.pack() ; 
	    				//dg.setResizable(true) ; // PERMITE REDIMENSIONARLO O AGRANDARL 
	    				jDesktopPane1.add(dg2);
	    				dg2.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME.. 
						
					}
				}
			};
		}
		return abstractAction1;
	}
	
	private JDesktopPane getJDesktopPane1() {
		if(jDesktopPane1 == null) {
			jDesktopPane1 = new JDesktopPane();
			jDesktopPane1.setBounds(12, 12, 494, 354);
			
			//JScrollPane scroll=new JScrollPane(jDesktopPane1);
			//getContentPane().add(scroll);
			///scroll.setBounds(12, 12, 494, 354);
			//jDesktopPane1.setResizable(true);
			
		}
		return jDesktopPane1;
	}

	/*
	private AbstractAction getAbstractAction2() {
		if(abstractAction2 == null) {
			abstractAction2 = new AbstractAction("Mostrar_AVL", null) {
				public void actionPerformed(ActionEvent evt) {
					AVL_Grafico dg2=new AVL_Grafico(avl);
					dg2.setSize(275, 286);
					Dimension dlgSize = dg2.getPreferredSize(); 
    				Dimension pantalla = getSize(); 
    				Dimension ventana = dg2.getSize() ; 
    				dg2.setLocation((pantalla.width - dg2.WIDTH ) / 2 , 
    						(pantalla.height - dg2.HEIGHT) / 2); 
    				dg2.setLocationRelativeTo(null) ; // CENTRA EL SEGUNDO FRAME EN LA PANTA 
    				dg2.pack() ; 
    				dg2.setResizable(true) ; // PERMITE REDIMENSIONARLO O AGRANDARL 
    				dg2.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME.. 
				}
			};
		}
		return abstractAction2;
	}*/

}