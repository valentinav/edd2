package Interfaz_Arboles;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import Logica_Negocio.Arbol_ABB;
import Logica_Negocio.Arbol_AVL;
import Logica_Negocio.Nodo_ABB;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class AVL_Grafico extends JInternalFrame  {

	/**
	 * @param args
	 */
	
	public JInternalFrame contenedor;
	private static Arbol_AVL raiz;

	public AVL_Grafico(Arbol_AVL avl) {
		raiz = avl;
	}
	public void prueba2 (){
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(100,100, 450, 300);
		contenedor = new JInternalFrame ();
		setContentPane(contenedor);
		contenedor.setLayout(null);
		contenedor.setBounds(100,100, 450,300);
		contenedor.setSize(300, 300);

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				 try {
					AVL_Grafico pr = new AVL_Grafico(raiz);
	                    pr.setVisible(true);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
			}
		});
	}
	
	
	public void paint (Graphics g){
		super.paint(g);
		raiz.raiz.dibuja(g, raiz.raiz,242,50,242,50,110);
		 
	//original/	raiz.raiz.dibuja(g, raiz.raiz,200,30,200,30,100);
	 
	}

}
