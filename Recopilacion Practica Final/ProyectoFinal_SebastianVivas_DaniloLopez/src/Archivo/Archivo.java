package Archivo;

/*Clase que permite escribir en un archivo de texto*/

//Importamos clases que se usaran
import java.awt.geom.CubicCurve2D;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import Logica_Negocio.Informacion;

public class Archivo {
	
	private ArrayList<Integer> sub_Izq ;
	private ArrayList<Integer> sub_Der ;
	private ArrayList<Integer> Grado_cNodo;
	private ArrayList<Integer> nodosInteriores ;
	private ArrayList<Integer> hijos_cNodo;
	private ArrayList<Integer> hermanos_cNodo ;
	private ArrayList<Integer> elem_Nivel;
	private ArrayList<Integer> nodos_Hoja;
	
	private String[] aux;

	public void escribirArchivo(String nombreArchivo, String info) {
		
		try {
			
			// Crear un objeto File se encarga de crear o abrir acceso a un
			// archivo que se especifica en su constructor
			File archivo = new File(nombreArchivo+".txt");

			// Creamos un objeto FileWriter el cual nos ayudara a escribir en el archivo
			// true para q escriba despues de la ultima linea
			FileWriter escribir = new FileWriter(archivo, true);

			// Escribimos en el archivo con ayuda del metodo write
			escribir.write(info);
	
			
			// Cerramos la conexion
			escribir.close();
		}

		// Si existe un problema al escribir cae aqui
		catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Ha ocurrido un problema Escribiendo en el archivo\nOperacion no completada","Estado del Programa",JOptionPane.ERROR_MESSAGE);

		}
	}
	
	

}
