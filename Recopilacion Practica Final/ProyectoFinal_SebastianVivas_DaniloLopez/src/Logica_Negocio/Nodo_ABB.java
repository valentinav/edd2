package Logica_Negocio;

import java.awt.Color;
import java.awt.Graphics;


public class Nodo_ABB {//inicio de la definici�n de la clase del nodo del �rbol
	public int elemento;
	public Nodo_ABB izq, der;
	public int grado = 0;
	
	public Nodo_ABB(int elemento) {
		this.elemento = elemento;
		this.izq = null;
		this.der = null;
	}
	
	public Nodo_ABB insertarNodo (Nodo_ABB n, int elemento) {
		if (n == null) {
			return new Nodo_ABB(elemento);
		} else if (elemento < n.elemento) {
			n.izq = insertarNodo(n.izq, elemento);
		} else if (elemento > n.elemento) {
			n.der = insertarNodo(n.der, elemento);
		}
		return n;
	}	
	
	public void dibujar(Graphics g,Nodo_ABB  raiz, int x,int y, int px, int py, int desplazamiento)
	 {
	 	if (raiz != null)
	 	{
	 		g.setColor(new Color(5,5,5));
	 		g.drawLine(x+10,y+10,px+10,py+18);
	 		g.setColor(new Color(255,255,0));
	 		g.fillOval(x,y,20,20);
	 		g.setColor(new Color(5,5,5));
	 	    g.drawString(String.valueOf(raiz.elemento),x+5,y+15);
	     	g.drawOval(x,y,20,20);

	 		dibujar(g,raiz.izq,x-desplazamiento,y+30,x,y,desplazamiento/2);
	 		dibujar(g,raiz.der,x+desplazamiento,y+30,x,y,desplazamiento/2);

	 	}
	 }
}