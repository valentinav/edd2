package Logica_Negocio;

import java.util.ArrayList;

public class Informacion {
	
	private int Raiz;
	private int Altura;
	private int numero_Nodos;
	private ArrayList<Integer> sub_Izq = new ArrayList<Integer>();
	private ArrayList<Integer> sub_Der = new ArrayList<Integer>();
	private ArrayList<String> Grado_cNodo = new ArrayList<String>();
	private ArrayList<Integer> nodosInteriores = new ArrayList<Integer>();
	private ArrayList<String> hijos_cNodo = new ArrayList<String>();
	private ArrayList<String> hermanos_cNodo = new ArrayList<String>();
	private ArrayList<String> elem_Nivel = new ArrayList<String>();
	private ArrayList<Integer> nodos_Hoja = new ArrayList<Integer>();
	private ArrayList<String> cont_elem = new ArrayList<String>();
	
	public Informacion(int raiz, int altura, int numero_Nodos, ArrayList<Integer> sub_Izq,
			ArrayList<Integer> sub_Der, ArrayList<String> grado_cNodo, ArrayList<Integer> nodosInteriores,
			ArrayList<String> hijos_cNodo, ArrayList<String> hermanos_cNodo, ArrayList<String> elem_Nivel,
			ArrayList<Integer> nodos_Hoja) {
		
		Raiz = raiz;
		Altura = altura;
		this.numero_Nodos = numero_Nodos;
		this.sub_Izq = sub_Izq;
		this.sub_Der = sub_Der;
		Grado_cNodo = grado_cNodo;
		this.nodosInteriores = nodosInteriores;
		this.hijos_cNodo = hijos_cNodo;
		this.hermanos_cNodo = hermanos_cNodo;
		this.elem_Nivel = elem_Nivel;
		this.nodos_Hoja = nodos_Hoja;
	}

	public int getRaiz() {
		return Raiz;
	}

	public void setRaiz(int raiz) {
		Raiz = raiz;
	}

	public int getAltura() {
		return Altura;
	}

	public void setAltura(int altura) {
		Altura = altura;
	}

	public int getNumero_Nodos() {
		return numero_Nodos;
	}

	public void setNumero_Nodos(int numero_Nodos) {
		this.numero_Nodos = numero_Nodos;
	}

	public ArrayList<Integer> getSub_Izq() {
		return sub_Izq;
	}

	public void setSub_Izq(ArrayList<Integer> sub_Izq) {
		this.sub_Izq = sub_Izq;
	}

	public ArrayList<Integer> getSub_Der() {
		return sub_Der;
	}

	public void setSub_Der(ArrayList<Integer> sub_Der) {
		this.sub_Der = sub_Der;
	}

	public ArrayList<String> getGrado_cNodo() {
		return Grado_cNodo;
	}

	public void setGrado_cNodo(ArrayList<String> grado_cNodo) {
		Grado_cNodo = grado_cNodo;
	}

	public ArrayList<Integer> getNodosInteriores() {
		return nodosInteriores;
	}

	public void setNodosInteriores(ArrayList<Integer> nodosInteriores) {
		this.nodosInteriores = nodosInteriores;
	}

	public ArrayList<String> getHijos_cNodo() {
		return hijos_cNodo;
	}

	public void setHijos_cNodo(ArrayList<String> hijos_cNodo) {
		this.hijos_cNodo = hijos_cNodo;
	}

	public ArrayList<String> getHermanos_cNodo() {
		return hermanos_cNodo;
	}

	public void setHermanos_cNodo(ArrayList<String> hermanos_cNodo) {
		this.hermanos_cNodo = hermanos_cNodo;
	}

	public ArrayList<String> getElem_Nivel() {
		return elem_Nivel;
	}

	public void setElem_Nivel(ArrayList<String> elem_Nivel) {
		this.elem_Nivel = elem_Nivel;
	}

	public ArrayList<Integer> getNodos_Hoja() {
		return nodos_Hoja;
	}

	public void setNodos_Hoja(ArrayList<Integer> nodos_Hoja) {
		this.nodos_Hoja = nodos_Hoja;
	}
	
	
	
	
	
}
