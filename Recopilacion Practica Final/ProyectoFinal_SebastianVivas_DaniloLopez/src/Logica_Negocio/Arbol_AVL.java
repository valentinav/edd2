package Logica_Negocio;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Arbol_AVL { //Inicio de la clase �rbol
	//Atributos
	public Nodo_AVL  raiz;
	private ArrayList<Integer> preorden = new ArrayList();
	private ArrayList<Integer> inorden = new ArrayList();
	private ArrayList<Integer> postorden = new ArrayList();
	
	private ArrayList<Integer> sub_Izq = new ArrayList<Integer>();
	private ArrayList<Integer> sub_Der = new ArrayList<Integer>();
	private ArrayList<String> Grado_cNodo = new ArrayList<String>();
	private ArrayList<Integer> nodosInteriores = new ArrayList<Integer>();
	private ArrayList<String> hijos_cNodo = new ArrayList<String>();
	private ArrayList<String> hermanos_cNodo = new ArrayList<String>();
	private ArrayList<String> elem_Nivel = new ArrayList<String>();
	private ArrayList<Integer> nodos_Hoja = new ArrayList<Integer>();
	private ArrayList<String> cont_elem = new ArrayList<String>();
	
	private int altura = 0;
	private int num_nodos = 0;
	private String info ;
	private int cont=0;
	private int suma=0;
	private int nv=0;
	private int cont_n=0;
	
	//Constructor
	public Arbol_AVL() {
		this.raiz = null;
	}
	
	//retornamos el valor de la raiz
	public Nodo_AVL getRaiz() {
			return raiz;
	}
	
//**************************************************************************************
	//metodo utilizado para retornar el valor del elemento de un nodo
	private Object elementoNodo(Nodo_AVL  arbol )
	{
		if (arbol==null)
			return null;
		else
			return arbol.elemento;
	}

//**************************************************************************************
	//insercion de un nuevo elemento al arbol
	public void insertar(int elemento ) {
		raiz = insertar(raiz,elemento );
	}
	
	private Nodo_AVL  insertar(Nodo_AVL arbol,int elemento) {
		if ( arbol == null )
			arbol = new Nodo_AVL (elemento, null, null);
		else if (elemento < arbol.elemento)
		{
			arbol.izq = insertar(arbol.izq,elemento);
			if (factorEquilibrio(arbol.izq) - factorEquilibrio(arbol.der) == 2 )
				if (elemento < arbol.izq.elemento  )
					arbol = RotacionSimpleIzq(arbol);
				else
					arbol = RotacionDobleIzq_Der(arbol);
		}
		else if (elemento > arbol.elemento )
		{
			arbol.der = insertar(arbol.der,elemento);
			if( factorEquilibrio(arbol.der) - factorEquilibrio(arbol.izq) == 2)
				if(elemento > arbol.der.elemento)
					arbol = RotacionSimpleDer(arbol);
				else
					arbol = RotacionDobleDer_Izq(arbol);
		}
		else
			JOptionPane.showMessageDialog(null, "Elemento no Ingresado\nYa se encontraba en el Arbol...", "Estado del Programa", JOptionPane.INFORMATION_MESSAGE);
		// En el caso de los Duplicados informa el estado
		arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.izq),factorEquilibrio(arbol.der)) + 1;
		return arbol;	
	}
	
	private static int factorEquilibrio(Nodo_AVL  arbol){
		if (arbol==null)
			return -1;
		else
			return arbol.factorEquilibrio;
	}

	private static int maxFB( int alturaIzquierdo, int alturaDerecho ) {
		if (alturaIzquierdo > alturaDerecho)
			return alturaIzquierdo;
		else
			return alturaDerecho;
	}
	
//**************************************************************************************
	//Eliminar un nodo
	public boolean eliminarNodo(int elementoEliminar){
		return eliminarNodo(this.raiz,elementoEliminar);
	}
	
	private boolean eliminarNodo(Nodo_AVL arbol, int elementoEliminar){
		Nodo_AVL padre = raiz;
		boolean hijoIzquierdo = true;
		//Iniciamos la b�squeda del elemento y su ubicaci�n
		while (arbol.elemento != elementoEliminar){
			padre = arbol; // creamos al padre
			if (elementoEliminar < arbol.elemento){ // caso en el que es un nodo izquierdo
				hijoIzquierdo = true;
				arbol = arbol.izq;
			}
			else{ // caso en el que es un nodo derecho
				hijoIzquierdo = false;
				arbol = arbol.der;
			}
			if (arbol == null) // caso cuando no est� el elemento a eliminar y no se
				return false; // puede realizar la eliminaci�n retornar� falso
		}
		//finalizaci�n del ciclo while

		if (arbol.izq==null && arbol.der==null){
			if(arbol == raiz)
				raiz = null;
			else if(hijoIzquierdo==true)
				padre.izq = null;
			else
				padre.der = null;
		}
		else if(arbol.der==null)
			if(arbol == raiz)
				raiz = arbol.izq;
			else if(hijoIzquierdo==true)
				padre.izq = arbol.izq;
			else
				padre.der = arbol.izq;
		else if(arbol.izq==null)
			if(arbol == raiz)
				raiz = arbol.der;
			else if(hijoIzquierdo==true)
				padre.izq = arbol.der;
			else
				padre.der = arbol.der;
		else {
			// Buscaremos el descendiente del nodo a eliminar y se lo asignamos a un
			//objeto de tipo nodo.
			Nodo_AVL descendiente= obtenerDescendiente(arbol);
			//Enlazar al padre con su nodo descendiente
			if (arbol == raiz)
				raiz = descendiente;
			else if (hijoIzquierdo==true)
				padre.izq = descendiente;
			else
				padre.der = descendiente;
			// Finalmente conectamos al descendiente el �rbol izquierdo restante
			descendiente. izq = arbol.izq;
		} // finaliza este requerimiento 3
//		arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.izq),factorEquilibrio(arbol.der)) + 1;
		return true; // retorna verdadero si se pudo eliminar el elemento
	} //Finaliza el m�todo eliminar nodo

	private Nodo_AVL obtenerDescendiente(Nodo_AVL subArbol) {
		Nodo_AVL descendientePadre = subArbol;
		Nodo_AVL descendiente = subArbol;
		Nodo_AVL arbol = subArbol.der; // para solo dirigir la b�squeda del
		//descendiente por la derecha
		while (arbol != null){
			// Hijo a la derecha
			descendientePadre = descendiente;
			descendiente = arbol;
			arbol = arbol.izq; // Vamos asignando los nodo hacia la izquierda
		}
		// Si el descendiente es diferente al sub�rbol derecho
		if (descendiente != subArbol.der) { // Hijo a la derecha
			// Realizamos el enlace
			descendientePadre.izq = descendiente.der;
			descendiente.der = subArbol.der;
		}
		return descendiente;
	}
//**************************************************************************************
	//Rotaciones simples
	private static Nodo_AVL  RotacionSimpleIzq(Nodo_AVL  arbol) {
		Nodo_AVL subArbol = arbol.izq;
		arbol.izq = subArbol.der;
		subArbol.der = arbol;
		arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.izq),factorEquilibrio(arbol.der)) + 1;
		subArbol.factorEquilibrio = maxFB(factorEquilibrio( subArbol.izq ), arbol.factorEquilibrio) + 1;
		return subArbol;
	}
	private static Nodo_AVL RotacionSimpleDer(Nodo_AVL  subArbol){
		Nodo_AVL arbol = subArbol.der;
		subArbol.der = arbol.izq;
		arbol.izq = subArbol;
		subArbol.factorEquilibrio = maxFB(factorEquilibrio(subArbol.izq),factorEquilibrio(subArbol.der)) + 1;
		arbol.factorEquilibrio = maxFB(factorEquilibrio(arbol.der),subArbol.factorEquilibrio) + 1;
		return arbol;
	}
//**************************************************************************************
	//Rotaciones dobles
	private static Nodo_AVL RotacionDobleIzq_Der(Nodo_AVL  arbol)
	{
		arbol.izq = RotacionSimpleDer( arbol.izq );
		return RotacionSimpleIzq( arbol );
	}
	private static Nodo_AVL RotacionDobleDer_Izq(Nodo_AVL subArbol)
	{
		subArbol.der = RotacionSimpleIzq(subArbol.der);
		return RotacionSimpleDer(subArbol);
	}
	
//**************************************************************************************
	//metodo para calcular la altura del arbol
	public int profundidad() {
		calcularAltura(this.raiz, 0);
		return altura;
	}
	private void calcularAltura(Nodo_AVL arbol, int nivel) {
		if (arbol != null) {
			calcularAltura(arbol.izq, nivel + 1);
			if (nivel > altura)
				altura = nivel + 1;
			calcularAltura(arbol.der, nivel + 1);
		}
	}
//*************************************************************************************
	//dvolver el numero de nodos del arbol
	public int numero_nodos() {
		num_nodos(this.raiz);
		return num_nodos;
	}
	private void num_nodos(Nodo_AVL arbol) {
		if (arbol != null) {
			num_nodos = num_nodos+1;
			num_nodos(arbol.izq);
			num_nodos(arbol.der);
		}
	}
//*************************************************************************************
	//enviar desde el main el respectivo subarbol
	//retorna un array list con los nodos de cada subarbol
	//implementar metodo para limpiar los arrays depues d q se envien...
	public ArrayList<Integer> subArbolDerecho(Nodo_AVL der) {
		if(der != null){
		sub_Der.add(der.elemento);
		subArbolDerecho(der.izq);
		subArbolDerecho(der.der);
		}
		return sub_Der;
	}
	public ArrayList<Integer> subArbolIzquierdo(Nodo_AVL izq) {
		if(izq != null){
		sub_Izq.add(izq.elemento);
		subArbolIzquierdo(izq.izq);
		subArbolIzquierdo(izq.der);
		}
		return sub_Izq;
	}
//*********************************************************************************
	//grado de cada nodo 
	//falta relaizar metodo para limpiar el array list
	private void calcularGrado(Nodo_AVL raiz){
		if (raiz != null) {
			if (raiz.der != null && raiz.izq != null) {
				raiz.grado = 2;
			} else {
				if (raiz.izq != null || raiz.der != null) {
					raiz.grado = 1;
				} else {
					if (raiz.der == null && raiz.izq == null) {
						raiz.grado = 0;
					}
				}
			}
			calcularGrado(raiz.izq);
			calcularGrado(raiz.der);
		}
		
	}
	public ArrayList<String> gradoCadaNodo(){
		calcularGrado(this.raiz);
		gradoCadaNodo(this.raiz);
		return Grado_cNodo;
	}
	private void gradoCadaNodo(Nodo_AVL raiz){
		if(raiz != null){
			Grado_cNodo.add("El Grado del nodo "+raiz.elemento +" es:  "+raiz.grado);
			gradoCadaNodo(raiz.der);
			gradoCadaNodo(raiz.izq);
		}
	}
//*************************************************************************************
	//metodo para retornar los nodos interiores
	//flata poner an limpio el array despues de enviado
	public ArrayList<Integer> retornarNodosInterirores(){
		retornar(this.raiz);
		return nodosInteriores;
	}
	private void retornar(Nodo_AVL raiz) {
		if (raiz != null) {
			if ((raiz.izq != null) || (raiz.der != null)) {
				if (raiz.elemento != this.raiz.elemento) {
					nodosInteriores.add(raiz.elemento);
				}
				if (raiz.izq != null)
					retornar(raiz.izq);
				if (raiz.der != null)
					retornar(raiz.der);
			}
		}
	}
//*************************************************************************************
	//Hijos cada nodo
	//hay q hacer metodo para limpiar el array despues de q se envie...
	public ArrayList<String> hijosCadaNodo(){
		hijosPrimerNodo(this.raiz);
		return hijos_cNodo;
	}
	private void hijosPrimerNodo(Nodo_AVL raiz) {
		if (raiz != null) {
			info = ("Los hijos del nodo  " + raiz.elemento + " son:   ");
			info =  hijosCadaNodo(raiz, info);
			hijos_cNodo.add(info);
			info = "";
			hijosPrimerNodo(raiz.izq);
			hijosPrimerNodo(raiz.der);
		}
	}
	private String hijosCadaNodo(Nodo_AVL raiz,String info){
		if(raiz != null){
			if(raiz.izq != null){
				//System.out.println(raiz.izq.elemento);
				info = info + raiz.izq.elemento + ", ";
				info = hijosCadaNodo(raiz.izq,info);
			}
			if (raiz.der != null) {
				//System.out.println(raiz.der.elemento);
				info = info + raiz.der.elemento + ", ";
				info = hijosCadaNodo(raiz.der,info);				
			}
			return info;	
		}
		return null;		
	}
//*************************************************************************************
	//hermanos de cada nodo 
	//falta limpiar el ArrayList
	public ArrayList<String> hnosCadaNodo(){
		hnosCadaNodo(getRaiz(),getRaiz());
		return hermanos_cNodo;
	}
	private void hnosCadaNodo(Nodo_AVL raiz,Nodo_AVL padre) {
		if (raiz != null && padre != null) {
			if (raiz != padre) {
				info = ("El hermano del nodo  " + raiz.elemento + " es: ");
				if (raiz == padre.der) {
					if(padre.izq != null){
					info = info + padre.izq.elemento;}
				} else {
					if(padre.der != null){
					info = info + padre.der.elemento;}
				}
				hermanos_cNodo.add(info);
				info = "";
				hnosCadaNodo(raiz.izq, raiz);
				hnosCadaNodo(raiz.der, raiz);
			} else {
				hnosCadaNodo(raiz.der, raiz);
				hnosCadaNodo(raiz.izq, raiz);
			}
		}
	}
//**************************************************************************************
	//metodos q nos ayudaran para imprimir los niveles y la cantidad de nodos por nivel
	//faltaria volver tooos los niveles vacios nuevamente
	public ArrayList<String> imprimirNiveles(){
		imprimirArbolNiveles(this.raiz, 0);
		return elem_Nivel;
	}
	public ArrayList<String> contadorNiveles(){
		niveles_elem();
		return cont_elem;
	}
	private void niveles_elem() {
		calcularAltura(this.raiz, 0);
		for (int i = 0; i < altura; i++) {
			nv = i;
			ArbolNiveles(this.raiz, 0);
			elem_Nivel.add("Total elementos Nivel " + i + ": " + cont_n);
			//System.out.println("Total elementos Nivel " + i + ": " + cont_n);
			cont_n = 0;
		}
	}
	private void imprimirArbolNiveles(Nodo_AVL arbol, int nivel) {
		if (arbol != null) {
			imprimirArbolNiveles(arbol.izq, nivel + 1);
			cont_elem.add(" " + nivel + "       " + arbol.elemento);
			//System.out.println(" " + nivel + "        " + arbol.elemento);
			imprimirArbolNiveles(arbol.der, nivel + 1);
		}
	}
	private void ArbolNiveles(Nodo_AVL arbol, int nivel) {
		if (arbol != null) {
			if (nivel == nv) {
				cont_n = cont_n + 1;
			}
			ArbolNiveles(arbol.izq, nivel + 1);
			ArbolNiveles(arbol.der, nivel + 1);
		}
	}
	
//*************************************************************************************
	//retornamos los nodos hoja del arbol
	public ArrayList<Integer> NodosHoja(){
		retornar_Hoja(this.raiz);
		return nodos_Hoja;
	}
	private void retornar_Hoja(Nodo_AVL raiz) {
		if (raiz != null) {
			if ((raiz.izq == null) && (raiz.der == null)) {
				nodos_Hoja.add(raiz.elemento);
			}
			if (raiz.izq != null)
				retornar_Hoja(raiz.izq);
			if (raiz.der != null)
				retornar_Hoja(raiz.der);
		}
	}
	
//*************************************************************************************
	//recorridos
	public ArrayList<Integer> imprimir_preorden() {
		preorden(this.raiz);
		return preorden;
	}
	private void preorden(Nodo_AVL arbol) {
		if (arbol != null) {
			preorden.add(arbol.elemento);
			//System.out.print(" " + arbol.elemento);
			preorden(arbol.izq);
			preorden(arbol.der);
		}
	}
	public ArrayList<Integer> imprimir_inorden() {
		inorden(this.raiz);
		return inorden;
}
	private void inorden(Nodo_AVL arbol) {
		if (arbol != null) {
			inorden(arbol.izq);
			inorden.add(arbol.elemento);
			//System.out.print(" " + arbol.elemento);
			inorden(arbol.der);
		}
	}
	public ArrayList<Integer> imprimir_postorden() {
		postorden(this.raiz);
		return postorden;
	}
	private void postorden(Nodo_AVL arbol) {
		if (arbol != null) {
			postorden(arbol.izq);
			postorden(arbol.der);
			postorden.add(arbol.elemento);
			//System.out.print(" " + arbol.elemento);
		}
	}
	//limpiar arrays recorridos
	public void limpiarArrays (){
		preorden.clear();
		inorden.clear();
		postorden.clear(); 
	}

//**************************************************************************************
	//Metodo el cual permite Eliminar el arbol por Completo
	public void eliminarArbol(){
	this.raiz = null;
	}
	
//*************************************************************************************

	
	public void primos(int primos) {
		profundidad1();
		if(this.raiz.elemento == primos)
		{
			System.out.println("El numero "+primos+" no tiene primos es la raiz");
		}
		if(altura <3)
		{
			System.out.println("El numero "+primos+" no tiene primos");
		}
		if(altura>=3)
		{
			if(this.raiz.izq.elemento==primos || this.raiz.der.elemento==primos)			
			{
				Nodo_AVL raiz1=hermanos(this.raiz);
				System.out.println("Los pirimos son"+ raiz1.izq.elemento+"y"+raiz1.der.elemento);
				
			}
		}
	}
		public Nodo_AVL hermanos(Nodo_AVL arbol){
			if(this.raiz.izq.elemento==arbol.elemento || this.raiz.der.elemento==arbol.elemento)	
			{	
			arbol=this.raiz;
			}
			return arbol;
		}
	public void profundidad1() {
		calcularAltura1(this.raiz, 0);
	//	System.out.println("La Altura del arbol es: " + altura);
	}
	private void calcularAltura1(Nodo_AVL arbol, int nivel) {
		if (arbol != null) {
			calcularAltura(arbol.izq, nivel + 1);
			if (nivel > altura)
				altura = nivel + 1;
			calcularAltura(arbol.der, nivel + 1);
		}
	}
	//metodo para inicializar todos los arrays 
	public void sacarBasura(){
		sub_Izq.clear();
		sub_Der.clear();
		Grado_cNodo.clear();
		nodosInteriores.clear();
		hijos_cNodo.clear();
		hermanos_cNodo.clear();
		elem_Nivel.clear();
		cont_elem.clear();
		nodos_Hoja.clear();
	}
	
}//Fin de la clase �rbol