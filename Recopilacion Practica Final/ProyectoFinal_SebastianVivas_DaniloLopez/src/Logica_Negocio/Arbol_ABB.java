package Logica_Negocio;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Arbol_ABB { //Inicio de la clase �rbol
	//Atributos
	public Nodo_ABB raiz;
	protected int altura=0;
	private int num_nodos=0;
	private int cont=0;
	private int suma=0;
	private int nv=0;
	private int cont_n=0;
	
	private String info;
	
	ArrayList<Integer> preorden = new ArrayList<Integer>();
	ArrayList<Integer> inorden = new ArrayList<Integer>();
	ArrayList<Integer> postorden = new ArrayList<Integer>();
	
	private ArrayList<Integer> sub_Izq = new ArrayList<Integer>();
	private ArrayList<Integer> sub_Der = new ArrayList<Integer>();
	private ArrayList<String> Grado_cNodo = new ArrayList<String>();
	private ArrayList<Integer> nodosInteriores = new ArrayList<Integer>();
	private ArrayList<String> hijos_cNodo = new ArrayList<String>();
	private ArrayList<String> hermanos_cNodo = new ArrayList<String>();
	private ArrayList<String> elem_Nivel = new ArrayList<String>();
	private ArrayList<String> cont_elem = new ArrayList<String>();
	private ArrayList<Integer> nodos_Hoja = new ArrayList<Integer>();
		
	//Constructor
	public Arbol_ABB() {
		this.raiz = null;
	}
	
//*************************************************************************************
	//insercion de elementos
	public void insertar(int elemento) 
	{
			if (this.raiz == null) {
				this.raiz = new Nodo_ABB(elemento);
		} else {
			raiz = raiz.insertarNodo(raiz, elemento);
		}
	}
	
//*************************************************************************************
	//eliminar un nodo
	public boolean eliminarNodo(int elementoEliminar){
		return eliminarNodo(this.raiz,elementoEliminar);
	}
	
	private boolean eliminarNodo(Nodo_ABB arbol, int elementoEliminar){
		Nodo_ABB padre = raiz;
		boolean hijoIzquierdo = true;
		//Iniciamos la b�squeda del elemento y su ubicaci�n
		while (arbol.elemento != elementoEliminar){
			padre = arbol; // creamos al padre
			if (elementoEliminar < arbol.elemento){ // caso en el que es un nodo izquierdo
				hijoIzquierdo = true;
				arbol = arbol.izq;
			}
			else{ // caso en el que es un nodo derecho
				hijoIzquierdo = false;
				arbol = arbol.der;
			}
			if (arbol == null) // caso cuando no est� el elemento a eliminar y no se
				return false; // puede realizar la eliminaci�n retornar� falso
		}
		//finalizaci�n del ciclo while

		if (arbol.izq==null && arbol.der==null){
			if(arbol == raiz)
				raiz = null;
			else if(hijoIzquierdo==true)
				padre.izq = null;
			else
				padre.der = null;
		}
		else if(arbol.der==null)
			if(arbol == raiz)
				raiz = arbol.izq;
			else if(hijoIzquierdo==true)
				padre.izq = arbol.izq;
			else
				padre.der = arbol.izq;
		else if(arbol.izq==null)
			if(arbol == raiz)
				raiz = arbol.der;
			else if(hijoIzquierdo==true)
				padre.izq = arbol.der;
			else
				padre.der = arbol.der;
		else {
			// Buscaremos el descendiente del nodo a eliminar y se lo asignamos a un
			//objeto de tipo nodo.
			Nodo_ABB descendiente= obtenerDescendiente(arbol);
			//Enlazar al padre con su nodo descendiente
			if (arbol == raiz)
				raiz = descendiente;
			else if (hijoIzquierdo==true)
				padre.izq = descendiente;
			else
				padre.der = descendiente;
			// Finalmente conectamos al descendiente el �rbol izquierdo restante
			descendiente. izq = arbol.izq;
		} // finaliza este requerimiento 3
		return true; // retorna verdadero si se pudo eliminar el elemento
	} //Finaliza el m�todo eliminar nodo

	private Nodo_ABB obtenerDescendiente(Nodo_ABB subArbol) {
		Nodo_ABB descendientePadre = subArbol;
		Nodo_ABB descendiente = subArbol;
		Nodo_ABB arbol = subArbol.der; // para solo dirigir la b�squeda del
		//descendiente por la derecha
		while (arbol != null){
			// Hijo a la derecha
			descendientePadre = descendiente;
			descendiente = arbol;
			arbol = arbol.izq; // Vamos asignando los nodo hacia la izquierda
		}
		// Si el descendiente es diferente al sub�rbol derecho
		if (descendiente != subArbol.der) { // Hijo a la derecha
			// Realizamos el enlace
			descendientePadre.izq = descendiente.der;
			descendiente.der = subArbol.der;
		}
		return descendiente;
	}
	
//*************************************************************************************
	//obtener la raiz del arbol
	public Nodo_ABB getRaiz(){
		return raiz;
	}
	
//*************************************************************************************
	//calcular la profundidad del arbol
	public int profundidad() {
		calcularAltura(this.raiz, 0);
		return altura;
	}
	private void calcularAltura(Nodo_ABB arbol, int nivel) {
		if (arbol != null) {
			calcularAltura(arbol.izq, nivel + 1);
			if (nivel > altura)
				altura = nivel + 1;
			calcularAltura(arbol.der, nivel + 1);
		}
	}
	
//*************************************************************************************
	public int numero_nodos() {
		num_nodos(this.raiz);
		return num_nodos;
	}
	private void num_nodos(Nodo_ABB arbol) {
		if (arbol != null) {
			num_nodos=num_nodos+1;
			num_nodos(arbol.izq);
			num_nodos(arbol.der);
		}
	}
	
//*************************************************************************************
	//enviar desde el main el respectivo subarbol
	//retorna un array list con los nodos de cada subarbol
	//implementar metodo para limpiar los arrays depues d q se envien...
	public ArrayList<Integer> subArbolDerecho(Nodo_ABB der) {
		if(der != null){
		sub_Der.add(der.elemento);
		subArbolDerecho(der.izq);
		subArbolDerecho(der.der);
		}
		return sub_Der;
	}
	public ArrayList<Integer> subArbolIzquierdo(Nodo_ABB izq) {
		if(izq != null){
		sub_Izq.add(izq.elemento);
		subArbolIzquierdo(izq.izq);
		subArbolIzquierdo(izq.der);
		}
		return sub_Izq;
	}
	
//*********************************************************************************
	//genera el grado de cada nodo
	//se envia la raiz desde el main
	private void calcularGrado(Nodo_ABB raiz){
		if (raiz != null) {
			if (raiz.der != null && raiz.izq != null) {
				raiz.grado = 2;
			} else {
				if (raiz.izq != null || raiz.der != null) {
					raiz.grado = 1;
				} else {
					if (raiz.der == null && raiz.izq == null) {
						raiz.grado = 0;
					}
				}
			}
			calcularGrado(raiz.izq);
			calcularGrado(raiz.der);
		}
	}
	//grado de cada nodo 
	//falta relaizar metodo para limpiar el array list
	public ArrayList<String> gradoCadaNodo(){
		calcularGrado(this.raiz);
		gradoCadaNodo(this.raiz);
		return Grado_cNodo;
	}
	private void gradoCadaNodo(Nodo_ABB raiz){
		if(raiz != null){
			Grado_cNodo.add("El Grado del nodo "+raiz.elemento +" es:  "+raiz.grado);
			gradoCadaNodo(raiz.izq);
			gradoCadaNodo(raiz.der);
			
		}
	}
	
//*************************************************************************************
	//metodo para retornar los nodos interiores
	//flata poner an limpio el array despues de enviado
	public ArrayList<Integer> retornarNodosInterirores(){
		retornar(this.raiz);
		return nodosInteriores;
	}
	private void retornar(Nodo_ABB raiz) {
		if (raiz != null) {
			if ((raiz.izq != null) || (raiz.der != null)) {
				if (raiz.elemento != this.raiz.elemento) {
					nodosInteriores.add(raiz.elemento);
				}
				if (raiz.izq != null)
					retornar(raiz.izq);
				if (raiz.der != null)
					retornar(raiz.der);
			}
		}
	}
	
//*************************************************************************************
	//Hijos cada nodo
	//hay q hacer metodo para limpiar el array despues de q se envie...
	public ArrayList<String> hijosCadaNodo(){
		hijosPrimerNodo(this.raiz);
		return hijos_cNodo;
	}
	private void hijosPrimerNodo(Nodo_ABB raiz) {
		if (raiz != null) {
			info = ("Los hijos del nodo  " + raiz.elemento + " son:   ");
			info =  hijosCadaNodo(raiz, info);
			hijos_cNodo.add(info);
			info = "";
			hijosPrimerNodo(raiz.izq);
			hijosPrimerNodo(raiz.der);
		}
	}
	private String hijosCadaNodo(Nodo_ABB raiz,String info){
		if(raiz != null){
			if(raiz.izq != null){
				//System.out.println(raiz.izq.elemento);
				info = info + raiz.izq.elemento + ", ";
				info = hijosCadaNodo(raiz.izq,info);
			}
			if (raiz.der != null) {
				//System.out.println(raiz.der.elemento);
				info = info + raiz.der.elemento + ", ";
				info = hijosCadaNodo(raiz.der,info);				
			}
			return info;	
		}
		return null;		
	}
	
//*************************************************************************************
	//hermanos de cada nodo 
	//falta limpiar el ArrayList
	public ArrayList<String> hnosCadaNodo(){
		hnosCadaNodo(getRaiz(),getRaiz());
		return hermanos_cNodo;
	}
	private void hnosCadaNodo(Nodo_ABB raiz,Nodo_ABB padre) {
		if (raiz != null && padre != null) {
			if (raiz != padre) {
				info = ("El hermano del nodo  " + raiz.elemento + " es: ");
				if (raiz == padre.der) {
					if(padre.izq != null){
					info = info + padre.izq.elemento;}
				} else {
					if(padre.der != null){
					info = info + padre.der.elemento;}
				}
				hermanos_cNodo.add(info);
				info = "";
				hnosCadaNodo(raiz.izq, raiz);
				hnosCadaNodo(raiz.der, raiz);
			} else {
				hnosCadaNodo(raiz.der, raiz);
				hnosCadaNodo(raiz.izq, raiz);
			}
		}
	}
	
//*************************************************************************************
	//metodos q nos ayudaran para imprimir los niveles y la cantidad de nodos por nivel
	//faltaria volver tooos los niveles vacios nuevamente
	public ArrayList<String> imprimirNiveles(){
		imprimirArbolNiveles(this.raiz, 0);
		return elem_Nivel;
	}
	public ArrayList<String> contadorNiveles(){
		niveles_elem();
		return cont_elem;
	}
	private void niveles_elem() {
		calcularAltura(this.raiz, 0);
		for (int i = 0; i < altura; i++) {
			nv = i;
			ArbolNiveles(this.raiz, 0);
			elem_Nivel.add("Total elementos Nivel " + i + ": " + cont_n);
			//System.out.println("Total elementos Nivel " + i + ": " + cont_n);
			cont_n = 0;
		}
	}
	private void imprimirArbolNiveles(Nodo_ABB arbol, int nivel) {
		if (arbol != null) {
			imprimirArbolNiveles(arbol.izq, nivel + 1);
			cont_elem.add(" " + nivel + "       " + arbol.elemento);
			//System.out.println(" " + nivel + "        " + arbol.elemento);
			imprimirArbolNiveles(arbol.der, nivel + 1);
		}
	}
	private void ArbolNiveles(Nodo_ABB arbol, int nivel) {
		if (arbol != null) {
			if (nivel == nv) {
				cont_n = cont_n + 1;
			}
			ArbolNiveles(arbol.izq, nivel + 1);
			ArbolNiveles(arbol.der, nivel + 1);
		}
	}
	
//*************************************************************************************
	//retornamos los nodos hoja del arbol
	public ArrayList<Integer> NodosHoja(){
		retornar_Hoja(this.raiz);
		return nodos_Hoja;
	}
	private void retornar_Hoja(Nodo_ABB raiz) {
		if (raiz != null) {
			if ((raiz.izq == null) && (raiz.der == null)) {
				nodos_Hoja.add(raiz.elemento);
			}
			if (raiz.izq != null)
				retornar_Hoja(raiz.izq);
			if (raiz.der != null)
				retornar_Hoja(raiz.der);
		}
	}
	
//*************************************************************************************
    //recorridos
	public ArrayList<Integer> imprimir_preorden() {
		preorden(this.raiz);
		return preorden;
	}
	private void preorden(Nodo_ABB arbol) {
		if (arbol != null) {
			preorden.add(arbol.elemento);
			//System.out.print(" " + arbol.elemento);
			preorden(arbol.izq);
			preorden(arbol.der);
		}
	}
	public ArrayList<Integer> imprimir_inorden() {
		inorden(this.raiz);
		return inorden;
}
	private void inorden(Nodo_ABB arbol) {
		if (arbol != null) {
			inorden(arbol.izq);
			inorden.add(arbol.elemento);
			//System.out.print(" " + arbol.elemento);
			inorden(arbol.der);
		}
	}
	public ArrayList<Integer> imprimir_postorden() {
		postorden(this.raiz);
		return postorden;
	}
	private void postorden(Nodo_ABB arbol) {
		if (arbol != null) {
			postorden(arbol.izq);
			postorden(arbol.der);
			postorden.add(arbol.elemento);
			//System.out.print(" " + arbol.elemento);
		}
	}
	//metodo que elimina el contenido de los arrays
	public void limpiarArrays (){
		preorden.clear();
		inorden.clear();
		postorden.clear();
	}
	
//*************************************************************************************
	//metodo utilizado para eliminar el arbol
	public void eliminarArbol(){
		this.raiz = null;
	}	

//*************************************************************************************
	public boolean hoja(Nodo_ABB arbol, int elemento){
		if (arbol!=null) {
			if (arbol.elemento==elemento){
				if(arbol.izq == null && arbol.der == null)
					return true;
				else
					return false;
			}
			else{
				return hoja(arbol.izq, elemento) || hoja(arbol.der,
						elemento);
			}
		}
		return false;
	} 
//*************************************************************************************
	//metodo para inicializar todos los arrays 
	public void sacarBasura(){
		sub_Izq.clear();
		sub_Der.clear();
		Grado_cNodo.clear();
		nodosInteriores.clear();
		hijos_cNodo.clear();
		hermanos_cNodo.clear();
		elem_Nivel.clear();
		cont_elem.clear();
		nodos_Hoja.clear();
		altura = 0;
		num_nodos = 0;
	}
	
}//Fin de la clase �rbol	