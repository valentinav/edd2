package logica;

public class Arbol<T> {
	private Nodo raiz;
	private String datosTemporales="";
	private int contador=0;
	private int altura=0;
	private int cantNodos=0;
	private int cantHijos=0;
	private String nodosExternos="";
	private int cantHojas=0;
	private String hojas="";
	public T papa;
	public int hijo;
	public Arbol ()
	{
		this.raiz=null;
	}
	public Nodo getRaiz() {
		return raiz;
	}
	public void setRaiz(Nodo raiz) {
		this.raiz = raiz;
	}
	/**
	 *TODO NO FUNCIONA 
   	* @param elemento elemento dentro del arbol  a los que busca sus hijos
	 * @return  la cantidad de hijos
	 */
	public int cantHijos(T elemento)
	{
		hijo=0;
		cantHijos(elemento,getRaiz());
		return hijo;
	}
	
	private void cantHijos(T elemento, Nodo arbol)
	{
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string")){
			
			String elementoCadena= (String.valueOf(elemento));
			if(arbol==null){
			}		
			else if(elementoCadena.compareTo(String.valueOf(arbol.getElemento()))<0){
			    buscarElementoCad(elemento, arbol.getIzquierdo());
			} else if(elementoCadena.compareTo(String.valueOf(arbol.getElemento()))>0){
			   buscarElementoCad(elemento, arbol.getDerecho());
			}else
			{
				if (arbol.getIzquierdo()!=null)
				{
					hijo++;
				}
				if (arbol.getDerecho()!=null)
				{
					hijo++;
				}
			}
		}else if (elemento.getClass().getSimpleName().toLowerCase().equals("integer"))			
		{
			if(arbol==null){
			}else if((int)elemento<(int)arbol.getElemento()){
			    buscarElementoInt(elemento, arbol.getIzquierdo());
			} else if((int)elemento>(int)arbol.getElemento()){
			   buscarElementoInt(elemento, arbol.getDerecho());
			}else
			{
				if (arbol.getIzquierdo()!=null)
				{
					hijo++;
				}
				if (arbol.getDerecho()!=null)
				{
					hijo++;
				}
			}
		}	
	}
	public void insertar(T elemento){
		if (getRaiz()==null)
			raiz = new Nodo (elemento);
		else
			raiz =getRaiz().insertarNodo(getRaiz(),elemento);
	}
	
	public String preorden(Nodo arbol)
	{
		if(arbol!= null)
		{
			//System.out.print(" "+arbol.getElemento());
			setTemporal((" "+arbol.getElemento()),"");
			preorden(arbol.getIzquierdo());
			preorden(arbol.getDerecho());
		}
		return this.getTemporal();
	}
	public String  preorden()
	{
		inicializaTemporal();
		return preorden(this.getRaiz());
	}
	public String[][] hermano( )
	{
		String[][]matrizHermano=new String[this.cantidadNodos()][2]; 
		for (int i=0 ;i<this.cantidadNodos();i++)
		{
			for (int j=0 ;j<2;j++)
			{
				matrizHermano[i][j]="No Tiene";
			}
		}
		matrizHermano[0][0]=this.getRaiz().getElemento()+"";
		matrizHermano[0][1]="la raiz no tiene hermanos";
		return hermano(this.getRaiz(),matrizHermano, this.cantidadNodos());
	}
	private String[][] hermano( Nodo arbol,String[][] matrizHermano, int cant)
	{
		if (arbol!=null)
		{
			hermano(arbol.getIzquierdo(),matrizHermano,cant);
				for (int i=0;i<cant;i++)
				{
					if((matrizHermano[i][0].equals("No Tiene"))&&(matrizHermano[i][1].equals("No Tiene")))
					{
						if (arbol.getIzquierdo()!=null)
							matrizHermano[i][0]=""+arbol.getIzquierdo().getElemento();
						if (arbol.getDerecho()!=null)
							matrizHermano[i][1]=""+arbol.getDerecho().getElemento();
						break;
					}
				}
			hermano(arbol.getDerecho(),matrizHermano,cant);
		}
		return matrizHermano ;
	}
	
	public String[][] hijosCada( )
	{
		String[][]matrizHijos=new String[this.cantidadNodos()][3]; 
		for (int i=0 ;i<this.cantidadNodos();i++)
		{
			for (int j=0 ;j<3;j++)
			{
				matrizHijos[i][j]="No Hay";
			}
		}
		return HijosCada(this.getRaiz(),matrizHijos, this.cantidadNodos());
	}
	public String[][] HijosCada( Nodo arbol,String[][] matrizHijos, int cant)
	{
		if (arbol!=null)
		{
			HijosCada(arbol.getIzquierdo(),matrizHijos,cant);
			for (int i=0;i<cant;i++)
			{
				if(matrizHijos[i][0].equals("No Hay"))
				{
					matrizHijos[i][0]=""+arbol.getElemento();
					if (arbol.getIzquierdo()!=null)
						matrizHijos[i][1]=""+arbol.getIzquierdo().getElemento();
					if (arbol.getDerecho()!=null)
						matrizHijos[i][2]=""+arbol.getDerecho().getElemento();
					break;
				}
			}
			setTemporal((" "+arbol.getElemento()),"\n");
			HijosCada(arbol.getDerecho(), matrizHijos, cant);
		}
		return matrizHijos;
	}
	public String mostrarArbol( )
	{
		datosTemporales="";
		return mostrarArbol(this.getRaiz(),0);
	}
	public String mostrarArbol( Nodo arbol,int nivel)
	{
		if (arbol!=null)
		{
			//System.out.println(" "+nivel+" "+arbol.getElemento()+"\n");
			mostrarArbol(arbol.getIzquierdo(), nivel+1);
			setTemporal((" "+nivel+" "+arbol.getElemento()),"\n");
			mostrarArbol(arbol.getDerecho(), nivel+1);
		}
		return this.getTemporal();
	}
	public String mostrarElementos( )
	{
		datosTemporales="";
		return mostrarElementos(this.getRaiz());
	}
	public String mostrarElementos( Nodo arbol)
	{
		if (arbol!=null)
		{
			mostrarElementos(arbol.getIzquierdo());
			setTemporal((""+arbol.getElemento())," ");
			mostrarElementos(arbol.getDerecho());
		}
		return this.getTemporal();
	}
	public String subArDer( )
	{
		datosTemporales="";
		return subArDer(this.getRaiz().getDerecho());
	}
	public String subArDer( Nodo arbol)
	{
		if (arbol!=null)
		{
			subArDer(arbol.getIzquierdo());
			setTemporal((""+arbol.getElemento())," ");
			subArDer(arbol.getDerecho());
		}
		return this.getTemporal();
	}
	public String SubArIzq( )
	{
		datosTemporales="";
		return SubArIzq(this.getRaiz().getIzquierdo());
	}
	public String SubArIzq( Nodo arbol)
	{
		if (arbol!=null)
		{
			SubArIzq(arbol.getIzquierdo());
			setTemporal((""+arbol.getElemento())," ");
			SubArIzq(arbol.getDerecho());
		}
		return this.getTemporal();
	}

	public int cantidadHijos()
	{	
		cantidadHijos(this.getRaiz());
		return cantHijos;
	}
	public void cantidadHijos(Nodo arbol)
	{	
		cantHijos=0;
		if(arbol.getDerecho()!=null)
			cantHijos++;
		if(arbol.getIzquierdo()!=null)
			cantHijos++;
	}
	private void setAltura(int n )
	{
		
		altura=n;
	}
	public int getAltura( )
	{
		calculaAlturayHojas();
		return altura;
	}
	public int cantidadNodos( )
	{
		calculaAlturayHojas();
		return cantNodos;
	}
	private void setCantHojas( )
	{
		cantHojas++;
	}
	public int getCantHojas( )
	{
		calculaAlturayHojas();
		return cantHojas;
	}
	public boolean esHoja(int n)
	{	
		String[] cadaHoja = getHojas().split(" ");
		for (int i=0; i<cadaHoja.length;i++)
		{
			if(Integer.parseInt(cadaHoja[i])==n)
				return true;
		}
		return false;
	}
	public String getHojas()
	{
		calculaAlturayHojas();
		return hojas;
	}
	private void setHojas(String n )
	{
		hojas=(n+" "+hojas);
	}
	public void calculaAlturayHojas( )
	{
		cantHojas=0;
		hojas="";
		altura=0;
		cantNodos=0;
		calculaAlturayHojas(this.getRaiz(),1);
		
	}
	
	public void calculaAlturayHojas( Nodo arbol,int nivel)
	{
		if (arbol!=null)
		{
			setCantNodos();
			calculaAlturayHojas(arbol.getIzquierdo(), nivel+1);
			if (nivel>altura)
				this.setAltura(nivel);
			if((arbol.getDerecho()==null)&&(arbol.getIzquierdo()==null))
			{ 
			this.setCantHojas();
			this.setHojas(String.valueOf(arbol.getElemento()));
			}
			
			calculaAlturayHojas(arbol.getDerecho(), nivel+1);
		}
	}
	public void setCantNodos()
	{
		this.cantNodos++;
	}
	public void setTemporal(String dato, String separador)
	{		
		this.datosTemporales=this.datosTemporales+separador+dato;		
	}
	public String getTemporal()
	{
		return datosTemporales;
	}
	public void inicializaTemporal()
	{
		datosTemporales="";
	}
	public String inorden( )
	{
		inicializaTemporal();
		return inorden(this.getRaiz());
	}
	public String inorden( Nodo arbol)
	{
		if (arbol!=null)
		{
			inorden(arbol.getIzquierdo());
			//System.out.print(" "+arbol.getElemento());

			setTemporal((" "+arbol.getElemento()),"");
			inorden(arbol.getDerecho());
		}
		return this.getTemporal();
		
	}
	public String posorden( )
	{
		inicializaTemporal();
		return posorden(this.getRaiz());
	}
	public String posorden( Nodo arbol)
	{
		if (arbol!=null)
		{
			posorden(arbol.getIzquierdo());
			posorden(arbol.getDerecho());
			//System.out.print(" "+arbol.getElemento());
			setTemporal((" "+arbol.getElemento()),"");
		}
		return this.getTemporal();
	}
	public boolean buscarElemento(T elemento)
	{
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
		return buscarElementoCad(elemento,getRaiz());
		}
		if (elemento.getClass().getSimpleName().toLowerCase().equals("integer")){
		return buscarElementoInt(elemento,getRaiz());	
		}
		return false;
	}
	
	public boolean buscarElementoCad(T elemento, Nodo arbol)
	{
			String elElemto =(String.valueOf(elemento));
		
		if(arbol==null){
			return false;
		}else if(elElemto.compareTo(String.valueOf(arbol.getElemento()))<0){
		  return  buscarElementoCad(elemento, arbol.getIzquierdo());
		} else if(elElemto.compareTo(String.valueOf(arbol.getElemento()))>0){
		  return buscarElementoCad(elemento, arbol.getDerecho());
		}else
			return true;		 
	}
	public boolean buscarElementoInt(T elemento, Nodo arbol)
	{
		Integer elElemto =(Integer)elemento;
		if(arbol==null){
			return false;
		}else if(elElemto<(int)arbol.getElemento()){
		  return  buscarElementoInt(elemento, arbol.getIzquierdo());
		} else if(elElemto>(int)arbol.getElemento()){
		  return buscarElementoInt(elemento, arbol.getDerecho());
		}else
			return true;		 
	}
	public String antecesor (T elemento)//se ubica en el antecesor
	{
		//papa=-2;
		String elElemento = (String.valueOf(elemento ));
		if (this.buscarElemento(elemento))//identifica si el elemento existe en el arbol
		{
			if(elElemento.compareTo(String.valueOf(this.raiz.getElemento()))==0)
			{
				return "-1";
			}else
			return  antecesor(elemento, this.raiz,0);
		}
		return elElemento;
	}
	public String antecesor (T elemento, Nodo arbol,int papa)//se ubica en el antecesor
	{
		if (arbol!=null)
		{
			if(arbol.getIzquierdo()!=null)//verifico excistencia del hijo izquierdo				
			{
				T izq=(T)arbol.getIzquierdo().getElemento();
				if((izq.equals(elemento)))
				{
					 this.setpapa((T) arbol.getElemento()) ;
					
				}else{System.out.println(elemento+"NO ento con "+ izq);}
			}
			if(arbol.getDerecho()!=null)//verifico excistencia del hijo derecho			
			{
				T der=(T) arbol.getDerecho().getElemento();
				if(der.equals(elemento))
				{
					this.setpapa((T)arbol.getElemento()) ;
					
				}else{System.out.println(elemento+"NO ento con "+ der);}
			}
				if(arbol.getDerecho()!=null)	
				 antecesor(elemento,arbol.getDerecho(),papa);
				if(arbol.getIzquierdo()!=null)		
				 antecesor(elemento,arbol.getIzquierdo(),papa );
			
		}
		return (String.valueOf( this.getpapa()));
	}
	
	public void setpapa(T p)
	{System.out.println("si ento con "+ p);
		papa=p;
	}
	public T getpapa()
	{
		return papa;
	}
	public int buscarNivel(T elemento )
	{
		datosTemporales="";
		if (elemento.getClass().getSimpleName().toLowerCase().equals("integer"))
		{
			int e=(int)elemento;
			return buscarNivel(e ,this.getRaiz(),0);
		}
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
			String e =(String)elemento;
			return buscarNivel(e ,this.getRaiz(),0);
		}
		return -1; 
	}
	public int buscarNivel(int elemento, Nodo arbol,int nivel)
	{
		if(arbol==null){
			return -1;
		}
		if (arbol!=null)
		{
			int elElemento =(elemento) ;
			if(elElemento<((int)arbol.getElemento())){
				  return  buscarNivel(elemento,arbol.getIzquierdo(), nivel+1);
				} else if(elElemento>((int)arbol.getElemento())){
				  return buscarNivel(elemento,arbol.getDerecho(), nivel+1);
				}else
					return nivel;
		}
		return nivel;
	}
	public int buscarNivel(String elemento, Nodo arbol,int nivel)
	{
		if(arbol==null){
			return -1;
		}
		if (arbol!=null)
		{
			String elElemento = (String.valueOf(elemento)) ;
			if(elElemento.compareTo(String.valueOf(arbol.getElemento()))<0){
				  return  buscarNivel(elemento,arbol.getIzquierdo(), nivel+1);
				} else if(elElemento.compareTo(String.valueOf( arbol.getElemento()))>0){
				  return buscarNivel(elemento,arbol.getDerecho(), nivel+1);
				}else
					return nivel;
		}
		return nivel;
	}	
	public boolean hijos(T elemento)
	{
		return hijos(elemento,getRaiz());
	}
	public boolean hijos (T elemento, Nodo arbol)
	{
		String elElemento =String.valueOf(elemento);
		if(arbol==null){
			return false;
		}else if(elElemento.compareTo(String.valueOf(arbol.getElemento()))<0){
		  return  hijos(elemento, arbol.getIzquierdo());
		} else if(elElemento.compareTo(String.valueOf(arbol.getElemento()))>0){
		  return hijos(elemento, arbol.getDerecho());
		}else
		{
			if(arbol.getDerecho()!=null){
			System.out.println("el hijo derecho es "+ arbol.getDerecho().getElemento());
			}
			if(arbol.getIzquierdo()!=null){
				System.out.println("el hijo izquierdo es "+ arbol.getIzquierdo().getElemento());
			}
			if((arbol.getIzquierdo()==null)&&(arbol.getDerecho()==null))
				System.out.println("el nodo "+ arbol.getElemento()+" es una HOJA por lo qque no tiene hijos");
			
			return true;
		}	 
	}
	public String mayor()
	{
		return mayor(getRaiz(),0);
	}
	
	public String mayor(Nodo arbol, int nivel)
	{
		if(arbol.getDerecho()==null){
			return " "+arbol.getElemento()+",en nivel "+nivel;
		}
		else 
		{
		return mayor(arbol.getDerecho(),nivel+1);
	
		}
	}
	public Nodo nodoMenor()
	{
		return nodoMenor(getRaiz());
	}
	
	public Nodo nodoMenor(Nodo arbol)
	{
		if(arbol.getIzquierdo()==null){
			return arbol;
		}
		else 
		{
		return nodoMenor(arbol.getIzquierdo());
	
		}
	}
	public Nodo nodoMayor()
	{
		return nodoMayor(getRaiz());
	}
	
	public Nodo nodoMayor(Nodo arbol)
	{
		if(arbol.getDerecho()==null){
			return arbol;
		}
		else 
		{
		return nodoMayor(arbol.getIzquierdo());
	
		}
	}
	public String menor()
	{
		return menor(getRaiz(),0);
	}
	
	public String menor(Nodo arbol,int nivel)
	{
		if(arbol.getIzquierdo()==null){
			return " "+arbol.getElemento()+",en nivel "+nivel;
		}
		else 
		{
		return menor(arbol.getIzquierdo(),nivel+1);
	
		}
	}
	public Arbol mostrarIzquierda()
	{
		datosTemporales="";
		contador=0;
		 mostrarIzquierda(getRaiz().getIzquierdo(),0);
		 Arbol miA =new Arbol();
		miA.setRaiz(getRaiz().getIzquierdo());
		return miA ;
	}
	
	public void mostrarIzquierda(Nodo arbol, int cant)
	{
		
		if(arbol!=null)
		{	if(arbol.getDerecho()!=null)
			{getCantidad();}
			if(arbol.getIzquierdo()!=null)
			{getCantidad();}
			
			mostrarIzquierda(arbol.getIzquierdo(),cant++);
			System.out.print(" "+arbol.getElemento());
			setTemporal(" "+arbol.getElemento()," ");
			mostrarIzquierda(arbol.getDerecho(), cant++);
		}
	}
	public int getCantidad()
	{
		 this.contador= this.contador+1;
		 return contador;
	}
	
	public Arbol mostrarDerecha()
	{
		contador=0;

		datosTemporales="";
		mostrarDerecha(getRaiz().getDerecho(),0);
		
		 Arbol miA =new Arbol();
			miA.setRaiz(getRaiz().getDerecho());
			return miA  ;
	}
	
	public void mostrarDerecha(Nodo arbol, int nivel)
	{
		if(arbol!=null)
		{
			if(arbol.getDerecho()!=null)
			{getCantidad();}
			if(arbol.getIzquierdo()!=null)
			{getCantidad();}
			mostrarDerecha(arbol.getIzquierdo(), nivel++);
			System.out.print(" "+arbol.getElemento());

			setTemporal(" "+arbol.getElemento()," ");
			mostrarDerecha(arbol.getDerecho(), nivel++);
		}
	}
	public String mostrarNivel(int nivelBuscar )
	{
		this.inicializaTemporal();
		mostrarNivel(this.getRaiz(),0,nivelBuscar);
		return this.getTemporal();
	}
	public void mostrarNivel( Nodo arbol,int nivel, int nivelBuscar)
	{
		if (arbol!=null)
		{
			mostrarNivel(arbol.getIzquierdo(), nivel+1,nivelBuscar);
			
			if (nivelBuscar==nivel){
				//System.out.println(" - "+arbol.getElemento());
				setTemporal(""+arbol.getElemento()," ");}
			mostrarNivel(arbol.getDerecho(), nivel+1,nivelBuscar);
		}
	}
	public boolean ubicacion(T elemento)
	{ 
		String elElement=String.valueOf(elemento);
	
		if(elElement.compareTo(String.valueOf( this.raiz.getElemento())) <0)
		{
			return true;
		}
			return false;
	}
	public Nodo eliminar(Nodo arbol, 	String elimino,Arbol nuevo)
	{
		if(arbol!= null)
		{
			if(!elimino.equals((String)arbol.getElemento()))
			{
				nuevo.insertar(arbol.getElemento());
			}
			eliminar(arbol.getIzquierdo(),elimino, nuevo);
			eliminar(arbol.getDerecho(),elimino, nuevo);
		}
		return nuevo.getRaiz();
	}
	public Nodo eliminar(Nodo arbol, int elimino,Arbol nuevo)
	{
		if(arbol!= null)
		{
			if((int)arbol.getElemento()!=elimino)
			{
				nuevo.insertar(arbol.getElemento());
			}
			eliminar(arbol.getIzquierdo(),elimino, nuevo);
			eliminar(arbol.getDerecho(),elimino, nuevo);
		}
		return nuevo.getRaiz();
	}
	public void eliminar(T elemento)
	{

		Arbol nuevoA =new Arbol();
		if(elemento.getClass().getSimpleName().toLowerCase().equals("integer"))
		{
			this.setRaiz(eliminar(this.getRaiz(), (int)elemento,nuevoA));
		}
		if(elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
			this.setRaiz(eliminar(this.getRaiz(), (String)elemento,nuevoA));
		}
		
	}
	public String nodosExtyInt()
	{
		String cad="";
		this.nodosExternos="";
		if(!this.soloRaiz(this.getRaiz())){
			//System.out.println("los nodos externos son: \n"+nodosExternos());

			cad="   los nodos externos son: "+nodosExternos()+"\n";
			//System.out.println("los nodos internos son: ");
			//nodosInternos();

			cad=cad+"   los nodos internos son: "+nodosInternos()+"\n";
		}else {System.out.println("el arbol solo posee la raiz");}
		return cad;
	}
	public boolean soloRaiz( Nodo arbol)
	{
			if((arbol.getDerecho()!=null)||(arbol.getIzquierdo()!=null))
				return false;
			return true;
		
	}
	public boolean soloesRaiz()
	{
		return soloRaiz(this.raiz);
	}
	public String nodosExternos( )
	{
		datosTemporales="";
		if (this.getRaiz()!=null)
		{
			if (!this.soloRaiz(getRaiz())){
				nodosExternosDerecho(this.getRaiz().getDerecho());
				this.nodosExternos=this.getRaiz().getElemento()+" "+this.nodosExternos;
				nodosExternosIzquierdo(this.getRaiz().getIzquierdo());
			}else {System.out.println("el arvol solo tiene raiz");}
		}else{System.out.println("el arbol esta vacio");}
		return this.nodosExternos;
	}
	public void nodosExternosIzquierdo( Nodo arbol)
	{
		if (arbol!=null)
		{
			this.nodosExternos=arbol.getElemento()+" "+this.nodosExternos;
			nodosExternosIzquierdo(arbol.getIzquierdo());
			
		}
	}
	public void nodosExternosDerecho( Nodo arbol)
	{
		if (arbol!=null)
		{
	
			this.nodosExternos=arbol.getElemento()+" "+this.nodosExternos;
			nodosExternosDerecho(arbol.getDerecho());
			
		}
	}
	public String nodosInternos( )
	{
		this.inicializaTemporal();
		return nodosInternos(this.getRaiz(),"");
	}
	public String nodosInternos( Nodo arbol,String cadena)
	{
		if (arbol!=null)
		{
			nodosInternos(arbol.getIzquierdo(),cadena);
			if(!this.estaNodosExternos((T) arbol.getElemento()))
			{
				String cad=arbol.getElemento()+"";
				this.setTemporal(cad, " ");
				//System.out.print(" "+arbol.getElemento());
			}
			nodosInternos(arbol.getDerecho(),cadena);
		}
		return this.getTemporal();
	}
	public boolean estaNodosExternos(T n )
	{
		String elValor=String.valueOf( n);
		String []array= nodosExternos.split(" ");
		for(int i=0;i<array.length;i++)
		{
			
			String num=(array[i]);
			//System.out.println(n+" ->"+num+"...");
			if(num.compareTo(elValor)==0)
				return true;
		}
		return false;
	}
	
	
}
