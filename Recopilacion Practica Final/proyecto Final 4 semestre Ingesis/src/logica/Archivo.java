package logica;
import java.io.*;
public class Archivo {
	private String temp="";
	private String nombre;
	private String nombreEscritura;
	private File archivo;
	private FileReader lectura;
	private BufferedReader leer;
	private FileWriter escritura;
	private BufferedWriter bufferEscritura;
	private PrintWriter escribir;
	
	public Archivo(String nombreLectura,String nombreEscritura) {
		this.nombre=nombreLectura;
		this.nombreEscritura=nombreEscritura;
		this.almacenar();
		//System.out.println(temp);
		try {
			archivo=new File(nombreEscritura);
			lectura=new FileReader(this.nombre);
			leer=new BufferedReader(lectura);
			try {
				escritura=new FileWriter(archivo);
			} catch (IOException e) {e.printStackTrace();}
			bufferEscritura= new BufferedWriter(escritura);
			escribir= new PrintWriter(bufferEscritura);
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
	}
	public Archivo(String nombreEscritura) {
		this.nombreEscritura=nombreEscritura;
		this.almacenar();
		//System.out.println(temp);
		try {
			archivo=new File(nombreEscritura);
			lectura=new FileReader(this.nombre);
			leer=new BufferedReader(lectura);
			try {
				escritura=new FileWriter(archivo);
			} catch (IOException e) {e.printStackTrace();}
			bufferEscritura= new BufferedWriter(escritura);
			escribir= new PrintWriter(bufferEscritura);
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
	}
	public void cerrar()
	{
		try {
			this.lectura.close();
			this.escribir.close();
			this.bufferEscritura.close();
		} catch (IOException e) {e.printStackTrace();}
		
	}
	public int contarRegistros()
	{
		String[] vector=null;
		String linea ;
		
		try {
			
			linea = leer.readLine();
			System.out.println(linea);
			vector= linea.split(" ");
		} catch (IOException e) {e.printStackTrace();}
		return vector.length ;
	}
	public String[] retornarLinea()
	{
		String[] vector=null;
		String linea ;
		
		try {
			
			linea = leer.readLine();
			vector= linea.split(" ");
		} catch (IOException e) {e.printStackTrace();}
		return vector;
	}
	public void ingresar(String dato)
	{
		this.escribir.write(dato);
	}
	public void almacenar()
	{
		try {
			FileReader fr =new FileReader(this.nombreEscritura);
			BufferedReader br= new BufferedReader(fr);
			String vacio="";
			while(vacio!=null)
			{
				
				try {
					vacio=br.readLine();
					if (vacio==null)
						break;
					temp=temp+vacio;
				} catch (IOException e) {e.printStackTrace();}				
			}
		} catch (FileNotFoundException e) {e.printStackTrace();} 
	}
}
