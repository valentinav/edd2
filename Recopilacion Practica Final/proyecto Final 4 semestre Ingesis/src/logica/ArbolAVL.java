package logica;

public class ArbolAVL<T> {
	private String datosTemporales="";
	private int contador=0;
	private int altura=0;
	private int cantNodos=0;
	private int cantHijos=0;
	private String nodosExternos="";
	private int hijo;
	private int cantHojas=0;
	private String hojas="";
	private NodoAVL raiz;
	public T papa;
	public ArbolAVL() {
		this.raiz=null;
	}
	/**
	 * TODO no me acuerdo si esto ahun sirve
	 */
	public NodoAVL buscar(T elemento ,NodoAVL arbol)
	{
		if (elemento.getClass().getSimpleName().toLowerCase().equals("integer"))
		{			
			if(this.raiz==null)
			{
				return null;
			}else if ((int)arbol.dato==(int)elemento)
			{
				return arbol;
			}else if((int)arbol.dato<(int)elemento)
			{
				return (buscar(elemento,arbol.hijoDerecho));
			}else{
				return (buscar(elemento,arbol.hijoIzquierdo));
			}
		}
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
			if(this.raiz==null)
			{
				return null;
			}else if (String.valueOf(arbol.dato).equals(String.valueOf(elemento)))
			{
				return arbol;
			}else if(String.valueOf(arbol.dato).compareTo(String.valueOf(elemento))<0)
			{
				return (buscar(elemento,arbol.hijoDerecho));
			}else{
				return (buscar(elemento,arbol.hijoIzquierdo));
			}
		}
		return null;//este retorno nulo es POR DEFECTO NUNCA DEBE ENTRAR AHI 
	}
	
	//FACTO EQUILIBRIO
	public int obtenerFE(NodoAVL x)
	{
		if(x==null)
		{
			return -1;
		}else
		{
			return x.fE;
		}
	}
	//rotacion simple izquierda
	public NodoAVL rotacionIzquieda(NodoAVL arbol)
	{
		NodoAVL auxiliar =arbol.hijoIzquierdo;
		arbol.hijoIzquierdo=auxiliar.hijoDerecho;
		auxiliar.hijoDerecho=arbol;
		arbol.fE=Math.max(obtenerFE(arbol.hijoIzquierdo), obtenerFE(arbol.hijoDerecho))+1;//funcion que retorna el mayor entre dos enteros
		auxiliar.fE=Math.max(obtenerFE(auxiliar.hijoIzquierdo), obtenerFE(auxiliar.hijoDerecho))+1;//funcion que retorna el mayor entre dos enteros
		return auxiliar;
	}
	//rotacion simple dreecha
	public NodoAVL rotacionDerecha(NodoAVL arbol)
	{
		
			NodoAVL auxiliar =arbol.hijoDerecho;
			arbol.hijoDerecho=auxiliar.hijoIzquierdo;
			auxiliar.hijoIzquierdo=arbol;
			arbol.fE=Math.max(obtenerFE(arbol.hijoIzquierdo), obtenerFE(arbol.hijoDerecho))+1;//funcion que retorna el mayor entre dos enteros
			auxiliar.fE=Math.max(obtenerFE(auxiliar.hijoIzquierdo), obtenerFE(auxiliar.hijoDerecho))+1;//funcion que retorna el mayor entre dos enteros
			return auxiliar;
		
	}
	//rotacion doble a la derecha
	public NodoAVL rotacionDobleIzquierda(NodoAVL arbol)
	{
		NodoAVL temporal;
		arbol.hijoIzquierdo=this.rotacionDerecha(arbol.hijoIzquierdo);
		temporal=rotacionIzquieda(arbol);
		return temporal;
	}
	//rotacion doble a la izquierda
	public NodoAVL rotacionDobleDerecha(NodoAVL arbol)
	{
		NodoAVL temporal;
		arbol.hijoDerecho=this.rotacionDerecha(arbol.hijoDerecho);
		temporal=rotacionDerecha(arbol);
		return temporal;
	}
	//Insertar AVL
	private NodoAVL insertarAVL(NodoAVL nuevo , NodoAVL subAr)
	{
		NodoAVL nuevoPadre=subAr;
		if (nuevo.getElemento().getClass().getSimpleName().toLowerCase().equals("integer"))
		{
			if((int)nuevo.dato<(int)subAr.dato)
			{
				if(subAr.hijoIzquierdo==null)
				{
					subAr.hijoIzquierdo=nuevo;
				}else
				{
					subAr.hijoIzquierdo=insertarAVL(nuevo, subAr.hijoIzquierdo);
					if((obtenerFE(subAr.hijoIzquierdo)-this.obtenerFE(subAr.hijoDerecho))==2)
					{
						if((int)nuevo.dato<(int)subAr.hijoIzquierdo.dato)
						{
							nuevoPadre=this.rotacionIzquieda(subAr);
						}else
						{
							try{
								nuevoPadre=this.rotacionDobleIzquierda(subAr);
							}catch(Exception e){}
						}
							
					}
				}
			}else if((int)nuevo.dato>(int)subAr.dato)
			{
				if(subAr.hijoDerecho==null)
				{
					subAr.hijoDerecho=nuevo;
					
				}else
				{
					subAr.hijoDerecho=this.insertarAVL(nuevo, subAr.hijoDerecho);
					if((obtenerFE(subAr.hijoDerecho)-obtenerFE(subAr.hijoIzquierdo)==2))
					{
						if((int)nuevo.dato>(int)subAr.hijoDerecho.dato)
						{
							nuevoPadre=this.rotacionDerecha(subAr);
						}else
						{
							try{
								nuevoPadre=this.rotacionDobleIzquierda(subAr);
							}catch(Exception e){}
						}
					}
				}
			}else
			{
				System.out.println("NodoDuplicado");
			}
			//actualiza FE
			if((subAr.hijoIzquierdo==null)&&(subAr.hijoDerecho!=null))
			{
				subAr.fE=subAr.hijoDerecho.fE+1;
			}else if((subAr.hijoDerecho==null)&&(subAr.hijoIzquierdo!=null))
			{
				subAr.fE=subAr.hijoIzquierdo.fE+1;
			}else{
				subAr.fE=Math.max(this.obtenerFE(subAr.hijoIzquierdo), this.obtenerFE(subAr.hijoDerecho))+1;
			}
		}else if (nuevo.getElemento().getClass().getSimpleName().toLowerCase().equals("string"))
		{
			if(String.valueOf(nuevo.dato).compareTo(String.valueOf(subAr.dato))<0)
			{
				if(subAr.hijoIzquierdo==null)
				{
					subAr.hijoIzquierdo=nuevo;
				}else
				{
					subAr.hijoIzquierdo=insertarAVL(nuevo, subAr.hijoIzquierdo);
					if((obtenerFE(subAr.hijoIzquierdo)-this.obtenerFE(subAr.hijoDerecho))==2)
					{
						if(String.valueOf(nuevo.dato).compareTo(String.valueOf(subAr.hijoIzquierdo.dato))<0)
						{
							nuevoPadre=this.rotacionIzquieda(subAr);
						}else
						{
							try{
							nuevoPadre=this.rotacionDobleIzquierda(subAr);
							}catch(Exception e){}
						}
					}
				}
			}else if(String.valueOf(nuevo.dato).compareTo(String.valueOf(subAr.dato))>0)
			{
				if(subAr.hijoDerecho==null)
				{
					subAr.hijoDerecho=nuevo;
					
				}else
				{
					subAr.hijoDerecho=this.insertarAVL(nuevo, subAr.hijoDerecho);
					if((obtenerFE(subAr.hijoDerecho)-obtenerFE(subAr.hijoIzquierdo)==2))
					{
						if(String.valueOf(nuevo.dato).compareTo(String.valueOf(subAr.hijoDerecho.dato))>(0))
						{
							nuevoPadre=this.rotacionDerecha(subAr);
						}else
						{
							try{
							nuevoPadre=this.rotacionDobleIzquierda(subAr);
							}catch(Exception e){}
						}
					}
				}
			}else
			{
				System.out.println("Nodo Duplicado");
			}
			//actualiza FE
			if((subAr.hijoIzquierdo==null)&&(subAr.hijoDerecho!=null))
			{
				subAr.fE=subAr.hijoDerecho.fE+1;
			}else if((subAr.hijoDerecho==null)&&(subAr.hijoIzquierdo!=null))
			{
				subAr.fE=subAr.hijoIzquierdo.fE+1;
			}else{
				subAr.fE=Math.max(this.obtenerFE(subAr.hijoIzquierdo), this.obtenerFE(subAr.hijoDerecho))+1;
			}
		}
		return nuevoPadre;
	}
	//metodoInsertar
	public void insertar(T dato)
	{
		NodoAVL<T> nuevo=new NodoAVL(dato);
		if(this.raiz==null)
		{
			raiz=nuevo;
		}else
		{
			raiz=this.insertarAVL(nuevo, raiz);
		}
	}
	public NodoAVL getRaiz() {
		return raiz;
	}
	public void setRaiz(NodoAVL raiz) {
		this.raiz = raiz;
	}
	
	
	
	public String preorden(NodoAVL arbol)
	{
		if(arbol!= null)
		{
			//System.out.print(" "+arbol.getElemento());
			setTemporal((" "+arbol.getElemento()),"");
			preorden(arbol.getIzquierdo());
			preorden(arbol.getDerecho());
		}
		return this.getTemporal();
	}
	public String  preorden()
	{
		inicializaTemporal();
		return preorden(this.getRaiz());
	}
	public String[][] hermano( )
	{
		String[][]matrizHermano=new String[this.cantidadNodos()][2]; 
		for (int i=0 ;i<this.cantidadNodos();i++)
		{
			for (int j=0 ;j<2;j++)
			{
				matrizHermano[i][j]="No Tiene";
			}
		}
		matrizHermano[0][0]=this.getRaiz().getElemento()+"";
		matrizHermano[0][1]="la raiz no tiene hermanos";
		return hermano(this.getRaiz(),matrizHermano, this.cantidadNodos());
	}
	private String[][] hermano( NodoAVL arbol,String[][] matrizHermano, int cant)
	{
		if (arbol!=null)
		{
			hermano(arbol.getIzquierdo(),matrizHermano,cant);
				for (int i=0;i<cant;i++)
				{
					if((matrizHermano[i][0].equals("No Tiene"))&&(matrizHermano[i][1].equals("No Tiene")))
					{
						if (arbol.getIzquierdo()!=null)
							matrizHermano[i][0]=""+arbol.getIzquierdo().getElemento();
						if (arbol.getDerecho()!=null)
							matrizHermano[i][1]=""+arbol.getDerecho().getElemento();
						break;
					}
				}
			hermano(arbol.getDerecho(),matrizHermano,cant);
		}
		return matrizHermano ;
	}
	public String subArDer( )
	{
		datosTemporales="";
		return subArDer(this.getRaiz().getDerecho());
	}
	public String subArDer( NodoAVL arbol)
	{
		if (arbol!=null)
		{
			subArDer(arbol.getIzquierdo());
			setTemporal((""+arbol.getElemento())," ");
			subArDer(arbol.getDerecho());
		}
		return this.getTemporal();
	}
	public String SubArIzq( )
	{
		datosTemporales="";
		return SubArIzq(this.getRaiz().getIzquierdo());
	}
	public String SubArIzq( NodoAVL arbol)
	{
		if (arbol!=null)
		{
			SubArIzq(arbol.getIzquierdo());
			setTemporal((""+arbol.getElemento())," ");
			SubArIzq(arbol.getDerecho());
		}
		return this.getTemporal();
	}
	public String[][] hijosCada( )
	{
		String[][]matrizHijos=new String[this.cantidadNodos()][3]; 
		for (int i=0 ;i<this.cantidadNodos();i++)
		{
			for (int j=0 ;j<3;j++)
			{
				matrizHijos[i][j]="No Hay";
			}
		}
		return HijosCada(this.getRaiz(),matrizHijos, this.cantidadNodos());
	}
	public String[][] HijosCada( NodoAVL arbol,String[][] matrizHijos, int cant)
	{
		if (arbol!=null)
		{
			HijosCada(arbol.getIzquierdo(),matrizHijos,cant);
			for (int i=0;i<cant;i++)
			{
				if(matrizHijos[i][0].equals("No Hay"))
				{
					matrizHijos[i][0]=""+arbol.getElemento();
					if (arbol.getIzquierdo()!=null)
						matrizHijos[i][1]=""+arbol.getIzquierdo().getElemento();
					if (arbol.getDerecho()!=null)
						matrizHijos[i][2]=""+arbol.getDerecho().getElemento();
					break;
				}
			}
			setTemporal((" "+arbol.getElemento()),"\n");
			HijosCada(arbol.getDerecho(), matrizHijos, cant);
		}
		return matrizHijos;
	}
	public String mostrarArbol( )
	{
		datosTemporales="";
		return mostrarArbol(this.getRaiz(),0);
	}
	public String mostrarArbol( NodoAVL arbol,int nivel)
	{
		if (arbol!=null)
		{
			mostrarArbol(arbol.getIzquierdo(), nivel+1);
			setTemporal((" "+nivel+" "+arbol.getElemento()),"\n");
			mostrarArbol(arbol.getDerecho(), nivel+1);
		}
		return this.getTemporal();
	}
	public String mostrarElementos( )
	{
		datosTemporales="";
		return mostrarElementos(this.getRaiz());
	}
	public String mostrarElementos( NodoAVL arbol)
	{
		if (arbol!=null)
		{
			mostrarElementos(arbol.getIzquierdo());
			setTemporal((""+arbol.getElemento())," ");
			mostrarElementos(arbol.getDerecho());
		}
		return this.getTemporal();
	}

	public int cantidadHijos()
	{	
		cantidadHijos(this.getRaiz());
		return cantHijos;
	}
	public void cantidadHijos(NodoAVL arbol)
	{	
		cantHijos=0;
		if(arbol.getDerecho()!=null)
			cantHijos++;
		if(arbol.getIzquierdo()!=null)
			cantHijos++;
	}
	private void setAltura(int n )
	{
		
		altura=n;
	}
	public int getAltura( )
	{
		calculaAlturayHojas();
		return altura;
	}
	public int cantidadNodos( )
	{
		calculaAlturayHojas();
		return cantNodos;
	}
	private void setCantHojas( )
	{
		cantHojas++;
	}
	public int getCantHojas( )
	{
		calculaAlturayHojas();
		return cantHojas;
	}
	public boolean esHoja(int n)
	{	
		String[] cadaHoja = getHojas().split(" ");
		for (int i=0; i<cadaHoja.length;i++)
		{
			if(Integer.parseInt(cadaHoja[i])==n)
				return true;
		}
		return false;
	}
	public String getHojas()
	{
		calculaAlturayHojas();
		return hojas;
	}
	private void setHojas(String n )
	{
		hojas=(n+" "+hojas);
	}
	public void calculaAlturayHojas( )
	{
		cantHojas=0;
		hojas="";
		altura=0;
		cantNodos=0;
		calculaAlturayHojas(this.getRaiz(),1);
		
	}
	
	public void calculaAlturayHojas( NodoAVL arbol,int nivel)
	{
		if (arbol!=null)
		{
			setCantNodos();
			calculaAlturayHojas(arbol.getIzquierdo(), nivel+1);
			if (nivel>altura)
				this.setAltura(nivel);
			if((arbol.getDerecho()==null)&&(arbol.getIzquierdo()==null))
			{ 
			this.setCantHojas();
			this.setHojas(String.valueOf(arbol.getElemento()));
			}
			
			calculaAlturayHojas(arbol.getDerecho(), nivel+1);
		}
	}
	public void setCantNodos()
	{
		this.cantNodos++;
	}
	public void setTemporal(String dato, String separador)
	{
		this.datosTemporales=this.datosTemporales+separador+dato;
	}
	public String getTemporal()
	{
		return datosTemporales;
	}
	public void inicializaTemporal()
	{
		datosTemporales="";
	}
	public String inorden( )
	{
		inicializaTemporal();
		return inorden(this.getRaiz());
	}
	public String inorden( NodoAVL arbol)
	{
		if (arbol!=null)
		{
			inorden(arbol.getIzquierdo());
			//System.out.print(" "+arbol.getElemento());

			setTemporal((" "+arbol.getElemento()),"");
			inorden(arbol.getDerecho());
		}
		return this.getTemporal();
		
	}
	public String posorden( )
	{
		inicializaTemporal();
		return posorden(this.getRaiz());
	}
	public String posorden( NodoAVL arbol)
	{
		if (arbol!=null)
		{
			posorden(arbol.getIzquierdo());
			posorden(arbol.getDerecho());
			//System.out.print(" "+arbol.getElemento());
			setTemporal((" "+arbol.getElemento()),"");
		}
		return this.getTemporal();
	}
	public boolean buscarElemento(T elemento)
	{
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
		return buscarElementoCad(elemento,getRaiz());
		}
		if (elemento.getClass().getSimpleName().toLowerCase().equals("integer")){
		return buscarElementoInt(elemento,getRaiz());	
		}
		return false;
	}
	
	public boolean buscarElementoCad(T elemento, NodoAVL arbol)
	{
			String elElemto =(String.valueOf(elemento));
		
		if(arbol==null){
			return false;
		}else if(elElemto.compareTo(String.valueOf(arbol.getElemento()))<0){
		  return  buscarElementoCad(elemento, arbol.getIzquierdo());
		} else if(elElemto.compareTo(String.valueOf(arbol.getElemento()))>0){
		  return buscarElementoCad(elemento, arbol.getDerecho());
		}else
			return true;		 
	}
	public boolean buscarElementoInt(T elemento, NodoAVL arbol)
	{
		Integer elElemto =(Integer)elemento;
		if(arbol==null){
			return false;
		}else if(elElemto<(int)arbol.getElemento()){
		  return  buscarElementoInt(elemento, arbol.getIzquierdo());
		} else if(elElemto>(int)arbol.getElemento()){
		  return buscarElementoInt(elemento, arbol.getDerecho());
		}else
			return true;		 
	}
	/**
	 * 
	 * @param elemento numero dentro del arbol  a los que busca sus hijos
	 */
	public int cantHijos(T elemento)
	{
		hijo=0;
		cantHijos(elemento,getRaiz());
		return hijo;
	}
	
	public void cantHijos(T elemento, NodoAVL arbol)
	{
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string")){
			
			String elementoCadena= (String.valueOf(elemento));
			if(arbol==null){
			}		
			else if(elementoCadena.compareTo(String.valueOf(arbol.getElemento()))<0){
			    buscarElementoCad(elemento, arbol.getIzquierdo());
			} else if(elementoCadena.compareTo(String.valueOf(arbol.getElemento()))>0){
			   buscarElementoCad(elemento, arbol.getDerecho());
			}else
			{
				if (arbol.getIzquierdo()!=null)
				{
					hijo++;
				}
				if (arbol.getDerecho()!=null)
				{
					hijo++;
				}
			}
		}else if (elemento.getClass().getSimpleName().toLowerCase().equals("integer"))			
		{
			if(arbol==null){
			}else if((int)elemento<(int)arbol.getElemento()){
			    buscarElementoInt(elemento, arbol.getIzquierdo());
			} else if((int)elemento>(int)arbol.getElemento()){
			   buscarElementoInt(elemento, arbol.getDerecho());
			}else
			{
				if (arbol.getIzquierdo()!=null)
				{
					hijo++;
				}
				if (arbol.getDerecho()!=null)
				{
					hijo++;
				}
			}
		}	
	}
	
	public String antecesor (T elemento)//se ubica en el antecesor
	{
		String elElemento = (String.valueOf(elemento ));
		if (this.buscarElemento(elemento))//identifica si el elemento existe en el arbol
		{
			if(elElemento.compareTo(String.valueOf(this.raiz.getElemento()))==0)
			{
				return "-1";
			}else
			return  antecesor(elemento, this.raiz,0);
		}
		return elElemento;
	}
	public String antecesor (T elemento, NodoAVL arbol,int papa)//se ubica en el antecesor
	{
		if (arbol!=null)
		{
			if(arbol.getIzquierdo()!=null)//verifico excistencia del hijo izquierdo				
			{
				T izq=(T)arbol.getIzquierdo().getElemento();
				if((izq.equals(elemento)))
				{
					 this.setpapa((T) arbol.getElemento()) ;
					
				}else{System.out.println(elemento+"NO ento con "+ izq);}
			}
			if(arbol.getDerecho()!=null)//verifico excistencia del hijo derecho			
			{
				T der=(T) arbol.getDerecho().getElemento();
				if(der.equals(elemento))
				{
					this.setpapa((T)arbol.getElemento()) ;
					
				}else{System.out.println(elemento+"NO ento con "+ der);}
			}
				if(arbol.getDerecho()!=null)	
				 antecesor(elemento,arbol.getDerecho(),papa);
				if(arbol.getIzquierdo()!=null)		
				 antecesor(elemento,arbol.getIzquierdo(),papa );
			
		}
		return (String.valueOf( this.getpapa()));
	}
	
	public void setpapa(T p)
	{System.out.println("si ento con "+ p);
		papa=p;
	}
	public T getpapa()
	{
		return papa;
	}
	public int buscarNivel(T elemento )
	{
		datosTemporales="";
		if (elemento.getClass().getSimpleName().toLowerCase().equals("integer"))
		{
			int e=(int)elemento;
			return buscarNivel(e ,this.getRaiz(),0);
		}
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
			String e =(String)elemento;
			return buscarNivel(e ,this.getRaiz(),0);
		}
		return -1; 
	}
	public int buscarNivel(int elemento, NodoAVL arbol,int nivel)
	{
		if(arbol==null){
			return -1;
		}
		if (arbol!=null)
		{
			int elElemento =(elemento) ;
			if(elElemento<((int)arbol.getElemento())){
				  return  buscarNivel(elemento,arbol.getIzquierdo(), nivel+1);
				} else if(elElemento>((int)arbol.getElemento())){
				  return buscarNivel(elemento,arbol.getDerecho(), nivel+1);
				}else
					return nivel;
		}
		return nivel;
	}
	public int buscarNivel(String elemento, NodoAVL arbol,int nivel)
	{
		if(arbol==null){
			return -1;
		}
		if (arbol!=null)
		{
			String elElemento = (String.valueOf(elemento)) ;
			if(elElemento.compareTo(String.valueOf(arbol.getElemento()))<0){
				  return  buscarNivel(elemento,arbol.getIzquierdo(), nivel+1);
				} else if(elElemento.compareTo(String.valueOf( arbol.getElemento()))>0){
				  return buscarNivel(elemento,arbol.getDerecho(), nivel+1);
				}else
					return nivel;
		}
		return nivel;
	}	
	public boolean hijos(T elemento)
	{
		return hijos(elemento,getRaiz());
	}
	public boolean hijos (T elemento, NodoAVL arbol)
	{
		String elElemento =String.valueOf(elemento);
		if(arbol==null){
			return false;
		}else if(elElemento.compareTo(String.valueOf(arbol.getElemento()))<0){
		  return  hijos(elemento, arbol.getIzquierdo());
		} else if(elElemento.compareTo(String.valueOf(arbol.getElemento()))>0){
		  return hijos(elemento, arbol.getDerecho());
		}else
		{
			if(arbol.getDerecho()!=null){
			System.out.println("el hijo derecho es "+ arbol.getDerecho().getElemento());
			}
			if(arbol.getIzquierdo()!=null){
				System.out.println("el hijo izquierdo es "+ arbol.getIzquierdo().getElemento());
			}
			if((arbol.getIzquierdo()==null)&&(arbol.getDerecho()==null))
				System.out.println("el nodo "+ arbol.getElemento()+" es una HOJA por lo qque no tiene hijos");
			
			return true;
		}	 
	}
	public String mayor()
	{
		return mayor(getRaiz(),0);
	}
	
	public String mayor(NodoAVL arbol, int nivel)
	{
		if(arbol.getDerecho()==null){
			return " "+arbol.getElemento()+",en nivel "+nivel;
		}
		else 
		{
		return mayor(arbol.getDerecho(),nivel+1);
	
		}
	}
	public NodoAVL nodoMenor()
	{
		return nodoMenor(getRaiz());
	}
	
	public NodoAVL nodoMenor(NodoAVL arbol)
	{
		if(arbol.getIzquierdo()==null){
			return arbol;
		}
		else 
		{
		return nodoMenor(arbol.getIzquierdo());
	
		}
	}
	public NodoAVL nodoMayor()
	{
		return nodoMayor(getRaiz());
	}
	
	public NodoAVL nodoMayor(NodoAVL arbol)
	{
		if(arbol.getDerecho()==null){
			return arbol;
		}
		else 
		{
		return nodoMayor(arbol.getIzquierdo());
	
		}
	}
	public String menor()
	{
		return menor(getRaiz(),0);
	}
	
	public String menor(NodoAVL arbol,int nivel)
	{
		if(arbol.getIzquierdo()==null){
			return " "+arbol.getElemento()+",en nivel "+nivel;
		}
		else 
		{
		return menor(arbol.getIzquierdo(),nivel+1);
	
		}
	}
	public ArbolAVL mostrarIzquierda()
	{
		contador=0;
		 mostrarIzquierda(getRaiz().getIzquierdo(),0);
		 ArbolAVL miA =new ArbolAVL();
		miA.setRaiz(getRaiz().getIzquierdo());
		return miA ;
	}
	
	public void mostrarIzquierda(NodoAVL arbol, int cant)
	{
		
		if(arbol!=null)
		{	if(arbol.getDerecho()!=null)
			{getCantidad();}
			if(arbol.getIzquierdo()!=null)
			{getCantidad();}
			
			mostrarIzquierda(arbol.getIzquierdo(),cant++);
			//System.out.print(" "+arbol.getElemento());
			setTemporal(" "+arbol.getElemento()," ");
			mostrarIzquierda(arbol.getDerecho(), cant++);
		}
	}
	public int getCantidad()
	{
		 this.contador= this.contador+1;
		 return contador;
	}
	
	public ArbolAVL mostrarDerecha()
	{
		contador=0;

		datosTemporales="";
		mostrarDerecha(getRaiz().getDerecho(),0);
		
		ArbolAVL miA =new ArbolAVL();
			miA.setRaiz(getRaiz().getDerecho());
			return miA  ;
	}
	
	public void mostrarDerecha(NodoAVL arbol, int nivel)
	{
		if(arbol!=null)
		{
			if(arbol.getDerecho()!=null)
			{getCantidad();}
			if(arbol.getIzquierdo()!=null)
			{getCantidad();}
			mostrarDerecha(arbol.getIzquierdo(), nivel++);
			System.out.print(" "+arbol.getElemento());

			setTemporal(" "+arbol.getElemento()," ");
			mostrarDerecha(arbol.getDerecho(), nivel++);
		}
	}
	public String mostrarNivel(int nivelBuscar )
	{
		this.inicializaTemporal();
		mostrarNivel(this.getRaiz(),0,nivelBuscar);
		return this.getTemporal();
	}
	public void mostrarNivel( NodoAVL arbol,int nivel, int nivelBuscar)
	{
		if (arbol!=null)
		{
			mostrarNivel(arbol.getIzquierdo(), nivel+1,nivelBuscar);
			
			if (nivelBuscar==nivel){
				//System.out.println(" - "+arbol.getElemento());
				setTemporal(""+arbol.getElemento()," ");}
			mostrarNivel(arbol.getDerecho(), nivel+1,nivelBuscar);
		}
	}
	public boolean ubicacion(int elemento)
	{ 
		String elElement=String.valueOf(elemento);
		
		if(elElement.compareTo(String.valueOf( this.raiz.getElemento())) <0)
		{
			return true;
		}
			return false;
	}
	public NodoAVL eliminar(NodoAVL arbol, 	String elimino,ArbolAVL nuevo)
	{
		if(arbol!= null)
		{
			if(!elimino.equals((String)arbol.getElemento()))
			{
				nuevo.insertar(arbol.getElemento());
			}
			eliminar(arbol.getIzquierdo(),elimino, nuevo);
			eliminar(arbol.getDerecho(),elimino, nuevo);
		}
		return nuevo.getRaiz();
	}
	public NodoAVL eliminar(NodoAVL arbol, int elimino,ArbolAVL nuevo)
	{
		if(arbol!= null)
		{
			if((int)arbol.getElemento()!=elimino)
			{
				nuevo.insertar(arbol.getElemento());
			}
			eliminar(arbol.getIzquierdo(),elimino, nuevo);
			eliminar(arbol.getDerecho(),elimino, nuevo);
		}
		return nuevo.getRaiz();
	}
	public void eliminar(T elemento)
	{

		ArbolAVL nuevoA =new ArbolAVL();
		if(elemento.getClass().getSimpleName().toLowerCase().equals("integer"))
		{
			this.setRaiz(eliminar(this.getRaiz(), (int)elemento,nuevoA));
		}
		if(elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
			this.setRaiz(eliminar(this.getRaiz(), (String)elemento,nuevoA));
		}
		
	}
	public String nodosExtyInt()
	{
		String cad="";
		this.nodosExternos="";
		if(!this.soloRaiz(this.getRaiz())){
			//System.out.println("los nodos externos son: \n"+nodosExternos());

			cad="   los nodos externos son: "+nodosExternos()+"\n";
			//System.out.println("los nodos internos son: ");
			//nodosInternos();

			cad=cad+"   los nodos internos son: "+nodosInternos()+"\n";
		}else {System.out.println("el arbol solo posee la raiz");}
		return cad;
	}
	public boolean soloRaiz( NodoAVL arbol)
	{
			if((arbol.getDerecho()!=null)||(arbol.getIzquierdo()!=null))
				return false;
			return true;
		
	}
	public boolean soloesRaiz()
	{
		return soloRaiz(this.raiz);
	}
	public String nodosExternos( )
	{
		datosTemporales="";
		if (this.getRaiz()!=null)
		{
			if (!this.soloRaiz(getRaiz())){
				nodosExternosDerecho(this.getRaiz().getDerecho());
				this.nodosExternos=this.getRaiz().getElemento()+" "+this.nodosExternos;
				nodosExternosIzquierdo(this.getRaiz().getIzquierdo());
			}else {System.out.println("el arvol solo tiene raiz");}
		}else{System.out.println("el arbol esta vacio");}
		return this.nodosExternos;
	}
	public void nodosExternosIzquierdo( NodoAVL arbol)
	{
		if (arbol!=null)
		{
			this.nodosExternos=arbol.getElemento()+" "+this.nodosExternos;
			nodosExternosIzquierdo(arbol.getIzquierdo());
			
		}
	}
	public void nodosExternosDerecho( NodoAVL arbol)
	{
		if (arbol!=null)
		{
	
			this.nodosExternos=arbol.getElemento()+" "+this.nodosExternos;
			nodosExternosDerecho(arbol.getDerecho());
			
		}
	}
	public String nodosInternos( )
	{
		this.inicializaTemporal();
		return nodosInternos(this.getRaiz(),"");
	}
	public String nodosInternos( NodoAVL arbol,String cadena)
	{
		if (arbol!=null)
		{
			nodosInternos(arbol.getIzquierdo(),cadena);
			if(!this.estaNodosExternos((T) arbol.getElemento()))
			{
				String cad=arbol.getElemento()+"";
				this.setTemporal(cad, " ");
				//System.out.print(" "+arbol.getElemento());
			}
			nodosInternos(arbol.getDerecho(),cadena);
		}
		return this.getTemporal();
	}
	public boolean estaNodosExternos(T n )
	{
		String elValor=String.valueOf( n);
		String []array= nodosExternos.split(" ");
		for(int i=0;i<array.length;i++)
		{
			
			String num=(array[i]);
			//System.out.println(n+" ->"+num+"...");
			if(num.compareTo(elValor)==0)
				return true;
		}
		return false;
	}
	
}
