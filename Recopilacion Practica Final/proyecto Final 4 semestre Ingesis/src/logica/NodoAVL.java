package logica;

public class NodoAVL<T> {
	int fE;
	T dato;
	NodoAVL <T>hijoIzquierdo,hijoDerecho;
	public NodoAVL(T d) {
		this.dato=d;
		this.fE=0;
		this.hijoDerecho=null;
		this.hijoIzquierdo=null;
	}
	
	
	public T getElemento() {
		return dato;
	}
	public void setElemento(T elemento) {
		this.dato = elemento;
	}
	public NodoAVL getIzquierdo() {
		return hijoIzquierdo;
	}
	public void setIzquierdo(NodoAVL izquierdo) {
		this.hijoIzquierdo = izquierdo;
	}
	public NodoAVL getDerecho() {
		return hijoDerecho;
	}
	public void setDerecho(NodoAVL derecho) {
		this.hijoDerecho = derecho;
	}



}
