package logica;

import java.util.Arrays;

import herramientas.Matriz;

public class Grafo <T> {

	private Lista<NodoGrafo> listaVertices;
	private String temp;
//CONTRUCTOR
	public Grafo() {
		listaVertices = new Lista<NodoGrafo>();
	}	
//INICIALIZADOR	DE VARIABLE TEMP
	public void inicializarTemp() {
			temp="";
	}
		
//GETTERS Y SETTERS	
	public Lista<NodoGrafo> getListaVertices() {
		return listaVertices;
	}

	public void setListaVertices(Lista<NodoGrafo> listaVertices) {
		this.listaVertices = listaVertices;
	}
	
	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = this.temp+temp ;
	}
	
//METODOS 	Y  FUNCIONES
	/**
	 * Convirte el valor en un nodo de el grafo {@link #NodoGrafo()} 
	 * @param vertice es el valor que se ingresara
	 */
	public void agregarVertices(T vertice) {
		NodoGrafo agregado =new NodoGrafo(vertice);
		listaVertices.agregar(agregado);
	}
	
	/**
	 * 
	 * @return una cadena que esta compuesta por todos los vertices sin espacios inicial ni final
	 */
	public String imprimirVertices () {
		return this.listaVertices.mostrarLista().trim();
	}
	
	/**
	 * manda a buscar en la lista un valor del vertice
	 * @param vertice el valor que sera buscado
	 * @return TRUE Si lo encontro; FALSE si no lo encontro
	 */
	public boolean verificarVertices (T vertice) {
		if(this.listaVertices.esVacia())
			return false;
		NodoGrafo buscar=new NodoGrafo(vertice);
		return this.listaVertices.BuscarLista(buscar);		
	}
	
	/**
	 * agrega arista en caso de que los dos valores existan con un peso de cero
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int  agregarAristaNoDirigido(T valor1,T valor2) {		
		int rta =this.existencia2Valores(valor1, valor2);
		NodoGrafo v1= new NodoGrafo(valor1);
		NodoGrafo v2= new NodoGrafo(valor2);
		if(interpretaExistencia(rta))
		{
			if(!valor1.equals(valor2) )
			{
				this.listaVertices.agregaAdyacencia(v1, v2);
				this.listaVertices.agregaAdyacencia(v2, v1);
			}else{
				System.out.println("se creo un bucle");
				this.listaVertices.agregaAdyacencia(v1, v2);
			}
		}
		return rta;		
	}
	/**
	 * agrega arista en un grafo ponderado en caso de que existan los dos valores 
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int  agregarAristaNoDirigido(T valor1,T valor2,int peso) {		
		int rta =this.existencia2Valores(valor1, valor2);
		int cantAristas=this.contarAdyacenciaEntre(valor1, valor2);
		NodoGrafo v1= new NodoGrafo(valor1);
		NodoGrafo v2= new NodoGrafo(valor2);
		if(interpretaExistencia(rta))
		{
			if(valor1!=valor2 ){
				this.listaVertices.agregaAdyacencia(v1, v2, peso,cantAristas );
				this.listaVertices.agregaAdyacencia(v2, v1, peso,cantAristas );
			}else{
				System.out.println("se creo un bucle");
				this.listaVertices.agregaAdyacencia(v1, v2, peso,cantAristas);
			}
		}else{
			//System.out.println("en agregarAristaDirigido por que no hay existencia");
		}
		return rta;		
	}
	/**
	 * agrega arista en un grafo ponderado en caso de que existan los dos valores 
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int  agregarAristaDirigido(T valor1,T valor2,int peso) {		
		int rta =this.existencia2Valores(valor1, valor2);
		int cantAristas=this.contarAdyacenciaEntre(valor1, valor2);
		NodoGrafo v1= new NodoGrafo(valor1);
		NodoGrafo v2= new NodoGrafo(valor2);
		if(interpretaExistencia(rta))
		{
			if(valor1!=valor2 ){
				this.listaVertices.agregaAdyacencia(v1, v2, peso,cantAristas );
			}else{
				System.out.println("se creo un bucle");
				this.listaVertices.agregaAdyacencia(v1, v2, peso,cantAristas);
			}
		}else{
			//System.out.println("en agregarAristaDirigido por que no hay existencia");
		}
		return rta;		
	}
	
	/**
	 * verifica que dos valores se encuentren en la lista
	  * @param x elemento del vertice 1
	 * @param xx elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	private int existencia2Valores(T val1 , T val2)
	{
		NodoGrafo x = new NodoGrafo(val1);
		NodoGrafo xx = new NodoGrafo(val2);
		if((this.listaVertices.BuscarLista(x))&&(this.listaVertices.BuscarLista(xx)))
		{
			return 0;			
		}else{
			int a=0;
			if( !this.listaVertices.BuscarLista(x))
			 {
				a= -1;				
			 }
			if( !this.listaVertices.BuscarLista(xx))
			{
				a= a-2;
			}
			return a;
		}
	}
	
	/**
	 * Hace una interpretacion en booleano del retorno del metodo {@link #existencia2Valores
	 * (int x, int xx)} debolviendo un booleano
	 * @param numero debe ser la llamada a un metodo que retorne cero en caso de existir 
	 * @return TRUE si Existe, FALSE no Existe
	 */
	private boolean interpretaExistencia(int numero)
	{
		if(numero==0)
			return true;
		return false;
	}
	
	/**
	 * Muestra todos los vertices adyacentes de un vertice 
	 * @param buscado es el vertise que se busca
	 * @return una cadena con sus vertices
	 */
	public String mostrarAdya (T buscado) {
		NodoGrafo elBuscado =new NodoGrafo (buscado);
		return this.listaVertices.mostrarAdyacencia(elBuscado);		
	}
	
	/**
	 * Muestra todas las aristas entre dos vertices
	 * 
	 * @param buscado es el vertise que se busca
	 * @return retorna una cadena con sus vertices y pesos
	 */
	
	public String mostrarAristasEntreVertices (T vertice1, T vertice2) {
		NodoGrafo v1= new NodoGrafo(vertice1);
		NodoGrafo v2= new NodoGrafo(vertice2);		
		String cadena= this.listaVertices.mostarArista(v1, v2);	
		
		return cadena;
	}
	
	
	/**
	 * Muestra todos los vertices adyacentes de un vertice  con su peso 
	 * @param buscado es el vertise que se busca
	 * @return retorna una cadena con sus vertices y pesos
	 */
	public String mostrarAdyaConPesos (T buscado) {
		NodoGrafo busqueda =new NodoGrafo(buscado);
		String cadena= this.listaVertices.mostrarAdyacenciaConPeso(busqueda);	
		
		return cadena;
	}
	
	/**
	 * Este metodo verifica si existe adyacencia o enlace entre dos numeros que existan o no 
	 * (cuando alguno de los dos o los dos no existen sera Falso)
	 * @param valor1  elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return un retorno TRUE significa que los dos existen y tienen un enlace (valor 1 esta
	 *  en la lista de valor dos por lo tanto tambien 2 en uno)
	 *  un retorno FALSE indica que o algun valor no existe o no tienen enlace 
	 */
	public boolean adyacenciaEntre(T valor1, T valor2) {
		NodoGrafo v1 =new NodoGrafo(valor1);
		NodoGrafo v2 =new NodoGrafo(valor2);
		int rta =this.existencia2Valores(valor1, valor2);
		if(interpretaExistencia(rta))
		{
			String [] miArregloAdyacentes =this.listaVertices.mostrarAdyacencia(v1).trim().split(" ");
			for(int i =0; i< miArregloAdyacentes.length;i++)
			{
				if (miArregloAdyacentes[i].equals(valor2+""))
				{
					return true;
				}else{}
			}			
		}		
		return true;		
	}
	/**
	 * este metodo cuenta que cantidad de aristas hay entre mismos dos vertices 
	 * @param valor1 vertice 1 
	 * @param valor2 vertice 2
	 * @return la <B>CANTIDAD</B> de aristas entre 2 vertices
	 */
	public int contarAdyacenciaEntre(T valor1, T valor2) {
		String defecto="no hay datos asociados a este valor";
		int cantidad=0;
		int rta =this.existencia2Valores(valor1, valor2);
		NodoGrafo elemento =new NodoGrafo(valor1);
		if(interpretaExistencia(rta))
		{
			//System.out.println("entramos con "+valor1+" y "+valor2);
			String [] miArregloAdyacentes =this.listaVertices.mostrarAdyacencia(elemento).trim().split(" ");
			System.out.println(this.listaVertices.mostrarAdyacencia(elemento));
			String [] miArregloDefecto =defecto.trim().split(" ");
			if(! Arrays.equals(miArregloDefecto,miArregloAdyacentes))
			{
				for(int i =0; i< miArregloAdyacentes.length;i++)
					//esta parte es un error pero igual funciona debi haber retornado solo el lenght del arreglo
				{
					if (miArregloAdyacentes[i].equals(valor2+""))
					{//System.out.println("cambiamos cant");
						cantidad++;
					}//else{System.out.println(miArregloAdyacentes[i]+" hizo nada "+valor2);}
				}		
			}//else{System.out.println("y no hay nada");}
		}//else{System.out.println("nunca hizo nada");}	
		return cantidad;		
	}
	
	/**
	 * Verifica una existencia en adyacencia y encuentra el peso entre dos datos 
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return retorna el peso de los dos datos o si no es posible un msj "no existe adyacencia " 
	 * (en caso de no existir alguno de los datos o no haber un camino entre ellos)
	 */
	public String pesoEntre(T valor1, T valor2) {
		if(this.adyacenciaEntre(valor1, valor2))
		{
			NodoGrafo v1 = new NodoGrafo(valor1);
			NodoGrafo v2 = new NodoGrafo(valor2);
			 return this.listaVertices.encontrarPeso(v1,v2);
		}else{
			return "no existe adyacencia ";
		}
	}
	
	/**
	 * Genera y debuelve una matriz de [N][N] llena de ceros (no hay camino)y 1 (que significan si hay camino )
	 * la matriz es formada por sus vertices tanto en las filas como columnas encontrando incidencias en el camino
	 * anotandolas como se dijo al inicio[1,0]
	 */
	public int [][]  matrizAdyacencia() 
	{
		
		String[] vertices= this.imprimirVertices().split(" ");
		int cant=this.cantidadVertices();
		int [][] matrizA=new int[cant][cant];
		for(int i =0; i< cant; i++)
		{
			for(int j =0; j< cant; j++)
			{
				//System.out.println((T)(vertices[i])+"  "+(T)(vertices[j])+"    "+contarAdyacenciaEntre((T)(vertices[i]),(T)( vertices[j])));
				if(this.adyacenciaEntre((T)(vertices[i]),(T)(vertices[j])))
				{
					matrizA[i][j]= this.contarAdyacenciaEntre((T)(vertices[i]),(T)( vertices[j]));
				}else{
					matrizA[i][j]= 0;
				}
			}			
		}
		
		return matrizA;
	}
	public String  imprimeMatrizAdyacencia() 
	{
		Matriz m2 =new Matriz (matrizAdyacencia());
		return m2.imprimirMatriz();
	}
	/**
	 * 
	 * @return retorna la cantidad de vertices ingresados 
	 */
	public int cantidadVertices() 
	{
		if(this.listaVertices.esVacia())
			return 0;
		return this.imprimirVertices().split(" ").length;
	}
	
	/**
	 * Este metodo cambia el peso entre dos nodos  que tienen mas de una arista pidiendo un identificador para 
	 * saber que peso cambiaremos
	 * @param vertice1 valor1 elemento del vertice 1
	 * @param vertice2 valor1 elemento del vertice 1
	 * @param nuevoPeso peso que actualizara el antiguo
	 * @param idVerticeArista es el identificador 
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio(valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int cambiarPeso(T vertice1, T vertice2, int nuevoPeso, int idVerticeArista) {
			
		int rta =this.existencia2Valores(vertice1, vertice2);
		if(interpretaExistencia(rta))
		{
			NodoGrafo v1 =new NodoGrafo(vertice1);
			NodoGrafo v2 =new NodoGrafo(vertice2);
			this.listaVertices.modificarPeso(v1, v2 ,nuevoPeso,idVerticeArista);	
		}
		return rta;		
	}
	/**
	 * 
	 * @param vertice1
	 * @param vertice2
	 * @param nuevoPeso
	 * @return
	 */
	public int cambiarPeso(T vertice1, T vertice2, int nuevoPeso) {
		return cambiarPeso( vertice1,  vertice2,  nuevoPeso,  0);		
	}
	
	
	
	public String  verticesFormalmento()
	{
		this.inicializarTemp();
		String [] vertices= this.imprimirVertices().trim().split(" ");
		if(vertices.length!=0){
			for (int i = 0 ; i<vertices.length; i++)
			{
				String val=this.mostrarAdya(((T)vertices[i]));
				if (val!="no hay datos asociados a este valor"){
					String [] adya= this.mostrarAdya(((T)vertices[i])).trim().split(" ");
					
					for (int j = 0 ; j<adya.length; j++)
					{
						if(this.existencia2Valores((T)vertices[i], (T)adya[j])==0){
							this.setTemp("("+vertices[i]+","+adya[j]+")");
						}
					}
				}
		}}
		return this.getTemp();
	}
	public boolean  grafoCompleto()
	{
		this.inicializarTemp();
		String [] vertices= this.imprimirVertices().trim().split(" ");
		int n = vertices.length,numEnlaces=0, numCompleto;
		
		for (int i = 0 ; i<vertices.length; i++)
		{
			String val=this.mostrarAdya(((T)vertices[i]));
			if (val!="no hay datos asociados a este valor"){
				String [] adya= this.mostrarAdya(((T)vertices[i])).trim().split(" ");
				for (int j = 0 ; j<adya.length; j++)
				{
					if (vertices[i]!=adya[j])
						numEnlaces++;
				}
			}			
		}
		numCompleto= (n*(n-1))/2;
		System.out.print("Tiene "+numEnlaces+" enlaces  DE "+numCompleto +"\n");
		if (numCompleto==numEnlaces)
		{
		return true	;
		}
		return false;
	}
	public void eliminar (T eliminado)
	{
		
		if(this.verificarVertices(eliminado))
		{
			NodoGrafo eliminar =new NodoGrafo(eliminado);
			listaVertices.eliminarA(eliminar);
		}else
		{
			System.out.println("el vertice no existe");
		}
	}
	
	public String grado (T vertice)
	{
		int entrada=0, salida=0;
		if (this.verificarVertices(vertice)){
			String[] vertices= this.imprimirVertices().split(" ");
			int cant=this.cantidadVertices();
			int[][]matrizA =this.matrizAdyacencia();
			int posicion=0 ;
			for(int i =0; i< cant; i++)
			{
					if(vertices[i].equals(""+vertice))
					{
						posicion =i;
						break;
					}					
			}
			//System.out.println(posicion);
			for(int j =0; j< cant; j++)
			{
				entrada=entrada+matrizA[j][posicion];
			}	
			for(int j =0; j< cant; j++)
			{
				salida=salida+matrizA[posicion][j];
			}	
			return ("entradas-> "+entrada+" ; salidas->"+salida);
		}else{return  ("el vertice no existe");}
	}
	
}// Fin clase Grafo
