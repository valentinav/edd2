package logica;

public class Nodo<T>{
	private T elemento;
	private Nodo<T> izquierdo;
	private Nodo<T> derecho;
	
	public Nodo(T elemento) {
		this.elemento=elemento;
		this.izquierdo=null;
		this.derecho=null;
	}
	public Nodo() {
		this.izquierdo=null;
		this.derecho=null;
	}
	
	public T getElemento() {
		return elemento;
	}
	public void setElemento(T elemento) {
		this.elemento = elemento;
	}
	public Nodo getIzquierdo() {
		return izquierdo;
	}
	public void setIzquierdo(Nodo izquierdo) {
		this.izquierdo = izquierdo;
	}
	public Nodo getDerecho() {
		return derecho;
	}
	public void setDerecho(Nodo derecho) {
		this.derecho = derecho;
	}


	public Nodo insertarNodo(Nodo n, T elemento) {
		//class mic =elemento.getClass();
		if (elemento.getClass().getSimpleName().toLowerCase().equals("integer"))
		{
			if (n==null)
				return new Nodo(elemento);
			else if((Integer)elemento< (Integer)n.getElemento())
			{
				n.izquierdo=this.insertarNodo(n.getIzquierdo(), elemento);
			}
			else if((Integer)elemento>(Integer)n.getElemento())
			{
				n.derecho=this.insertarNodo(n.getDerecho(), elemento);
			}
		}else
		if (elemento.getClass().getSimpleName().toLowerCase().equals("string"))
		{
			String comparable =(String.valueOf( elemento));
			
			if (n==null)
				return new Nodo(elemento);
			else if(comparable.compareTo(String.valueOf( n.getElemento()))<0)
			{
				n.izquierdo=this.insertarNodo(n.getIzquierdo(), elemento);
			}
			else if(comparable.compareTo(String.valueOf(n.getElemento()) )>0)
			{
				n.derecho=this.insertarNodo(n.getDerecho(), elemento);
			}
		}
		return n;	
	}	
}
