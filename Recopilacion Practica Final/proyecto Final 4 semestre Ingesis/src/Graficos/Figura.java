package Graficos;
import java.awt.*;
import java.awt.geom.QuadCurve2D;
import java.util.Random;
/**
 * 
 * @author Janeth Cristina Cifuentes Manrique
 *
 *logica de los metodos para el manejo de figuras 
 */
public class Figura implements Dibujo{

	public Figura()  {
		// TODO Auto-generated constructor stub
	}
	@Override
	public Color colorAleatorio(Graphics g )
	{
		Color miColor;
		Random numAzar = new Random();
		int Rojo = 255;
		int Verde = 0;
		int Azul = 0;    
	           
		 Rojo = (int)(numAzar.nextDouble()*255);
	     Verde = (int)(numAzar.nextDouble()*255);
	     Azul = (int)(numAzar.nextDouble()*255);
	           
	     miColor = new Color(Rojo, Verde, Azul);
	           
		 return miColor;
	}
	@Override
	public void linea(Graphics g,String palabra, int x1, int y1, int x2, int y2 )
	{
		
			//System.out.println(" hace linea con"+palabra);
			int alto=25,ancho=25;
			x1=x1*ancho+(25/2);
			y1=y1*alto+(25/2);
			x2=x2*ancho+(25/2);
			y2=y2*alto+(25/2);
			g.drawLine(x1, y1, x2, y2);
	}
	@Override
	public void prueba(Graphics g, int x1, int y1, int x2, int y2 )
	{
		 int puntoControX=0,puntoControY=0;
			//System.out.println(" hace linea con"+palabra);
			int alto=25,ancho=25;
			x1=x1*ancho+(25/2);
			y1=y1*alto+(25/2);
			x2=x2*ancho+(25/2);
			y2=y2*alto+(25/2);
			g.setColor(this.colorAleatorio(g));
			Graphics2D g2 = (Graphics2D) g;
			if(x1==x2){puntoControX=x1+25;}
			if(x1<x2){puntoControX=x2;}
			if(x1>x2){puntoControX=x1;}
			if(y1==y2){puntoControY=y1+25;}
			if(y1<y2){puntoControY=y1;}
			if(y1>y2){puntoControY=y2;}
			// create new QuadCurve2D.Float
			QuadCurve2D q = new QuadCurve2D.Float();
			q.setCurve(x1, y1, puntoControX, puntoControY, x2, y2);
			g2.draw(q);
	}
	@Override
	public void grafico(Graphics g,String palabra, int x, int y , String color)
	{
		//System.out.println(y);
		
		Color c1=new Color( 210, 228, 237);
		if(color.equals("blanco"))
			c1=new Color(255,255,255);
		int alto=25,ancho=25;
		x=x*ancho;
		y=y*alto;
		g.setColor(c1);
		g.fillOval(x, y, alto, ancho);
		g.setColor(Color.black);
		g.drawOval(x, y, alto, ancho);
		g.drawString(palabra,  ((ancho/2)+x)-(palabra.length()*3),(alto/2)+y+5);
	}
	
}
