package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import java.awt.CardLayout;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import logica.*;
import Graficos.*;
import Graficos.*;

import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Principal {

	private JFrame frmProyectofinal;
	private JTextField textUsuario;
	private JTextField textUsuarioG1;
	private JTextField textUsuarioG2;
	//creacion de grupos para radioButton
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private final ButtonGroup buttonGroup_4 = new ButtonGroup();
	//creacion de arboles 
	private Arbol<Integer> miArbol =new Arbol();
	private Arbol<String> miArbolCad =new Arbol();
	private ArbolAVL<Integer> miArbolAVL =new ArbolAVL();
	private ArbolAVL<String> miArbolAVLCad =new ArbolAVL();
	//creacion de grafos
	private Grafo<Integer> GrafoDirigido =new Grafo();
	private Grafo<String> GrafoDirigidoCad =new Grafo();
	private Grafo<Integer> GrafoNoDirigido =new Grafo();
	private Grafo<String> GrafoNoDirigidoCad =new Grafo();
	//creacion de bipartitos
	private int cant_V=0;
	private int cant_U=0;
	private String v[];
	private String u[];
	private Grafo<String> bipartito;	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmProyectofinal.setLocationRelativeTo(null);;
					window.frmProyectofinal.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmProyectofinal = new JFrame();
		frmProyectofinal.setTitle("ProyectoFinal");
		frmProyectofinal.setBounds(100, 100, 800, 657);
		frmProyectofinal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmProyectofinal.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		frmProyectofinal.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelArbol = new JPanel();
		panelArbol.setToolTipText("Arboles\r\nGrafos");
		tabbedPane.addTab("Arboles", null, panelArbol, null);
		panelArbol.setLayout(null);
		
		JLabel label = new JLabel("Zona de Dibujo Arbol (Z.D):");
		label.setFont(new Font("Yu Mincho Light", Font.BOLD, 18));
		label.setBounds(22, 11, 347, 25);
		panelArbol.add(label);
		
		JPanel panelDibujoA = new JPanel();
		panelDibujoA.setForeground(new Color(0, 0, 0));
		panelDibujoA.setBorder(new LineBorder(SystemColor.activeCaptionBorder, 1, true));
		panelDibujoA.setLayout(null);
		panelDibujoA.setToolTipText("Zona de Dibujo");
		panelDibujoA.setBounds(12, 47, 437, 323);
		panelArbol.add(panelDibujoA);
		
		JPanel panelZIAr = new JPanel();
		panelZIAr.setLayout(null);
		panelZIAr.setBorder(UIManager.getBorder("CheckBox.border"));
		panelZIAr.setBounds(466, 47, 303, 323);
		panelArbol.add(panelZIAr);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 283, 301);
		panelZIAr.add(scrollPane);
		
		JTextArea textImprecionA = new JTextArea();
		textImprecionA.setEditable(false);
		scrollPane.setViewportView(textImprecionA);
		
		JPanel panelUsuarioA = new JPanel();
		panelUsuarioA.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(180, 180, 180), new Color(191, 205, 219), new Color(244, 247, 252), new Color(153, 180, 209)));
		panelUsuarioA.setBounds(12, 381, 757, 187);
		panelArbol.add(panelUsuarioA);
		panelUsuarioA.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "ARBOL:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 11, 107, 79);
		panelUsuarioA.add(panel);
		panel.setLayout(null);
		
		JRadioButton seleccionABB = new JRadioButton("ABB");
		buttonGroup.add(seleccionABB);
		seleccionABB.setBounds(6, 49, 71, 23);
		panel.add(seleccionABB);
		
		JRadioButton seleccionAvl = new JRadioButton("AVL");
		seleccionAvl.setSelected(true);
		seleccionAvl.setBounds(6, 17, 71, 23);
		panel.add(seleccionAvl);
		buttonGroup.add(seleccionAvl);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "TIPO:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 101, 107, 75);
		panelUsuarioA.add(panel_1);
		panel_1.setLayout(null);
		
		JRadioButton NumerosA = new JRadioButton("Numeros");
		NumerosA.setSelected(true);
		buttonGroup_1.add(NumerosA);
		NumerosA.setBounds(6, 19, 95, 23);
		panel_1.add(NumerosA);
		
		JRadioButton LetrasA = new JRadioButton("Letras");
		buttonGroup_1.add(LetrasA);
		LetrasA.setBounds(6, 45, 95, 23);
		panel_1.add(LetrasA);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new LineBorder(new Color(180, 180, 180), 1, true), "Inserta/Elimina", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		panel_2.setBounds(165, 33, 212, 101);
		panelUsuarioA.add(panel_2);
		panel_2.setLayout(null);
		
		JButton btnInsertar = new JButton("Insertar");
		btnInsertar.setBounds(10, 33, 89, 23);
		panel_2.add(btnInsertar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(10, 67, 89, 23);
		panel_2.add(btnEliminar);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(109, 33, 93, 32);
		panel_2.add(textUsuario);
		textUsuario.setColumns(10);
		
		JButton btnRecorridos = new JButton("Recorridos");
		btnRecorridos.setEnabled(false);
		btnRecorridos.setBounds(416, 29, 132, 23);
		panelUsuarioA.add(btnRecorridos);
		
		JButton btnInformacionA = new JButton("Informacion");
		btnInformacionA.setEnabled(false);
		btnInformacionA.setBounds(416, 63, 132, 23);
		panelUsuarioA.add(btnInformacionA);
		
		JButton btnGernerarArchivoA = new JButton("Gernerar Archivo");
		btnGernerarArchivoA.setEnabled(false);
		btnGernerarArchivoA.setBounds(416, 97, 174, 23);
		panelUsuarioA.add(btnGernerarArchivoA);
		
		JButton btnLimpiarA = new JButton("");
		btnLimpiarA.setToolTipText("Limpiar Arbol");
		btnLimpiarA.setIcon(new ImageIcon(Principal.class.getResource("/com/sun/javafx/scene/control/skin/modena/dialog-error.png")));
		btnLimpiarA.setBounds(726, 11, 21, 23);
		panelUsuarioA.add(btnLimpiarA);
		
		JButton btnImprecion = new JButton("imprecion");
		
		btnImprecion.setVisible(false);
		btnImprecion.setBounds(658, 153, 89, 23);
		panelUsuarioA.add(btnImprecion);
		
		JPanel panelGrafo = new JPanel();
		tabbedPane.addTab("Grafos", null, panelGrafo, null);
		panelGrafo.setLayout(null);
		
		JLabel label_1 = new JLabel("Zona de Dibujo Grafo (Z.D):");
		label_1.setFont(new Font("Yu Mincho Light", Font.BOLD, 18));
		label_1.setBounds(22, 11, 347, 25);
		panelGrafo.add(label_1);
		
		JPanel panelDibujoG = new JPanel();
		panelDibujoG.setLayout(null);
		panelDibujoG.setToolTipText("Zona de Dibujo");
		panelDibujoG.setForeground(Color.BLACK);
		panelDibujoG.setBorder(new LineBorder(SystemColor.activeCaptionBorder, 1, true));
		panelDibujoG.setBounds(12, 47,437, 323);
		panelGrafo.add(panelDibujoG);
		
		JPanel panelZIG = new JPanel();
		panelZIG.setLayout(null);
		panelZIG.setBorder(UIManager.getBorder("CheckBox.border"));
		panelZIG.setBounds(466, 47, 303, 323);
		panelGrafo.add(panelZIG);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 283, 301);
		panelZIG.add(scrollPane_1);
		
		JTextArea textImprecionG = new JTextArea();
		textImprecionG.setEditable(false);
		scrollPane_1.setViewportView(textImprecionG);
		
		JPanel panelUsuarioGrafo = new JPanel();
		panelUsuarioGrafo.setBorder(new BevelBorder(BevelBorder.LOWERED, UIManager.getColor("Button.shadow"), SystemColor.inactiveCaption, SystemColor.inactiveCaptionBorder, SystemColor.activeCaption));
		panelUsuarioGrafo.setBounds(12, 381,757, 187);
		panelGrafo.add(panelUsuarioGrafo);
		panelUsuarioGrafo.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "GRAFO:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(10, 11, 107, 79);
		panelUsuarioGrafo.add(panel_3);
		
		JRadioButton NoDirigido = new JRadioButton("No Dirigido");
		buttonGroup_3.add(NoDirigido);
		NoDirigido.setBounds(6, 49, 88, 23);
		panel_3.add(NoDirigido);
		
		JRadioButton Dirigido = new JRadioButton("Dirigido");
		buttonGroup_3.add(Dirigido);
		Dirigido.setSelected(true);
		Dirigido.setBounds(6, 17, 71, 23);
		panel_3.add(Dirigido);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBorder(new TitledBorder(null, "TIPO:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(10, 101, 107, 75);
		panelUsuarioGrafo.add(panel_4);
		
		JRadioButton NumerosG = new JRadioButton("Numeros");
		buttonGroup_4.add(NumerosG);
		NumerosG.setSelected(true);
		NumerosG.setBounds(6, 19, 95, 23);
		panel_4.add(NumerosG);
		
		JRadioButton LetrasG = new JRadioButton("Letras");
		buttonGroup_4.add(LetrasG);
		LetrasG.setBounds(6, 45, 95, 23);
		panel_4.add(LetrasG);
		
		JComboBox Vertice_o_Arista = new JComboBox();
		Vertice_o_Arista.setModel(new DefaultComboBoxModel(new String[] {"Vertice", "Arista"}));
		Vertice_o_Arista.setBounds(159, 11, 119, 20);
		panelUsuarioGrafo.add(Vertice_o_Arista);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(SystemColor.activeCaptionBorder, 1, true));
		panel_5.setBounds(154, 40, 314, 136);
		panelUsuarioGrafo.add(panel_5);
		panel_5.setLayout(null);
		
		textUsuarioG1 = new JTextField();
		textUsuarioG1.setBounds(65, 29, 86, 20);
		panel_5.add(textUsuarioG1);
		textUsuarioG1.setColumns(10);
		
		textUsuarioG2 = new JTextField();
		textUsuarioG2.setEnabled(false);
		textUsuarioG2.setBounds(65, 74, 86, 20);
		panel_5.add(textUsuarioG2);
		textUsuarioG2.setColumns(10);
		
		JLabel lblVertice = new JLabel("Vertice:");
		lblVertice.setBounds(9, 21, 46, 14);
		panel_5.add(lblVertice);
		
		JLabel lblVertice_1 = new JLabel("Vertice2: ");
		lblVertice_1.setBounds(10, 67, 46, 14);
		panel_5.add(lblVertice_1);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(UIManager.getBorder("TextArea.border"));
		panel_6.setBounds(194, 11, 110, 88);
		panel_5.add(panel_6);
		panel_6.setLayout(null);
		
		JRadioButton Inserta = new JRadioButton("Inserta");
		Inserta.setSelected(true);
		buttonGroup_2.add(Inserta);
		Inserta.setBounds(0, 7, 109, 23);
		panel_6.add(Inserta);
		
		JRadioButton Elimina = new JRadioButton("Elimina");
		buttonGroup_2.add(Elimina);
		Elimina.setBounds(0, 33, 109, 23);
		panel_6.add(Elimina);
		
		JRadioButton Edita = new JRadioButton("Edita");
		Edita.setEnabled(false);
		buttonGroup_2.add(Edita);
		Edita.setBounds(0, 60, 109, 23);
		panel_6.add(Edita);
		
		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(65, 105, 139, 20);
		panel_5.add(btnAceptar);
		
		JButton btnInformacionG = new JButton("Informacion");
		btnInformacionG.setEnabled(false);
		btnInformacionG.setBounds(512, 57, 132, 23);
		panelUsuarioGrafo.add(btnInformacionG);
		
		JButton btnGenerarArchivoG = new JButton("Gernerar Archivo");
		btnGenerarArchivoG.setEnabled(false);
		btnGenerarArchivoG.setBounds(512, 97, 174, 23);
		panelUsuarioGrafo.add(btnGenerarArchivoG);
		
		JButton btnLimpiarG = new JButton("");
		btnLimpiarG.setIcon(new ImageIcon(Principal.class.getResource("/com/sun/javafx/scene/control/skin/modena/dialog-error@2x.png")));
		btnLimpiarG.setToolTipText("Limpiar Grafo");
		btnLimpiarG.setBounds(726, 11, 21, 23);
		panelUsuarioGrafo.add(btnLimpiarG);
		
		JButton Desactiva = new JButton("");
		Desactiva.setBounds(293, 11, 43, 23);
		panelUsuarioGrafo.add(Desactiva);
		Desactiva.setVisible(false);
		
		JButton Dibujar = new JButton("");
		Dibujar.setBounds(354, 11, 43, 23);
		panelUsuarioGrafo.add(Dibujar);
		
		JRadioButton GBipartito = new JRadioButton("G. Bipartito");
		buttonGroup_3.add(GBipartito);
		GBipartito.setBounds(642, 153, 109, 23);
		panelUsuarioGrafo.add(GBipartito);
		
		JLabel lblOtroGrafo = new JLabel("Otro Grafo:");
		lblOtroGrafo.setForeground(SystemColor.controlDkShadow);
		lblOtroGrafo.setBounds(571, 156, 65, 20);
		panelUsuarioGrafo.add(lblOtroGrafo);
		Dibujar.setVisible(false);
/**
 * TODO inicio de Arboles		
 */
	// en esta seccion esta el control de tipo de dato ingresado
		textUsuario.addKeyListener(new KeyAdapter() {// ingreso de arboles
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c=arg0.getKeyChar();

				if((c)!= KeyEvent.VK_BACK_SPACE)
					if(NumerosA.isSelected())
					{
						if(!Character.isDigit(c)){ 
				        	  if (c=='-')
				        	  {
				        		  if(! (textUsuario.getText().equals("")))
				        		  {
				        			  arg0.consume();  
				        			  JOptionPane.showMessageDialog(null,"El Arbol es de Numeros : \n "
				        			  		+ "si el numero es negativo ingrese al inicio el menos \n el "
				        			  		+ "formato ingresado no es correcto. Ingrese Nuevamente ", "Error", JOptionPane.ERROR_MESSAGE);					              
				        		  }
				        	  }else
				        	  {
				        		  arg0.consume();  
				        		  JOptionPane.showMessageDialog(null,"El Arbol es de Numeros : \n "
				        			  		+ "y el formato ingresado no es correcto. Ingrese Nuevamente ", "Error", JOptionPane.ERROR_MESSAGE);
				        	  }
				          };
					}
			}
		});
		
		textUsuarioG1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar(); 
				if((c)!= KeyEvent.VK_BACK_SPACE){
					if(NumerosG.isSelected())
					{
						if(!Character.isDigit(c)){ 
				        	  if (c=='-')
				        	  {
				        		  if(! (textUsuarioG1.getText().equals("")))
				        		  {
				        			  e.consume();  
				        			  JOptionPane.showMessageDialog(null,"El Arbol es de Numeros : \n "
				        			  		+ "si el numero es negativo ingrese al inicio el menos \n el "
				        			  		+ "formato ingresado no es correcto. Ingrese Nuevamente ", "Error", JOptionPane.ERROR_MESSAGE);					              
				        		  }
				        	  }else
				        	  {
				        		  e.consume();  
				        		  JOptionPane.showMessageDialog(null,"El Arbol es de Numeros : \n "
				        			  		+ "y el formato ingresado no es correcto. Ingrese Nuevamente ", "Error", JOptionPane.ERROR_MESSAGE);
				        	  }
				          };
					}
				}else
					{
						if((GBipartito.isSelected())&&(!textUsuarioG1.getText().substring(0, 1).equals("V_")))
						{
							textUsuarioG1.setText("V_");
						}
					}

			}
		});
		textUsuarioG2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar(); 
				if((c)!= KeyEvent.VK_BACK_SPACE)
				{
					if(NumerosG.isSelected())
					{
						if(!Character.isDigit(c)){ 
				        	  if (c=='-')
				        	  {
				        		  if(! (textUsuarioG2.getText().equals("")))
				        		  {
				        			  e.consume();  
				        			  JOptionPane.showMessageDialog(null,"El Arbol es de Numeros : \n "
				        			  		+ "si el numero es negativo ingrese al inicio el menos \n el "
				        			  		+ "formato ingresado no es correcto. Ingrese Nuevamente ",  "Error",JOptionPane.ERROR_MESSAGE);					              
				        		  }
				        	  }else
				        	  {
				        		  e.consume();  
				        		  JOptionPane.showMessageDialog(null,"El Arbol es de Numeros : \n "
				        			  		+ "y el formato ingresado no es correcto. Ingrese Nuevamente ", "Error", JOptionPane.ERROR_MESSAGE);
				        	  }
				          };
					}
				}else
				{
					if((GBipartito.isSelected())&&(!textUsuarioG2.getText().substring(0, 1).equals("U_")))
					{
						textUsuarioG2.setText("U_");
					}
				}
			}
		});
	//sentencias de grafico
		btnImprecion.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Graphics k=panelDibujoA.getGraphics();
				Dibujo t=new Figura();
				String [] vectorIno;
				String [] vectorPoso;
				
				if(seleccionAvl.isSelected())
				{
					if(NumerosA.isSelected())
					{
						if (miArbolAVL.getRaiz()!=null)
						{
							vectorIno= (miArbolAVL.inorden().trim().split(" "));
							vectorPoso= (miArbolAVL.posorden().trim().split(" "));
							boolean[][] grafico=new boolean[vectorPoso.length][vectorIno.length] ;
							String[][] DatosDibujo=new String[vectorPoso.length][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=papaV;C4=Xpapa;C5=Ypapa;
							for(int i =vectorPoso.length-1;i>=0;i--)
							{
								for(int j =0;j<vectorIno.length;j++)
								{
									
									if(vectorPoso[i].equals(vectorIno[j]))
									{
										grafico[i][j]=true;
										DatosDibujo[i][0]=vectorPoso[i];
										DatosDibujo[i][1]=j+"";
										DatosDibujo[i][2]=""+miArbolAVL.buscarNivel(Integer.parseInt(vectorPoso[i]));
										DatosDibujo[i][3]=""+miArbolAVL.antecesor(Integer.parseInt(vectorPoso[i]));
									}else
									{
										grafico[i][j]=false;
									}
								}
							}
							for(int i =0;i<vectorPoso.length;i++)
							{
								String[]poscicionPadre=posicionPapa(DatosDibujo,DatosDibujo[i][3]);
								DatosDibujo[i][4]=poscicionPadre[0];
								DatosDibujo[i][5]=poscicionPadre[1];
								
							}
							for(int i =0;i<vectorPoso.length-1;i++)
							{
								t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
							}
							for(int i =0;i<vectorPoso.length;i++)
							{
								t.grafico(k,DatosDibujo[i][0],Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),"");
							}
						}else{JOptionPane.showMessageDialog(null,"No se dibujara hasta ingresar valores en los nodos","ERROR!!!", JOptionPane.ERROR_MESSAGE);}
					}else 
						if(LetrasA.isSelected())
						{
							if (miArbolAVLCad.getRaiz()!=null)
							{
								vectorIno= (miArbolAVLCad.inorden().trim().split(" "));
								vectorPoso= (miArbolAVLCad.posorden().trim().split(" "));
								boolean[][] grafico=new boolean[vectorPoso.length][vectorIno.length] ;
								String[][] DatosDibujo=new String[vectorPoso.length][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=papaV;C4=Xpapa;C5=Ypapa;
								for(int i =vectorPoso.length-1;i>=0;i--)
								{
									for(int j =0;j<vectorIno.length;j++)
									{
										
										if(vectorPoso[i].equals(vectorIno[j]))
										{
											grafico[i][j]=true;
											DatosDibujo[i][0]=vectorPoso[i];
											DatosDibujo[i][1]=j+"";
											DatosDibujo[i][2]=""+miArbolAVLCad.buscarNivel((vectorPoso[i]));
											DatosDibujo[i][3]=""+miArbolAVLCad.antecesor((vectorPoso[i]));
										}else
										{
											grafico[i][j]=false;
										}
									}
								}
								for(int i =0;i<vectorPoso.length;i++)
								{
									String[]poscicionPadre=posicionPapa(DatosDibujo,DatosDibujo[i][3]);
									DatosDibujo[i][4]=poscicionPadre[0];
									DatosDibujo[i][5]=poscicionPadre[1];
									
								}
								for(int i =0;i<vectorPoso.length-1;i++)
								{
									t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
								}
								for(int i =0;i<vectorPoso.length;i++)
								{
									t.grafico(k,DatosDibujo[i][0],Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),"");
								}
							}else{JOptionPane.showMessageDialog(null,"No se dibujara hasta ingresar valores en los nodos","ERROR!!!", JOptionPane.ERROR_MESSAGE);}
							}
							
				}else if(seleccionABB.isSelected())
				{
					if(NumerosA.isSelected())
					{
						if(miArbol.getRaiz()!=null)
						{
							vectorIno= (miArbol.inorden().trim().split(" "));
							vectorPoso= (miArbol.posorden().trim().split(" "));
							boolean[][] grafico=new boolean[vectorPoso.length][vectorIno.length] ;
							String[][] DatosDibujo=new String[vectorPoso.length][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=papaV;C4=Xpapa;C5=Ypapa;
							for(int i =vectorPoso.length-1;i>=0;i--)
							{
								for(int j =0;j<vectorIno.length;j++)
								{
									
									if(vectorPoso[i].equals(vectorIno[j]))
									{
										grafico[i][j]=true;
										DatosDibujo[i][0]=vectorPoso[i];
										DatosDibujo[i][1]=j+"";
										DatosDibujo[i][2]=""+miArbol.buscarNivel(Integer.parseInt(vectorPoso[i]));
										DatosDibujo[i][3]=""+miArbol.antecesor(Integer.parseInt(vectorPoso[i]));
									}else
									{
										grafico[i][j]=false;
									}
								}
							}
							for(int i =0;i<vectorPoso.length;i++)
							{
								String[]poscicionPadre=posicionPapa(DatosDibujo,DatosDibujo[i][3]);
								DatosDibujo[i][4]=poscicionPadre[0];
								DatosDibujo[i][5]=poscicionPadre[1];
								
							}
							for(int i =0;i<vectorPoso.length-1;i++)
							{
								t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
							}
							for(int i =0;i<vectorPoso.length;i++)
							{
								t.grafico(k,DatosDibujo[i][0],Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),"");
							}
						}else{JOptionPane.showMessageDialog(null,"No se dibujara hasta ingresar valores en los nodos","ERROR!!!", JOptionPane.ERROR_MESSAGE);}
					}else 
					if(LetrasA.isSelected())
					{

						if(miArbolCad.getRaiz()!=null)
						{
							vectorIno= (miArbolCad.inorden().trim().split(" "));
							vectorPoso= (miArbolCad.posorden().trim().split(" "));
							boolean[][] grafico=new boolean[vectorPoso.length][vectorIno.length] ;
							String[][] DatosDibujo=new String[vectorPoso.length][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=papaV;C4=Xpapa;C5=Ypapa;
							for(int i =vectorPoso.length-1;i>=0;i--)
							{
								for(int j =0;j<vectorIno.length;j++)
								{
									
									if(vectorPoso[i].equals(vectorIno[j]))
									{
										grafico[i][j]=true;
										DatosDibujo[i][0]=vectorPoso[i];
										DatosDibujo[i][1]=j+"";
										DatosDibujo[i][2]=""+miArbolCad.buscarNivel((vectorPoso[i]));
										DatosDibujo[i][3]=""+miArbolCad.antecesor((vectorPoso[i]));
									}else
									{
										grafico[i][j]=false;
									}
								}
							}
							for(int i =0;i<vectorPoso.length;i++)
							{
								String[]poscicionPadre=posicionPapa(DatosDibujo,DatosDibujo[i][3]);
								DatosDibujo[i][4]=poscicionPadre[0];
								DatosDibujo[i][5]=poscicionPadre[1];
								
							}
							for(int i =0;i<vectorPoso.length-1;i++)
							{
								t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
							}
							for(int i =0;i<vectorPoso.length;i++)
							{
								t.grafico(k,DatosDibujo[i][0],Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),"");
							}
						}else{JOptionPane.showMessageDialog(null,"No se dibujara hasta ingresar valores en los nodos","ERROR!!!", JOptionPane.ERROR_MESSAGE);}
					
					}
				}				
			}
			public String[] posicionPapa(String[][]datos, String papa)
			{
				String[]poscicion=new String[2];
				for(int i =0;i<datos.length;i++)
				{
					if(datos[i][0].equals(papa))
					{
						poscicion[0]=datos[i][1];
						poscicion[1]=datos[i][2];
						break;
					}					
				}
				return poscicion;
			}
		});
	//sentencia de botones
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelDibujoA.repaint();
				if(!textUsuario.getText().equals(""))
				{
					if (seleccionABB.isSelected())
					{
						if (NumerosA.isSelected())
						{
							if(!miArbol.buscarElemento(Integer.parseInt(textUsuario.getText())))
							{
								miArbol.insertar(Integer.parseInt(textUsuario.getText()));
								JOptionPane.showMessageDialog(null,"Se Ha Insertado el numero : "
								+textUsuario.getText() ,"confirmacion", JOptionPane.INFORMATION_MESSAGE);
								textImprecionA.setText("El arbol con la Insercion es \n"+miArbol.mostrarArbol());
							}else{
								JOptionPane.showMessageDialog(null,"El elemento ya existe en el arbol  ",
										"Error",JOptionPane.ERROR_MESSAGE);
							}
							if(miArbol.getRaiz()!=null)
							{
								btnEliminar.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnInformacionA.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnGernerarArchivoA.setEnabled(true);
							}
						}else if (LetrasA.isSelected())
						{
							if(!miArbolCad.buscarElemento((textUsuario.getText())))
							{
								miArbolCad.insertar((textUsuario.getText()));
								JOptionPane.showMessageDialog(null,"Se Ha Insertado el numero : "
								+textUsuario.getText() ,"confirmacion", JOptionPane.INFORMATION_MESSAGE);
								textImprecionA.setText("El arbol con la Insercion es \n"+miArbolCad.mostrarArbol());
							}else{
								JOptionPane.showMessageDialog(null,"El elemento ya existe en el arbol  ",
										"Error",JOptionPane.ERROR_MESSAGE);
							}
							if(miArbolCad.getRaiz()!=null)
							{
								btnEliminar.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnInformacionA.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnGernerarArchivoA.setEnabled(true);
							}
						}
						
					}else
					if(seleccionAvl.isSelected())
					{
						if (NumerosA.isSelected())
						{
							if(!miArbolAVL.buscarElemento(Integer.parseInt(textUsuario.getText())))
							{
								miArbolAVL.insertar(Integer.parseInt(textUsuario.getText()));
								JOptionPane.showMessageDialog(null,"Se Ha Insertado el numero : "
								+textUsuario.getText() ,"confirmacion", JOptionPane.INFORMATION_MESSAGE);
								textImprecionA.setText("El arbol con la Insercion es \n"+miArbolAVL.mostrarArbol());
							}else{
								JOptionPane.showMessageDialog(null,"El elemento ya existe en el arbol  ",
										"Error",JOptionPane.ERROR_MESSAGE);
							}
							if(miArbolAVL.getRaiz()!=null)
							{
								btnEliminar.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnInformacionA.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnGernerarArchivoA.setEnabled(true);
							}
						}else if (LetrasA.isSelected())
						{
							if(!miArbolAVLCad.buscarElemento((textUsuario.getText())))
							{
								miArbolAVLCad.insertar((textUsuario.getText()));
								JOptionPane.showMessageDialog(null,"Se Ha Insertado el numero : "
								+textUsuario.getText() ,"confirmacion", JOptionPane.INFORMATION_MESSAGE);
								textImprecionA.setText("El arbol con la Insercion es \n"+miArbolAVLCad.mostrarArbol());
							}else{
								JOptionPane.showMessageDialog(null,"El elemento ya existe en el arbol  ",
										"Error",JOptionPane.ERROR_MESSAGE);
							}
							if(miArbolAVLCad.getRaiz()!=null)
							{
								btnEliminar.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnInformacionA.setEnabled(true);
								btnRecorridos.setEnabled(true);
								btnGernerarArchivoA.setEnabled(true);
							}
						}
					}
					
					btnImprecion.doClick();
				}else{JOptionPane.showMessageDialog(null,"Esriba algo en la casilla vacia ","ERROR!!!", JOptionPane.ERROR_MESSAGE);}
				btnImprecion.doClick();
			}
		});
		
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelDibujoA.repaint();
				if(!textUsuario.getText().equals(""))
				{
					if (seleccionABB.isSelected())
					{
						if (NumerosA.isSelected())
						{
							
							if(miArbol.buscarElemento(Integer.parseInt(textUsuario.getText())))
							{
								JOptionPane.showMessageDialog(null,"Se Ha eliminado el numero : "+
										textUsuario.getText(),"Confirmacion",JOptionPane.INFORMATION_MESSAGE);
								miArbol.eliminar(Integer.parseInt(textUsuario.getText()));
								textImprecionA.setText("El arbol con eliminacion es \n"+miArbol.mostrarArbol());
								if (miArbol.getRaiz()==null)
								{
									btnGernerarArchivoA.setEnabled(false);	
									btnEliminar.setEnabled(false);
									btnInformacionA.setEnabled(false);
									btnRecorridos.setEnabled(false);
									//btnLimpiarA.setEnabled(false);
									JOptionPane.showMessageDialog(null,"SU ARBOL QUEDO VACIO : Seleccione tipo de arbol,de CLICK EN insertar e Ingrese un valor");
								}
							}else{
								JOptionPane.showMessageDialog(null,"El elemento No existe en el arbol  "+Integer.parseInt(textUsuario.getText()),
										"Error",JOptionPane.ERROR_MESSAGE);
							}
						}else if (LetrasA.isSelected())
						{
							if(miArbolCad.buscarElemento((textUsuario.getText())))
							{
								miArbolCad.eliminar(((String)textUsuario.getText()));
								JOptionPane.showMessageDialog(null,"Se Ha eliminado el numero : "+
										textUsuario.getText(),"Confirmacion",JOptionPane.INFORMATION_MESSAGE);
								
								textImprecionA.setText("El arbol con eliminacion es \n"+miArbolCad.mostrarArbol());
								if (miArbolCad.getRaiz()==null)
								{
									btnGernerarArchivoA.setEnabled(false);	
									btnEliminar.setEnabled(false);
									btnInformacionA.setEnabled(false);
									btnRecorridos.setEnabled(false);
									//btnLimpiarA.setEnabled(false);
									JOptionPane.showMessageDialog(null,"SU ARBOL QUEDO VACIO : Seleccione tipo de arbol,de CLICK EN insertar e Ingrese un valor");
								}
							}else{
								JOptionPane.showMessageDialog(null,"El elemento NO existe en el arbol  ",
										"Error",JOptionPane.ERROR_MESSAGE);
							}
						}
					}else if (seleccionAvl.isSelected())
					{
						if (NumerosA.isSelected())
						{
							
							if(miArbolAVL.buscarElemento(Integer.parseInt(textUsuario.getText())))
							{
								JOptionPane.showMessageDialog(null,"Se Ha eliminado el numero : "+
										textUsuario.getText(),"Confirmacion",JOptionPane.INFORMATION_MESSAGE);
								miArbolAVL.eliminar(Integer.parseInt(textUsuario.getText()));
								textImprecionA.setText("El arbol con eliminacion es \n"+miArbolAVL.mostrarArbol());
								if (miArbolAVL.getRaiz()==null)
								{
									btnEliminar.setEnabled(false);
									btnInformacionA.setEnabled(false);
									btnRecorridos.setEnabled(false);
									//btnLimpiarA.setEnabled(false);
									btnGernerarArchivoA.setEnabled(false);	
									JOptionPane.showMessageDialog(null,"SU ARBOL QUEDO VACIO : Seleccione tipo de arbol,de CLICK EN insertar e Ingrese un valor");
								}
							}else{
								JOptionPane.showMessageDialog(null,"El elemento No existe en el arbol  "+Integer.parseInt(textUsuario.getText()),
										"Error",JOptionPane.ERROR_MESSAGE);
							}
						}else if (LetrasA.isSelected())
						{
							if(miArbolAVLCad.buscarElemento((textUsuario.getText())))
							{
								miArbolAVLCad.eliminar(((String)textUsuario.getText()));
								JOptionPane.showMessageDialog(null,"Se Ha eliminado el numero : "+
										textUsuario.getText(),"Confirmacion",JOptionPane.INFORMATION_MESSAGE);
								
								textImprecionA.setText("El arbol con eliminacion es \n"+miArbolAVLCad.mostrarArbol());
								if (miArbolAVLCad.getRaiz()==null)
								{
									btnGernerarArchivoA.setEnabled(false);	
									btnEliminar.setEnabled(false);
									btnInformacionA.setEnabled(false);
									btnRecorridos.setEnabled(false);
									//btnLimpiarA.setEnabled(false);
									JOptionPane.showMessageDialog(null,"SU ARBOL QUEDO VACIO : Seleccione tipo de arbol,de CLICK EN insertar e Ingrese un valor");
								}
							}else{
								JOptionPane.showMessageDialog(null,"El elemento NO existe en el arbol  ",
										"Error",JOptionPane.ERROR_MESSAGE);
							}
						}
					}
					
				}else{JOptionPane.showMessageDialog(null,"Esriba algo en la casilla vacia ","ERROR!!!", JOptionPane.ERROR_MESSAGE);}
				btnImprecion.doClick();
			}
		});
		btnRecorridos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(seleccionABB.isSelected())
				{
					if(NumerosA.isSelected())
					{
						textImprecionA.setText("Inorden: "+miArbol.inorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPreorden: "+miArbol.preorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPosOrden :"+miArbol.posorden());
					}else if(LetrasA.isSelected())
					{
						textImprecionA.setText("Inorden: "+miArbolCad.inorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPreorden: "+miArbolCad.preorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPosOrden :"+miArbolCad.posorden());
					}
					
				}else if(seleccionAvl.isSelected())
				{
					if(NumerosA.isSelected())
					{
						textImprecionA.setText("Inorden: "+miArbolAVL.inorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPreorden: "+miArbolAVL.preorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPosOrden :"+miArbolAVL.posorden());
					}else if(LetrasA.isSelected())
					{
						textImprecionA.setText("Inorden: "+miArbolAVLCad.inorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPreorden: "+miArbolAVLCad.preorden());
						textImprecionA.setText(textImprecionA.getText()+"\nPosOrden :"+miArbolAVLCad.posorden());
					}
				}
				
			}
		});
		btnInformacionA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(seleccionABB.isSelected())
				{
					if(NumerosA.isSelected())
					{
						textImprecionA.setText("\tINFORMACION");
						textImprecionA.setText(textImprecionA.getText()+"\nRaiz: "+miArbol.getRaiz().getElemento());
						textImprecionA.setText(textImprecionA.getText()+"\nAltura: "+miArbol.getAltura());
						textImprecionA.setText(textImprecionA.getText()+"\nCantidad de Nodos: "+miArbol.cantidadNodos());
						if(!miArbol.soloesRaiz())
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNodos Internos Y Externos: \n"+miArbol.nodosExtyInt());
						}else
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNodos Internos Y Externos:\n Solo esta el nodo raiz"+miArbol.getRaiz().getElemento());	
						}
						String cadaHijo="";
						String[][]hijos=miArbol.hijosCada();
						String [][]grado=new String[2][hijos.length];
						for (int i=0; i<hijos.length;i++)
						{
							int cont =0;
								cadaHijo=cadaHijo+" \n Numero: "+hijos[i][0]+"\n   ->hijo Izq: "+hijos[i][1]+"   ->hijo derecho: "+hijos[i][2];
								grado[0][i]=hijos[i][0];
								if(!hijos[i][1].trim().equals("No Hay"))
								{
									cont ++;
								}
								if(!hijos[i][2].trim().equals("No Hay"))
								{
									cont ++;
								}
								grado[1][i]=cont+"";
						}
						textImprecionA.setText(textImprecionA.getText()+"\nHIJOS \n"+cadaHijo);
						
						String niveles="\nGRADO \n";
						for (int i=0; i<hijos.length;i++)
						{
							niveles= niveles + " \n numero"+grado[0][i]+"  cant hijos :"+grado[1][i];
						}
						textImprecionA.setText(textImprecionA.getText()+niveles);
						String hermanos="";
						String[][]hermano=miArbol.hermano();
						for (int i=0; i<hermano.length;i++)
						{
							if(!hermano[i][0].equals("No Tiene"))
							{
								hermanos=hermanos+" \n"+hermano[i][0]+"<-Hermanos-> "+hermano[i][1];
							}else
							{
								if(!hermano[i][1].equals("No Tiene"))
								{
									hermanos=hermanos+" \n"+hermano[i][1]+"<-Hermanos-> "+hermano[i][0];
								}
							}
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHERMANOS: \n"+hermanos);
						textImprecionA.setText(textImprecionA.getText()+"\n\nNIVELES: \n");
						for(int i =0 ; i<(miArbol.getAltura());i++)
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNivel "+i+" (Cant="+miArbol.mostrarNivel(i).trim().split(" ").length+") :"+miArbol.mostrarNivel(i));
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHOJAS (cant="+miArbol.getCantHojas()+") :"+miArbol.getHojas());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Derecho: \n"+miArbol.subArDer());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Izquiedo: \n"+miArbol.SubArIzq());
					}else 
					if(LetrasA.isSelected())
					{
						textImprecionA.setText("\tINFORMACION");
						textImprecionA.setText(textImprecionA.getText()+"\nRaiz: "+miArbolCad.getRaiz().getElemento());
						textImprecionA.setText(textImprecionA.getText()+"\nAltura: "+miArbolCad.getAltura());
						textImprecionA.setText(textImprecionA.getText()+"\nCantidad de Nodos: "+miArbolCad.cantidadNodos());
						if(!miArbolCad.soloesRaiz())
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNodos Internos Y Externos: \n"+
									miArbolCad.nodosExtyInt());
						}else
						{
							textImprecionA.setText(textImprecionA.getText()+
									"\nNodos Internos Y Externos:\n Solo esta el nodo raiz"+miArbolCad.getRaiz().getElemento());	
						}
						String cadaHijo="";
						String[][]hijos=miArbolCad.hijosCada();
						String [][]grado=new String[2][hijos.length];
						for (int i=0; i<hijos.length;i++)
						{
							int cont =0;
								cadaHijo=cadaHijo+" \n Elemento: "+hijos[i][0]+
										"\n   ->hijo Izq: "+hijos[i][1]+"   ->hijo derecho: "+hijos[i][2];
								grado[0][i]=hijos[i][0];
								if(!hijos[i][1].trim().equals("No Hay"))
								{
									cont ++;
								}
								if(!hijos[i][2].trim().equals("No Hay"))
								{
									cont ++;
								}
								grado[1][i]=cont+"";
						}
						textImprecionA.setText(textImprecionA.getText()+"\nHIJOS \n"+cadaHijo);
						
						String niveles="\nGRADO \n";
						for (int i=0; i<hijos.length;i++)
						{
							niveles= niveles + " \n Elemento "+grado[0][i]+"  cant hijos :"+grado[1][i];
						}
						textImprecionA.setText(textImprecionA.getText()+niveles);
						String hermanos="";
						String[][]hermano=miArbolCad.hermano();
						for (int i=0; i<hermano.length;i++)
						{
							if(!hermano[i][0].equals("No Tiene"))
							{
								hermanos=hermanos+" \n"+hermano[i][0]+"<-Hermanos-> "+hermano[i][1];
							}else
							{
								if(!hermano[i][1].equals("No Tiene"))
								{
									hermanos=hermanos+" \n"+hermano[i][1]+"<-Hermanos-> "+hermano[i][0];
								}
							}
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHERMANOS: \n"+hermanos);
						textImprecionA.setText(textImprecionA.getText()+"\n\nNIVELES: \n");
						for(int i =0 ; i<(miArbolCad.getAltura());i++)
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNivel "+i+" (Cant="+
									miArbolCad.mostrarNivel(i).trim().split(" ").length+") :"+miArbolCad.mostrarNivel(i));
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHOJAS (cant="+
								miArbolCad.getCantHojas()+") :"+miArbolCad.getHojas());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Derecho: \n"+miArbolCad.subArDer());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Izquiedo: \n"+miArbolCad.SubArIzq());
					
					}
					
				}else if(seleccionAvl.isSelected())
				{
					if(NumerosA.isSelected())
					{
						textImprecionA.setText("\tINFORMACION");
						textImprecionA.setText(textImprecionA.getText()+"\nRaiz: "+miArbolAVL.getRaiz().getElemento());
						textImprecionA.setText(textImprecionA.getText()+"\nAltura: "+miArbolAVL.getAltura());
						textImprecionA.setText(textImprecionA.getText()+"\nCantidad de Nodos: "+miArbolAVL.cantidadNodos());
						if(!miArbolAVL.soloesRaiz())
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNodos Internos Y Externos: \n"+miArbolAVL.nodosExtyInt());
						}else
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNodos Internos Y Externos:\n Solo esta el nodo raiz"
									+miArbolAVL.getRaiz().getElemento());	
						}
						String cadaHijo="";
						String[][]hijos=miArbolAVL.hijosCada();
						String [][]grado=new String[2][hijos.length];
						for (int i=0; i<hijos.length;i++)
						{
							int cont =0;
								cadaHijo=cadaHijo+" \n Numero: "+hijos[i][0]+"\n   ->hijo Izq: "+hijos[i][1]+"   ->hijo derecho: "+hijos[i][2];
								grado[0][i]=hijos[i][0];
								if(!hijos[i][1].trim().equals("No Hay"))
								{
									cont ++;
								}
								if(!hijos[i][2].trim().equals("No Hay"))
								{
									cont ++;
								}
								grado[1][i]=cont+"";
						}
						textImprecionA.setText(textImprecionA.getText()+"\nHIJOS \n"+cadaHijo);
						
						String niveles="\nGRADO \n";
						for (int i=0; i<hijos.length;i++)
						{
							niveles= niveles + " \n numero"+grado[0][i]+"  cant hijos :"+grado[1][i];
						}
						textImprecionA.setText(textImprecionA.getText()+niveles);
						String hermanos="";
						String[][]hermano=miArbolAVL.hermano();
						for (int i=0; i<hermano.length;i++)
						{
							if(!hermano[i][0].equals("No Tiene"))
							{
								hermanos=hermanos+" \n"+hermano[i][0]+"<-Hermanos-> "+hermano[i][1];
							}else
							{
								if(!hermano[i][1].equals("No Tiene"))
								{
									hermanos=hermanos+" \n"+hermano[i][1]+"<-Hermanos-> "+hermano[i][0];
								}
							}
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHERMANOS: \n"+hermanos);
						textImprecionA.setText(textImprecionA.getText()+"\n\nNIVELES: \n");
						for(int i =0 ; i<(miArbolAVL.getAltura());i++)
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNivel "+i+" (Cant="+
									miArbolAVL.mostrarNivel(i).trim().split(" ").length+") :"+miArbolAVL.mostrarNivel(i));
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHOJAS (cant="+miArbolAVL.getCantHojas()+") :"+miArbolAVL.getHojas());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Derecho: \n"+miArbolAVL.subArDer());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Izquiedo: \n"+miArbolAVL.SubArIzq());
					}else 
					if(LetrasA.isSelected())
					{
						textImprecionA.setText("\tINFORMACION");
						textImprecionA.setText(textImprecionA.getText()+"\nRaiz: "+miArbolAVLCad.getRaiz().getElemento());
						textImprecionA.setText(textImprecionA.getText()+"\nAltura: "+miArbolAVLCad.getAltura());
						textImprecionA.setText(textImprecionA.getText()+"\nCantidad de Nodos: "+miArbolAVLCad.cantidadNodos());
						if(!miArbolAVLCad.soloesRaiz())
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNodos Internos Y Externos: \n"+
									miArbolAVLCad.nodosExtyInt());
						}else
						{
							textImprecionA.setText(textImprecionA.getText()+
									"\nNodos Internos Y Externos:\n Solo esta el nodo raiz"+miArbolAVLCad.getRaiz().getElemento());	
						}
						String cadaHijo="";
						String[][]hijos=miArbolAVLCad.hijosCada();
						String [][]grado=new String[2][hijos.length];
						for (int i=0; i<hijos.length;i++)
						{
							int cont =0;
								cadaHijo=cadaHijo+" \n Elemento: "+hijos[i][0]+
										"\n   ->hijo Izq: "+hijos[i][1]+"   ->hijo derecho: "+hijos[i][2];
								grado[0][i]=hijos[i][0];
								if(!hijos[i][1].trim().equals("No Hay"))
								{
									cont ++;
								}
								if(!hijos[i][2].trim().equals("No Hay"))
								{
									cont ++;
								}
								grado[1][i]=cont+"";
						}
						textImprecionA.setText(textImprecionA.getText()+"\nHIJOS \n"+cadaHijo);
						
						String niveles="\nGRADO \n";
						for (int i=0; i<hijos.length;i++)
						{
							niveles= niveles + " \n Elemento "+grado[0][i]+"  cant hijos :"+grado[1][i];
						}
						textImprecionA.setText(textImprecionA.getText()+niveles);
						String hermanos="";
						String[][]hermano=miArbolAVLCad.hermano();
						for (int i=0; i<hermano.length;i++)
						{
							if(!hermano[i][0].equals("No Tiene"))
							{
								hermanos=hermanos+" \n"+hermano[i][0]+"<-Hermanos-> "+hermano[i][1];
							}else
							{
								if(!hermano[i][1].equals("No Tiene"))
								{
									hermanos=hermanos+" \n"+hermano[i][1]+"<-Hermanos-> "+hermano[i][0];
								}
							}
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHERMANOS: \n"+hermanos);
						textImprecionA.setText(textImprecionA.getText()+"\n\nNIVELES: \n");
						for(int i =0 ; i<(miArbolAVLCad.getAltura());i++)
						{
							textImprecionA.setText(textImprecionA.getText()+"\nNivel "+i+" (Cant="+
									miArbolAVLCad.mostrarNivel(i).trim().split(" ").length+") :"+miArbolAVLCad.mostrarNivel(i));
						}
						textImprecionA.setText(textImprecionA.getText()+"\n\n\nHOJAS (cant="+
								miArbolAVLCad.getCantHojas()+") :"+miArbolAVLCad.getHojas());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Derecho: \n"+miArbolAVLCad.subArDer());
						textImprecionA.setText(textImprecionA.getText()+"\nSubArbol Izquiedo: \n"+miArbolAVLCad.SubArIzq());
					
					}
				}
			}
		});
		btnGernerarArchivoA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
					String nomArch="", tipo1 ="",tipo2="";
					if(seleccionABB.isSelected()){
						if(NumerosA.isSelected()){
							nomArch="ABB.txt";
							tipo1= "numeros";
							tipo2="ABB";
						}else
						if(LetrasA.isSelected()){
							nomArch="ABBcad.txt";	
							tipo2="ABB";
							tipo1="letras";
						}
					}else if(seleccionAvl.isSelected())
					{
						if(NumerosA.isSelected()){
							nomArch="AVL.txt";
							tipo1= "numeros";
							tipo2="AVL";
						}else
						if(LetrasA.isSelected()){
							nomArch="AVLcad.txt";
							tipo2="AVL";
							tipo1="letras";
						}
					}
					if(!textImprecionA.getText().equals(""))
					{
						int resp=JOptionPane.showConfirmDialog(null,"Desea generar el Archivo de " +tipo2+"<"+tipo1+"> con la informacion en pantalla?");
						if (JOptionPane.OK_OPTION == resp){
							JOptionPane.showMessageDialog(null, "se ha guardado en el archivo en : "+nomArch,"informacion",JOptionPane.INFORMATION_MESSAGE);
							
					    	Archivo ArABB =new Archivo("entrada.txt",nomArch);
					    	String info=(textImprecionA.getText());
					    	//System.out.println(info);
					    	ArABB.ingresar(textImprecionA.getText());
					    	ArABB.cerrar();
					    	
					    }else if(JOptionPane.CANCEL_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted cancelo guardar en el Archivo");
					   }else if(JOptionPane.NO_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted No guardo en el archivo");
					   }
					}else{
						JOptionPane.showMessageDialog(null, "No se pudo guardar nada  Reacarge la Informacion"
								,"Error",JOptionPane.ERROR_MESSAGE);
					}
			}
		});
		btnLimpiarA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionA.setText("");
				int resp;
				if(seleccionABB.isSelected()){
					if(NumerosA.isSelected()){
						resp=JOptionPane.showConfirmDialog(null,"Desea borrar el arbol ABB<Numeros>?");
						if (JOptionPane.OK_OPTION == resp){
							JOptionPane.showMessageDialog(null, "Arbol Borrado .");
							miArbol.setRaiz(null);
							panelDibujoA.repaint();
					    }else if(JOptionPane.CANCEL_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Arbol");
					   }else if(JOptionPane.NO_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted No Borro el Arbol");
					   }
					}else
					if(LetrasA.isSelected()){
							resp=JOptionPane.showConfirmDialog(null,"Desea borrar el arbol ABB<Letras>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Arbol Borrado .");
								miArbolCad.setRaiz(null);
								panelDibujoA.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Arbol");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Arbol");
						   }
					}
				}else if(seleccionAvl.isSelected())
				{
					if(NumerosA.isSelected()){
						resp=JOptionPane.showConfirmDialog(null,"Desea borrar el arbol AVL<Numeros>?");
						if (JOptionPane.OK_OPTION == resp){
							JOptionPane.showMessageDialog(null, "Arbol Borrado .");
							miArbolAVL.setRaiz(null);
							panelDibujoA.repaint();
					    }else if(JOptionPane.CANCEL_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Arbol");
					   }else if(JOptionPane.NO_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted No Borro el Arbol");
					   }
					}else
					if(LetrasA.isSelected()){
							resp=JOptionPane.showConfirmDialog(null,"Desea borrar el arbol AVL<Letras>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Arbol Borrado .");
								miArbolAVLCad.setRaiz(null);
								panelDibujoA.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Arbol");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Arbol");
						   }
					}
				}
				
			}
		});
//sentencias de habilitacion o des habilitacion de botones por Tipo de Arbol elegido con RadioButton
		seleccionAvl.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				textImprecionA.setText("");
				if(NumerosA.isSelected())
				{
					if (miArbolAVL.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}else if(LetrasA.isSelected())
				{
					if (miArbolAVLCad.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}
			}
		});
		seleccionABB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textImprecionA.setText("");
				if(NumerosA.isSelected())
				{
					if (miArbol.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}else if(LetrasA.isSelected())
				{
					if (miArbolCad.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}		
			}
		});

		NumerosA.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textImprecionA.setText("");
				if(seleccionAvl.isSelected())
				{
					if (miArbolAVL.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}else if(seleccionABB.isSelected())
				{
					if (miArbol.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}
			}
		});
		
		LetrasA.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textImprecionA.setText("");
				if(seleccionAvl.isSelected())
				{
					if (miArbolAVLCad.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}else if(seleccionABB.isSelected())
				{
					if (miArbolCad.getRaiz()==null)
					{
						btnEliminar.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnInformacionA.setEnabled(false);
						btnRecorridos.setEnabled(false);
						btnGernerarArchivoA.setEnabled(false);						
					}else
					{
						btnEliminar.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnInformacionA.setEnabled(true);
						btnRecorridos.setEnabled(true);
						btnGernerarArchivoA.setEnabled(true);	
					}
				}
			}
		});
/**
 * TODO fin de arboles
 */
		/**
		 * TODO Inicia Grafos
		 */
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionG.setText("");
				if (Vertice_o_Arista.getSelectedIndex()==0)//el primer item es de vertice
				{
					if(Elimina.isSelected())//eliminar Vertice
					{
						if(Dirigido.isSelected())
						{	
							if(NumerosG.isSelected())
							{
								if(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
								{
									GrafoDirigido.eliminar(Integer.parseInt(textUsuarioG1.getText()));
									JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
								}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
										JOptionPane.ERROR_MESSAGE);}
							}else 
							if (LetrasG.isSelected())
							{
								if(GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
								{
									GrafoDirigidoCad.eliminar((textUsuarioG1.getText()));
									JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
								}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
										JOptionPane.ERROR_MESSAGE);}
							}
						}else if(NoDirigido.isSelected())
						{
							if(NumerosG.isSelected())
							{
								if(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
								{
									GrafoNoDirigido.eliminar(Integer.parseInt(textUsuarioG1.getText()));
									JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
								}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
										JOptionPane.ERROR_MESSAGE);}
							}else 
							if (LetrasG.isSelected())
							{
								if(GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
								{
									GrafoNoDirigidoCad.eliminar((textUsuarioG1.getText()));
									JOptionPane.showMessageDialog(null, "Vertice eliminado con Exito:"+textUsuarioG1.getText());
								}else{JOptionPane.showMessageDialog(null, "El Vertice NO Existe : "+textUsuarioG1.getText(),"Error",
										JOptionPane.ERROR_MESSAGE);}
							}
						}else if(GBipartito.isSelected())
						{
							JOptionPane.showMessageDialog(null, "No Se puede eliminar un vertice por que es un Grafo ("+
									cant_V+" X "+cant_U+")","Error",
									JOptionPane.ERROR_MESSAGE);
						}
					}
					if(Inserta.isSelected())//insetrar vertice
					{
						if(Dirigido.isSelected())
						{
							if(NumerosG.isSelected())
							{
								if(!GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
								{
									GrafoDirigido.agregarVertices(Integer.parseInt(textUsuarioG1.getText()));
									JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
								}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
										JOptionPane.ERROR_MESSAGE);}
							}else 
							if (LetrasG.isSelected())
							{
								if(!GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
								{
									GrafoDirigidoCad.agregarVertices((textUsuarioG1.getText()));
									JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
								}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
										JOptionPane.ERROR_MESSAGE);}
							}
						}else
						if (NoDirigido.isSelected())
						{
									if(NumerosG.isSelected())
									{
										if(!GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
										{
											GrafoNoDirigido.agregarVertices(Integer.parseInt(textUsuarioG1.getText()));
											JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
										}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",
												JOptionPane.ERROR_MESSAGE);}
									}else 
									if (LetrasG.isSelected())
									{
										if(!GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										{
											GrafoNoDirigidoCad.agregarVertices((textUsuarioG1.getText()));
											JOptionPane.showMessageDialog(null, "Vertice ingresado con Exito:"+textUsuarioG1.getText());
										}else{JOptionPane.showMessageDialog(null, "El Vertice ya Existe : "+textUsuarioG1.getText(),"Error",JOptionPane.ERROR_MESSAGE);}
									}
						}else if(GBipartito.isSelected())
						{
							JOptionPane.showMessageDialog(null, "No Se puede Agregar otro vertice por que Usted escogio un Grafo ("+
									cant_V+" X "+cant_U+")","Error",
									JOptionPane.ERROR_MESSAGE);
						}
						
					}
				}if (Vertice_o_Arista.getSelectedIndex()==1)//el segundo item es de arista
				{
					if(Edita.isSelected())//edita peso de arista
					{
						if(Dirigido.isSelected())
						{
							if(NumerosG.isSelected())
							{
								int vertice1=Integer.parseInt(textUsuarioG1.getText());
								int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
								if((GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
										&&(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
								{
									String []mensaje=(GrafoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
									if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
									{	
										int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
									            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
									            mensaje, mensaje[0]);
	
										try{
											int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
											GrafoDirigido.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
											mensaje=(GrafoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
											JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
													+vertice1+" y "+vertice2+"quedan asi:\n\n ", "Actualizacion",
													JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
											
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
										E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
										}
									}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
											"Error",JOptionPane.ERROR_MESSAGE);}
								}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
							
							}else 
							if (LetrasG.isSelected())
							{
								String vertice1=(textUsuarioG1.getText());
								String vertice2=(textUsuarioG2.getText());
								if((GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										&&(GrafoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
								{
									String []mensaje=(GrafoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
									if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
									{	
										int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
									            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
									            mensaje, mensaje[0]);
	
										try{
											int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
											GrafoDirigidoCad.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
											mensaje=(GrafoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
											JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
													+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
													JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
											
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
										E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
										}
									}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
											"Error",JOptionPane.ERROR_MESSAGE);}
								}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
							
							}
						}else if(NoDirigido.isSelected())
						{ 
							if(NumerosG.isSelected())
							{
								int vertice1=Integer.parseInt(textUsuarioG1.getText());
								int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
								if((GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
										&&(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
								{
									String []mensaje=(GrafoNoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
									if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
									{	
										int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
									            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
									            mensaje, mensaje[0]);
	
										try{
											int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
											GrafoNoDirigido.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
											mensaje=(GrafoNoDirigido.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
											JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
													+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
													JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
											
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
										E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
										}
									}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
											"Error",JOptionPane.ERROR_MESSAGE);}
								}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
							
							}else 
							if (LetrasG.isSelected())
							{
								String vertice1=(textUsuarioG1.getText());
								String vertice2=(textUsuarioG2.getText());
								if((GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										&&(GrafoNoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
								{
									String []mensaje=(GrafoNoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
									if(!"   No hay vertices asociados a este valor ".trim().equals(mensaje[0]))
									{	
										int seleccion = JOptionPane.showOptionDialog(null, "Seleccione Arista: ","Arista",
									            JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
									            mensaje, mensaje[0]);
	
										try{
											int nuevoPeso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Nuevo peso de la Arista: "));
											GrafoNoDirigidoCad.cambiarPeso(vertice1, vertice2,nuevoPeso, seleccion);
											mensaje=(GrafoNoDirigidoCad.mostrarAristasEntreVertices(vertice1, vertice2)).trim().split("\n");
											JOptionPane.showInputDialog(null,"Modificacion Exitosa Las aristas entre el vertice"
													+vertice1+" y "+vertice1+"quedan asi:\n\n ", "Actualizacion",
													JOptionPane.INFORMATION_MESSAGE, null,mensaje, mensaje[0]);
											
										}catch(Exception E)
										{
											JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+
										E.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
										}
									}else{JOptionPane.showMessageDialog(null,"Error: No Hay Arista Entre Los Vertices",
											"Error",JOptionPane.ERROR_MESSAGE);}
								}else{JOptionPane.showMessageDialog(null,"Error: Algun Vertice No Existe","Error",JOptionPane.ERROR_MESSAGE);}
							
							}
						}else if(GBipartito.isSelected())
						{
							JOptionPane.showMessageDialog(null,"Error: No puede editar su peso por que es un Grafo No Ponderado",
									"Error",JOptionPane.ERROR_MESSAGE);
						}
					}
					String mensaje="";
					//TODO: inserta arista
					if(Inserta.isSelected())//inserta arista
					{
						if(Dirigido.isSelected())
						{
							if(NumerosG.isSelected())
							{
								int vertice1=Integer.parseInt(textUsuarioG1.getText());
								int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
								if((GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
										&&(GrafoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
								{
									try{
										int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
										int val =GrafoDirigido.agregarAristaDirigido(vertice1, vertice2,peso);
										if(val==0)
											JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
									}catch(Exception E)
									{
										JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
									}
								}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
							}else 
							if (LetrasG.isSelected())
							{
								if((GrafoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										&&(GrafoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
								{
									try{
										int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
										int val =GrafoDirigidoCad.agregarAristaDirigido((textUsuarioG1.getText()), (textUsuarioG2.getText()),peso);
										if(val==0)
											JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
									}catch(Exception E)
									{
										JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
									}
								}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
							}
						}else
						if (NoDirigido.isSelected())
						{
							if(NumerosG.isSelected())
							{
								int vertice1=Integer.parseInt(textUsuarioG1.getText());
								int vertice2=(Integer.parseInt(textUsuarioG2.getText()));
								if((GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG1.getText())))
										&&(GrafoNoDirigido.verificarVertices(Integer.parseInt(textUsuarioG2.getText()))))
								{
									try{
										int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
										int val =GrafoNoDirigido.agregarAristaNoDirigido(vertice1, vertice2,peso);
										if(val==0)
											JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
									}catch(Exception E)
									{
										JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
									}
								}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
							}else 
							if (LetrasG.isSelected())
							{
								if((GrafoNoDirigidoCad.verificarVertices((textUsuarioG1.getText())))
										&&(GrafoNoDirigidoCad.verificarVertices((textUsuarioG2.getText()))))
								{
									try{
										int peso=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la Arista: "));
										int val =GrafoNoDirigidoCad.agregarAristaNoDirigido((textUsuarioG1.getText()), (textUsuarioG2.getText()),peso);
										if(val==0)
											JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
									}catch(Exception E)
									{
										JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
									}
								}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
							}
						}else if(GBipartito.isSelected())
						{
							if((bipartito.verificarVertices((textUsuarioG1.getText())))
									&&(bipartito.verificarVertices((textUsuarioG2.getText()))))
							{
								try{
									
									int val =bipartito.agregarAristaNoDirigido((textUsuarioG1.getText()), (textUsuarioG2.getText()));
									if(val==0)
										JOptionPane.showMessageDialog(null,"Arista Ingresada con exito");
								}catch(Exception E)
								{
									JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles:"+ E.getMessage());
								}
							}else{JOptionPane.showMessageDialog(null,"Error: no se pudo crear la arista \n\n Detalles: Algun Vertice No Existe");}
						}
					}
				}
				
				Desactiva.doClick();
			}
			
			
		}
		
		);
		//TODO:
		btnInformacionG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionG.setText("");
				if(Dirigido.isSelected())
				{
					 if (LetrasG.isSelected())
					 {
						 String []vertices=GrafoDirigidoCad.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoDirigidoCad.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoDirigidoCad.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoDirigidoCad.mostrarAristasEntreVertices((vertice1),(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoDirigidoCad.grado((num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoDirigidoCad.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE ADYACENCIA :\n "+GrafoDirigidoCad.imprimeMatrizAdyacencia());						 
					 
						 
					 }else if (NumerosG.isSelected())
					 {
						 String []vertices=GrafoDirigido.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoDirigido.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoDirigido.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoDirigido.mostrarAristasEntreVertices(Integer.parseInt(vertice1),
										 Integer.parseInt(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoDirigido.grado(Integer.parseInt(num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoDirigido.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE ADYACENCIA :\n "+GrafoDirigido.imprimeMatrizAdyacencia());						 
					 }
				}else if(NoDirigido.isSelected())
				{
					if (LetrasG.isSelected())
					 {
						 String []vertices=GrafoNoDirigidoCad.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoNoDirigidoCad.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoNoDirigidoCad.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoNoDirigidoCad.mostrarAristasEntreVertices((vertice1),(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoNoDirigidoCad.grado((num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoNoDirigidoCad.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE ADYACENCIA :\n "+
						 GrafoNoDirigidoCad.imprimeMatrizAdyacencia());						 
					 
						 
					 }else if (NumerosG.isSelected())
					 {
						 String []vertices=GrafoNoDirigido.imprimirVertices().trim().split(" ");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
						 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
						 textImprecionG.setText(textImprecionG.getText()+"  V ={"+GrafoNoDirigido.imprimirVertices()+"}\n");
						 textImprecionG.setText(textImprecionG.getText()+"  E ={"+GrafoNoDirigido.verticesFormalmento()+"}\n");
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
						 for (String vertice1: vertices){
							 for (String vertice2: vertices){ 
								 String defecto="   No hay vertices asociados a este valor ".trim();
								 String resivido=GrafoNoDirigido.mostrarAristasEntreVertices(Integer.parseInt(vertice1),
										 Integer.parseInt(vertice2)).trim();
								 if(!defecto.equals(resivido))
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
									 String []Aristas=resivido.trim().split("\n");	
									 for(String arista:Aristas)
									 {
										 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
									 }
								 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
								
							 }
						 }
						 
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
						 for (String num: vertices){
							 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+GrafoNoDirigido.grado(Integer.parseInt(num)));
						 }
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+GrafoNoDirigido.grafoCompleto());
						 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE ADYACENCIA :\n "+GrafoNoDirigido.imprimeMatrizAdyacencia());						 
					 }
				}else if(GBipartito.isSelected())
				{
					 String []vertices=bipartito.imprimirVertices().trim().split(" ");
					 
					 textImprecionG.setText(textImprecionG.getText()+"\t INFORMACION:");
					 textImprecionG.setText(textImprecionG.getText()+"\n Exprecion Formal:\n");
					 textImprecionG.setText(textImprecionG.getText()+"  V ={"+bipartito.imprimirVertices()+"}\n");
					 textImprecionG.setText(textImprecionG.getText()+"  E ={"+bipartito.verticesFormalmento()+"}\n");
					 
					 textImprecionG.setText(textImprecionG.getText()+"\n\n Vertices y Vertices Adyacentes (VA) :\n".toUpperCase());
					 for (String vertice1: vertices){
						 for (String vertice2: vertices){
							 String defecto="   No hay vertices asociados a este valor ".trim();
							 String resivido=bipartito.mostrarAristasEntreVertices((vertice1),(vertice2)).trim();
							 if(!defecto.equals(resivido))
							 {
								 textImprecionG.setText(textImprecionG.getText()+"\n* Vertice: "+vertice1); 
								 String []Aristas=resivido.trim().split("\n");	
								 for(String arista:Aristas)
								 {
									 textImprecionG.setText(textImprecionG.getText()+"\n  "+arista.substring(3));
								 }
							 }else{/*textImprecionG.setText(textImprecionG.getText()+"  VA: "+vertice2 +"->No Hay Arista enter estos Vertices");*/}
							
						 }
					 }
					 
					 textImprecionG.setText(textImprecionG.getText()+"\n\n\nGRADO :");
					 for (String num: vertices){
						 textImprecionG.setText(textImprecionG.getText()+"\n  Vertice: "+num+", "+bipartito.grado((num)));
					 }
					 textImprecionG.setText(textImprecionG.getText()+"\n\n\nCOMPLETO : "+bipartito.grafoCompleto());
					 textImprecionG.setText(textImprecionG.getText()+"\n\n\n\nMATRIZ DE ADYACENCIA :\n "+
							 bipartito.imprimeMatrizAdyacencia());
					
				}
			}
		});
		btnLimpiarG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textImprecionG.setText("");
				int resp;
				if(NoDirigido.isSelected())
				{
					 if(NumerosG.isSelected())
					 {
						 resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo No Dirigido<Numeros>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Grafo Borrado .");

								 GrafoNoDirigido=new Grafo();
								panelDibujoG.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
						   }
					 }else if(LetrasG.isSelected())
					 {
						 resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo No Dirigido<Letras>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Grafo Borrado .");
								 GrafoNoDirigidoCad=new Grafo();
								panelDibujoG.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
						   }
					 }
				}else
				if(Dirigido.isSelected())
				{
					if(NumerosG.isSelected())
					 {
						resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo Dirigido<Numeros>?");
						if (JOptionPane.OK_OPTION == resp){
							JOptionPane.showMessageDialog(null, "Grafo Borrado .");

							 GrafoDirigido=new Grafo();
							panelDibujoG.repaint();
					    }else if(JOptionPane.CANCEL_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
					   }else if(JOptionPane.NO_OPTION==resp)
					    {
					    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
					   }
					 }else if(LetrasG.isSelected())
					 {
						 resp=JOptionPane.showConfirmDialog(null,"Desea borrar el Grafo Dirigido<Letras>?");
							if (JOptionPane.OK_OPTION == resp){
								JOptionPane.showMessageDialog(null, "Grafo Borrado .");
								 GrafoDirigidoCad=new Grafo();
								panelDibujoG.repaint();
						    }else if(JOptionPane.CANCEL_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted cancelo Borrar Grafo");
						   }else if(JOptionPane.NO_OPTION==resp)
						    {
						    	JOptionPane.showMessageDialog(null, "Usted No Borro el Grafo");
						   }
					 }
				}else if(GBipartito.isSelected()){JOptionPane.showMessageDialog(null, "Usted Usted Podra Borrar el Grafo Bipartico seleccionando "
						+ "de nuevo a �G.Bipartito� ");}
				Desactiva.doClick();
			}
		});
		btnGenerarArchivoG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomArch="", tipo1 ="",tipo2="";
				if(Dirigido.isSelected()){
					if(NumerosG.isSelected()){
						nomArch="Dirigido.txt";
						tipo1= "numeros";
						tipo2="Dirigido";
					}else
					if(LetrasG.isSelected()){
						nomArch="Dirigidocad.txt";	
						tipo2="Dirigido";
						tipo1="letras";
					}
				}else if(NoDirigido.isSelected())
				{
					if(NumerosG.isSelected()){
						nomArch="NoDirigido.txt";
						tipo1= "numeros";
						tipo2="No Dirigido";
					}else
					if(LetrasG.isSelected()){
						nomArch="NoDirigidocad.txt";
						tipo2="No Dirigido";
						tipo1="letras";
					}
				}else if(GBipartito.isSelected())
				{
					nomArch="bipartito.txt";
					tipo1= cant_V+","+cant_U;
					tipo2="Bipartito";
				}
				
				if(!textImprecionG.getText().equals(""))
				{
					int resp=JOptionPane.showConfirmDialog(null,"Desea generar el Archivo de Grafo " +tipo2+"<"+tipo1+"> con la informacion en pantalla?");
					if (JOptionPane.OK_OPTION == resp){
						JOptionPane.showMessageDialog(null, "se ha guardado en el archivo en : "+nomArch,"informacion",JOptionPane.INFORMATION_MESSAGE);
						
				    	Archivo miArch =new Archivo("entrada.txt",nomArch);
				    	String info=(textImprecionG.getText());
				    	//System.out.println(info);
				    	miArch.ingresar(info);
				    	miArch.cerrar();
				    	
				    }else if(JOptionPane.CANCEL_OPTION==resp)
				    {
				    	JOptionPane.showMessageDialog(null, "Usted cancelo guardar en el Archivo");
				   }else if(JOptionPane.NO_OPTION==resp)
				    {
				    	JOptionPane.showMessageDialog(null, "Usted No guardo en el archivo");
				   }
				}else{
					JOptionPane.showMessageDialog(null, "No se pudo guardar nada  Reacarge la Informacion"
							,"Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
//modificacion del estado enable desde el combobox
		Vertice_o_Arista.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(GBipartito.isSelected())
				{
					textUsuarioG1.setText("V_");
					textUsuarioG2.setText("U_");
				}
				Desactiva.doClick();			
			}
			
		});
		Dirigido.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Desactiva.doClick();
				textImprecionG.setText("");
				textUsuarioG1.setText("");
				textUsuarioG2.setText("");
				
			}
		});

		NoDirigido.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Desactiva.doClick();
				textImprecionG.setText("");
				textUsuarioG1.setText("");
				textUsuarioG2.setText("");
			}
		});
		NumerosG.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(GBipartito.isSelected())
				{
					NumerosG.setSelected(false);
					LetrasG.setSelected(true);
					JOptionPane.showMessageDialog(null, "No Es Posible este Cambio De Tipo En El Grafo Bipartito","Error", JOptionPane.CANCEL_OPTION);
				}else{
					Desactiva.doClick();
					textImprecionG.setText("");
					textUsuarioG1.setText("");
					textUsuarioG2.setText("");
				}
			}
		});
		LetrasG.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Desactiva.doClick();
				textImprecionG.setText("");
				textUsuarioG1.setText("");
				textUsuarioG2.setText("");				
			}
		});
		Desactiva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelDibujoG.repaint();
				Dibujar.doClick();
				if (Vertice_o_Arista.getSelectedIndex()==0)
				{
					if(Dirigido.isSelected()){
						if(NumerosG.isSelected()){
							if(GrafoDirigido.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
								
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
							}
						}else
						if(LetrasG.isSelected()){
							if(GrafoDirigidoCad.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
							}
						}
					}else if(NoDirigido.isSelected())
					{
						if(NumerosG.isSelected()){
							if(GrafoNoDirigido.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
								
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
							}
						}else
						if(LetrasG.isSelected()){
							if(GrafoNoDirigidoCad.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(true);
								Edita.setEnabled(false);
								textUsuarioG2.setEnabled(false);
							}
						}
						
					}else if (GBipartito.isSelected())
					{
						if(bipartito.cantidadVertices()==0)
						{
							btnGenerarArchivoG.setEnabled(false);
							btnInformacionG.setEnabled(false);
							Elimina.setEnabled(true);
							Edita.setEnabled(false);
							textUsuarioG2.setEnabled(false);
						}else{
							btnGenerarArchivoG.setEnabled(true);
							btnInformacionG.setEnabled(true);
							Elimina.setEnabled(true);
							Edita.setEnabled(false);
							textUsuarioG2.setEnabled(false);
						}
					}
				}
				if (Vertice_o_Arista.getSelectedIndex()==1)
				{
					if(Dirigido.isSelected()){
						if(NumerosG.isSelected()){
							if(GrafoDirigido.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
								
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
							}
						}else
						if(LetrasG.isSelected()){
							if(GrafoDirigidoCad.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
							}
						}
					}else if(NoDirigido.isSelected())
					{
						if(NumerosG.isSelected()){
							if(GrafoNoDirigido.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
								
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
							}
						}else
						if(LetrasG.isSelected()){
							if(GrafoNoDirigidoCad.cantidadVertices()==0)
							{
								btnGenerarArchivoG.setEnabled(false);
								btnInformacionG.setEnabled(false);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
							}else{
								btnGenerarArchivoG.setEnabled(true);
								btnInformacionG.setEnabled(true);
								Elimina.setEnabled(false);
								Edita.setEnabled(true);
								textUsuarioG2.setEnabled(true);
							}
						}
						
					}else if (GBipartito.isSelected())
					{
						if(bipartito.cantidadVertices()==0)
						{
							btnGenerarArchivoG.setEnabled(false);
							btnInformacionG.setEnabled(false);
							Elimina.setEnabled(false);
							Edita.setEnabled(true);
							textUsuarioG2.setEnabled(true);
						}else{
							btnGenerarArchivoG.setEnabled(true);
							btnInformacionG.setEnabled(true);
							Elimina.setEnabled(false);
							Edita.setEnabled(true);
							textUsuarioG2.setEnabled(true);
						}
					}
				
				}
			}
		});
		
		Dibujar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Se graficara en la zona de imprecion", "Graficar",JOptionPane.WARNING_MESSAGE);
				Graphics k=panelDibujoG.getGraphics();
				Dibujo t=new Figura();
				int cantV=0;//CANTIDAD VERTICES
				if(Dirigido.isSelected()){
					if(NumerosG.isSelected()){
						if(GrafoDirigido.cantidadVertices()!=0)
						{
							String[] vertices= GrafoDirigido.imprimirVertices().trim().split(" ");
							cantV=GrafoDirigido.cantidadVertices();
							String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
							int cantMP=calcularTamMatriz(cantV);
							String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
							boolean salirSegundoBucle=false;
							int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
							for (int i=0 ; i<cantMP;i++)
							{
								for (int j=0 ; j<cantMP;j++)
								{
									int datosLlenos=i*cantMP+(j+1);
									if (datosLlenos>cantV)
									{
										salirSegundoBucle=true;
										break;
									}else
									{									
										DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
										DatosDibujo[datoIngresado][1]=String.valueOf(i);
										DatosDibujo[datoIngresado][2]=String.valueOf(j);
										datoIngresado++;
									}
								}
								if(salirSegundoBucle)
									break;
							}
							int acumulador=0;
							int disparador=-1;
							for(int i =0;i<cantV;i++)
							{
								
								if(DatosDibujo[i][0]!=null)
								{
									
									int x=Integer.parseInt(DatosDibujo[i][1]);
									int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									if(x!=disparador){acumulador=0;}
									if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
									if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
									 
									y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									DatosDibujo[i][2]=y+"";//este es opcional 
									//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
									t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
								}
							}
							String[]aristasG;
							if(!GrafoDirigido.verticesFormalmento().equals(""))
							{
								String cadenaAristas=GrafoDirigido.verticesFormalmento().trim().replace("(", "").replace(")", " ");
								//System.out.println(""+cadenaAristas);
								aristasG=cadenaAristas.split(" ");
								
								for(String arista:aristasG)
								{
									String[]inicioFin=arista.split(",");
									if((GrafoDirigido.verificarVertices(Integer.parseInt(inicioFin[0])))&&
											(GrafoDirigido.verificarVertices(Integer.parseInt(inicioFin[0]))))
									{
										String[] inicio=posicion(DatosDibujo,inicioFin[0]);
										String[] fin=posicion(DatosDibujo,inicioFin[1]);
										//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
										t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
												Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
									}
								}
							}
								/*
							for(int i =0;i<aristasG.length;i++)
							{
								t.linea(k,DatosDibujo[i][0], Integer.parseInt(DatosDibujo[i][1]),Integer.parseInt(DatosDibujo[i][2]),Integer.parseInt(DatosDibujo[i][4]),Integer.parseInt(DatosDibujo[i][5]));
							}*/
						}
					}else
					if(LetrasG.isSelected())
					{
						if(GrafoDirigidoCad.cantidadVertices()!=0)
						{
							String[] vertices= GrafoDirigidoCad.imprimirVertices().trim().split(" ");
							cantV=GrafoDirigidoCad.cantidadVertices();
							String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
							int cantMP=calcularTamMatriz(cantV);
							String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
							boolean salirSegundoBucle=false;
							int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
							for (int i=0 ; i<cantMP;i++)
							{
								for (int j=0 ; j<cantMP;j++)
								{
									int datosLlenos=i*cantMP+(j+1);
									if (datosLlenos>cantV)
									{
										salirSegundoBucle=true;
										break;
									}else
									{									
										DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
										DatosDibujo[datoIngresado][1]=String.valueOf(i);
										DatosDibujo[datoIngresado][2]=String.valueOf(j);
										datoIngresado++;
									}
								}
								if(salirSegundoBucle)
									break;
							}
							int acumulador=0;
							int disparador=-1;
							for(int i =0;i<cantV;i++)
							{
								
								if(DatosDibujo[i][0]!=null)
								{
									
									int x=Integer.parseInt(DatosDibujo[i][1]);
									int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									if(x!=disparador){acumulador=0;}
									if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
									if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
									 
									y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									DatosDibujo[i][2]=y+"";//este es opcional 
									//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
									t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
								}
							}
							String[]aristasG;
							if(!GrafoDirigidoCad.verticesFormalmento().equals(""))
							{
								String cadenaAristas=GrafoDirigidoCad.verticesFormalmento().trim().replace("(", "").replace(")", " ");
								//System.out.println(""+cadenaAristas);
								aristasG=cadenaAristas.split(" ");
								
								for(String arista:aristasG)
								{
									String[]inicioFin=arista.split(",");
									if((GrafoDirigidoCad.verificarVertices((inicioFin[0])))&&
											(GrafoDirigidoCad.verificarVertices((inicioFin[0]))))
									{
										String[] inicio=posicion(DatosDibujo,inicioFin[0]);
										String[] fin=posicion(DatosDibujo,inicioFin[1]);
										//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
										t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
												Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
									}
								}
							}
						}
					}		
				}else if(NoDirigido.isSelected())
				{
					if(NumerosG.isSelected()){
						if(GrafoNoDirigido.cantidadVertices()!=0)
						{
							String[] vertices= GrafoNoDirigido.imprimirVertices().trim().split(" ");
							cantV=GrafoNoDirigido.cantidadVertices();
							String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
							int cantMP=calcularTamMatriz(cantV);
							String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
							boolean salirSegundoBucle=false;
							int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
							for (int i=0 ; i<cantMP;i++)
							{
								for (int j=0 ; j<cantMP;j++)
								{
									int datosLlenos=i*cantMP+(j+1);
									if (datosLlenos>cantV)
									{
										salirSegundoBucle=true;
										break;
									}else
									{									
										DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
										DatosDibujo[datoIngresado][1]=String.valueOf(i);
										DatosDibujo[datoIngresado][2]=String.valueOf(j);
										datoIngresado++;
									}
								}
								if(salirSegundoBucle)
									break;
							}
							int acumulador=0;
							int disparador=-1;
							for(int i =0;i<cantV;i++)
							{
								
								if(DatosDibujo[i][0]!=null)
								{
									
									int x=Integer.parseInt(DatosDibujo[i][1]);
									int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									if(x!=disparador){acumulador=0;}
									if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
									if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
									 
									y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									DatosDibujo[i][2]=y+"";//este es opcional 
									//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
									t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
								}
							}
							String[]aristasG;
							if(!GrafoNoDirigido.verticesFormalmento().equals(""))
							{
								String cadenaAristas=GrafoNoDirigido.verticesFormalmento().trim().replace("(", "").replace(")", " ");
								//System.out.println(""+cadenaAristas);
								aristasG=cadenaAristas.split(" ");
								
								for(String arista:aristasG)
								{
									String[]inicioFin=arista.split(",");
									if((GrafoNoDirigido.verificarVertices(Integer.parseInt(inicioFin[0])))&&
											(GrafoNoDirigido.verificarVertices(Integer.parseInt(inicioFin[0]))))
									{
										String[] inicio=posicion(DatosDibujo,inicioFin[0]);
										String[] fin=posicion(DatosDibujo,inicioFin[1]);
										//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
										t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
												Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
									}
								}
							}
						}
					}else
					if(LetrasG.isSelected())
					{
						if(GrafoNoDirigidoCad.cantidadVertices()!=0)
						{
							String[] vertices= GrafoNoDirigidoCad.imprimirVertices().trim().split(" ");
							cantV=GrafoNoDirigidoCad.cantidadVertices();
							String[][] DatosDibujo=new String[cantV][6] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
							int cantMP=calcularTamMatriz(cantV);
							String[][] MatrizPosicion=new String[cantMP][cantMP];//�posiciona los vertices en una matiz cuadrada
							boolean salirSegundoBucle=false;
							int datoIngresado=0;//por que no en todos los recorridos de los bucles ingresa datos se crea un contador adicional
							for (int i=0 ; i<cantMP;i++)
							{
								for (int j=0 ; j<cantMP;j++)
								{
									int datosLlenos=i*cantMP+(j+1);
									if (datosLlenos>cantV)
									{
										salirSegundoBucle=true;
										break;
									}else
									{									
										DatosDibujo[datoIngresado][0]=vertices[datoIngresado];
										DatosDibujo[datoIngresado][1]=String.valueOf(i);
										DatosDibujo[datoIngresado][2]=String.valueOf(j);
										datoIngresado++;
									}
								}
								if(salirSegundoBucle)
									break;
							}
							int acumulador=0;
							int disparador=-1;
							for(int i =0;i<cantV;i++)
							{
								
								if(DatosDibujo[i][0]!=null)
								{
									
									int x=Integer.parseInt(DatosDibujo[i][1]);
									int y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									if(x!=disparador){acumulador=0;}
									if((x%2!=0)&&(y%2==0)){acumulador++;disparador=x;}
									if((x%2==0)&&(y%2!=0)){acumulador++;disparador=x;}
									 
									y=Integer.parseInt(DatosDibujo[i][2])+acumulador;
									DatosDibujo[i][2]=y+"";//este es opcional 
									//System.out.println(i+")"+DatosDibujo[i][0]+"-"+x+":"+y);
									t.grafico(k,DatosDibujo[i][0],x,y,"blanco");
								}
							}
							String[]aristasG;
							if(!GrafoNoDirigidoCad.verticesFormalmento().equals(""))
							{
								String cadenaAristas=GrafoNoDirigidoCad.verticesFormalmento().trim().replace("(", "").replace(")", " ");
								//System.out.println(""+cadenaAristas);
								aristasG=cadenaAristas.split(" ");
								
								for(String arista:aristasG)
								{
									String[]inicioFin=arista.split(",");
									if((GrafoNoDirigidoCad.verificarVertices((inicioFin[0])))&&
											(GrafoNoDirigidoCad.verificarVertices((inicioFin[0]))))
									{
										String[] inicio=posicion(DatosDibujo,inicioFin[0]);
										String[] fin=posicion(DatosDibujo,inicioFin[1]);
										//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
										t.prueba(k, Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
												Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
									}
								}
							}
						}
					}
				}else if(GBipartito.isSelected())
				{
					String[] vertices= bipartito.imprimirVertices().trim().split(" ");
					String[][] DatosDibujo=new String[vertices.length][3] ;//C0=valor; C1=Xpos;C2=Ypos;C3=;C4=;C5=;
					for (int i=0 ; i<vertices.length;i++)
					{								
						DatosDibujo[i][0]=vertices[i];
					}
					for(int i =0; i<cant_V;i++)
					{
						t.grafico(k,v[i],6,i,"blanco");
						DatosDibujo[i][1]=6+"";
						DatosDibujo[i][2]=i+"";
					}
					int y=0;
					for(int i =cant_V; i<(cant_V+cant_U);i++)
					{
						t.grafico(k,u[y],10,y,"blanco");
						
						DatosDibujo[i][1]=10+"";
						DatosDibujo[i][2]=y+"";
						y++;
					}
					String[]aristasG;
					if(!bipartito.verticesFormalmento().equals(""))
					{
						String cadenaAristas=bipartito.verticesFormalmento().trim().replace("(", "").replace(")", " ");
						//System.out.println(""+cadenaAristas);
						aristasG=cadenaAristas.split(" ");
						
						for(String arista:aristasG)
						{
							String[]inicioFin=arista.split(",");
							if((bipartito.verificarVertices((inicioFin[0])))&&
									(bipartito.verificarVertices((inicioFin[0]))))
							{
								String[] inicio=posicion(DatosDibujo,inicioFin[0]);
								String[] fin=posicion(DatosDibujo,inicioFin[1]);
								//t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
								t.linea(k,"", Integer.parseInt(inicio[0]),Integer.parseInt(inicio[1]),
										Integer.parseInt(fin[0]),Integer.parseInt(fin[1]));
							}
						}
					}
				}
			}
			public int calcularTamMatriz(int vertices)
			{
				int i;
				for( i =0; i<214748364; i++)
				{
					int tam=0;
					tam=i*i;
					if(tam>=vertices)
						break;
				}
				return (i);
			}
			public String[] posicion(String[][]datos, String buscando)
			{
				String[]poscicion=new String[2];
				for(int i =0;i<datos.length;i++)
				{
					if(datos[i][0].equals(buscando))
					{
						poscicion[0]=datos[i][1];
						poscicion[1]=datos[i][2];
						break;
					}					
				}
				return poscicion;
			}
		});
	
//Bipartitos
		GBipartito.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				bipartito =new Grafo();
				LetrasG.setSelected(true);
				//Vertice_o_Arista.setSelectedIndex(0);
				panelDibujoG.repaint();
				JComboBox numeros1 = new JComboBox();
				numeros1.setModel(new DefaultComboBoxModel(new String[] {"1","2","3","4","5","6"}));
				JComboBox numeros2 = new JComboBox();
				numeros2.setModel(new DefaultComboBoxModel(new String[] {"1","2","3","4","5","6"}));
				String msg = "elija el conjunto V y U para el bipartito VxU: ";
				Object[] msgMostrar= { msg, numeros1,numeros2 };
				JOptionPane.showConfirmDialog ( null,  msgMostrar, "Bipartito", JOptionPane.CLOSED_OPTION);
				cant_V=Integer.parseInt((String) numeros1.getSelectedItem());
				cant_U=Integer.parseInt((String) numeros2.getSelectedItem());
				v=new String[cant_V];
				u=new String[cant_U];
				for(int i =0; i<cant_V;i++)
				{
					v[i]="V_"+(i+1);
					bipartito.agregarVertices(v[i]);
				}
				for(int i =0; i<cant_U;i++)
				{
					u[i]="U_"+(i+1);
					bipartito.agregarVertices(u[i]);
				}
				Dibujar.doClick();		
				Vertice_o_Arista.setSelectedIndex(0);
			}
		});
	}
}
