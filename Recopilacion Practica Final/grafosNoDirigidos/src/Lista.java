


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author  Santiago Guzman  ,Janeth Cifuentes 
 */
public class Lista<T>
{
    private NodoLista Cabeza;
    private int tamanio;
    public Lista()
    {
        this.Cabeza=null;
    }
    public NodoLista getCabeza()
    {
        return Cabeza;
    }
    public boolean esvacia()
    {
        if(Cabeza==null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void agregar(T valor)
    {
        NodoLista<T> nuevo=new NodoLista<T>();
        nuevo.setValor(valor);
        if(esvacia()==true)
        {
            Cabeza = nuevo;
        }
        else
        {
            //creo un auxiliar para agregar al final de la lista
            NodoLista<T> auxiliar=Cabeza;
            while(auxiliar.getsiguiente() != null)
            {
                auxiliar = auxiliar.getsiguiente();
            }
            auxiliar.setSiguiente(nuevo);
        }
        tamanio++;
    }
    public boolean existelemento(T elemento)
    {
        
        NodoLista<T> aux = Cabeza;
        while(aux!= null)
        {
         if(aux.getValor()==elemento)
         {
           return true;  
         }
         aux=aux.getsiguiente();
        }
        return false;
    }
    public NodoLista buscarElemento(T elemento)
    {
         NodoLista<T> aux = Cabeza;
        while(aux!= null)
        {
         if(aux.getValor()==elemento)
         {
           return aux; 
         }
         aux=aux.getsiguiente();
        }
        return aux;
    }
    public void imprimirLista()
    {
        NodoLista<T> aux = Cabeza;

        while(aux != null)
        {
            System.out.print(aux.getValor()+"-> ");
         
            aux=aux.getsiguiente();
        }
        System.out.print("null");
     
       
    }
}
