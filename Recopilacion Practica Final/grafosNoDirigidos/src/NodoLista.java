/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author  Santiago Guzman  ,Janeth Cifuentes 
 
 */
public class NodoLista<T> 
{
    //Atributos
    private T valor;
    private NodoLista<T> siguiente;
    //constructores
    public NodoLista(Comparable elemento)
    {
        this.valor=valor;
        this.siguiente=null;
    }
    public NodoLista(T valor,NodoLista siguiente)
    {
       this.valor=valor;
       this.siguiente=siguiente;
    }
    public NodoLista()
    {
        
    }
    //getters y setters
    public NodoLista getsiguiente()
    {
        return siguiente;
    }
    public void setValor(T valor)
    {
        this.valor=valor;
    }

    public T getValor() {
        return valor;
    }

    public void setSiguiente(NodoLista<T> siguiente) {
        this.siguiente = siguiente;
    }
    

  
    
}
