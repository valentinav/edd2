
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Santiago
 */
public class Grafo<T>
{
    //atributos
    ArrayList<NodoGrafo> listavertices = new ArrayList<>();
    //constructores
   
    //getters y setters
    public ArrayList<NodoGrafo> getlistavertices()
    {
        return listavertices;
    }
    //metodos
    //primero Observo si existe un vertice
    public boolean verticesvacios()
    {
        
        if(listavertices==null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public boolean existeVertice (NodoGrafo vertice)
        {
            for(int i=0;i<listavertices.size();i++)
            {
                if(vertice.getElemento().equals(listavertices.get(i).getElemento()))
                {
                    return true;
                }
            }
            return false;
        }
    public NodoGrafo buscarVertice(NodoGrafo vertice)
    {
       for(int i=0;i<listavertices.size();i++)
            {
                if(vertice.getElemento().equals(listavertices.get(i).getElemento()))
                {
                    return listavertices.get(i);
                }
            }
            return null;
    }
    public void agregarvertices(NodoGrafo vertice)
    {
      
        if(existeVertice(vertice)!=true)
        {
         listavertices.add(vertice);
            System.out.println("vertice agregado con exito");
        }
        else
            System.out.println("el vertice ya existe ");  
    }
 
    
    public void agregarAyacencia(NodoGrafo vertice,NodoGrafo vertAyac)
    {
        
        if(listavertices!=null)
        {
            if(existeVertice(vertice)  && existeVertice(vertAyac))
                
            {
                NodoGrafo aux1=buscarVertice(vertice);
                NodoGrafo aux2=buscarVertice(vertAyac);
                
                if(!aux1.getListaNodos().existelemento(vertAyac.getElemento()))
                {
                 aux1.getListaNodos().agregar(vertAyac.getElemento());
                 if(vertice!= vertAyac)
                    {
                    aux2.getListaNodos().agregar(vertice.getElemento());
                    }

                    System.out.println("ayacencia creada");
                 }
                 else
                    System.out.println("esta ayacencia ya existe");
                
            }
            else
            System.out.println("no esxiste vertice o el cvertice de ayacencia");
        }
        else
        System.out.println("no existen vertices en el grafo");
    }
    
    

    public void imprimirAyacencia(NodoGrafo vertice)
    {
        if(listavertices!=null)
        {
            if(existeVertice(vertice))
                
                {
                 System.out.print(" vetice "+vertice.getElemento()+" lista ");
                 buscarVertice(vertice).getListaNodos().imprimirLista();  
                  
                }
            else
            System.out.println("vertice no existe");
        }
        else
        System.out.println("no existen vertices en el grafo");
    }
   
}
