/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Santiago Guzman  ,Janeth Cifuentes 
 */
public class NodoGrafo<T>
{
    //Atributos
    private Lista<T> ListaNodos;
    private T elemento;
    //constructores
    public NodoGrafo(T elemento)
    {
         this.ListaNodos = new Lista<T>();
        this.elemento=elemento;
    }
    //get y set de mis variables
    public Lista<T> getListaNodos()
    {
        return ListaNodos;
    }
    public void setListaNodos(T valor)
    {
        this.ListaNodos.agregar(valor);
    }
    public T getElemento()
    {
        return elemento;
    }
    public void setElemento(T elem)
    {
        this.elemento=elem;
    }
}
