/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author  Santiago Guzman  ,Janeth Cifuentes 
 */
public class Control {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Grafo g = new Grafo();
       
          NodoGrafo v1 = new NodoGrafo(1);
           g.agregarvertices(v1);
           
          NodoGrafo v2 = new NodoGrafo(2);
           g.agregarvertices(v2);
           
          NodoGrafo v3 = new NodoGrafo(3);
           g.agregarvertices(v3);
           
            
        g.existeVertice(v2);
        g.agregarAyacencia(v1, v2);
        g.agregarAyacencia(v2, v3);
        g.agregarAyacencia(v1, v3);
        g.agregarAyacencia(v2, v2);
         g.agregarAyacencia(v1, v3);
       
        
       
        g.imprimirAyacencia(v2);
        
        System.out.println("");
        
        g.imprimirAyacencia(v1);
        
        
        System.out.println("");
        
    }
    
}
