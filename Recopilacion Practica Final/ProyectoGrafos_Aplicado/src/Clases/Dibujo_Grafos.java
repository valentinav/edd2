package Clases;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.html.HTMLDocument.Iterator;

import Logica.Grafo;
import Logica.Kruskal;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Dibujo_Grafos extends javax.swing.JFrame {
	private JDesktopPane jDesktopPane1;
	private JButton MostrarG;
	private AbstractAction abstractAction1;
	private JButton jButton1;
	private JLabel jLabel1;
	private JTextField jTextField1;
	private JDesktopPane jDesktopPane2;
	private JSeparator jSeparator1;
	private JLabel lbl_grafo_PPal;
	private JButton btn_MAtAdyacencia2;
	private JButton btn_MAtAdyacencia;
	private JButton btn_GradoVertice;
	private JButton brn_montarInfo;
	private JLabel lbl_Recubrimineto;
	private JComboBox jComboBox_Recorridos;
	private JTextArea jTar_Informacion;
	private AbstractAction MostarG;
	private JScrollPane Scrll_Datos;
	List<circulo> ListLKruskal = new ArrayList<circulo>();
	
	lienzo lien= new lienzo(null);
	Grafo <String> objGrafo = new Grafo<String> ();
	/**
	* Auto-generated main method to display this JFrame
	*/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Dibujo_Grafos inst = new Dibujo_Grafos();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
				JOptionPane.showMessageDialog(null, "lea la informacion a su Derecha");
			}
		});
	}
	
	public Dibujo_Grafos() {
		super();
		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jDesktopPane1 = new JDesktopPane();
				getContentPane().add(jDesktopPane1);
				jDesktopPane1.setBounds(12, 29, 306, 178);
				lienzo l=new lienzo(jDesktopPane1);
				lien=l;
				ListLKruskal=l.getListCirculo() ;
				////
			
				objGrafo=l.objGrafo;
				///
				int numElementos =ListLKruskal.size();
				System.out.println("\n\nEl ArrayList tiene "+numElementos+" elementos");
				l.setBounds(0, 0, 306, 178);
			
				jDesktopPane1.add(l);
			}
			{
				MostrarG = new JButton();
				getContentPane().add(MostrarG);
				MostrarG.setText("Mostrar");
				MostrarG.setBounds(12, 435, 87, 23);
				MostrarG.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
					
						try {
							if(objGrafo.conexo()==true){
						//	if(objGrafo.conexo()==true){	
//	lienzo l=new lienzo(jDesktopPane1);
//	l.setBounds(0, 0, 400, 400);
//	jDesktopPane1.add(l);
							
							Grafo <String> ordenado_G = new Grafo<String> ();
							
							Grafo <String> ordenado_G1 = new Grafo<String> ();
							////
							
							Kruskal krus=new Kruskal();
							int[][] mat_O = null;
							try {
								mat_O = objGrafo. Ordenar_Insercion(objGrafo);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							int l=objGrafo.getFilas_k();
							try {
								int[][] mat_c=objGrafo.matrizCaminos();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
							try {
								ordenado_G= krus.aplicarKruskal(objGrafo, mat_O, l);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int i = 0; i < ordenado_G.listaVertices.getTamanio(); i++) {
								System.out.print((i+1)+".  ");
								try {
									ordenado_G.imprimirAdyacencias(i);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							
int[][] mat_A = null;

							try {
							//	 mat_A=ordenado_G.Ordenar_Insercion(ordenado_G);
								 mat_A=ordenado_G.MatrizSInOrden(ordenado_G);
								 
								
							;
							
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							int l1=ordenado_G.getFilas_k();
							
							   System.out.println("La Matriz es:");
							///
							for (int i = 0; i <l1 ; i++) {
								
							    System.out.println();
								for (int j = 0; j < 3; j++) {
								
									
									System.out.print(mat_A[i][j]+" ");
									
								}
								
								
							
							}
							
							
							int[][] mat_c1=ordenado_G.generarMatrizAdyacencias();
							/*
							 System.out.println("LA MATRIZ DE ADYACENCIAS ES:");
								///
								for (int i = 0; i <ordenado_G.listaVertices.getTamanio(); i++) {
									
								    System.out.println();
									for (int j = 0; j<ordenado_G.listaVertices.getTamanio(); j++) {
									
										
										System.out.print(mat_c1[i][j]+" ");
										
									}
								}
							*/
							
							
/*	mat_A=ordenado_G.ordenar_matP(mat_A, l1);
							
							  System.out.println("La Matriz Ordenada es:");
							  
							for (int i = 0; i <l1 ; i++) {
								
							    System.out.println();
								for (int j = 0; j < 3; j++) {
								
									
									System.out.print(mat_A[i][j]+" ");
									
								}
								
								
							
							}*/
							//
							
							
							  List<linea> ListArista = new ArrayList<linea>();
							
							 int	suma=0;
							for (int i = 0; i < l1; i++) {
								  // System.out.println("es"+i);
								   suma=suma+mat_A[i][0];
								for (int j = 0; j < 3; j++) {
									circulo a = ListLKruskal.get(mat_A[i][1]);
									circulo b= ListLKruskal.get(mat_A[i][2]);
									
							        linea lineas = new linea (a,b ,mat_A[i][0]);	
									ListArista.add(lineas);
								
								}
							}	
							
							
							
							
							
							///
							//int numElemento =ListLKruskal.size();
							//System.out.println("\n\nEl ArrayList tiene "+numElementos+" elementos");
							/*for(int i=0;i<ListLKruskal.size();i++)
							{
								circulo nodo=ListLKruskal.get(i);
 
								System.out.println("\nElemento "+nodo.n);
								System.out.println("\nCoordenada X "+nodo.x);
								System.out.println("\nCoordenada Y "+nodo.y);
							}*/
							
							Dibujo_Kruskal dg=new Dibujo_Kruskal(ListLKruskal,ListArista,lien);
						//	dg.setResizable(true);
							///**
							//int x = jDesktopPane1.getWidth()/2;
							//int y = jDesktopPane1.getHeight()/2;
							dg.setLocation(1, 1);
							//dg.setBounds(12, 76, 494, 282);
							//**
							dg.setSize(500, 400);
							Dimension dlgSize = dg.getPreferredSize(); 
							Dimension pantalla = getSize(); 
							Dimension ventana = dg.getSize() ; 
							//dg.setLocation((pantalla.width - dg.WIDTH ) / 2 , 
								//	(pantalla.height - dg.HEIGHT) / 2); 
			//	dg.setLocationRelativeTo(null) ; // CENTRA EL SEGUNDO FRAME EN LA PANTA 
							//dg.pack() ; 
							//dg.setResizable(true) ; // PERMITE REDIMENSIONARLO O AGRANDARL 
							jTextField1.setText(""+suma/2);
							
							dg.repaint();
							jDesktopPane2.add(dg);
					
							dg.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
                             }
							 else
						    		JOptionPane.showMessageDialog(null,"El Grafo no es Conexo no se puede aplicar kruskal", "informe de estado",
											JOptionPane.ERROR_MESSAGE);
							
							
						}
						
						
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}	
					
				});
			}
			
			{
				lbl_grafo_PPal = new JLabel();
				lbl_grafo_PPal.setText("Area de dibujo del Grafo");
				lbl_grafo_PPal.setBounds(12, 12, 143, 14);
				getContentPane().add(lbl_grafo_PPal);
			}
			
			
			{
				jDesktopPane2 = new JDesktopPane();
				getContentPane().add(jDesktopPane2);
				jDesktopPane2.setBounds(12, 218, 306, 178);//12, 29, 306, 178
			}
			{
				jTextField1 = new JTextField();
				getContentPane().add(jTextField1);
				jTextField1.setText(" ");
				jTextField1.setEditable(false);
				jTextField1.setBounds(181, 408, 84, 15);
			}
			{
				jLabel1 = new JLabel();
				getContentPane().add(jLabel1);
				jLabel1.setText("Longitud de Camino");
				jLabel1.setBounds(19, 407, 155, 17);
			}
			{
				jButton1 = new JButton();
				getContentPane().add(jButton1);
				jButton1.setText("Editar Arista");
				jButton1.setBounds(234, 435, 77, 23);
				jButton1.setAction(getAbstractAction1());
			}
			pack();
			this.setSize(338, 508);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	  private int ingresarPeso(){
	        String peso = JOptionPane.showInputDialog("Digite el nuevo peso de la arista");
	        int intPeso = 0;
	        try{
	            intPeso = Integer.parseInt(peso);  
	        }catch(Exception ex){
	            intPeso = ingresarPeso();
	        }
	        
	        return intPeso;
	    }
	
	private AbstractAction getAbstractAction1() {
		if(abstractAction1 == null) {
			abstractAction1 = new AbstractAction("Editar Arista", null) {
				public void actionPerformed(ActionEvent evt) {
					String ini = JOptionPane.showInputDialog("Nombre de circulo inicial");
					String fin = JOptionPane.showInputDialog("Nombre de circulo final");
					if (!ini.equals(fin)) {
						try {
							if (objGrafo.estaVertice(ini, false)) {
								try {
									if(objGrafo.estaVertice(fin, false)){
										int peso = ingresarPeso();
										try {
											
											lien.editar_pesoarista(ini, fin, peso);
									        repaint();
									        
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}else
									JOptionPane.showMessageDialog(null,"No Encontro un vertice", "informe de estado",
											JOptionPane.INFORMATION_MESSAGE);
								} catch (HeadlessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else
								JOptionPane.showMessageDialog(null,"No Encontro un vertice", "informe de estado",
										JOptionPane.INFORMATION_MESSAGE);
						} catch (HeadlessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else 
						JOptionPane.showMessageDialog(null,"Los nombres son iguales", "informe de estado",
								JOptionPane.ERROR_MESSAGE);
				}
			};
		}
		return abstractAction1;
	}

}