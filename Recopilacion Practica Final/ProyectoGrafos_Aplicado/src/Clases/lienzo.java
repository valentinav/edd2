
package Clases;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Logica.Grafo;
import Logica.Lista;

public class lienzo extends /*JPanel*/JPanel implements MouseListener,MouseMotionListener{

    int x=0,y=0;
    linea linea;
    circulo circulo;
    circulo jalada=null;
    List<circulo> ListCirculo = new ArrayList<circulo>();
    public List<circulo> getListCirculo() {
		return ListCirculo;
	}

	public void setListCirculo(List<circulo> listCirculo) {
		ListCirculo = listCirculo;
	}

	List<circulo> ListLKruskal = new ArrayList<circulo>();
    List<linea> ListArista = new ArrayList<linea>();
    List<linea> AristaKruskal = new ArrayList<linea>();
	Grafo <String> objGrafo = new Grafo<String> ();
	
    public Grafo<String> getObjGrafo() {
		return objGrafo;
	}

	public void setObjGrafo(Grafo<String> objGrafo) {
		this.objGrafo = objGrafo;
	}

	JDesktopPane p; 


    //   Principal p;

    public lienzo(JDesktopPane prin/*Principal prin*/) {
    	p=prin;
    
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.setVisible(true);
        this.setDoubleBuffered(true);
        
    }
    int i=0;
    public void anadirCirculo(int x, int y) throws Exception{
    	String ini = JOptionPane.showInputDialog("Digite el nombre del vertice");
		if (!objGrafo.estaVertice(ini, false)) {
			objGrafo.agregarVertices(ini);
			circulo = new circulo(ini, x, y,i);
			
			// circulo = new circulo(ListCirculo.size()+1,x,y);
			ListCirculo.add(circulo);
			ListLKruskal.add(circulo);
			repaint();
			p.repaint();
			i++;
			
		} else
    		JOptionPane.showMessageDialog(null,"El nombre de la ARISTA ya se encuentre asigando", "informe de estado",
					JOptionPane.ERROR_MESSAGE);
    		
    }
    public void anadirLinea( int x, int y){
        //try{
    	    int intPeso = ingresarPeso(x,y);
             linea = new linea (ListCirculo.get(x),ListCirculo.get(y),intPeso );
            this.ListArista.add(linea);
            
            repaint();
            p.repaint();
        /*}catch(IndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(null, "No se encontro circulo");
        }*/
    }

    @Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		
		for (linea f : ListArista) {
			f.painter(g);
		}
		
		for (circulo f : ListCirculo) {
			f.painter(g, this);
		}
	}
    
    //////////////
    
    
 public     List<circulo> lista_circulo()
 {
	return ListCirculo;
	 
 }
    
    
    
    
      private int ingresarPeso(int x , int y){
        String peso = JOptionPane.showInputDialog("Digite el peso.");
        int intPeso = 0;
        try{
            intPeso = Integer.parseInt(peso);  
            objGrafo.agregarAdyacencias(x, y,intPeso);
        }catch(Exception ex){
            intPeso = ingresarPeso(x,y);
        }
        
        return intPeso;
    }
      
 	 public void editar_pesoarista(String pos_i,String posf,int peso) throws Exception
	  	{
	  		boolean a;
	  		a=false;
	  		int posini=objGrafo.buscarPosicion(pos_i);
	  		int posfi=objGrafo.buscarPosicion(posf);
	  		Lista listaAux;
	  		listaAux = objGrafo.listaVertices.getValor(posini).getListaNodos();
	  		Lista listaAux1;
	  		listaAux1 = objGrafo.listaVertices.getValor(posfi).getListaNodos();
	  		
	  		
	  	
	  		for (int j = 0; j < listaAux.getTamanio(); j++) {
	  		
	  			if( listaAux.getValor(j).toString().compareTo(posf)==0)
	  			{
	  				
	  			//	
	  				System.out.println("\n\nInicial"+ListArista.get(posini).getInicial().getN()+" elementos");
	  			System.out.println("\n\nPEso q tenia "+ListArista.get(posini).getPeso()+" elementos");
	  				//int posfin=buscarPosicion(posf);
	  			//	listaAux.getValor(j).
	  				listaAux.setPeso(posf, peso);
	  				ListArista.get(j).setPeso(peso);
	  			}
	  		}
	  		
	  		for (int b = 0; b < listaAux1.getTamanio(); b++) {
	  			
				if( listaAux1.getValor(b).toString().compareTo(pos_i)==0)
	  			{
	  				//a=true;
					System.out.println("ARRAYLIST");
	  				System.out.println("\n\n Final "+ListArista.get(posini).getFfinal().getN()+" elementos");
	  				System.out.println("\n\nInicial"+ListArista.get(posini).getInicial().getN()+" elementos");
	  				System.out.println("\n\nPEso q tenia "+ListArista.get(posini).getPeso()+" elementos");
	  				//int posfin=buscarPosicion(posf);
	  				listaAux1.setPeso(pos_i, peso);
	  				ListArista.get(b).setPeso(peso);
	  			}
	  		}
	  				
	  	}
      
      
      

   ///////////
	public void mouseClicked(MouseEvent e) {
		try {
			if (e.isMetaDown()) { // bot�n derecho del rat�n
				String ini = JOptionPane.showInputDialog("Nombre de circulo inicial");
				String fin = JOptionPane.showInputDialog("Nombre de circulo final");
				
				if (!ini.equals(fin)) {
					if (objGrafo.estaVertice(ini, false)) {
						if(objGrafo.estaVertice(fin, false)){

						int ini1 = objGrafo.buscarPosicion(ini);
						int fin1 = objGrafo.buscarPosicion(fin);
						anadirLinea(ini1, fin1);
					}else
						JOptionPane.showMessageDialog(null,"No Encontro un vertice", "informe de estado",
								JOptionPane.INFORMATION_MESSAGE);
					}else
						JOptionPane.showMessageDialog(null,"No Encontro un vertice", "informe de estado",
								JOptionPane.INFORMATION_MESSAGE);
				}else 
					JOptionPane.showMessageDialog(null,"Los nombres son iguales", "informe de estado",
							JOptionPane.ERROR_MESSAGE);
					
			}

			else if (e.isAltDown()) { // bot�n de en medio del rat�n
				x = e.getX();
				y = e.getY();
				anadirCirculo(x, y);
			}

			else // bot�n izquierdo del rat�n
			{
				x = e.getX();
				y = e.getY();
				anadirCirculo(x, y);
			}

		} catch (Exception ex) {

		}
	}

    public void mousePressed(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {}

    public void mouseEntered(MouseEvent e) {}

    public void mouseExited(MouseEvent e) {}

    public void mouseDragged(MouseEvent e) {
       if(jalada==null)
        {
           for (circulo f:ListCirculo)
           {
             if(f.jaladopor(e.getPoint()))
               {
                 jalada=f;
               }
             x=e.getPoint().x;
             y=e.getPoint().y;
             repaint();
             p.repaint();
            }
       }
       else{
           jalada.transladar(e.getPoint().x-x,e.getPoint().y-y);
           x=e.getPoint().x;
           y=e.getPoint().y;
           repaint();
           p.repaint();
           }
    }

    public void mouseMoved(MouseEvent e){
        jalada=null;
    }

}