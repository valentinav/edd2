package Logica;

public class NodoGrafo<T> {

	public Lista<T> listaNodos;
	private T elemento;

	public NodoGrafo(T elemento) {
		this.listaNodos = new Lista<T>();
		this.elemento = elemento;
	}

	public Lista<T> getListaNodos() {
		return listaNodos;
	}

	public void setListaNodos(T valor) {
		this.listaNodos.agregar(valor);
	}

	public T getElemento() {
		return elemento;
	}

	public void setElemento(T elemento) {
		this.elemento = elemento;
	}
	
}// Fin clase NodoGrafo

