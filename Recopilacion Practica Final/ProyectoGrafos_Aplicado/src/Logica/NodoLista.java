package Logica;

public class NodoLista<T> {
	private T valor;
	public int pesoVertice;
	private NodoLista<T> siguiente;

	//constructores
	public NodoLista() {
		valor = null;
		siguiente = null;
		pesoVertice = 0;
	}
	public NodoLista(T elemento){
		valor = elemento;
		siguiente = null;
		pesoVertice =0;
	}
	
	//getters and setters
	public T getValor() {
		return valor;
	}

	public void setValor(T valor) {
		this.valor = valor;
	}

	public NodoLista<T> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoLista<T> siguiente) {
		this.siguiente = siguiente;
	}
	
	public int getPesoVertice() {
		return pesoVertice;
	}
	
	public void setPesoVertice(int pesoVertice) {
		this.pesoVertice = pesoVertice;
	}
	
	
	
}// Fin clase NodoLista

