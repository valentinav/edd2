/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Graficos;

import Colecciones_SEED.ListaCD;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

/**
 *
 *  @author Wilfred Uriel García  cod.1150204
 *          Yulieth Pabón Sánchez cod.1150159
 * 
 * Clase que representa el concepto de un Vértice Gráfico
 */
public class VerticeG {
   
    private char info;
    private ListaCD<VerticeG> vecinos;
    private VerticeG predecesor;
    private boolean esVisit;
    private Circle c;
    private Label l;

    public VerticeG(char info, Circle c, Label l) {
        this.info = info;
        this.c = c;
        this.l = l;
        this.vecinos = new ListaCD<VerticeG>();
        this.predecesor = null;
        this.esVisit = false;
    }

    public char getInfo() {
        return info;
    }

    public void setInfo(char info) {
        this.info = info;
    }

    public ListaCD<VerticeG> getVecinos() {
        return vecinos;
    }

    public void setVecinos(ListaCD<VerticeG> vecinos) {
        this.vecinos = vecinos;
    }

    public VerticeG getPredecesor() {
        return predecesor;
    }

    public void setPredecesor(VerticeG predecesor) {
        this.predecesor = predecesor;
    }

    public boolean isEsVisit() {
        return esVisit;
    }

    public void setEsVisit(boolean esVisit) {
        this.esVisit = esVisit;
    }

    public Circle getC() {
        return c;
    }

    public void setC(Circle c) {
        this.c = c;
    }

    public Label getL() {
        return l;
    }

    public void setL(Label l) {
        this.l = l;
    }

    public void insertarVecino(VerticeG v2) {
        this.vecinos.insertarAlFinal(v2);
    }

    public void eliminarVecino(VerticeG v2) {
        ListaCD<VerticeG> l = new ListaCD<VerticeG>();
        for(VerticeG vert: this.vecinos){
            if(vert.getInfo()!=v2.getInfo())
                l.insertarAlFinal(vert);
        }
        this.vecinos = l;
    }
    
    

    
}
