package Logica;

public class Lista<T> {
	private NodoLista<T> cabeza;
	private int tamanio;

	public int getTamanio() {
		return tamanio;
	}

	public Lista() {
		cabeza = null;
		tamanio = 0;
	}

	/**
	 * Consulta si la lista est� vacia o o
	 * 
	 * @return true cuando esta vacia, false cuando no
	 */
	public boolean esVacia() {
		return (cabeza == null);
	}

	/**
	 * Agrega un nuevo nodo al final de la lista
	 * 
	 * @param valor
	 *            valor a agregar
	 */
	public void agregar(T valor) {
		NodoLista<T> nuevo = new NodoLista<T>();
		nuevo.setValor(valor);
		if (esVacia()) {
			cabeza = nuevo;
		} else {
			// agregar al final de la lista
			NodoLista<T> aux = cabeza;
			while (aux.getSiguiente() != null) {
				aux = aux.getSiguiente();
			}
			aux.setSiguiente(nuevo);
			
		}
		tamanio++;
	}

	/**
	 * inserta un nuevo nodo en la lista
	 * 
	 * @param valor
	 *            valor a agregar
	 * @param pos
	 *            posicion donde se insertara el nodo
	 * @throws PosicionIlegalException
	 *             excepcion en caso que la posicion no exista
	 */
	public void insertar(T valor, int pos) throws Exception {
		if (pos >= 0 && pos <= tamanio) {
			NodoLista<T> nuevo = new NodoLista<T>();
			nuevo.setValor(valor);
			// el nodo a insertar esta al comienzo de la lista
			if (pos == 0) {
				nuevo.setSiguiente(cabeza);
				cabeza = nuevo;
			} else {
				// El nodo a insertar va al final de la lista
				if (pos == tamanio) {
					NodoLista<T> aux = cabeza;
					while (aux.getSiguiente() != null) {
						aux = aux.getSiguiente();
					}
					aux.setSiguiente(nuevo);
				} // el nodo a insertar esta en medio
				else {
					NodoLista<T> aux = cabeza;
					for (int i = 0; i < pos - 1; i++) {
						aux = aux.getSiguiente();
					}
					NodoLista<T> siguiente = aux.getSiguiente();
					aux.setSiguiente(nuevo);
					nuevo.setSiguiente(siguiente);
				}
			}
			tamanio++;
		} else {
			throw new Exception("posici�n ilegal en la lista");
		}
	}

	/**
	 * Devuelve el valor de una determinada posicion
	 * 
	 * @param pos
	 *            posicion
	 * @return el valor de tipo T
	 * @throws PosicionIlegalException
	 */
	public T getValor(int pos) throws Exception {
		if (pos >= 0 && pos < tamanio) {
			T valor;
			if (pos == 0) {
				valor = cabeza.getValor();
				return valor;
			} else {
				NodoLista<T> aux = cabeza;
				for (int i = 0; i < pos; i++) {
					aux = aux.getSiguiente();
				}
				valor = aux.getValor();
			}
			return valor;
		} else {
			throw new Exception("posici�n ilegal en la lista");
		}
	}
	/**
	 * 
	 * @param pos
	 * @return
	 * @throws Exception
	 */
	public int getpeso(int pos) throws Exception {
		if (pos >= 0 && pos < tamanio) {
			int valor;
			if (pos == 0) {
				valor = cabeza.getPesoVertice();
				return valor;
			} else {
				NodoLista<T> aux = cabeza;
				for (int i = 0; i < pos; i++) {
					aux = aux.getSiguiente();
				}
				valor = aux.getPesoVertice();
			}
			return valor;
		} else {
			throw new Exception("posici�n ilegal en la lista");
		}
	}
	/**
	 * 
	 * @param dato
	 * @param peso
	 * @throws Exception
	 */
	public void setPeso(String dato,int peso) throws Exception{
		NodoLista<T> aux = cabeza;
		for (int i = 0; i < tamanio; i++) {
			if (aux.getValor().toString().equals(dato)) {
				i = tamanio;
				aux.setPesoVertice(peso);
			}
			aux = aux.getSiguiente();
		}
	}

	/**
	 * Elimina un nodo en una determinada posicion
	 * 
	 * @param pos
	 *            posicion
	 * @throws PosicionIlegalException
	 */
	public void remover(int pos) throws Exception {
		if (pos >= 0 && pos < tamanio) {
			if (pos == 0) {
				// El nodo a eliminar esta en la primera posicion
				cabeza = cabeza.getSiguiente();
			} else {
				NodoLista<T> aux = cabeza;
				for (int i = 0; i < pos - 1; i++) {
					aux = aux.getSiguiente();
				}
				NodoLista<T> prox = aux.getSiguiente();
				aux.setSiguiente(prox.getSiguiente());
			}
			tamanio--;
		} else {
			throw new Exception("posici�n ilegal en la lista");
		}
	}

	/**
	 * Clear, elimina todos los nodos de la lista
	 */
	public void limpiar() {
		cabeza = null;
		tamanio = 0;
	}
	
	
	
	
	
	

}// Fin clase Lista
