package Logica;
import java.util.*;

import javax.swing.JOptionPane;


import Logica.Grafo;
import Logica.Lista;
import Logica.NodoGrafo;

public class Dijkstra {
	
	//similar a los defines de C++
		static final int MAX = 10005; 
		//maximo numero de v�rtices
		static final int INF = 1<<30;  
		//definimos un valor grande que represente la distancia infinita inicial, 
		//basta conque sea superior al maximo valor del peso en alguna de las aristas
		
		static Scanner sc = new Scanner( System.in );	   //para lectura de datos
		
		static List< List< Node > > ady = new ArrayList< List< Node > >(); //lista de adyacencia
		
		static int distancia[ ] = new int[ MAX ];          //distancia[ u ] distancia de v�rtice inicial a v�rtice con ID = u
		static boolean visitado[ ] = new boolean[ MAX ];   //para v�rtices visitados
		static int previo[] = new int[ MAX ];              //para la impresion de caminos
		
		//priority queue propia de Java, usamos el comparador definido para que el de menor valor este en el tope
		static PriorityQueue< Node > Q = new PriorityQueue<Node>(); 
		
		static int V;                //numero de vertices
		 
		
		// En el caso de java usamos una clase que representara el pair de C++
		static class Node implements Comparable<Node> {

			int first, second;

			Node(int d, int p) {
				// constructor

				this.first = d;

				this.second = p;
			}
	
			public int compareTo(Node other) {
				// es necesario definir un comparador para el correcto
				// funcionamiento
				// del PriorityQueue
				if (second > other.second)
					return 1;

				if (second == other.second)
					return 0;
				return -1;
			}
		}
		
		//funci�n de inicializaci�n
		static void init(){
		    for( int i = 0 ; i < V ; ++i ){
		        distancia[ i ] = INF;  //inicializamos todas las distancias con valor infinito
		        visitado[ i ] = false; //inicializamos todos los v�rtices como no visitados
		        previo[ i ] = -1;      //inicializamos el previo del vertice i con -1
		    }
		}

		public Grafo<String> dijkstra( int inicial, Grafo<String> g ) throws Exception{
			if(!g.listaVertices.esVacia()){
				V = g.listaVertices.getTamanio();
		    init(); //inicializamos nuestros arreglos
		    Q.add( new Node( inicial , 0 ) ); //Insertamos el v�rtice inicial en la Cola de Prioridad
		    distancia[ inicial ] = 0;      //Este paso es importante, inicializamos la distancia del inicial como 0
		    int actual , adyacente , peso;
		    while( !Q.isEmpty() ){                   //Mientras cola no este vacia
		        actual = Q.element().first;            //Obtengo de la cola el nodo con menor peso, en un comienzo ser� el inicial
		        Q.remove();                           //Sacamos el elemento de la cola
		        if( visitado[ actual ] ) 
		        	continue; //Si el v�rtice actual ya fue visitado entonces sigo sacando elementos de la cola
		        visitado[ actual ] = true;         //Marco como visitado el v�rtice actual
		        
		        int tamanio = g.listaVertices.getValor(actual).getListaNodosADY().getTamanio();
		        for( int i = 0 ; i < tamanio  ; ++i ){ //reviso sus adyacentes del vertice actual
					
					adyacente = g.buscarPosicion(g.listaVertices.getValor(actual).getListaNodosADY().getValor(i).toString());	        	
		        	peso = g.listaVertices.getValor(actual).getListaNodosADY().getpeso(i);
		        	
		            if( !visitado[ adyacente ] ){        //si el vertice adyacente no fue visitado
		                relajacion( actual , adyacente , peso ); //realizamos el paso de relajacion
		            }
		        }
		    }
		    
		    //creamos el nuevo grafo con las adyacencias
		    Grafo<String> grafoDJT = new Grafo<String>();
		    for (int i = 0; i < g.listaVertices.getTamanio(); i++) {
		    	grafoDJT.agregarVertices(g.getListaVertices().getValor(i).getElemento().toString());//agregamos vertices
		    	/*if(previo[i] != -1){     //controlamos q no hayan posciciones ilegales
		    		grafoDJT.agregarAdyacencias(i, previo[i], distancia[i]);  //agregamos adyacencias
		    	}*/
			}
		    for (int i = 0; i < g.listaVertices.getTamanio(); i++) {
		    	if(previo[i] != -1){     //controlamos q no hayan posciciones ilegales
		    		grafoDJT.agregarAdyacencias(i, previo[i], distancia[i]);  //agregamos adyacencias
		    	}
			}
		    return grafoDJT;
		}else{
			JOptionPane.showMessageDialog(null, "Aun no existe un grafo", "Informe de estado", JOptionPane.INFORMATION_MESSAGE);
		}
			
		return null;
		}
		
		//Paso de relajacion
		static void relajacion( int actual , int adyacente , int peso ){
		    //Si la distancia del origen al vertice actual + peso de su arista es menor a la distancia del origen al vertice adyacente
		    if( distancia[ actual ] + peso < distancia[ adyacente ] ){
		        distancia[ adyacente ] = distancia[ actual ] + peso;  //relajamos el vertice actualizando la distancia
		        previo[ adyacente ] = actual;                         //a su vez actualizamos el vertice previo
		        Q.add( new Node( adyacente , distancia[ adyacente ] ) ); //agregamos adyacente a la cola de prioridad
		    }
		}
}