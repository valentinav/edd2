package Logica;

import Logica.Lista;

public class NodoGrafo<T> {

	public Lista<T> listaNodosADY;
	private T elemento;

	public NodoGrafo(T elemento) {
		this.listaNodosADY = new Lista<T>();
		this.elemento = elemento;
	}

	public Lista<T> getListaNodosADY() {
		return listaNodosADY;
	}

	public void setListaNodosADY(T valor) {
		this.listaNodosADY.agregar(valor);
	}

	public T getElemento() {
		return elemento;
	}

	public void setElemento(T elemento) {
		this.elemento = elemento;
	}
	
}// Fin clase NodoGrafo
