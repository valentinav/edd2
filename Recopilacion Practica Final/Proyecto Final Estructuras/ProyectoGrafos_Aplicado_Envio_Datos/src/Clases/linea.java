

package Clases;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

public class linea{

    circulo inicial;
    circulo ffinal;
    Color color= Color.black;
	private int peso;

  

	public linea(circulo inicial, circulo ffinal, int peso) {
        this.inicial=inicial;
        this.ffinal=ffinal;
        this.peso=peso;
    }

    public void painter(Graphics g) {
        g.setColor(color);
       
       // g.drawLine(inicial.getX()+3, inicial.getY()+3, ffinal.getX()+3, ffinal.getY()+3);
      
     //   g.drawLine(inicial.getX()+4, inicial.getY()+4, ffinal.getX()+4, ffinal.getY()+4);
        
        g.drawString(String.valueOf(peso),(((inicial.getX()+6)+(ffinal.getX()+6))/2),(((inicial.getY()+6)+(ffinal.getY()+6))/2)-7);
       
        
   
        
       dibujarFlecha(inicial.getX(),inicial.getY(),ffinal.getX(),ffinal.getY(),g);
        
        

    }
    
    public void dibujarFlecha(int x,int y, int x1,int y1,Graphics g)
    {
      double ang=0.0, angSep=0.0;
      double tx,ty;
      int dist=0;
      Point punto1=null,punto2=null;

      //defino dos puntos extremos
      punto1=new Point(x,y);
      punto2=new Point(x1,y1);

      //tama�o de la punta de la flecha
      dist=15;

      /* (la coordenadas de la ventana es al revez)
          calculo de la variacion de "x" y "y" para hallar el angulo
       **/

      ty=-(punto1.y-punto2.y)*1.0;
      tx=(punto1.x-punto2.x)*1.0;
      //angulo
      ang=Math.atan (ty/tx);

      if(tx<0)
      {// si tx es negativo aumentar 180 grados
         ang+=Math.PI;
      }

      //puntos de control para la punta
      //p1 y p2 son los puntos de salida
      Point p1=new Point(),p2=new Point(),punto=punto2;

      //angulo de separacion
      angSep=25.0;
     
      p1.x=(int)(punto.x+dist*Math.cos (ang-Math.toRadians (angSep)));
      p1.y=(int)(punto.y-dist*Math.sin (ang-Math.toRadians (angSep)));
      p2.x=(int)(punto.x+dist*Math.cos (ang+Math.toRadians (angSep)));
      p2.y=(int)(punto.y-dist*Math.sin (ang+Math.toRadians (angSep)));

      Graphics2D g2D=(Graphics2D)g;

      //dale color a la linea
      g.setColor (Color.BLUE);
      // grosor de la linea
      g2D.setStroke (new BasicStroke(1.2f));
      //dibuja la linea de extremo a extremo
      g.drawLine (punto1.x,punto1.y,punto.x,punto.y);
      //dibujar la punta
      g.drawLine (p1.x,p1.y,punto.x,punto.y);
      g.drawLine (p2.x,p2.y,punto.x,punto.y);    

    }

    public void setColor(Color color) {
        this.color = color;
    }

    public circulo getFfinal() {
        return ffinal;
    }

    public circulo getInicial() {
        return inicial;
    }
    
    public int getPeso() {
  		return peso;
  	}

  	public void setPeso(int peso) {
  		this.peso = peso;
  	}
    
}