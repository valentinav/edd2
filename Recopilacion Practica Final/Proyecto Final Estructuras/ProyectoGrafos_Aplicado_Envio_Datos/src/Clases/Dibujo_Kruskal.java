package Clases;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;




/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Dibujo_Kruskal extends JPanel {

	/**
	 * @param args
	 */
	
	public JPanel contenedor;
	private static List<circulo> raiz;
	private static List<linea> raiz1;
	static lienzo l1;
	public Dibujo_Kruskal(List<circulo> listLKruskal,List<linea> ArisKruskal,lienzo l) {
		raiz = listLKruskal;
		raiz1 = ArisKruskal;
		l1=l;
	}
	public void prueba (){
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(12, 12, 494, 282);
		//setBounds(100,100, 450, 300);
		contenedor = new JPanel ();
		//setContentPane(contenedor);
		contenedor.setLayout(null);
	//	contenedor.setBounds(100,100, 450,300);
		contenedor.setBounds(12, 12, 484, 346);
		contenedor.setSize(300, 300);
	
		//contenedor.setResizable(true);
	
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				 try {
					 Dibujo_Kruskal pr = new  Dibujo_Kruskal(raiz,raiz1,l1);
	                    pr.setVisible(true);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
			}
		});
	}
	

	public void paint (Graphics g){
		super.paint(g);
		for(int i=0;i<raiz.size();i++)
		{
			
				circulo nodo=raiz.get(i);


				if(i==0)
				{
					Toolkit t = Toolkit.getDefaultToolkit ();
					Image imagen = t.getImage ("PC_Principal.png");
					g.drawImage(imagen, nodo.x, nodo.y,l1);
				}
				else	
				{
					Toolkit t = Toolkit.getDefaultToolkit ();
					Image imagen = t.getImage ("pc.png");
					g.drawImage(imagen, nodo.x, nodo.y,l1);
				}

				g.setColor(Color.red);
				g.drawString(""+nodo.n, nodo.x-2, nodo.y-1);

				g.setColor(Color.YELLOW);



		}
		for(int i=0;i<raiz1.size();i++)	
		{
			
			  g.setColor(Color.black);
			
		        g.drawLine(	raiz1.get(i).inicial.getX()+3, raiz1.get(i).inicial.getY()+3, raiz1.get(i).getFfinal().getX()+3, raiz1.get(i).getFfinal().getY()+3);
		      
		        g.drawLine(	raiz1.get(i).inicial.getX()+4, raiz1.get(i).inicial.getY()+4, raiz1.get(i).getFfinal().getX()+4, raiz1.get(i).getFfinal().getY()+4);
		        
		        g.drawString(String.valueOf(raiz1.get(i).getPeso()),(((raiz1.get(i).inicial.getX()+6)+(raiz1.get(i).getFfinal().getX()+6))/2),(((raiz1.get(i).inicial.getY()+6)+(raiz1.get(i).getFfinal().getY()+6))/2)-7);
		
		        dibujarFlecha(raiz1.get(i).inicial.getX(),raiz1.get(i).inicial.getY(),raiz1.get(i).getFfinal().getX(),raiz1.get(i).getFfinal().getY(),g);
		}
	}	
	
	
	 public void dibujarFlecha(int x,int y, int x1,int y1,Graphics g)
	    {
	      double ang=0.0, angSep=0.0;
	      double tx,ty;
	      int dist=0;
	      Point punto1=null,punto2=null;

	      //defino dos puntos extremos
	      punto1=new Point(x,y);
	      punto2=new Point(x1,y1);

	      //tama�o de la punta de la flecha
	      dist=15;

	      /* (la coordenadas de la ventana es al revez)
	          calculo de la variacion de "x" y "y" para hallar el angulo
	       **/

	      ty=-(punto1.y-punto2.y)*1.0;
	      tx=(punto1.x-punto2.x)*1.0;
	      //angulo
	      ang=Math.atan (ty/tx);

	      if(tx<0)
	      {// si tx es negativo aumentar 180 grados
	         ang+=Math.PI;
	      }

	      //puntos de control para la punta
	      //p1 y p2 son los puntos de salida
	      Point p1=new Point(),p2=new Point(),punto=punto2;

	      //angulo de separacion
	      angSep=25.0;
	     
	      p1.x=(int)(punto.x+dist*Math.cos (ang-Math.toRadians (angSep)));
	      p1.y=(int)(punto.y-dist*Math.sin (ang-Math.toRadians (angSep)));
	      p2.x=(int)(punto.x+dist*Math.cos (ang+Math.toRadians (angSep)));
	      p2.y=(int)(punto.y-dist*Math.sin (ang+Math.toRadians (angSep)));

	      Graphics2D g2D=(Graphics2D)g;

	      //dale color a la linea
	      g.setColor (Color.BLUE);
	      // grosor de la linea
	      g2D.setStroke (new BasicStroke(1.2f));
	      //dibuja la linea de extremo a extremo
	      g.drawLine (punto1.x,punto1.y,punto.x,punto.y);
	      //dibujar la punta
	      g.drawLine (p1.x,p1.y,punto.x,punto.y);
	      g.drawLine (p2.x,p2.y,punto.x,punto.y);    

	    }
	
	}

//raiz.raiz.dibujar(g, raiz.raiz,245,50,245,50,110);

	//originial/raiz.raiz.dibujar(g, raiz.raiz,350,50,350,50,150);

 
	//350,50,350,50,150

 		/*g.setColor(new Color(5,5,5));
 		g.drawLine(350+10,50+10,350+10,50+18);
 		g.setColor(new Color(255,255,0));
 		g.fillOval(350,50,20,20);
 		g.setColor(new Color(5,5,5));
 	    g.drawString(String.valueOf(656),350+5,50+15);
     	g.drawOval(350,50,20,20);*/


	
	/*g.setColor(Color.BLACK);
	//a
	int valor=(int)(Math.random()*(10000)%50);
	 g.setColor(Color.BLACK);
    g.drawString(String.valueOf(valor),15,390);
    

  
		try { Thread.sleep(1250);
	 
  	} catch (Exception e) { }
  	//	raiz = raiz.insertarNodo(raiz,valor);
		g.setColor(Color.WHITE);
		g.fillRect(10,370,40,40);
	 //
	//char [] vocales = {'a'};
	//g.drawRect(440, 290,50,59);*/
