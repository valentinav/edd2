package Clases;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.html.HTMLDocument.Iterator;

import Clases.Dibujo_Dijkstra;
import  Clases.circulo;
import  Clases.linea;
import Logica.Dijkstra;
import Logica.Grafo;
import Logica.Kruskal;








/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Dibujo_Grafos extends javax.swing.JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JDesktopPane jDesktopPane1;
	private JButton MostrarG;
	private AbstractAction abstractAction1;
	private JButton jButton1;
	private JLabel jLabel1;
	private JTextField jTextField1;

	private JDesktopPane jDesktopPane2;
	private JLabel lbl_grafo_PPal;
	private JComboBox jComboBox_Recorridos;
	private JTextArea jTar_Informacion;
    private JMenuItem mMat,mMatm,mGrd,mBusv,OrdG, Pseu;
	private AbstractAction MostarG;
    private JMenu menu2;
	private JMenuBar mb;
	private JScrollPane Scrll_Datos;
	List<circulo> ListLKruskal = new ArrayList<circulo>();
	List<circulo> ListDijkstra = new ArrayList<circulo>();
	
	lienzo lien= new lienzo(null);
	Grafo <String> objGrafo = new Grafo<String> ();
	/**
	* Auto-generated main method to display this JFrame
	*/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Dibujo_Grafos inst = new Dibujo_Grafos();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
				String titulo = "La siguiente aplicacion simula el funcionamiento de una red de computadoras\n " +
						"interconectadas a un sistema de envio de informacion el cual se encarga de\n " +
						"administrar el proceso de escojer el camino por el cual puede enviar la informacion\n " +
						"lo mas rapido posible. para esto se le solitara adicionar uja erie de vertices con\n " +
						"sus respectivas conexiones. el peso de la conexion sera equivalente a la rapidez de\n" +
						" envio de DATOS";
				
				JOptionPane.showMessageDialog(null, titulo,"Datos de la aplicacion",JOptionPane.INFORMATION_MESSAGE);
				JOptionPane.showMessageDialog(null, "lea la informacion a su Derecha");
			}
		});
	}
	
	public Dibujo_Grafos() {
		super();
		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jDesktopPane1 = new JDesktopPane();
				getContentPane().add(jDesktopPane1);
				jDesktopPane1.setBounds(12, 29, 350, 178);
				lienzo l=new lienzo(jDesktopPane1);
				lien=l;
				ListLKruskal=l.getListCirculo() ;
				ListDijkstra=l.getListCirculo() ;
				////
			
				objGrafo=l.objGrafo;
				///
				int numElementos =ListLKruskal.size();
				System.out.println("\n\nEl ArrayList tiene "+numElementos+" elementos");
				l.setBounds(0, 0, 350, 178);
			
				jDesktopPane1.add(l);
			}
			{
				setLayout(null);
				mb = new JMenuBar();
		        setJMenuBar(mb);
			}
			
			{
		        menu2 = new JMenu("Informacion");
		        mb.add(menu2);
		        mMat=new JMenuItem("Matriz Adyacencias");
		        mMat.addActionListener(this);
		        menu2.add(mMat);
		        mMatm=new JMenuItem("Matriz Adyacencias m*m");
		        mMatm.addActionListener(this);
		        menu2.add(mMatm);
		        mGrd=new JMenuItem("Grado Vertice");
		        mGrd.addActionListener(this);
		        
		        mBusv=new JMenuItem("Buscar Vertice");
		        mBusv.addActionListener(this);
		        menu2.add( mBusv);
		        menu2.add(mGrd);
		        
		     
		        OrdG=new JMenuItem("Orden del Grafo");
		        OrdG.addActionListener(this);
		        menu2.add(OrdG);
		        
		        Pseu=new JMenuItem("Determinar si es PseudoGrafo");
		        Pseu.addActionListener(this);
		        menu2.add(Pseu);
		      
			}
			
			
			{
				MostrarG = new JButton();
				getContentPane().add(MostrarG);
				MostrarG.setText("Aplicar Dijkstra");
				MostrarG.setBounds(380, 375, 107, 23);
				MostrarG.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
			            //f.setBackground(new Color(0,255,0));
			        	Dijkstra dj = new Dijkstra(); 
						try {
						//	 if(objGrafo.conexo()==true){
							 Grafo<String> gr1;
							//gr1 = dj.dijkstra(0, l.getObjGrafo());
							String opc = JOptionPane.showInputDialog(null, "digite el vertice inicial","Receptor Datos",JOptionPane.INFORMATION_MESSAGE);
							
							if (objGrafo.estaVertice(opc, false)) {
								int pos = objGrafo.buscarPosicion(opc);
								gr1 = dj.dijkstra(0, objGrafo);
								 int[][] mat_B = null;

								 try {
									 //	 mat_A=ordenado_G.Ordenar_Insercion(ordenado_G);
									 mat_B=gr1.MatrizSInOrden(gr1);

								 } catch (Exception e5) {
									 e5.printStackTrace();
								 }
								 int l1=gr1.getFilas_k();

								 int[][] mat_c1=gr1.generarMatrizAdyacencias();
								 List<linea> ListArista1 = new ArrayList<linea>();
								 int	suma1=0;
								 System.out.println("Matriz B");
								
							
								 for (int i = 0; i < l1; i++) {
									 suma1=suma1+mat_B[i][0];
									 for (int j = 0; j < 3; j++) {
										 circulo a =  ListDijkstra.get(mat_B[i][1]);
										 circulo b=  ListDijkstra.get(mat_B[i][2]);

										 linea lineas1 = new linea (a,b ,mat_B[i][0]);	
										 ListArista1.add(lineas1);
									 }
								 }
								 
								 Dibujo_Dijkstra dg1=new Dibujo_Dijkstra(ListDijkstra,ListArista1,lien);
								 jDesktopPane2.setVisible(false);
								 repaint();
								 dg1.setLocation(0, 0);

								 dg1.setSize(500, 400);
								 Dimension dlgSize = dg1.getPreferredSize(); 
							//	 Dimension pantalla = getSize(); 
								 Dimension ventana = dg1.getSize() ; 

								 jTextField1.setText(""+suma1/2);
								 repaint();
							//	 dg1.repaint();
								 jDesktopPane2.add(dg1);
								 jDesktopPane2.setVisible(true);
								 dg1.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
							}	
							//	String titulo = gr1.imprimirMatriz(gr1.generarMatrizAdyacencias());
								//System.out.println(titulo);
								 
						//	}
					//		else
						//		JOptionPane.showMessageDialog(null, "El vertice no se encuentra actualmente en el grafo", "Informe de estado", JOptionPane.INFORMATION_MESSAGE);
							

							

							
							// jDesktopPane2.setVisible(true);
							 
							// }
							// else
							//	 JOptionPane.showMessageDialog(null,"El Grafo no es Conexo no se puede aplicar Dijkstra", "informe de estado",
								//		 JOptionPane.ERROR_MESSAGE);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
			        
						
						
						
					}	
					
				});
			}
			{
				jTar_Informacion = new JTextArea();
				Scrll_Datos = new JScrollPane(jTar_Informacion);
				Scrll_Datos.setBounds(380, 10, 300, 360);
				getContentPane().add(Scrll_Datos);
				String titulo = "\nPara dibujar el grafo:"
						+ "\n 1.Click Izquierdo sobre el area de dibujo del grafo"
						+ "\n 2.Digite el nombre del vertice"
						+ "\n\nPara agregar una arista:"
						+ "\n 1.Click derecho en el area de Dibujo"
						+ "\n 2.Digite los valores solicitados";
				jTar_Informacion.setText(titulo);
				jTar_Informacion.setEditable(false);
			}
			{
				lbl_grafo_PPal = new JLabel();
				lbl_grafo_PPal.setText("Area de dibujo del Grafo");
				lbl_grafo_PPal.setBounds(12, 12, 143, 14);
				getContentPane().add(lbl_grafo_PPal);
			}
			{
				ComboBoxModel jComboBox_RecorridosModel = 
						new DefaultComboBoxModel(
								new String[] { " ","Djkstra", "Prim", "Kruskal" });
				jComboBox_Recorridos = new JComboBox();
				jComboBox_Recorridos.setModel(jComboBox_RecorridosModel);
				jComboBox_Recorridos.setBounds(330, -32, 133, 20);
				getContentPane().add(jComboBox_Recorridos);
			}
			{
				jDesktopPane2 = new JDesktopPane();
				getContentPane().add(jDesktopPane2);
				jDesktopPane2.setBounds(12, 218, 350, 178);
			}
			{
				jTextField1 = new JTextField();
				getContentPane().add(jTextField1);
				jTextField1.setText(" ");
				jTextField1.setEditable(false);
				jTextField1.setBounds(174, 409, 84, 15);
			}
			{
				jLabel1 = new JLabel();
				getContentPane().add(jLabel1);
				jLabel1.setText("Longitud de Camino");
				jLabel1.setBounds(12, 408, 155, 17);
			}
			{
				jButton1 = new JButton();
				getContentPane().add(jButton1);
				jButton1.setText("Editar Peso Arista");
				jButton1.setBounds(565, 375, 116, 24);
				jButton1.setAction(getAbstractAction1());
			}
			pack();
			this.setSize(700, 489);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
		
		  //lector opcion matriz Adyacencia
        
	}
	
	  private int ingresarPeso(){
	        String peso = JOptionPane.showInputDialog("Digite el nuevo peso de la arista");
	        int intPeso = 0;
	        try{
	            intPeso = Integer.parseInt(peso);  
	        }catch(Exception ex){
	            intPeso = ingresarPeso();
	        }
	        
	        return intPeso;
	    }
	
	private AbstractAction getAbstractAction1() {
		if(abstractAction1 == null) {
			abstractAction1 = new AbstractAction("Editar Arista", null) {
				public void actionPerformed(ActionEvent evt) {
					String ini = JOptionPane.showInputDialog("Nombre de circulo inicial");
					String fin = JOptionPane.showInputDialog("Nombre de circulo final");
					if (!ini.equals(fin)) {
						try {
							if (objGrafo.estaVertice(ini, false)) {
								try {
									if(objGrafo.estaVertice(fin, false)){
										int peso = ingresarPeso();
										try {
											
											lien.editar_pesoarista(ini, fin, peso);
									        repaint();
											
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
}else
									JOptionPane.showMessageDialog(null,"No Encontro un vertice", "informe de estado",
											JOptionPane.INFORMATION_MESSAGE);
								} catch (HeadlessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else
								JOptionPane.showMessageDialog(null,"No Encontro un vertice", "informe de estado",
										JOptionPane.INFORMATION_MESSAGE);
						} catch (HeadlessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else 
						JOptionPane.showMessageDialog(null,"Los nombres son iguales", "informe de estado",
								JOptionPane.ERROR_MESSAGE);
						
					
					
					
					
				}
			};
		}
		return abstractAction1;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		 if(e.getSource() == mMatm){
        	 Grafo<String>	grafoAux;
        	grafoAux =objGrafo;
			try {
				String titulo = grafoAux.matrizMM();
				titulo = "Matriz de adyacencia m*m\n"+titulo  ;
				jTar_Informacion.setText(titulo);
			} catch (Exception a) {
				// TODO Auto-generated catch block
				a.printStackTrace();
			}
        }
		 if(e.getSource() == Pseu){
			 Grafo<String>	grafoAux;
	        	grafoAux =objGrafo;
				try {
					String titulo = grafoAux.pseudografo1();
					titulo = " \n"+titulo  ;
					jTar_Informacion.setText(titulo);
				} catch (Exception a) {
					// TODO Auto-generated catch block
					a.printStackTrace();
				}
	        }
		 
		   if(e.getSource() == mMat){
			   Grafo<String>	grafoAux;
	        	grafoAux =objGrafo;
				try {
					String titulo = grafoAux.imprimirMatriz(grafoAux.generarMatrizAdyacencias());
					titulo = "Matriz de adyacencia\n"+titulo  ;
					jTar_Informacion.setText(titulo);
					
				} catch (Exception s) {
					// TODO Auto-generated catch block
					s.printStackTrace();
				}
	        	
	        }
		   
		   if(e.getSource() == mGrd){
			   Grafo<String>	grafoAux;
	        	grafoAux =objGrafo;
				String valor = JOptionPane.showInputDialog(null, "Digite el nombre del vertice", "Solicitud dato",JOptionPane.QUESTION_MESSAGE );
				try {
					grafoAux.gradoVertice(valor);
				} catch (Exception q) {
					// TODO Auto-generated catch block
					q.printStackTrace();
				}
	        }

	        if(e.getSource() == OrdG){
	        	 Grafo<String>	grafoAux;
		        	grafoAux =objGrafo;
			
				try {
					grafoAux.ordenGrafo();
					
					
				} catch (Exception q) {
					// TODO Auto-generated catch block
					q.printStackTrace();
				}
	        }
	        
	        if(e.getSource() ==  mBusv){
	        	 Grafo<String>	grafoAux;
		        	grafoAux =objGrafo;
				String valor = JOptionPane.showInputDialog(null, "Digite el nombre del vertice a Buscar", "Solicitud dato",JOptionPane.QUESTION_MESSAGE );
				try {
					if (grafoAux.estaVertice(valor, false)) {
						JOptionPane.showMessageDialog(null,"El vertice si se encuentra en el grafo");
						grafoAux.estaVertice(valor, true);
					}else
						JOptionPane.showMessageDialog(null,"El vertice ingresado no hace parte del Grafo");
						
					
					
				} catch (Exception q) {
					// TODO Auto-generated catch block
					q.printStackTrace();
				}
	        }
	        
		
	}

	
	/*private AbstractAction getMostarG() {
		if(MostarG == null) {
			MostarG = new AbstractAction("MostarG", null) {
				public void actionPerformed(ActionEvent evt) {
					
					
				}
			};
		}
		return MostarG;
	}	*/

}