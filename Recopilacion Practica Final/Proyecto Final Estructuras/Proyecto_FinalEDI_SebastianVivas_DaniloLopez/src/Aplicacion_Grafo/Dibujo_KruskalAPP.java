package Aplicacion_Grafo;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;

import Dibujo_Grafo.circulo;
import Dibujo_Grafo.lienzo;
import Dibujo_Grafo.linea;




/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Dibujo_KruskalAPP extends JPanel {

	/**
	 * @param args
	 */
	
	public JPanel contenedor;
	private static List<circuloAPP> raiz;
	private static List<lineaAPP> raiz1;
	static lienzoAPP l1;
	
	public Dibujo_KruskalAPP(List<circuloAPP> listLKruskal,List<lineaAPP> ArisKruskal,lienzoAPP l) {
		raiz = listLKruskal;
		raiz1 = ArisKruskal;
		l1=l;
	}
	public void prueba (){
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(12, 12, 494, 282);
		//setBounds(100,100, 450, 300);
		contenedor = new JPanel ();
		//setContentPane(contenedor);
		contenedor.setLayout(null);
	//	contenedor.setBounds(100,100, 450,300);
		contenedor.setBounds(12, 12, 484, 346);
		contenedor.setSize(300, 300);
	
		//contenedor.setResizable(true);
	
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				 try {
					 Dibujo_KruskalAPP pr = new  Dibujo_KruskalAPP(raiz,raiz1,l1);
	                    pr.setVisible(true);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
			}
		});
	}
	

	public void paint (Graphics g){
		super.paint(g);
		for(int i=0;i<raiz.size();i++)
		{
			
				circuloAPP nodo=raiz.get(i);


				if(i==0)
				{
					Toolkit t = Toolkit.getDefaultToolkit ();
					Image imagen = t.getImage ("PC_Principal.png");
					g.drawImage(imagen, nodo.x, nodo.y,l1);
				}
				else	
				{
					Toolkit t = Toolkit.getDefaultToolkit ();
					Image imagen = t.getImage ("pc.png");
					g.drawImage(imagen, nodo.x, nodo.y,l1);
				}

				g.setColor(Color.red);
				g.drawString(""+nodo.n, nodo.x-2, nodo.y-1);

				g.setColor(Color.YELLOW);



		}
		for(int i=0;i<raiz1.size();i++)	
		{
			
			  g.setColor(Color.black);
			
		        g.drawLine(	raiz1.get(i).inicial.getX()+3, raiz1.get(i).inicial.getY()+3, raiz1.get(i).getFfinal().getX()+3, raiz1.get(i).getFfinal().getY()+3);
		      
		        g.drawLine(	raiz1.get(i).inicial.getX()+4, raiz1.get(i).inicial.getY()+4, raiz1.get(i).getFfinal().getX()+4, raiz1.get(i).getFfinal().getY()+4);
		        
		        g.drawString(String.valueOf(raiz1.get(i).getPeso()),(((raiz1.get(i).inicial.getX()+6)+(raiz1.get(i).getFfinal().getX()+6))/2),(((raiz1.get(i).inicial.getY()+6)+(raiz1.get(i).getFfinal().getY()+6))/2)-7);
		}
	}	
	
	}
