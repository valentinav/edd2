package Clases_Arboles;

import java.awt.Color;
import java.awt.Graphics;

public class Nodo_AVL {
	public int elemento;
	public Nodo_AVL  izq;
	public Nodo_AVL  der;
	protected int factorEquilibrio;
	protected int grado = 0;
	
	//constructores
	public Nodo_AVL(int elemento) {
		this(elemento, null, null);
	}

	public Nodo_AVL(int elemento, Nodo_AVL izq, Nodo_AVL der) {
		this.elemento = elemento;
		this.izq = izq;
		this.der = der;
		factorEquilibrio = 0;
	}
	
	public void dibuja(Graphics g ,Nodo_AVL  raiz,int i, int j, int pi, int pj, int desplaza)
    {
    	if (raiz!= null)
    	{
    		g.setColor(Color.blue);
    		g.drawLine(i+12,j+12,pi+12,pj+12);
    		dibuja(g,raiz.izq,i-desplaza,j+40,i,j,desplaza/2);
    		dibuja(g,raiz.der,i+desplaza,j+40,i,j,desplaza/2);
    		g.setColor(new Color(220,220,230));
    		g.fillOval(i,j,25,25);
    		g.setColor(Color.blue);
    		g.drawOval(i,j,25,25);
    		g.drawString(String.valueOf(raiz.elemento),i+5,j+20);
    		g.setColor(Color.red);
    		g.drawString(String.valueOf(raiz.factorEquilibrio),i+5,j+40);


    	}
    }
}