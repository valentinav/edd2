package Dibujo_Grafo;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


import Clases_Grafo.Grafo;
import Clases_Grafo.Lista;
import Clases_Grafo.NodoGrafo;

public class lienzo_Grafo extends JPanel {
	
	int x=0,y=0;
    linea linea;
    circulo circulo;
    circulo jalada=null;
    List<circulo> ListCirculo = new ArrayList<circulo>();
    List<linea> ListArista = new ArrayList<linea>();
	static Grafo <String> objGrafo = new Grafo<String> ();
	static Grafo <String> grafoOriginal ;
    JDesktopPane p; 
    
    
    public lienzo_Grafo(JDesktopPane prin, Grafo<String> grafo) {
    	p=prin;
        this.setVisible(true);
        this.setDoubleBuffered(true);
        grafoOriginal = grafo;
        
    }
    
    public void anadirCirculo(int x, int y) throws Exception{
    	String ini = JOptionPane.showInputDialog("Digite el nombre del vertice");
		if (!objGrafo.estaVertice(ini, false)) {
			objGrafo.agregarVertices(ini);
			circulo = new circulo(ini, x, y);
			// circulo = new circulo(ListCirculo.size()+1,x,y);
			ListCirculo.add(circulo);
			repaint();
			p.repaint();
		} else
    		JOptionPane.showMessageDialog(null,"El nombre de la ARISTA ya se encuentre asigando", "informe de estado",
					JOptionPane.ERROR_MESSAGE);
    		
    }

	public void anadirLinea(int x, int y) throws Exception {
		// try{
		//int intPeso = ingresarPeso(x,y);
		Lista aux = objGrafo.listaVertices.getValor(1).getListaNodosADY();
        //System.out.println(aux.getpeso(0));
		//linea = new linea(ListCirculo.get(x), ListCirculo.get(y), aux.getpeso(i));
		this.ListArista.add(linea);

		repaint();
		p.repaint();
		/*
		 * }catch(IndexOutOfBoundsException e){
		 * JOptionPane.showMessageDialog(null, "No se encontro circulo"); }
		 */
	}
    
    public void paintComponent(Graphics g) {
		super.paintComponents(g);
		
		for (linea f : ListArista) {
			f.painter(g);
		}
		
		for (circulo f : ListCirculo) {
			f.painterGrafo(g, this);
			
		}
	}

	public static Grafo<String> getObjGrafo() {
		return objGrafo;
	}

	public static void setObjGrafo(Grafo<String> objGrafo) {
		lienzo_Grafo.objGrafo = objGrafo;
	}

    
    
    
    
  
   
    
   
	
    
    
    
    
    
	
    
}





/*

package Clases;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;








/*
public class Dibujo_Kruskal extends JInternalFrame {
	/**
	 * @param args
	 */
	
	/*public JInternalFrame contenedor;
	private static List<circulo> raiz;
	private static List<linea> raiz1;
	public Dibujo_Kruskal(List<circulo> listLKruskal,List<linea> ArisKruskal) {
		raiz = listLKruskal;
		raiz1 = ArisKruskal;
	}
	public void prueba (){
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(12, 12, 494, 282);
		//setBounds(100,100, 450, 300);
		contenedor = new JInternalFrame();
		//setContentPane(contenedor);
		contenedor.setLayout(null);
	//	contenedor.setBounds(100,100, 450,300);
		contenedor.setBounds(12, 12, 484, 346);
		contenedor.setSize(300, 300);
	
		//contenedor.setResizable(true);
	
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				 try {
					 Dibujo_Kruskal pr = new  Dibujo_Kruskal(raiz,raiz1);
	                    pr.setVisible(true);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
			}
		});
	}
	
	public void paint (Graphics g){
		super.paint(g);
		for(int i=0;i<raiz.size();i++)
		{
			
				circulo nodo=raiz.get(i);
		
				 g.setColor(Color.red);
		         g.drawString(""+nodo.n, nodo.x-2, nodo.y-1);
		         //g.drawString(""+n, x, y);
		         
		   		g.fillOval(nodo.x,nodo.y,20,20);
		   		g.setColor(Color.YELLOW);
		   		
		   		
		   	   //g.drawString(""+(n),x+4,y+15);
		   	   // g.drawString("A",x+3,y+14);
		
		   	    g.drawOval(nodo.x,nodo.y,20,20);
		}
		for(int i=0;i<raiz1.size();i++)	
		{
			
			  g.setColor(Color.black);
			
		        g.drawLine(	raiz1.get(i).inicial.getX()+3, raiz1.get(i).inicial.getY()+3, raiz1.get(i).getFfinal().getX()+3, raiz1.get(i).getFfinal().getY()+3);
		      
		        g.drawLine(	raiz1.get(i).inicial.getX()+4, raiz1.get(i).inicial.getY()+4, raiz1.get(i).getFfinal().getX()+4, raiz1.get(i).getFfinal().getY()+4);
		        
		        g.drawString(String.valueOf(raiz1.get(i).getPeso()),(((raiz1.get(i).inicial.getX()+3)+(raiz1.get(i).getFfinal().getX()+3))/2),(((raiz1.get(i).inicial.getY()+3)+(raiz1.get(i).getFfinal().getY()+3))/2));
		}
	}	
	
	}*/
