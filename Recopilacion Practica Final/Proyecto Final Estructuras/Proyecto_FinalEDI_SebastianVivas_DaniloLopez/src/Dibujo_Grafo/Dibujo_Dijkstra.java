package Dibujo_Grafo;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class Dibujo_Dijkstra extends  JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 85191254172972853L;
	/**
	 * @param args
	 */

	public    JInternalFrame  contenedor1;
	private static List<circulo> raiz;
	private static List<linea> raiz1;
	public Dibujo_Dijkstra(List<circulo> ListDijkstra,List<linea> ArisDijkstra) {
		raiz = ListDijkstra;
		raiz1 = ArisDijkstra;
	}
	public void prueba (){
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(12, 12, 494, 282);
		//setBounds(100,100, 450, 300);
		contenedor1 = new    JInternalFrame ();
		//setContentPane(contenedor);
		contenedor1.setLayout(null);
		//	contenedor.setBounds(100,100, 450,300);
		contenedor1.setBounds(12, 12, 484, 346);
		contenedor1.setSize(300, 300);

		//contenedor.setResizable(true);

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {
					Dibujo_Dijkstra pr1 = new  Dibujo_Dijkstra(raiz,raiz1);
					pr1.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public void paint (Graphics g){
		super.paint(g);
		for(int i=0;i<raiz.size();i++)
		{

			circulo nodo=raiz.get(i);


			g.setColor(Color.red);
			g.drawString(""+nodo.n, nodo.x-4, nodo.y-2);

			g.fillOval(nodo.x,nodo.y,20,20);
			g.setColor(Color.YELLOW);

			g.drawOval(nodo.x,nodo.y,20,20);
		}
		for(int i=0;i<raiz1.size();i++)	
		{

			g.setColor(Color.black);

			g.drawLine(	raiz1.get(i).inicial.getX()+3, raiz1.get(i).inicial.getY()+3, raiz1.get(i).getFfinal().getX()+3, raiz1.get(i).getFfinal().getY()+3);

			g.drawLine(	raiz1.get(i).inicial.getX()+4, raiz1.get(i).inicial.getY()+4, raiz1.get(i).getFfinal().getX()+4, raiz1.get(i).getFfinal().getY()+4);

			g.drawString(String.valueOf(raiz1.get(i).getPeso()),(((raiz1.get(i).inicial.getX()+3)+(raiz1.get(i).getFfinal().getX()+3))/2),(((raiz1.get(i).inicial.getY()+3)+(raiz1.get(i).getFfinal().getY()+3))/2));
		}
	}	

}