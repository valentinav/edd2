package Clases_Grafo;

import javax.swing.JOptionPane;




public class Grafo <T> {
	// Atributos
	public Lista<NodoGrafo> listaVertices;
	public int[][] matAdyacencia; 
	public int filas_k;

	// Constructores
	public Grafo() {
		listaVertices = new Lista<NodoGrafo>();
		
	}
	
	/**
	 * agrega un nuevo vertice al grafo
	 * 
	 * @param valor
	 *            valor a agregar de tipo T
	 */
	public void agregarVertices(T vertice) {
		
		NodoGrafo<T> nuevoVertice = new NodoGrafo<T>(vertice);
		listaVertices.agregar(nuevoVertice);
		//System.out.println("Elemento agregado al Grafo");
	}
	
	public boolean conexo() throws Exception {
		boolean a = true;
		int[][] copiaMatriz = generarMatrizAdyacencias();
		int tamanio = listaVertices.getTamanio();
		for (int i = 0; i < tamanio; i++) {

			System.out.println();
			for (int j = 0; j < tamanio; j++) {
				System.out.print(copiaMatriz[i][j] + " ");
			}
		}
		int x = 0;

		for (int i = 0; i < tamanio; i++) {
			for (int j = 0; j < tamanio; j++) {
				if (copiaMatriz[i][j] == 0)// &&
				{
					x = x + 1;
				}
			}
			if (x == tamanio) {
				a = false;
			}
			x = 0;
		}
		return a;
	}
	
	/**
	 * agrega las adyacencias pertinentes a cada vertice
	 * 
	 * @param posInicial
	 *            posicion del primer grafo
	 * @param posFinal
	 *            posicion del segundo grafo
	 *            
	 * @throws PosicionIlegalException
	 *             excepcion en caso que la posicion no exista
	 */
	public void agregarAdyacencias(int posInicial, int posFinal, int peso)throws Exception {
		if (!listaVertices.esVacia()) {
			if (((posInicial >= 0) && (posInicial < listaVertices.getTamanio()))
					&& ((posFinal >= 0) && (posFinal < listaVertices.getTamanio()))) {
				if (posFinal == posInicial) {
					String valor = listaVertices.getValor(posFinal).getElemento().toString();
					//NodoLista<String> nuevoVert = new NodoLista<String>(a);
					listaVertices.getValor(posFinal).setListaNodosADY(valor);
					//listaVertices.getValor(posInicial).getListaNodos().getValor(posFinal)posFinal;
					} else {
						String valor = listaVertices.getValor(posFinal).getElemento().toString();
						listaVertices.getValor(posInicial).setListaNodosADY(valor);
						listaVertices.getValor(posInicial).listaNodosADY.setPeso(listaVertices.getValor(posFinal).getElemento().toString(), peso);	
						
						valor = listaVertices.getValor(posInicial).getElemento().toString();
						listaVertices.getValor(posFinal).setListaNodosADY(valor);
						listaVertices.getValor(posFinal).listaNodosADY.setPeso(listaVertices.getValor(posInicial).getElemento().toString(), peso);	
				}
			} else
				System.out.println("\tUNA O AMBAS DE LAS POSICIONES NO EXISTEN AUN...");
		} else
			System.out.println("\tEL GRAFO SE ENCUENTRA VACIO");
	}
	
	public Lista<NodoGrafo> getListaVertices() {
		return listaVertices;
	}

	/**
	 * Inprime todos los vertices del grafo junto con su posicion
	 * 
	 * @throws Exception 
	 */
	public void imprimirVertices() throws Exception{
		NodoGrafo aux;
		for (int i = 0; i < listaVertices.getTamanio(); i++) {
			//aux = listaVertices.getValor(i).elemento;
			System.out.println("Vertice "+(i+1)+" : "+listaVertices.getValor(i).getElemento().toString());
		}
	
	}
	
	/**
	 * verifica si un vertice del grafo existe
	 * 
	 * @param valor
	 *            valor a buscar
	 *           
	 * @return boolean 
	 *            
	 * @throws PosicionIlegalException
	 *             excepcion en caso que exista un error de puntero nulo
	 */
	public boolean estaVertice(T elemento, boolean var) throws Exception{
		if(listaVertices.esVacia())
			return false;
		else{
			for (int i = 0; i < listaVertices.getTamanio(); i++) {
				if(listaVertices.getValor(i).getElemento().equals(elemento)){
					if(var)
						imprimirAdyacencias(i);
					i = listaVertices.getTamanio();
					return true;	
				}
			}	
		}
		return false;
	}
	/**
	 * @param pos
	 *        posicion del vertice encontrado
	 * 
	 * @throws Exception
	 */
	public void imprimirAdyacencias(int pos) throws Exception {
		
		Lista listaAux;
		listaAux = listaVertices.getValor(pos).getListaNodosADY();
		String vertices = ""; 
		for (int i = 0; i < listaAux.getTamanio(); i++) {
			vertices = vertices + listaAux.getValor(i)+", ";
		}
			System.out.println("vertices adyacentes de  "+listaVertices.getValor(pos).getElemento()+"   :"+vertices);
	}
	/**
	 * realiza el llenado de la matriz de adyacencias para el grafo
	 * 
	 * @throws Exception
	 */
	 public int[][] generarMatrizAdyacencias() throws Exception {
		matAdyacencia = new int[listaVertices.getTamanio()][listaVertices.getTamanio()];
		
		for (int i = 0; i < listaVertices.getTamanio(); i++) {
			
			Lista listaAux = listaVertices.getValor(i).getListaNodosADY();

			for (int j = 0; j < listaAux.getTamanio(); j++) {
				
				//mandamos cada adyacencia de del vertice correpondiente 
				int pos = buscarPosicion( (T)listaAux.getValor(j).toString());
				
				//asiganmos un uno, el cual representa un arco
				matAdyacencia[i][pos] = 1;
				
			}
		}
		return matAdyacencia;
	}
	/**
	 * busca la posicion del vertice enviado por parametro
	 * 
	 * @param vertice
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public int buscarPosicion(T vertice) throws Exception {
		int pos = 0;
		for (int j = 0; j < listaVertices.getTamanio(); j++) {

			if (listaVertices.getValor(j).getElemento().equals(vertice) ) {
				return j;
			}
		}
		return pos;
	}
	public void imprimirMatrizAdyacencias() throws Exception{
		int[][] copiaMatriz =  generarMatrizAdyacencias();
		imprimirMatriz(copiaMatriz);
	}
	/**
	 * muestra en concola la matriz recibida
	 * 
	 * @throws Exception
	 */
	public String imprimirMatriz (int[][] matAdyacencia) throws Exception{
		String matriz = " ";
		
		matriz = "   ";
		for (int i = 0; i < listaVertices.getTamanio(); i++) {
			matriz = matriz + (listaVertices.getValor(i).getElemento().toString()+"  ");
		}
		matriz = matriz + "\n";
		for (int i = 0; i < listaVertices.getTamanio(); i++) {
			matriz = matriz + (listaVertices.getValor(i).getElemento().toString()+"  ");
			for (int j = 0; j < listaVertices.getTamanio(); j++) {
				matriz = matriz + (matAdyacencia[i][j]+"  " );
			}
			matriz = matriz + "\n";
		}
		
		return matriz;
	}
	
	/**
	 * determina cual es el orden del grafo y lo muestra por pantalla
	 */
	public void ordenGrafo(){
		if (!listaVertices.esVacia()) {
			System.out.println("\nEl orden del grafo es: "+ listaVertices.getTamanio()+"\n");
			JOptionPane.showMessageDialog(null,"El orden del grafo es: "+ listaVertices.getTamanio());
		} else
		{
			System.out.println("El grafo no se ha creado aun...");
	     	JOptionPane.showMessageDialog(null,"El grafo no se ha creado aun...");
		}
	}
	
	/**
	 * determina el grado de un vertice 
	 * 
	 * @param valor
	 *        vertice a determinar
	 * @throws Exception
	 *         emn caso de que ocurra un error
	 */
	public void gradoVertice(T valor) throws Exception{
		if(estaVertice(valor, false)){
			int posicion = buscarPosicion(valor);
			JOptionPane.showMessageDialog(null, "El grado del vertice " + valor + " es : "+listaVertices.getValor(posicion).getListaNodosADY().getTamanio(),"Informe peticion",JOptionPane.INFORMATION_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(null,"el vertice no Existe","Error",JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * imprime la matriz de adyacencias m*m
	 * 
	 * @throws Exception
	 */
	public String matrizMM() throws Exception{
		int[][] copiaMatriz = generarMatrizAdyacencias();
		int suma = 0;  
		int tamanio = listaVertices.getTamanio();
        int result[][] = new int[tamanio][tamanio];  
        for(int i = 0; i < tamanio; i++){  
            for(int j = 0; j < tamanio; j++){  
                suma = 0;  
                for(int k = 0; k < tamanio; k++){  
                    suma += copiaMatriz[i][k] * copiaMatriz[k][j];  
                }  
                result[i][j] = suma;  
            }  
        }
        return imprimirMatriz(result);
	}
	/**
	 * determinar si un grafo es un pseudografo
	 * @throws Exception
	 */
	public void pseudografo() throws Exception{
		int[][] copiaMatriz = generarMatrizAdyacencias();
		boolean aux = false;
		for (int i = 0; i < listaVertices.getTamanio(); i++) {
			if(copiaMatriz[i][i] != 0){
				aux =  true;
				i = listaVertices.getTamanio();
			}
		}
		if(aux){
			System.out.println("El grafo si es un pseudografo: \nVertices que cumplen la denominacion son: ");
			for (int i = 0; i < listaVertices.getTamanio(); i++) {
				if(copiaMatriz[i][i] != 0){
					System.out.print(listaVertices.getValor(i).getElemento()+" ");
				}
			}
		}
		else
			System.out.println("El grafo no es un pseudografo");
			
	}
	
	
	public String pseudografo1() throws Exception{
		int[][] copiaMatriz = generarMatrizAdyacencias();
		String a="";
		boolean aux = false;
		for (int i = 0; i < listaVertices.getTamanio(); i++) {
			if(copiaMatriz[i][i] != 0){
				aux =  true;
				i = listaVertices.getTamanio();
			}
		}
		if(aux){
			System.out.println("El grafo si es un pseudografo: \nVertices que cumplen la denominacion son: ");
			for (int i = 0; i < listaVertices.getTamanio(); i++) {
				if(copiaMatriz[i][i] != 0){
					System.out.print(listaVertices.getValor(i).getElemento()+" ");
					a=a+listaVertices.getValor(i).getElemento()+",";
				}
			}
		}
		else
		{
		//	System.out.println("El grafo no es un pseudografo");
		     a=("El grafo no es un pseudografo");
		}
		return a;
			
	}
	
	
	// devuelve la matriz de caminos P.
		public int[][] matrizCaminos() throws Exception{
			int tamanio = listaVertices.getTamanio();
			int[][] copiaMatriz = generarMatrizAdyacencias();

			// se obtienen, virtualmente, a partir de P0, las sucesivas
			// matrices P1, P2, P3 ,..., Pn-1, Pn que es la matriz de caminos
			for (int k = 0; k < tamanio; k++)
				for (int i = 0; i <tamanio; i++)
					for (int j = 0; j < tamanio ; j++)
						copiaMatriz[i][j] = Math.min(copiaMatriz[i][j] + copiaMatriz[i][k] * copiaMatriz[k][j], 1);
			return copiaMatriz;
		}
		
		public int[][] ordenar_mat(int [][] mat,int l)
		{
		
			int[][] mat_k=new int[l][3];
		//	int[] vec_k=new int[l];
			//menor=mat[0][0];

			int menor =0;
			
			   for(int i=0;i<l-1;i++)            
			   { 
				   int min=i;
				    for(int j=i+1;j<l;j++)   
				    {
				    	if(mat[j][0]<mat[min][0])
				    	{
				    		min=j;
				    	}
				    	
				    	 if(i!=min){
		                       int aux  = mat[i][0];
		                       
		                       int aux_1 =mat[i][1];
		                       int aux_2= mat[i][2];
		                       mat[i][0]= mat[min][0];
		                       
		                       mat[i][1]= mat[min][1];
		                       mat[i][2]= mat[min][2];
		                       
		                       mat[min][0] = aux;
		                       
				    	 	   mat[min][1] = aux_1;
				               mat[min][2] = aux_2;
				    	
				    }
		    	
			}
			   }
				System.out.println("");
				for (int s = 0; s <l ; s++) {
		    		
		    	    System.out.println();
		    		for (int j = 0; j < 3; j++) {
		    		
		    			
		    			System.out.print(mat[s][j]+" ");
		    			
		    		}
		    	
		    	}
				
			return  mat;
			
		}
		


		public int[][] Ordenar_Insercion(Grafo<String> grafo)
				throws Exception {
			int menor=0;
		
			int[][] mat_k=new int[grafo.listaVertices.getTamanio()*(grafo.listaVertices.getTamanio())-1][3]; 
			//String[][] mat_k=new String[grafo.listaVertices.getTamanio()*(grafo.listaVertices.getTamanio())-1][3]; 
			//String[][] mat_k=new String[grafo.listaVertices.getTamanio()*4][/*3*/5]; 
			System.out.println(" ");
		//	for (int i = 0; i < grafo.listaVertices.getTamanio(); i++) {
			//	System.out.print((i+1)+".  ");
				//objGrafo.imprimirAdyacencias(0);
			int l=0;;
		     	for (int i = 0; i < grafo.listaVertices.getTamanio(); i++) {
				
				Lista listaAux;
				listaAux = listaVertices.getValor(i).getListaNodosADY();
				String vertices = ""; 
				String[] vect=new String[ grafo.listaVertices.getTamanio()];
				menor=listaAux.getpeso(0);
				for (int j = 0; j < listaAux.getTamanio(); j++) {
					vertices = vertices + listaAux.getValor(j)+", ";
					System.out.println("Varlor "+listaAux.getValor(j));
					System.out.println("Peso: "+listaAux.getpeso(j));
				//	mat_k[l][0]=String.valueOf(listaAux.getpeso(j));
				//	mat_k[l][1]=(String) listaVertices.getValor(i).getElemento();
					mat_k[l][0]=listaAux.getpeso(j);
				//	mat_k[l][2]=listaAux.getValor(j).toString();
					mat_k[l][1]=grafo.buscarPosicion(listaVertices.getValor(i).getElemento().toString());
					mat_k[l][2]=grafo.buscarPosicion(listaAux.getValor(j).toString());
				//	if(listaAux.getpeso(j)<menor)
				//	{
					//	menor=listaAux.getpeso(j);
				//	}	
					l++;
				}
			
			//	mat_k[l][1]=grafo.buscarPosicion(listaVertices.getValor(i).getElemento().toString());
			
					System.out.println("vertices adyacentes de  "+listaVertices.getValor(i).getElemento()+"   :"+vertices);
					setFilas_k(l);
				//	vect[i]=menor;
					//	System.out.println("Menor: "+vect[i]+",");
					
				//menor=0;
			}
		     	//grafo.listaVertices.getTamanio()*(grafo.listaVertices.getTamanio())-1
		    	System.out.println("Matriz");
		    	for (int i = 0; i <l ; i++) {
		    		
		    	    System.out.println();
		    		for (int j = 0; j < 3; j++) {
		    		
		    			
		    			System.out.print(mat_k[i][j]+" ");
		    			
		    		}
		    	
		    	}
			
			return 	ordenar_mat(mat_k,l); 
		}
		
		
		public int[][] MatrizSInOrden(Grafo<String> grafo)
				throws Exception {
			int menor=0;
		
			int[][] mat_k=new int[grafo.listaVertices.getTamanio()*(grafo.listaVertices.getTamanio())-1][3]; 
			//String[][] mat_k=new String[grafo.listaVertices.getTamanio()*(grafo.listaVertices.getTamanio())-1][3]; 
			//String[][] mat_k=new String[grafo.listaVertices.getTamanio()*4][/*3*/5]; 
			System.out.println(" ");
		//	for (int i = 0; i < grafo.listaVertices.getTamanio(); i++) {
			//	System.out.print((i+1)+".  ");
				//objGrafo.imprimirAdyacencias(0);
			int l=0;;
		     	for (int i = 0; i < grafo.listaVertices.getTamanio(); i++) {
				
				Lista listaAux;
				listaAux = listaVertices.getValor(i).getListaNodosADY();
				String vertices = ""; 
				String[] vect=new String[ grafo.listaVertices.getTamanio()];
			//	menor=listaAux.getpeso(0);
				for (int j = 0; j < listaAux.getTamanio(); j++) {
					vertices = vertices + listaAux.getValor(j)+", ";
					System.out.println("Varlor "+listaAux.getValor(j));
					System.out.println("Peso: "+listaAux.getpeso(j));
				//	mat_k[l][0]=String.valueOf(listaAux.getpeso(j));
				//	mat_k[l][1]=(String) listaVertices.getValor(i).getElemento();
					mat_k[l][0]=listaAux.getpeso(j);
				//	mat_k[l][2]=listaAux.getValor(j).toString();
					mat_k[l][1]=grafo.buscarPosicion(listaVertices.getValor(i).getElemento().toString());
					mat_k[l][2]=grafo.buscarPosicion(listaAux.getValor(j).toString());
				//	if(listaAux.getpeso(j)<menor)
				//	{
					//	menor=listaAux.getpeso(j);
				//	}	
					l++;
				}
			
			//	mat_k[l][1]=grafo.buscarPosicion(listaVertices.getValor(i).getElemento().toString());
			
					System.out.println("vertices adyacentes de  "+listaVertices.getValor(i).getElemento()+"   :"+vertices);
					setFilas_k(l);
				//	vect[i]=menor;
					//	System.out.println("Menor: "+vect[i]+",");
					
				//menor=0;
			}
		     	//grafo.listaVertices.getTamanio()*(grafo.listaVertices.getTamanio())-1
		    	System.out.println("Matriz");
		    
		    	
		  
			
			return 	mat_k; 
		}
		
		
		
		
		public int getFilas_k() {
			return filas_k;
		}

		public void setFilas_k(int filas_k) {
			this.filas_k = filas_k;
		}
	
	
}// Fin clase Grafo
