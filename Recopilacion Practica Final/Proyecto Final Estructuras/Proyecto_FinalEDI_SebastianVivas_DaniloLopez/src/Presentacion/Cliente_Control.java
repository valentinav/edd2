package Presentacion;

import javax.swing.JOptionPane;

import ALg_Recubrimiento.Dijkstra;
import Aplicacion_Grafo.Dibujo_GrafosAPP;
import Dibujo_Grafo.Dibujo_Grafos;
import Interfaz_Arboles.Dibujo;

public class Cliente_Control {

	public static Dibujo inst = new Dibujo();
	public static Dibujo_Grafos inst1 = new Dibujo_Grafos();
	public static Dibujo_GrafosAPP inst2 = new Dibujo_GrafosAPP();
	
	public static void main(String[] args) throws Exception {
		
		int opc = 0;
		//do{
			opc = menuOpciones();
		
		switch (opc){
		case 1:
			
			inst.setLocationRelativeTo(null);
			inst.setVisible(true);
			inst.setResizable(false);
			
			break;
			
		case 2:
			
			inst1.setLocationRelativeTo(null);
			inst1.setVisible(true);
			inst1.setResizable(false);			
			JOptionPane.showMessageDialog(null, "lea la informacion a su Derecha");
			
			break;
			
		case 3:
			
			inst2.setLocationRelativeTo(null);
			inst2.setVisible(true);
			inst2.setResizable(true);
			JOptionPane.showMessageDialog(null, "APLICACION SIMULADORA DE ENVIO DE DATOS\nBIENVENIDO", "Bienvenido", JOptionPane.INFORMATION_MESSAGE);
			String titulo = "\nPara dibujar el grafo:"
					+ "\n  1.Click Izquierdo sobre el area de dibujo\n    del grafo"
					+ "\n  2.Digite el nombre del vertice"
					+ "\n\nPara agregar una arista:"
					+ "\n  1.Click derecho en el area de Dibujo"
					+ "\n  2.Digite los valores solicitados";
			JOptionPane.showMessageDialog(null, ""+titulo, "Bienvenido", JOptionPane.INFORMATION_MESSAGE);

			
			break;
			
		default: 
			if(opc == 4){
				JOptionPane.showMessageDialog(null, "Aplicacion terminada con Exito");
			}
			else
			JOptionPane.showMessageDialog(null, "Opcion Incorrecta\nEscoja nuevamente", "Mensaje de error",JOptionPane.ERROR_MESSAGE);
			break;
		}
		//}while(opc != 2 );/*while(inst.dispose() == true);*/
		
	}
	
	
	static int menuOpciones() {
		String opc = JOptionPane.showInputDialog(null,
				"Que Aplicacion desea Ejecutar\n"
						+ "1. Arboles Binarios Busqueda\n" 
						+ "2. Grafos\n"
						+ "3. Simulador Sala de internet\n");
		int intOpc = 0;
		if(opc != null){
			try {
				intOpc = Integer.parseInt(opc);
			} catch (NumberFormatException nfe) {
				intOpc = menuOpciones();
			}
		}else
			intOpc = 4;
		return intOpc;
	}

}
