package herramientas;
/**
 * clase de tipo excepciones que atrapa y muestra el mensaje de un Throw
 * @author Usuario
 *
 */
public class Excepciones extends Exception{

	public static final long serialVersionUID=700L;
	/**
	 * resive un numero que sera comparado con las excepciones creadas para
	 *  mostrar en pantalla un mensaje
	 * @param NoExcepcion numero 1 o 2
	 */
	public Excepciones(int NoExcepcion)
	{
		System.out.print("\n ERROR: ");
		switch(NoExcepcion)
		{
			case 1:
				System.out.print("La Respuesta No Fue Valida !!!\n ");
				break;
			case 2:
				System.out.print("opcion no esta dentro del rago: ");
				break;
			default:
				System.out.print("excepcion no conocida ");
				break;
		}		
	}

}
