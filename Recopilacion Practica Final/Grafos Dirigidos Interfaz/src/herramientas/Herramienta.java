package herramientas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import Grafo.Grafo;
/**
 *Esta clase contiene metodos generales como de validar , y
 *  propios de este codigo como de interpretar resultado y Los metodos de opcion (op).
 *  
 *	<p> Mantiene un esquema mas organizado y entendible para su implementacion en la Clase <big><i>Main</i></big></p>
 */
public class Herramienta {
	
	static int op=-1;
	static int intMax=2147483647;
	static int intMin=-2147483647;
	protected static Scanner sc =new Scanner(System.in);
	
	static int dato=0;
	static Grafo <Integer>miGrafo =new Grafo <Integer> () ;		
	
	// metodos y funciones generales de validacion 

	/**
	 * interpreta el si o no de un booleano para mostrar en pantalla
	 * @param b booleano 
	 * @return Si, No
	 */
	public static String validarBooleano(boolean b)
	{
		if(b)
			return "Si";
		return "No";
	}
	
	/**
	 * pide un valor entre el rango posible de enteros 
	 * @param pregunta  texto que mostrara antes de pedir el dato
	 * @return un numero comprendido entre -2147483647 y 2147483647
	 */
	public static int validar(String pregunta)
	{
		return validar( pregunta, intMin,  intMax);
	}
	/**
	 * pide un valor entre un rango determinado 
	 * @param pregunta  texto que mostrara antes de pedir el dato
	 * @param valMin minimo valor que admite
	 * @param valMax maximo Valor que Admite
	 * @return un numero comprendido enter valMin y valMax
	 */
	public static int validar(String pregunta,int valMin, int valMax)
	{
		int trans,i=0;
		Scanner sc =new Scanner(System.in);
		do{
			if(i!=0)
			{
				try {
					throw new Excepciones(2);
				} catch (Excepciones e) {
					
					System.out.println(""+(valMin)+"-"+valMax+"\n");
				}
			}
				
			i++;
			System.out.print(pregunta);
			trans=sc.nextInt();	
		}while((trans>valMax)||(trans<valMin));
		return trans;
	}
	
	/**
	 * pide una respuesta en palabras "si" o "no"
	 * @param pregunta  texto que mostrara antes de pedir el dato
	 * @return un booleano ;True si, False no 
	 */
	public static boolean validarSiNo(String pregunta)
	{
		boolean op=false;
		boolean seguir=true;
		String rta="";
		InputStreamReader in=new InputStreamReader(System.in);
		BufferedReader buffer = new BufferedReader(in);
		int i=0;
		
		do{
			if(i!=0)
			{
				try {
					throw new Excepciones(1);
				} catch (Excepciones e) {
				}
			}
			i++;
			System.out.print(pregunta);
			try {
				rta= buffer.readLine().toLowerCase().trim();
			} catch (IOException e) {
			}	
			if((rta.equals("si"))||(rta.equals("no")))
				 seguir=false;
			if(rta.equals("si"))
				op=true;
		}while(seguir);
		return op;
	}

	
	//otros metodos y funciones propios de este codigo
	
	/**
	 * Interpreta el valor que es resivido a partir de un metodo que necesita de datos
	 * @param es la LLamada al metodo  agregarAdyacentes del grafo
	 * @return 
	 */
	public static  String interpretacionEncontrarDatos(int val ,String mensajeDeRealizado)
	{
		String msj="";
		switch(val)
		{
		case 0:msj=(mensajeDeRealizado);
		break;
		case -1:msj=("   No existe el primer elemento");
			break;
		case -2:msj=("   No existe el segundo elemento");
			break;
		case -3:msj=("   No existe ninguno de los dos elementos ");
			break;
	
		}
		return msj;
	}
	public static void menu()
	{
		System.out.println("1)agrega un vertice");
		System.out.println("2)verificar si esta un vertice");
		System.out.println("3)agregar arista con ponderacion");
		System.out.println("4) modifica una ponderacion");
		System.out.println("5) imprime vertices adyacentes de un vertice");
		System.out.println("6)matiz de adyacencia");
		System.out.println("7)SALIR");
	}
	public static String op1(int  verticeValor)
	{
		String mensaje="";
		if(!miGrafo.verificarVertices(	verticeValor))
		{
		miGrafo.agregarVertices(verticeValor);
		mensaje=("Vertice ingresado con Exito:");
		}else{mensaje=("el vertice ya existe:");}
		return mensaje;
	}
	public static String op2(int  buscado)
	{
		String mensaje="";
		mensaje=("  "+validarBooleano(miGrafo.verificarVertices(buscado))+" se encontro "+ buscado);
		return mensaje;
	}
	public static String op3( int vertice1 ,int vertice2,int peso )
	{
		String mensaje="";
		mensaje=interpretacionEncontrarDatos(miGrafo.agregarArista(vertice1, vertice2,peso), "   Adyacencia ingresada ");
	
		return mensaje;
	}
	public static boolean existen(int  buscado, int b2)
	{
		
		if ((miGrafo.verificarVertices(buscado))&&(miGrafo.verificarVertices(b2)))
			return true;
		return false;
		
	}
	public static String op4( int vertice1 ,int vertice2,int nuevoPeso )
	{
		String mensaje="";
		int incidencia=miGrafo.contarAdyacenciaEntre(	vertice1, vertice2);
		if (incidencia==1)
		{
			miGrafo.cambiarPeso(vertice1, vertice2,nuevoPeso);
			mensaje="El peso se modifico";
		}else if(incidencia==0)
		{
			mensaje=("No se encontro esa arista");
		}else
		{
			mensaje=("Se encontro mas de una arista igual ");
			}
		return mensaje;
	}
	public static boolean op4_masDeUnaArista( int vertice1 ,int vertice2 )
	{
		int incidencia=miGrafo.contarAdyacenciaEntre(	vertice1, vertice2);
		if ((incidencia!=0)&&(incidencia!=0))
			return true;
		return false;
		
	}
	public static String[] op4_cargarAristas( int vertice1 ,int vertice2 )
	{
		String []mensaje=(miGrafo.mostrarAristasEntreVertices(vertice1, vertice2)).split("\n");
		return mensaje;
	}
	public static String op4_cambiarPeso( int vertice1 ,int vertice2,int nuevoPeso, int eleccionDeID )
	{
		String mensaje="Se Ha Modificado El Peso por "+nuevoPeso;
		interpretacionEncontrarDatos(miGrafo.cambiarPeso(vertice1, vertice2,nuevoPeso, eleccionDeID), "   Peso Modificado ");
		
		return mensaje;
	}
	public static String op5( int vertice1 )
	{
		String mensaje="";
		
		if(miGrafo.verificarVertices(vertice1))
		{
			mensaje=("   Los adyacentes del Vertice"+vertice1+" : \n"+miGrafo.mostrarAdyaConPesos( vertice1));
		}else{mensaje=("ese vertice no existe");}
		return mensaje;
	}
	public static String op6( )
	{
		String mensaje="";

		mensaje =("Matriz de adyacencia \n");
		Matriz miMatriz =new Matriz(miGrafo.matrizAdyacencia());
		mensaje =(miMatriz.imprimirMatriz());
		return mensaje;
	}
	
	
}
