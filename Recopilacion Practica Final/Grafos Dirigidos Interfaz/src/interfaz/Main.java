package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.UIManager;

import Grafo.Grafo;
import herramientas.Herramienta;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
/**
 * 
 * @author  Janeth Cristina Cifuentes M 
 *
 */
public class Main extends Herramienta{

	private JFrame frame;
	private JTextField textBox1;
	private JTextField textBox2;
	private JTextField textBox3_1;
	private JTextField textBox3_3;
	private JTextField textBox3_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main()  {
		initialize(); 
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 613);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel mensaje = new JLabel("");
		mensaje.setForeground(Color.RED);
		mensaje.setBounds(10, 60, 380, 28);
		frame.getContentPane().add(mensaje);
		
		JLabel lblGrafo = new JLabel("Grafo <Integer>");
		lblGrafo.setForeground(new Color(72, 61, 139));
		lblGrafo.setFont(new Font("Dialog", Font.BOLD, 15));
		lblGrafo.setBounds(142, 11, 148, 19);
		frame.getContentPane().add(lblGrafo);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(180, 180, 180), 1, true), "Vertices", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 99, 414, 97);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel label1 = new JLabel("Vertice:");
		label1.setBounds(10, 29, 46, 14);
		panel_1.add(label1);
		
		textBox1 = new JTextField();
		textBox1.setBounds(82, 26, 141, 20);
		panel_1.add(textBox1);
		textBox1.setColumns(10);
		
		JButton btnIngresar = new JButton("Ingresar Vertice");
		btnIngresar.setBounds(233, 25, 156, 23);
		panel_1.add(btnIngresar);
		
		JLabel label2 = new JLabel("Vertice:");
		label2.setBounds(10, 67, 46, 14);
		panel_1.add(label2);
		
		textBox2 = new JTextField();
		textBox2.setBounds(82, 64, 141, 20);
		panel_1.add(textBox2);
		textBox2.setColumns(10);
		
		JButton VERIFICAR = new JButton("Verificar Vercice");
		VERIFICAR.setBounds(233, 63, 156, 23);
		panel_1.add(VERIFICAR);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(new LineBorder(new Color(180, 180, 180), 1, true), "ADYACENCIA", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(10, 411, 414, 152);
		frame.getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setToolTipText(" Zona De Impresion");
		textArea.setLineWrap(true);
		textArea.setForeground(Color.BLACK);
		textArea.setEditable(false);
		textArea.setBounds(228, 22, 176, 119);
		panel_3.add(textArea);
		
		JLabel label = new JLabel("Vertice:");
		label.setBounds(10, 37, 46, 14);
		panel_3.add(label);
		
		textBox = new JTextField();
		textBox.setColumns(10);
		textBox.setBounds(52, 34, 141, 20);
		panel_3.add(textBox);
		
		JButton btnBuscarAdyacentes = new JButton("Buscar Adyacentes");
		btnBuscarAdyacentes.setBounds(10, 62, 183, 20);
		panel_3.add(btnBuscarAdyacentes);
		
		JButton btnMatrizAyacencia = new JButton("Matriz Ayacencia");
		
		btnMatrizAyacencia.setBounds(10, 108, 183, 20);
		panel_3.add(btnMatrizAyacencia);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new LineBorder(new Color(180, 180, 180), 1, true), "Aristas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 207, 414, 190);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 30, 239, 35);
		panel_2.add(panel);
		
		JRadioButton rdbtn1 = new JRadioButton("ingresar arista");
		rdbtn1.setSelected(true);
		buttonGroup.add(rdbtn1);
		panel.add(rdbtn1);
		
		JRadioButton rdbtn2 = new JRadioButton("cambia Peso");
		buttonGroup.add(rdbtn2);
		panel.add(rdbtn2);
		
		JLabel lblVertice_1 = new JLabel("Vertice1 :");
		lblVertice_1.setBounds(10, 80, 71, 14);
		panel_2.add(lblVertice_1);
		
		textBox3_1 = new JTextField();
		textBox3_1.setBounds(91, 77, 141, 20);
		panel_2.add(textBox3_1);
		textBox3_1.setColumns(10);
		
		JLabel lblVertice = new JLabel("Vertice2 :");
		lblVertice.setBounds(10, 105, 71, 14);
		panel_2.add(lblVertice);
		
		textBox3_2 = new JTextField();
		textBox3_2.setBounds(91, 102, 141, 20);
		panel_2.add(textBox3_2);
		textBox3_2.setColumns(10);
		
		JButton btnComprovar = new JButton("COMPROVAR");
		btnComprovar.setBounds(268, 73, 116, 28);
		panel_2.add(btnComprovar);
		
		JLabel lblPeso = new JLabel("Peso :");
		lblPeso.setBounds(10, 140, 71, 14);
		panel_2.add(lblPeso);
		
		textBox3_3 = new JTextField();
		textBox3_3.setBounds(91, 133, 141, 20);
		panel_2.add(textBox3_3);
		textBox3_3.setEnabled(false);
		textBox3_3.setColumns(10);
		
		JButton btnCambia = new JButton("cambiar Peso");
		btnCambia.setBounds(268, 119, 136, 57);
		panel_2.add(btnCambia);
		btnCambia.setEnabled(false);
		
		JComboBox aristas = new JComboBox();
		aristas.setBounds(10, 166, 210, 20);
		panel_2.add(aristas);
		aristas.setEnabled(false);
		btnCambia.setVisible(false);
		btnCambia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textBox3_1.setEnabled(true );
				lblPeso.setText("Peso");
				textBox3_2.setEnabled(true);
				btnComprovar.setEnabled(true);
				btnCambia.setEnabled(false );
				textBox3_3.setEnabled(false);
				aristas.setEnabled(false);
				if(rdbtn1.isSelected())
				{
					
					mensaje.setText(op3(Integer.parseInt(textBox3_1.getText()),
							Integer.parseInt(textBox3_2.getText()),Integer.parseInt(textBox3_3.getText())));
					
				}else if(rdbtn2.isSelected())
				{
					mensaje.setText(op4_cambiarPeso(Integer.parseInt(textBox3_1.getText()),
							Integer.parseInt(textBox3_2.getText()),Integer.parseInt(textBox3_3.getText())
							,aristas.getSelectedIndex()));
					
				
				}
			}
		});
		
		btnComprovar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (existen((Integer.parseInt(textBox3_1.getText())),(Integer.parseInt(textBox3_2.getText()))))
				{
					if(rdbtn1.isSelected())
					{
						btnCambia.setText("ingresar arista");
						btnCambia.setVisible(true);
						btnCambia.setEnabled(true);
						textBox3_3.setEnabled(true);
						textBox2.setText("");
						textBox3_1.setEnabled(false);
						textBox3_2.setEnabled(false);
						btnComprovar.setEnabled(false);
					}else if(rdbtn2.isSelected())
					{
						lblPeso.setText("nuevo peso");
						textBox3_1.setEnabled(false);
						textBox3_2.setEnabled(false);
						btnComprovar.setEnabled(false);
						btnCambia.setText("cambia peso");
						btnCambia.setVisible(true);
						btnCambia.setEnabled(true);
						textBox3_3.setEnabled(true);
						aristas.setEnabled(true);
						mensaje.setText(op4(Integer.parseInt(textBox3_1.getText()),
								Integer.parseInt(textBox3_2.getText()),Integer.parseInt(textBox3_3.getText())));
						aristas.setModel(new javax.swing.DefaultComboBoxModel(op4_cargarAristas(Integer.parseInt(textBox3_1.getText()),Integer.parseInt(textBox3_2.getText()))));
						aristas.removeItemAt(0);
						aristas.setSelectedIndex(0);
						textBox2.setText("");
					
					}
				}else
				{
					mensaje.setText("algun vertice no existe");
				}
				
			}
		});
		textBox3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if((textBox3_1.getText()!="")&&(textBox3_2.getText()!=""))
				{
					if (op4_masDeUnaArista(Integer.parseInt(textBox3_1.getText()),Integer.parseInt(textBox3_2.getText())))
					{
						mensaje.setText("entrando");
						aristas.enable(true);						
					}
					
				}
			}
		});
		rdbtn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtn2.isSelected())
				{

					textBox3_1.setEnabled(true );
					textBox3_2.setEnabled(true);
					textBox3_3.setEnabled(false);
					btnComprovar.setText("COMPROBAR");
					btnComprovar.setEnabled(true);
					aristas.setVisible(true);
					btnCambia.setVisible(true);
					btnCambia.setEnabled(false);
				}
			}
		});
		rdbtn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtn1.isSelected())
				{
					textBox3_1.setEnabled(true );
					textBox3_2.setEnabled(true);
					btnComprovar.setEnabled(true);
					textBox3_3.setEnabled(false);
					aristas.setVisible(false);
					btnCambia.setVisible(false);
				}
			}
		});
		VERIFICAR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mensaje.setText(op2(Integer.parseInt(textBox2.getText())));
				textBox2.setText("");
			}
		});
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mensaje.setText(op1(Integer.parseInt(textBox1.getText()))+" "+textBox1.getText());
				textBox1.setText("");
			}
		});
		btnBuscarAdyacentes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(op5(Integer.parseInt(textBox.getText())));
				textBox2.setText("");
			}
		});
		btnMatrizAyacencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(op6());
			}
		});
		///CAMBIANDO TIPO DE TEXTBOX
		textBox1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c=arg0.getKeyChar(); 
		          if(!Character.isDigit(c)){ 
		        	  if (c=='-')
		        	  {
		        		  if(! (textBox1.getText().equals("")))
		        		  {
		        			  arg0.consume();  
		        			  mensaje.setText("si el numero es negativo ingrese al inicio el menos"); 
		        		  }
		        	  }else
		        	  {
		        		  arg0.consume();  
		        		  mensaje.setText("Ingresa Solo Numeros"); 
		        	  }
		          };
		         
			}
		});
		textBox2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c=arg0.getKeyChar(); 
		          if(!Character.isDigit(c)){ 
		        	  if (c=='-')
		        	  {
		        		  if(! (textBox2.getText().equals("")))
		        		  {
		        			  arg0.consume();  
		        			  mensaje.setText("si el numero es negativo ingrese al inicio el menos"); 
		        		  }
		        	  }else
		        	  {
		        		  arg0.consume();  
		        		  mensaje.setText("Ingresa Solo Numeros"); 
		        	  }
		          };
			}
		});
		textBox3_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar(); 
		          if(!Character.isDigit(c)){ 
		        	  if (c=='-')
		        	  {
		        		  if(! (textBox3_1.getText().equals("")))
		        		  {
		        			  e.consume();  
		        			  mensaje.setText("si el numero es negativo ingrese al inicio el menos"); 
		        		  }
		        	  }else
		        	  {
		        		  e.consume();  
		        		  mensaje.setText("Ingresa Solo Numeros"); 
		        	  }
		          };
			}
		});
		textBox3_2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar(); 
		          if(!Character.isDigit(c)){ 
		        	  if (c=='-')
		        	  {
		        		  if(! (textBox3_2.getText().equals("")))
		        		  {
		        			  e.consume();  
		        			  mensaje.setText("si el numero es negativo ingrese al inicio el menos"); 
		        		  }
		        	  }else
		        	  {
		        		  e.consume();  
		        		  mensaje.setText("Ingresa Solo Numeros"); 
		        	  }
		          };
			}
		});
		textBox3_3.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar(); 
		          if(!Character.isDigit(c)){ 
		        	  if (c=='-')
		        	  {
		        		  if(! (textBox3_3.getText().equals("")))
		        		  {
		        			  e.consume();  
		        			  mensaje.setText("si el numero es negativo ingrese al inicio el menos"); 
		        		  }
		        	  }else
		        	  {
		        		  e.consume();  
		        		  mensaje.setText("Ingresa Solo Numeros"); 
		        	  }
		          };
			}
		});
		textBox.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar(); 
		          if(!Character.isDigit(c)){ 
		        	  if (c=='-')
		        	  {
		        		  if(! (textBox.getText().equals("")))
		        		  {
		        			  e.consume();  
		        			  mensaje.setText("si el numero es negativo ingrese al inicio el menos"); 
		        		  }
		        	  }else
		        	  {
		        		  e.consume();  
		        		  mensaje.setText("Ingresa Solo Numeros"); 
		        	  }
		          };
			}
		});
	}
}
