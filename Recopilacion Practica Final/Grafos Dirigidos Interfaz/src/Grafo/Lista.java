package Grafo;


public class Lista<T> {
	
	private NodoLista<T> cabeza;
	private int tamanio;
	private String datosTemporales;	
	
//INICIALIZADOR	DE VARIABLE TEMP
	public void inicializaDatosTemporales() {
		this.datosTemporales = "";
	}
	
//GETTERS Y SETTERS	
	private String getDatosTemporales() {
		return datosTemporales;
	}

	private void setDatosTemporales(String datosTemporales) {
		this.datosTemporales = this.datosTemporales+ datosTemporales;
	}	

	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}
	/**
	 * a diferencia del otro Metodo SET de temporal {@link #setDatosTemporales(String)} este es ya acumulador
	 * @param string
	 */
	private void setTemporal(String string) {
		this.datosTemporales=this.datosTemporales+string;
		
	}

	public NodoLista<T> getCabeza() {
		return cabeza;
	}

	public void setCabeza(NodoLista<T> cabeza) {
		this.cabeza = cabeza;
	}
//METODOS Y FUNCIONES
	
	/**
	 * Resive el nodo grafo y lo combierte en un nodo lista que agrega a la 
	 * lista para tener un siguiente y simular un arreglo Dinamico
	 * @param valor
	 */
	public void agregar(T valor) {
		NodoLista<T> nuevo = new NodoLista<T>();		
		nuevo.setValor(valor);		
		if (esVacia()) {
			cabeza = nuevo;
		} else {
			// agregar al final de la lista
			NodoLista<T> aux = cabeza;
			while (aux.getSiguiente() != null) {
				aux = aux.getSiguiente();
			}
			aux.setSiguiente(nuevo);
		}
		tamanio++;
	}
	
	/**
	 * 
	 * @return retorna una cadena con el vertice adyacente con su respectivo peso
	 */
	public String mostrarListaConPesos( int imprimir)
	{
		this.inicializaDatosTemporales();
		return mostrarListaConPesos(this.getCabeza(), imprimir);
	}
	
	/**
	 * desde la cabeza empezamos a rrecorrer los siguientes hasta llegar al nulo  almacenando en un 
	 * temporal tanto el vertice adyacente como el peso que corresponde
	 * @param miNodo es la cabeza de la lista
	 * @return
	 */
	private String mostrarListaConPesos( NodoLista miNodo, int imprimir)
	{
		if (miNodo!=null)
		{			
			setTemporal(imprimir+ "  -> "+((NodoGrafo) miNodo.getValor()).getElemento()+"; coste="
					+ ((NodoGrafo) miNodo.getValor()).getPeso()+" \n");
			mostrarListaConPesos(miNodo.getSiguiente(), imprimir);
		}
		return this.getDatosTemporales();
	}
	/**
	 *  Retornara el peso que se encuentra a travez de este metodo de un determinado vertice
	 * @param adyacente es el valor entero del vertice adyacente al que se le encontrara su peso
	 * @return el peso del adyacente
	 */
	public String mostrarPesos(int adyacente )
	{
		this.inicializaDatosTemporales();
		return mostrarPesos(this.getCabeza(), adyacente);
	}
	
	/**
	 * compara con todos los vertices adyacentes de un solo vertice  hasta encontrar
	 *  el peso (anteriormente ya se valido que debia existir arista estos)
	 * @param miNodo es la cabeza que nos permite movernos en la lista 
	 * @param adyacente es el valor entero del vertice adyacente que buscamos
	 * @return el peso del ayacente
	 */
	private String mostrarPesos( NodoLista miNodo, int adyacente )
	{
		if (miNodo!=null)
		{			
			if(((NodoGrafo) miNodo.getValor()).getElemento().equals(adyacente))
			{
				setTemporal(""+ ((NodoGrafo) miNodo.getValor()).getPeso());
			}else{
				System.out.println("no entro por que "+adyacente+" difeente "+((NodoGrafo) miNodo.getValor()).getElemento());
				mostrarPesos(miNodo.getSiguiente(), adyacente );
			}			
		}
		return this.getDatosTemporales();
	}
	
	/**
	 * 
	 * @return cadena con todos los vertices de la lista 
	 */
	public String mostrarLista( )
	{
		this.inicializaDatosTemporales();;
		return mostrarLista(this.getCabeza());
	}
	
	/**
	 * Reccorre con el apuntador de la cabeza cada uno de los nodos grafo ingresados en la lista  obteniendo
	 *  el valor del elemento y acumulandolos en un temporal hasta llegar al final
	 * @param miNodo es la cabea que nos permite movernos en toda la lista
	 * @return cadena con todos los vertices de la lista 
	 */
	private String mostrarLista( NodoLista miNodo)
	{
		if (miNodo!=null)
		{
			setTemporal(" "+((NodoGrafo) miNodo.getValor()).getElemento());
			mostrarLista(miNodo.getSiguiente());
		}
		return this.getDatosTemporales();
	}

	/**
	 * verifica si la cabeza esta vacia 
	 * @return TRUE vacia; False no vacia
	 */
	public  boolean esVacia() {
		if(cabeza==null)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * busca su existe en la lista un valor
	 * @param vertice elemento a buscar
	 * @return booleano TRUE si se encontro, False no se encontro
	 */
	public boolean BuscarLista(int vertice) {		
		return BuscarLista(vertice,this.cabeza );
	}
	
	/**
	 * este metodo busca un elemento para este ejemplo entero dentro de la lista retorna booleano
	 * @param elemento el elemento que se busca
	 * @param lista la cabeza
	 * @return booleano TRUR si se encontro, False no se encontro
	 */
	private boolean BuscarLista(int  buscado, NodoLista<T> lista) {
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) buscado;
		if(lista==null){
			return false;
		}else if(valor ==elElemento){
			//System.out.println("el elemento se encontro");
		  return true;
		} else {
			if (lista.getSiguiente()!=null){
				return BuscarLista(buscado,lista.getSiguiente() );
			 }else{
				 return false;
			}
		}
	}
	
	/**
	 * agrega una arista entre dos vertices con un respectivo peso
	 * @param buscado  vertice 1 
	 * @param adyacente vertice 2
	 * @param peso valor de la arista
	 */
	
	public void agregaAdyacencia(int buscado, int adyacente, int peso,int id) {
		agregaAdyacencia(buscado, adyacente,this.cabeza, peso,id);
	}
	/**
	 * nos movemos en la lista  con ayuda del metodo siguiente del NL hasta encontrar el VA 
	 * 
	 * @param buscado  vertice 1 
	 * @param adyacente vertice 2
	 * @param lista cabeza  con la que nos movemos en la lista
	 * @param peso valor de la arista
	 */
	private void agregaAdyacencia(int buscado, int adyacente, NodoLista<T> lista , int peso,int id) {
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) buscado;
		if(lista==null){
			//return false;
		}else if(valor ==elElemento){
			((NodoGrafo) lista.getValor()).setListaNodos(new NodoGrafo(adyacente,peso,id));			
		} else {
			if (lista.getSiguiente()!=null){
				agregaAdyacencia(buscado,adyacente, lista.getSiguiente(),peso , id);
			}
		}
	}
	

	public void agregaAdyacencia(int buscado, int adyacente) {
		agregaAdyacencia(buscado, adyacente,this.cabeza);
	}
	
	private void agregaAdyacencia(int buscado, int adyacente, NodoLista<T> lista ) {
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) buscado;
		if(lista==null){
			//return false;
		}else if(valor ==elElemento){
			((NodoGrafo) lista.getValor()).setListaNodos(new NodoGrafo(adyacente));			
		} else {
			if (lista.getSiguiente()!=null){
				agregaAdyacencia(buscado,adyacente, lista.getSiguiente() );
			}
		}
	}
	/**
	 * Busca los vertices adyacentes de un Vertice
	 * @param buscado el valor del vertice buscado
	 * @return
	 */
	public String mostrarAdyacencia(int buscado )
	{
		this.inicializaDatosTemporales();;
		 mostrarAdyacencia(this.getCabeza(),buscado);
		 if( this.getDatosTemporales().equals("")){
				return "no hay datos asociados a este valor";
			}else{
			 return this.getDatosTemporales();
			}
	}
	
	private void mostrarAdyacencia( NodoLista lista, int buscado)
	{
		String ady="naa";
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) buscado;
		 if(valor ==elElemento){
			this.setDatosTemporales(((NodoGrafo) lista.getValor()).getListaNodos().mostrarLista());
			ady=((NodoGrafo) lista.getValor()).getListaNodos().mostrarLista();
		 }else {
			if (lista.getSiguiente()!=null){
				mostrarAdyacencia( lista.getSiguiente(),buscado );
			}
		}
	}
	/**
	 * imprime las aristas entre dos vertices(con el peso)asi se encuentren repetidas
	 * @param vertice1 vertice 
	 * @param vertice2 vertice adyacente
	 * @return retorna una cadena con los vertices adyacentes que coinciden con las aristas entre Vertice 1 y 2  con su respectivo peso
	 */
	public String mostarArista(int vertice1, int vertice2 )
	{
		this.inicializaDatosTemporales();
		mostarArista(this.getCabeza(),vertice1, vertice2);
		if( this.getDatosTemporales().equals("")){
			return "   No hay vertices asociados a este valor ";
		}else{
		 return this.getDatosTemporales();
		}
	}
	/**
	 * este metodo localiza el vertice y envia a otro metodo  {@link #mostrarVerticeCoincida(int)}  metodo el vertice adyacente 
	 * para encontrar todas las aristas entre estos 2
	 * @param lista es la cabeza
	 * @param vertice1 vertice
	 * @param vertice2 vertice adyacente
	 */
	private void mostarArista( NodoLista lista, int vertice1, int vertice2 )
	{
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) vertice1;
		 if(valor ==elElemento){
			this.setDatosTemporales(((NodoGrafo) lista.getValor()).getListaNodos().mostrarVerticeCoincida( vertice2));
		} else {
			if (lista.getSiguiente()!=null){
				mostarArista( lista.getSiguiente(),vertice1,  vertice2 );
			}
		}
	}
	/**
	 * es un metodo que recorre SOLO la lista interna de vertices adyacentes
	 * @param vertice es un vertice adyacente que buscamos 
	 * @return una cadena con  el identificador de cada arista seguido del vertice adyacente y luego su peso 
	 */ 
	private String mostrarVerticeCoincida( int vertice )
	{
		this.inicializaDatosTemporales();
		return mostrarVerticeCoincida(this.getCabeza(),vertice);
	}
	/**
	 * este metodo es la logica del retorno de {@link #mostrarVerticeCoincida(int)}
	 * @param miNodo es la cabeza de la lista que contiene los Vertices Adyacentes 
	 * @param vertice es un vertice adyacente 
	 * @return una cadena con  el identificador de cada arista seguido del vertice adyacente y luego su peso 
	 */
	private String mostrarVerticeCoincida( NodoLista miNodo,int vertice)
	{
		if (miNodo!=null)
		{			
			if (((NodoGrafo) miNodo.getValor()).getElemento().equals(vertice)){
				setTemporal("\n "+((NodoGrafo) miNodo.getValor()).getCantidad()+"->(VA: "+((NodoGrafo) miNodo.getValor()).getElemento()+"; peso="
					+ ((NodoGrafo) miNodo.getValor()).getPeso()+" )");
			}
			mostrarVerticeCoincida(miNodo.getSiguiente(), vertice);
		}
		return this.getDatosTemporales();
	}
	
	public String mostrarAdyacenciaConPeso(int buscado )
	{
		this.inicializaDatosTemporales();
		mostrarAdyacenciaConPeso(this.getCabeza(),buscado);
		if( this.getDatosTemporales().equals("")){
			return "   No hay vertices asociados a este valor ";
		}else{
		 return this.getDatosTemporales();
		}
	}
	private void mostrarAdyacenciaConPeso( NodoLista lista, int buscado)
	{
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) buscado;
		 if(valor ==elElemento){
			this.setDatosTemporales(((NodoGrafo) lista.getValor()).getListaNodos().mostrarListaConPesos(buscado));
		} else {
			if (lista.getSiguiente()!=null){
				mostrarAdyacenciaConPeso( lista.getSiguiente(),buscado );
			}
		}
	}
	
	/**
	 * 
	 * @param buscado es el  que se busca vertice
	 * @param adyacente es  el vertice adyacente que se busca en la lista de vertice
	 * @return el peso
	 */
	public String encontrarPeso(int buscado , int adyacente)
	{
		this.inicializaDatosTemporales();
		encontrarPeso(this.getCabeza(),buscado, adyacente);
		if( this.getDatosTemporales().equals("")){
			return "no hay peso algo anda mal por deferco deberia ser cero TENER CUIDADO SI SALE ESTE MENSAJE";
		}else{
		 return this.getDatosTemporales();
		}
	}
	
	/**
	 * ubica el vertice y manda el vertice adyacente para encontar el peso entre la lista de vertices adyacentes
	 * @param lista es la cabeza  que hace posible navegar entre la lista al apuntar a un siguiente
	 * @param buscado es el valor entero que se busca en los vertice
	 * @param adyacente es  valor entero del vertice adyacente que se busca en la lista del vertice
	 */
	private void encontrarPeso( NodoLista lista, int buscado, int adyacente)
	{
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) buscado;
		 if(valor ==elElemento){
			this.setDatosTemporales(((NodoGrafo) lista.getValor()).getListaNodos().mostrarPesos(adyacente));
		} else {
			if (lista.getSiguiente()!=null){
				encontrarPeso( lista.getSiguiente(),buscado,adyacente );
			}
		}
	}
	/**
	 * este metodo cambiara el peso asignado a una arista
	 * @param vertice1 vertice
	 * @param vertice2 vertice adyacente
	 * @param nuevoPeso peso que remplaza el antiguo
	 * @param idVerticeArista identificador de arista entre 2 mismos vertices 
	 */
	public void modificarPeso(int vertice1, int vertice2, int nuevoPeso, int idVerticeArista) {
		modificarPeso(this.cabeza, vertice1,  vertice2,  nuevoPeso,  idVerticeArista);
	}
	/**
	 * logica del metodo {@link #modificarPeso(int, int, int, int)} 
	 * este metodo recorre la lista de <b>  Vertices</b> hasta encontrar el valor almacenado en <big><i>vertice1</i></big>
	 * para recuperar la lista de <i>Vertices Adyacentes</i> y enviarle al metodo {@link #setPeso(int, int, int)}} el 
	 * Vertices Adyacente con su nuevo peso
	 * @param lista es la cabeza de la lista
	 * @param vertice1 es valor del vertice
	 * @param vertice2 es el valor del Vertice Adyancte
	 * @param nuevoPeso peso que remplaza el antiguo
	 * @param idVerticeArista identificador de arista entre 2 mismos vertices 
	 */
	private void modificarPeso( NodoLista<T> lista, int vertice1, int vertice2, int nuevoPeso, int idVerticeArista) {
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) vertice1;
		if(lista==null){
			//return false;
		}else if(valor ==elElemento){
			Lista VA=((NodoGrafo) lista.getValor()).getListaNodos();
			VA.setPeso(vertice2, nuevoPeso, idVerticeArista);
			
		} else {
			if (lista.getSiguiente()!=null){
				modificarPeso( lista.getSiguiente(),  vertice1,  vertice2,  nuevoPeso,  idVerticeArista);
			}
		}

		
	}
	/**
	 * este metodo sirve SOLO para recorrer la lista de Vertices Adyacentes con el objetivo de cambiar un peso determinado
	 * @param VetriceA es el vertice adyacente
	 * @param nuevoPeso es el peso que remplazara al antiguo
	 * @param idVerticeArista es el que identificara la arista en caso de que haya mas de una entre los 2 vertices
	 */
	private void setPeso(int VetriceA, int nuevoPeso,int idVerticeArista) {
		setPeso(VetriceA, nuevoPeso,this.cabeza,  idVerticeArista);
	}
	/** 
	 *  es la logica de {@link #setPeso(int, int, int)} donde recorre desde la cabeza de la lista hasta encontrar coincidencias en 
	 *  el valor del vertice y luego verifica la de su identificador <big><i>  idVerticeArista </i></big> para proceder a 
	 *  remplazar el peso
	* @param VetriceA es el vertice adyacente
	 * @param nuevoPeso es el peso que remplazara al antiguo
	 * @param lista es la cabeza de los vertices adyacentes
	 * @param idVerticeArista es el que identificara la arista en caso de que haya mas de una entre los 2 vertices
	 */
	private void setPeso(int VetriceA, int nuevoPeso, NodoLista<T> lista, int idVerticeArista ) {
		
		int valor =(int) ((NodoGrafo) lista.getValor()).getElemento();
		int elElemento=(int) VetriceA;
		if(lista==null){
			//return false;
		}else if(valor ==elElemento)
		{
			Integer id=((NodoGrafo) lista.getValor()).getCantidad(); 
			if(id.equals((idVerticeArista)))
			{
				((NodoGrafo) lista.getValor()).setPeso(nuevoPeso);	
			}else{ 
				if (lista.getSiguiente()!=null)
				{
					setPeso(VetriceA,nuevoPeso, lista.getSiguiente(), idVerticeArista );
				}
			}		
		} else {
			
			if (lista.getSiguiente()!=null){
				setPeso(VetriceA,nuevoPeso, lista.getSiguiente(), idVerticeArista );
			}
		}
	}
}