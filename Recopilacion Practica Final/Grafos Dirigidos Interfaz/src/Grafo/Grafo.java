package Grafo;

import herramientas.Matriz;

public class Grafo <T> {

	private Lista<NodoGrafo> listaVertices;
	private String temp;
//CONTRUCTOR
	public Grafo() {
		listaVertices = new Lista<NodoGrafo>();
	}	
//INICIALIZADOR	DE VARIABLE TEMP
	public void inicializarTemp() {
			temp="";
	}
		
//GETTERS Y SETTERS	
	public Lista<NodoGrafo> getListaVertices() {
		return listaVertices;
	}

	public void setListaVertices(Lista<NodoGrafo> listaVertices) {
		this.listaVertices = listaVertices;
	}
	
	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = this.temp+temp ;
	}
	
//METODOS 	Y  FUNCIONES
	/**
	 * Convirte el valor en un nodo de el grafo {@link #NodoGrafo()} 
	 * @param vertice es el valor que se ingresara
	 */
	public void agregarVertices(T vertice) {
		NodoGrafo agregado =new NodoGrafo(vertice);
		listaVertices.agregar(agregado);
	}
	
	/**
	 * 
	 * @return una cadena que esta compuesta por todos los vertices sin espacios inicial ni final
	 */
	public String imprimirVertices () {
		return this.listaVertices.mostrarLista().trim();
	}
	
	/**
	 * manda a buscar en la lista un valor del vertice
	 * @param vertice el valor que sera buscado
	 * @return TRUE Si lo encontro; FALSE si no lo encontro
	 */
	public boolean verificarVertices (int vertice) {
		if(this.listaVertices.esVacia())
			return false;
		return this.listaVertices.BuscarLista(vertice);		
	}
	
	/**
	 * agrega arista en caso de que los dos valores existan con un peso de cero
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int  agregarArista(int valor1,int valor2) {		
		int rta =this.existencia2Valores(valor1, valor2);
		if(interpretaExistencia(rta))
		{
			if(valor1!=valor2 )
			{
				this.listaVertices.agregaAdyacencia(valor1, valor2);
				this.listaVertices.agregaAdyacencia(valor2, valor1);
			}else{
				System.out.println("se creo un arco");
				this.listaVertices.agregaAdyacencia(valor1, valor2);
			}
		}
		return rta;		
	}
	
	/**
	 * agrega arista en un grafo ponderado en caso de que existan los dos valores 
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int  agregarArista(int valor1,int valor2,int peso) {		
		int rta =this.existencia2Valores(valor1, valor2);
		int cantAristas=this.contarAdyacenciaEntre(valor1, valor2);
		
		if(interpretaExistencia(rta))
		{
			if(valor1!=valor2 ){
				this.listaVertices.agregaAdyacencia(valor1, valor2, peso,cantAristas );
			}else{
					System.out.println("se creo un arco");
					this.listaVertices.agregaAdyacencia(valor1, valor2, peso,cantAristas);
			}
		}
		return rta;		
	}
	
	/**
	 * verifica que dos valores se encuentren en la lista
	  * @param x elemento del vertice 1
	 * @param xx elemento del vertice 2
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio
	 * (valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	private int existencia2Valores(int x , int xx)
	{
		if((this.listaVertices.BuscarLista(x))&&(this.listaVertices.BuscarLista(xx)))
		{
			return 0;			
		}else{
			int a=0;
			if( !this.listaVertices.BuscarLista(x))
			 {
				a= -1;				
			 }
			if( !this.listaVertices.BuscarLista(xx))
			{
				a= a-2;
			}
			return a;
		}
	}
	
	/**
	 * Hace una interpretacion en booleano del retorno del metodo {@link #existencia2Valores
	 * (int x, int xx)} debolviendo un booleano
	 * @param numero debe ser la llamada a un metodo que retorne cero en caso de existir 
	 * @return TRUE si Existe, FALSE no Existe
	 */
	private boolean interpretaExistencia(int numero)
	{
		if(numero==0)
			return true;
		return false;
	}
	
	/**
	 * Muestra todos los vertices adyacentes de un vertice 
	 * @param buscado es el vertise que se busca
	 * @return una cadena con sus vertices
	 */
	public String mostrarAdya (int buscado) {
		return this.listaVertices.mostrarAdyacencia(buscado);		
	}
	
	/**
	 * Muestra todas las aristas entre dos vertices
	 * 
	 * @param buscado es el vertise que se busca
	 * @return retorna una cadena con sus vertices y pesos
	 */
	
	public String mostrarAristasEntreVertices (int vertice1, int vertice2) {
		String cadena= this.listaVertices.mostarArista(vertice1, vertice2);	
		
		return cadena;
	}
	
	
	/**
	 * Muestra todos los vertices adyacentes de un vertice  con su peso 
	 * @param buscado es el vertise que se busca
	 * @return retorna una cadena con sus vertices y pesos
	 */
	public String mostrarAdyaConPesos (int buscado) {
		String cadena= this.listaVertices.mostrarAdyacenciaConPeso(buscado);	
		
		return cadena;
	}
	
	/**
	 * Este metodo verifica si existe adyacencia o enlace entre dos numeros que existan o no 
	 * (cuando alguno de los dos o los dos no existen sera Falso)
	 * @param valor1  elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return un retorno TRUE significa que los dos existen y tienen un enlace (valor 1 esta
	 *  en la lista de valor dos por lo tanto tambien 2 en uno)
	 *  un retorno FALSE indica que o algun valor no existe o no tienen enlace 
	 */
	public boolean adyacenciaEntre(int valor1, int valor2) {
		int rta =this.existencia2Valores(valor1, valor2);
		if(interpretaExistencia(rta))
		{
			String [] miArregloAdyacentes =this.listaVertices.mostrarAdyacencia(valor1).trim().split(" ");
			for(int i =0; i< miArregloAdyacentes.length;i++)
			{
				if (miArregloAdyacentes[i].equals(valor2+""))
				{
					return true;
				}
			}			
		}		
		return false;		
	}
	/**
	 * este metodo cuenta que cantidad de aristas hay entre mismos dos vertices 
	 * @param valor1 vertice 1 
	 * @param valor2 vertice 2
	 * @return la <B>CANTIDAD</B> de aristas entre 2 vertices
	 */
	public int contarAdyacenciaEntre(int valor1, int valor2) {
		int cantidad=0;
		int rta =this.existencia2Valores(valor1, valor2);
		if(interpretaExistencia(rta))
		{
			String [] miArregloAdyacentes =this.listaVertices.mostrarAdyacencia(valor1).trim().split(" ");
			for(int i =0; i< miArregloAdyacentes.length;i++)
			{
				if (miArregloAdyacentes[i].equals(valor2+""))
				{
					cantidad++;
				}
			}			
		}	
		return cantidad;		
	}
	
	/**
	 * Verifica una existencia en adyacencia y encuentra el peso entre dos datos 
	 * @param valor1 elemento del vertice 1
	 * @param valor2 elemento del vertice 2
	 * @return retorna el peso de los dos datos o si no es posible un msj "no existe adyacencia " 
	 * (en caso de no existir alguno de los datos o no haber un camino entre ellos)
	 */
	public String pesoEntre(int valor1, int valor2) {
		if(this.adyacenciaEntre(valor1, valor2))
		{
			 return this.listaVertices.encontrarPeso(valor1,valor2);
		}else{
			return "no existe adyacencia ";
		}
	}
	
	/**
	 * Genera y debuelve una matriz de [N][N] llena de ceros (no hay camino)y 1 (que significan si hay camino )
	 * la matriz es formada por sus vertices tanto en las filas como columnas encontrando incidencias en el camino
	 * anotandolas como se dijo al inicio[1,0]
	 */
	public int [][]  matrizAdyacencia() 
	{
		String[] vertices= this.imprimirVertices().split(" ");
		int cant=this.cantidadVertices();
		int [][] matrizA=new int[cant][cant];
		for(int i =0; i< cant; i++)
		{
			for(int j =0; j< cant; j++)
			{
				if(this.adyacenciaEntre(Integer.parseInt(vertices[i]), Integer.parseInt(vertices[j])))
				{
					matrizA[i][j]= this.contarAdyacenciaEntre(Integer.parseInt(vertices[i]),Integer.parseInt( vertices[j]));
				}else{
					matrizA[i][j]= 0;
				}
			}			
		}
		return matrizA;
	}
	public int [][]  matrizAdyacencia2() 
	{
		String[] vertices= this.imprimirVertices().split(" ");
		int cant=this.cantidadVertices();
		int [][] matrizA=new int[cant][cant];
		Matriz miMatriz =new Matriz(matrizA);
		miMatriz.llenarConCeros();
		for(int i =0; i< cant; i++)
		{
			for(int j =0; j< cant; j++)
			{
				if(this.adyacenciaEntre(Integer.parseInt(vertices[i]), Integer.parseInt(vertices[j])))
				{
					int nuevoValor=miMatriz.gerPosicion(i, j)+1 ;
					miMatriz.setPosicion(i, j,nuevoValor );
				}
			}			
		}
		return matrizA;
	}
	/**
	 * 
	 * @return retorna la cantidad de vertices ingresados 
	 */
	public int cantidadVertices() 
	{
		return this.imprimirVertices().split(" ").length;
	}
	
	/**
	 * Este metodo cambia el peso entre dos nodos  que tienen mas de una arista pidiendo un identificador para 
	 * saber que peso cambiaremos
	 * @param vertice1 valor1 elemento del vertice 1
	 * @param vertice2 valor1 elemento del vertice 1
	 * @param nuevoPeso peso que actualizara el antiguo
	 * @param idVerticeArista es el identificador 
	 * @return si es retorno negarivo  No se hizo la operacion por que algun dato no cumplio(valor 1 es -1 , valor2 es -2, ninguno -3) , si es 0 se realizo correctamente
	 */
	public int cambiarPeso(int vertice1, int vertice2, int nuevoPeso, int idVerticeArista) {
			
		int rta =this.existencia2Valores(vertice1, vertice2);
		if(interpretaExistencia(rta))
		{
				this.listaVertices.modificarPeso(vertice1, vertice2,nuevoPeso,idVerticeArista);	
		}
		return rta;		
	}
	/**
	 * 
	 * @param vertice1
	 * @param vertice2
	 * @param nuevoPeso
	 * @return
	 */
	public int cambiarPeso(int vertice1, int vertice2, int nuevoPeso) {
		return cambiarPeso( vertice1,  vertice2,  nuevoPeso,  0);		
	}
	
}// Fin clase Grafo
