package Grafo;

public class NodoLista<T> {
	// Atributos
	private T valor;
	private NodoLista<T> siguiente;
	//getters y setters
	public T getValor() {
		return valor;
	}
	public void setValor(T valor) {
		this.valor = valor;
	}
	public NodoLista<T> getSiguiente() {
		return siguiente;
	}
	public void setSiguiente(NodoLista<T> siguiente) {
		this.siguiente = siguiente;
	}	
}