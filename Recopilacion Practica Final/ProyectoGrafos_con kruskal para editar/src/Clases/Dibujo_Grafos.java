package Clases;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.html.HTMLDocument.Iterator;

import Logica.Grafo;
import Logica.Kruskal;

public class Dibujo_Grafos extends javax.swing.JFrame {
	private JDesktopPane jDesktopPane1;
	private JButton MostrarG;
	private JTextField jTextField1;
	private JLabel jLabel1;
	private JDesktopPane jDesktopPane2;
	private JSeparator jSeparator1;
	private JLabel lbl_grafo_PPal;
	private JButton btn_MAtAdyacencia2;
	private JButton btn_MAtAdyacencia;
	private JButton btn_GradoVertice;
	private JButton brn_montarInfo;
	private JLabel lbl_Recubrimineto;
	private JComboBox jComboBox_Recorridos;
	private JTextArea jTar_Informacion;
	private AbstractAction MostarG;
	private JScrollPane Scrll_Datos;
	List<circulo> ListLKruskal = new ArrayList<circulo>();
	  List<linea> ListArista = new ArrayList<linea>();
	
	Grafo <String> objGrafo = new Grafo<String> ();
	/**
	* Auto-generated main method to display this JFrame
	*/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Dibujo_Grafos inst = new Dibujo_Grafos();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
				JOptionPane.showMessageDialog(null, "lea la informacion a su Derecha");
			}
		});
	}
	
	public Dibujo_Grafos() {
		super();
		initGUI();
	}
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		
		for (circulo f : ListLKruskal) {
			f.painter2(g);
		}
	}
	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jDesktopPane1 = new JDesktopPane();
				getContentPane().add(jDesktopPane1);
				jDesktopPane1.setBounds(12, 29, 306, 178);
				lienzo l=new lienzo(jDesktopPane1);
				ListLKruskal=l.getListCirculo() ;
			
				objGrafo=l.objGrafo;
				int numElementos =ListLKruskal.size();
				System.out.println("\n\nEl ArrayList tiene "+numElementos+" elementos");
				l.setBounds(0, 0, 306, 178);
				jDesktopPane1.add(l);
			}
			{
				MostrarG = new JButton();
				getContentPane().add(MostrarG);
				MostrarG.setText("Aplicar Kruskal");
				MostrarG.setBounds(330, 326, 135, 23);
				MostrarG.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
					try {
						if(objGrafo.conexo()==true){	
							
							Grafo <String> ordenado_G = new Grafo<String> ();
							
							Kruskal krus=new Kruskal();
							int[][] mat_O = null;
							try {
								mat_O = objGrafo. Ordenar_Insercion(objGrafo);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							int l=objGrafo.getFilas_k();
							try {
								int[][] mat_c=objGrafo.matrizCaminos();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							try {
								ordenado_G= krus.aplicarKruskal(objGrafo, mat_O, l);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int i = 0; i < ordenado_G.listaVertices.getTamanio(); i++) {
								System.out.print((i+1)+".  ");
								try {
									ordenado_G.imprimirAdyacencias(i);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							
						int[][] mat_A = null;
							try {
							//	 mat_A=ordenado_G.Ordenar_Insercion(ordenado_G);
								 mat_A=ordenado_G.MatrizSInOrden(ordenado_G);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							int l1=ordenado_G.getFilas_k();
							
						  int	suma=0;
							for (int i = 0; i < l1; i++) {
								   System.out.println("es"+i);
									suma=suma+mat_A[i][0];
								for (int j = 0; j < 3; j++) {
									circulo a = ListLKruskal.get(mat_A[i][1]);
									circulo b= ListLKruskal.get(mat_A[i][2]);
								
							        linea lineas = new linea (a,b ,mat_A[i][0]);	
									ListArista.add(lineas);
								
								}
							}	
							
							for(int i=0;i<ListLKruskal.size();i++)
							{
								circulo nodo=ListLKruskal.get(i);
 
								System.out.println("\nElemento "+nodo.n);
								System.out.println("\nCoordenada X "+nodo.x);
								System.out.println("\nCoordenada Y "+nodo.y);
							}
							
							Dibujo_Kruskal dg=new Dibujo_Kruskal(ListLKruskal,ListArista);
							
							dg.setLocation(1, 1);
							
							dg.setSize(500, 400);
							Dimension dlgSize = dg.getPreferredSize(); 
							Dimension pantalla = getSize(); 
							Dimension ventana = dg.getSize() ; 
							 
							jDesktopPane2.add(dg);
							
							jTextField1.setText(""+suma/2);
							dg.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
						 }
						
						
					 else
			    		JOptionPane.showMessageDialog(null,"El Grafo no es Conexo no se puede aplicar kruskal", "informe de estado",
								JOptionPane.ERROR_MESSAGE);
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}	
				});
			}
			{
				lbl_grafo_PPal = new JLabel();
				lbl_grafo_PPal.setText("Area de dibujo del Grafo");
				lbl_grafo_PPal.setBounds(12, 12, 143, 14);
				getContentPane().add(lbl_grafo_PPal);
			}
			{
				jSeparator1 = new JSeparator();
				getContentPane().add(jSeparator1);
				jSeparator1.setBounds(12, 216, 306, 2);
			}
			{
				jDesktopPane2 = new JDesktopPane();
				getContentPane().add(jDesktopPane2);
				jDesktopPane2.setBounds(12, 230, 301, 168);
			}
			{
				jLabel1 = new JLabel();
				getContentPane().add(jLabel1);
				jLabel1.setText("Longitud de Camino");
				jLabel1.setBounds(12, 410, 155, 17);
			}
			{
				jTextField1 = new JTextField();
				getContentPane().add(jTextField1);
				jTextField1.setText(" ");
				jTextField1.setBounds(179, 409, 89, 15);
				jTextField1.setEditable(false);
			}
			pack();
			this.setSize(653, 499);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}

}