package Logica_Negocio;

import Clases_Informacion.*;

public class Reserva {
	
	private String fecha;
	private String hora;
	
	private Detalle_Reserva detalles_res;
	private Cliente cliente;
	private Vendedor vendedor;
	
	public Reserva() {
		super();
		this.fecha = "";
		this.hora = "";
		this.detalles_res = null;
		this.cliente = null;
		this.vendedor = null;
	}
	
	public Reserva(String fecha, String hora, Detalle_Reserva detalles_res,
			Cliente cliente, Vendedor vendedor) {
		super();
		this.fecha = fecha;
		this.hora = hora;
		this.detalles_res = detalles_res;
		this.cliente = cliente;
		this.vendedor = vendedor;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Detalle_Reserva getDetalles_res() {
		return detalles_res;
	}

	public void setDetalles_res(Detalle_Reserva detalles_res) {
		this.detalles_res = detalles_res;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	
	
	
}
