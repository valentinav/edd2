package Logica_Negocio;

import Clases_Informacion.*;

public class Detalle_Reserva {
	
	private int cantidad_vajilla;
	private int cantidad_mesa;
	private int cantidad_sillla;
	private int cantidad_traje;

	private Traje traje;
	private Mesa mesa;
	private Silla silla;
	private Vajilla vajilla;
	
	public Detalle_Reserva() {
		super();
		this.cantidad_vajilla = 0;
		this.cantidad_mesa = 0;
		this.cantidad_sillla = 0;
		this.cantidad_traje = 0;
		this.traje = null;
		this.mesa = null;
		this.silla = null;
		this.vajilla = null;
	}
	
	public Detalle_Reserva(int cantidad_vajilla, int cantidad_mesa,
			int cantidad_sillla, int cantidad_traje, Traje traje, Mesa mesa,
			Silla silla, Vajilla vajilla) {
		this.cantidad_vajilla = cantidad_vajilla;
		this.cantidad_mesa = cantidad_mesa;
		this.cantidad_sillla = cantidad_sillla;
		this.cantidad_traje = cantidad_traje;
		this.traje = traje;
		this.mesa = mesa;
		this.silla = silla;
		this.vajilla = vajilla;
	}

	public int getCantidad_vajilla() {
		return cantidad_vajilla;
	}

	public void setCantidad_vajilla(int cantidad_vajilla) {
		this.cantidad_vajilla = cantidad_vajilla;
	}

	public int getCantidad_mesa() {
		return cantidad_mesa;
	}

	public void setCantidad_mesa(int cantidad_mesa) {
		this.cantidad_mesa = cantidad_mesa;
	}

	public int getCantidad_sillla() {
		return cantidad_sillla;
	}

	public void setCantidad_sillla(int cantidad_sillla) {
		this.cantidad_sillla = cantidad_sillla;
	}

	public int getCantidad_traje() {
		return cantidad_traje;
	}

	public void setCantidad_traje(int cantidad_traje) {
		this.cantidad_traje = cantidad_traje;
	}

	public Traje getTraje() {
		return traje;
	}

	public void setTraje(Traje traje) {
		this.traje = traje;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Silla getSilla() {
		return silla;
	}

	public void setSilla(Silla silla) {
		this.silla = silla;
	}

	public Vajilla getVajilla() {
		return vajilla;
	}

	public void setVajilla(Vajilla vajilla) {
		this.vajilla = vajilla;
	}
	

	

}
