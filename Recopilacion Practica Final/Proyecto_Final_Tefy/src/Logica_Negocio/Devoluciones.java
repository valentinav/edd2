package Logica_Negocio;

import Clases_Informacion.*;

public class Devoluciones {
	
	private String fecha;
	private String hora;

	private Cliente cliente;
	private Reserva reserva;
	
	public Devoluciones() {
		this.fecha = "";
		this.hora = "";
		this.cliente = null;
		this.reserva = null;
	}
	
	public Devoluciones(String fecha, String hora, Cliente cliente,
			Reserva reserva) {
		this.fecha = fecha;
		this.hora = hora;
		this.cliente = cliente;
		this.reserva = reserva;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
	
}