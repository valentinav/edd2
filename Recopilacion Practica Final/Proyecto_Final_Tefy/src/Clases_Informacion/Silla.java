package Clases_Informacion;

public class Silla {
	private String color;
	private String forma;

	public Silla() {
		this.color = "";
		this.forma = "";
	}

	public Silla(String color, String forma) {
		this.color = color;
		this.forma = forma;
	}

	public void setcolor(String c) {
		color = c;
	}

	public void setforma(String f) {
		forma = f;
	}

	public String getcolor() {
		return color;
	}

	public String getforma() {
		return forma;
	}
}
