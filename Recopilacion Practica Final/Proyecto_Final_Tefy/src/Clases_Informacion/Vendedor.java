package Clases_Informacion;

public class Vendedor {
	private String nombre;
	private String codigo;

	public Vendedor() {
		this.nombre = "";
		this.codigo = "";
	}

	public Vendedor(String n, String c) {
		nombre = n;
		codigo = c;
	}

	public String getnombre() {
		return nombre;
	}

	public void setnombre(String n) {
		{
			nombre = n;
		}
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String c) {
		codigo = c;
	}

}
