package Clases_Informacion;

public class Traje {
	private float talla;
	private String genero;

	public Traje() {
		this.talla = 0;
		this.genero = "";
	}

	public Traje(float talla, String genero) {
		this.talla = talla;
		this.genero = genero;
	}

	public void settalla(float t) {
		talla = t;
	}

	public void setgenero(String g) {
		genero = g;
	}

	public float getalla() {
		return talla;
	}

	public String getgenero() {
		return genero;
	}

}
