package Clases_Informacion;

public class Mesa {
	private String marca;
	private String forma;

	public Mesa() {
		this.forma = "";
		this.marca = "";
	}

	public Mesa(String marca, String forma) {
		this.marca = marca;
		this.forma = forma;
	}

	public void setmarca(String m) {
		marca = m;
	}

	public void setforma(String f) {
		forma = f;
	}

	public String getmarca() {
		return marca;
	}

	public String getforma() {
		return forma;
	}
}
