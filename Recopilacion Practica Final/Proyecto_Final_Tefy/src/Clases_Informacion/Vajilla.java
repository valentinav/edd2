package Clases_Informacion;

public class Vajilla {
	private int tama�o;
	private String color;

	public Vajilla() {
		this.color = "";
		this.tama�o = 0;
	}

	public Vajilla(int tama�o, String color) {
		this.tama�o = tama�o;
		this.color = color;
	}

	public void settama�o(int t) {
		tama�o = t;
	}

	public void setcolor(String c) {
		color = c;
	}

	public int gettama�o() {
		return tama�o;
	}

	public String getcolor() {
		return color;
	}

}
