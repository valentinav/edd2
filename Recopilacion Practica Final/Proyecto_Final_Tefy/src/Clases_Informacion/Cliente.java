package Clases_Informacion;

public class Cliente {
	private String nombre;
	private String cedula;
	
	public Cliente() {
		this.nombre = "";
		this.cedula = "";
	}
	public Cliente(String n, String cc) {
		nombre = n;
		cedula = cc;
	}
	public void setnombre(String n) {
		nombre = n;
	}

	public void setcedula(String cc) {
		cedula = cc;
	}

	public String getnombre() {
		return nombre;
	}

	public String getcedula() {
		return cedula;
	}

}