package Presentacion;

import java.io.*;
import java.util.Scanner;

import Clases_Informacion.*;
import Logica_Negocio.Reserva;


//import Logica_Negocio.cliente;

public class Usuario {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		//BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("\t*****     TIENDA ALQUITRAJES     *****");
		int opcion = 0; 
		
		do{
			menuOpciones();
			opcion = leerEntero();
			
			switch(opcion) {
				
			case 1 :
				System.out.println("\tSe�or vendedor \nDigite su nombre: ");
				String nombreV = leer.next();
				System.out.println("Digite su codigo");
				String codigoV = leer.next();
				Vendedor vd = new Vendedor(nombreV,codigoV);
				
				System.out.println("\tDatos del cliente");
				System.out.println("Nombre: ");
				String nombreC = leer.next();
				System.out.println("Cedula: ");
				String cedulaC = leer.next();
				Cliente cl = new Cliente(nombreC,cedulaC);
				
				System.out.println("\tDatos de la reserva");
				System.out.println("Digite la fecha: ");
				String fecha = leer.next();
				System.out.println("Digite la hora: ");
				String hora = leer.next();
				
				
				System.out.println("\tDetalles de la Reserva:");
				
				System.out.println("Numero de mesas: ");
				int numMesas = leer.nextInt();
				String tipoMesa = "";
				if(numMesas != 0){
					System.out.println("Tipo de mesa: ");
					tipoMesa = leer.next();
				}
				
				System.out.println("\nNumero de trajes: ");
				int numTrajes = leer.nextInt();
				String tipoTraje = "";
				if(numTrajes != 0){
					System.out.println("Tipo de Traje: ");
					System.out.println("1. Caballero\n2. Dama");
					if(leer.nextInt() == 1){
						tipoTraje = "Caballero";
					}else
						tipoTraje = "Dama";
				}
				
				System.out.println("\nNumero de sillas: ");
				int numSillas = leer.nextInt();
				String tipoSillas = "";
				if(numSillas != 0){
					System.out.println("Tipo de Sillas: ");
					tipoSillas = leer.next();
				}
				
				System.out.println("\nNumero de vajillas: ");
				int numVajillas = leer.nextInt();
				String tipoVajillas = "";
				if(numVajillas != 0){
					System.out.println("Tipo de Vajillas: ");
					tipoVajillas = leer.next();
				}
								
				
				break;
				
			case 2:
				
				break;
				
			case 3:
				
				break;
				
			case 4:
				
				break;
				
			case 5:
				
				break;
				
			default: 
				if(opcion != 0)
				System.out.println("Opcion incorrecta, escoja nuevamente...");
				else
					System.out.println("*********  ADIOS  ***********");
				break;
			}
			
		}while(opcion != 0 );
		
		
		
	}
	
	public static void menuOpciones(){
		System.out.println("\n\n*****************************");
		System.out.println("1. Alquilar un Producto");
		System.out.println("2. ");
		System.out.println("3. ");
		System.out.println("4. ");
		System.out.println("5. ");
		System.out.println("0. Salir");
		
	}
	
	public static int leerEntero()  {
		Scanner in = new Scanner(System.in);
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nDigite su opcion: ");
		int entero = in.nextInt();
		System.out.println();
		return entero;
	}

}
