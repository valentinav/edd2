package Dibujo_Grafo;
import java.awt.Color;
import java.awt.Graphics;

public class linea{

    public circulo inicial;
    circulo ffinal;
    Color color= Color.black;
	private int peso;

    public linea(circulo inicial, circulo ffinal, int peso) {
        this.inicial=inicial;
        this.ffinal=ffinal;
        this.peso=peso;
    }

    public void painter(Graphics g) {
        g.setColor(color);
       
        g.drawLine(inicial.getX()+3, inicial.getY()+3, ffinal.getX()+3, ffinal.getY()+3);
      
        g.drawLine(inicial.getX()+4, inicial.getY()+4, ffinal.getX()+4, ffinal.getY()+4);
        
        g.drawString(String.valueOf(peso),(((inicial.getX())+(ffinal.getX()))/2),(((inicial.getY())+(ffinal.getY()))/2));
        //

    }
    
    public int getPeso() {
 		return peso;
 	}

    public void setColor(Color color) {
        this.color = color;
    }

    public circulo getFfinal() {
        return ffinal;
    }

    public circulo getInicial() {
        return inicial;
    }

  	public void setPeso(int peso) {
  		this.peso = peso;
  	}
    
}