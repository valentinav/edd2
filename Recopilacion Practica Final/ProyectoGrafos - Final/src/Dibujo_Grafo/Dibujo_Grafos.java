package Dibujo_Grafo;
//import java.awt.*;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import ALg_Recubrimiento.Dijkstra;
import ALg_Recubrimiento.Kruskal;
import Clases_Grafo.Grafo;
import Presentacion.Cliente_Control;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Dibujo_Grafos extends javax.swing.JFrame implements ActionListener{
	private static JDesktopPane jDesktopPane1;
	private JPopupMenu jPopupMenu1;
	private JMenu jMenu1;
	private JSeparator jSeparator1;
	private JLabel lbl_grafo_PPal;
	private JLabel jLabel1;
	private JButton btn_MAtAdyacencia2;
	private JButton btn_MAtAdyacencia;
	private JButton btn_GradoVertice;
	private JComboBox jComboBox_Recorridos;
	private JTextArea jTar_Informacion;
	private static JDesktopPane jDesktopPane2;
	private AbstractAction MostarG;
	private JScrollPane Scrll_Datos;
	private JTextField jTextField1;
	
	//se cambio a publico para acceder a el mas facilmente
	public lienzo l;
	private lienzo l2;
	
	private JMenuBar mb;
    private JMenu menu1,menu2;
    private JMenuItem mKrus,mDjtr;
    private JMenuItem mMat,mMatm,mGrd;
    private JMenuItem eVer,eArt,ePeso;
	
	private Grafo<String> grafoAux;
	private Grafo<String> gr;
	
	List<circulo> ListLKruskal = new ArrayList<circulo>();
	private Grafo <String> objGrafo = new Grafo<String> ();
	private List<linea> ListArista = new ArrayList<linea>();
	/**
	* Auto-generated main method to display this JFrame
	*/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				/*Dibujo_Grafos inst = new Dibujo_Grafos();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
				JOptionPane.showMessageDialog(null, "lea la informacion a su Derecha");
				*/
			}
		});
	}
	
	public Dibujo_Grafos() {
		super("Aplicacion Grafos - Teoria");
		initGUI();
	}
	
	private void initGUI() {
		try {
			setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
			getContentPane().setLayout(null);
			
			{
				setLayout(null);
				mb = new JMenuBar();
		        setJMenuBar(mb);
			}
			{		        
		        menu1=new JMenu("Recorridos");
		        mb.add(menu1);
		        mKrus=new JMenuItem("Kruskal");
		        mKrus.addActionListener(this);
		        menu1.add(mKrus);
		        mDjtr=new JMenuItem("Dijktra");
		        mDjtr.addActionListener(this);
		        menu1.add(mDjtr);
			}
			{
		        menu2 = new JMenu("Informacion");
		        mb.add(menu2);
		        mMat=new JMenuItem("Matriz Adyacencias");
		        mMat.addActionListener(this);
		        menu2.add(mMat);
		        mMatm=new JMenuItem("Matriz Adyacencias m*m");
		        mMatm.addActionListener(this);
		        menu2.add(mMatm);
		        mGrd=new JMenuItem("Grado Vertice");
		        mGrd.addActionListener(this);
		        menu2.add(mGrd);
			}
			{
		        menu2 = new JMenu("Operaciones");
		        mb.add(menu2);
		        eVer=new JMenuItem("Eliminar vertice");
		        eVer.addActionListener(this);
		        menu2.add(eVer);
		        eArt=new JMenuItem("Eliminar Arista");
		        eArt.addActionListener(this);
		        menu2.add(eArt);
		        ePeso=new JMenuItem("Editar Peso");
		        ePeso.addActionListener(this);
		        menu2.add(ePeso);
		        
			}
			{
				jDesktopPane1 = new JDesktopPane();
				getContentPane().add(jDesktopPane1);
				jDesktopPane1.setBounds(12, 32, 365, 200);
				l = new lienzo(jDesktopPane1);
				ListLKruskal=l.getListCirculo() ;
				
				objGrafo=l.objGrafo;
				int numElementos =ListLKruskal.size();
				l.setBounds(0, 0, 365, 200);
				jDesktopPane1.add(l);
			}
			{
				lbl_grafo_PPal = new JLabel();
				lbl_grafo_PPal.setText("Area de dibujo del Grafo");
				lbl_grafo_PPal.setBounds(12, 12, 143, 14);
				getContentPane().add(lbl_grafo_PPal);
			}
			
			
			{
				btn_MAtAdyacencia = new JButton();
				btn_MAtAdyacencia.setText("TERMINAR LA EJECUCION");
				btn_MAtAdyacencia.setBounds(389, 470, 244, 21);
				getContentPane().add(btn_MAtAdyacencia);
				btn_MAtAdyacencia.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
			        	Cliente_Control cc = new Cliente_Control(); 
			        	
			        	//System.exit( 0 ); 
			        	cc.inst1.dispose();
						try {
							cc.main(null);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
				});
			}
			{
				jDesktopPane2 = new JDesktopPane();
				jDesktopPane2.setBounds(12, 255, 365, 200);
				getContentPane().add(jDesktopPane2);
				l2 =new lienzo(jDesktopPane2);
				l2.setBounds(0, 0, 365, 200);
				jDesktopPane2.add(l2);
			}
			{
				jSeparator1 = new JSeparator();
				getContentPane().add(jSeparator1);
				jSeparator1.setBounds(12, 243, 365, 7);
			}
			{
					jTar_Informacion = new JTextArea();
					Scrll_Datos = new JScrollPane(jTar_Informacion);
					Scrll_Datos.setBounds(390, 33, 243, 425);
					add(Scrll_Datos);
					
					
					String titulo = "\nPara dibujar el grafo:"
							+ "\n 1.Click Izquierdo sobre el area de dibujo\n    del grafo"
							+ "\n 2.Digite el nombre del vertice"
							+ "\n\nPara agregar una arista:"
							+ "\n 1.Click derecho en el area de Dibujo"
							+ "\n 2.Digite los valores solicitados";
					jTar_Informacion.setText(titulo);
					jTar_Informacion.setEditable(false);
					
				}
			{
				jLabel1 = new JLabel();
				getContentPane().add(jLabel1);
				jLabel1.setText("Longitud de Camino:");
				jLabel1.setBounds(12, 470, 155, 17);
			}
			{
				jTextField1 = new JTextField();
				getContentPane().add(jTextField1);
				jTextField1.setText(" ");
				jTextField1.setBounds(179, 470, 90, 17);
				jTextField1.setEditable(false);
			}
			

			pack();
			this.setSize(653, 562);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	 public void actionPerformed(ActionEvent e) {
	    	//Container f =this.getContentPane();
	    	//f.setBounds(-4, -5, 645, 532);
		 
		 if (e.getSource() == mKrus) {
			 try {
				 if(objGrafo.conexo()==true){

					 
					 Grafo <String> ordenado_G = new Grafo<String> ();

					 Grafo <String> ordenado_G1 = new Grafo<String> ();
					 ////
					 Kruskal krus=new Kruskal();
					 int[][] mat_O = null;
					 try {
						 mat_O = objGrafo. Ordenar_Insercion(objGrafo);
					 } catch (Exception e1) {
						 // TODO Auto-generated catch block
						 e1.printStackTrace();
					 }
					 int l=objGrafo.getFilas_k();
					 try {
						 int[][] mat_c=objGrafo.matrizCaminos();
					 } catch (Exception e2) {
						 // TODO Auto-generated catch block
						 e2.printStackTrace();
					 }


					 try {
						 ordenado_G= krus.aplicarKruskal(objGrafo, mat_O, l);
					 } catch (Exception e3) {
						 // TODO Auto-generated catch block
						 e3.printStackTrace();
					 }
					 for (int i = 0; i < ordenado_G.listaVertices.getTamanio(); i++) {
						 System.out.print((i+1)+".  ");
						 try {
							 ordenado_G.imprimirAdyacencias(i);
						 } catch (Exception e4) {
							 // TODO Auto-generated catch block
							 e4.printStackTrace();
						 }
					 }


					 int[][] mat_A = null;

					 try {
						 //	 mat_A=ordenado_G.Ordenar_Insercion(ordenado_G);
						 mat_A=ordenado_G.MatrizSInOrden(ordenado_G);

					 } catch (Exception e5) {
						 e5.printStackTrace();
					 }
					 int l1=ordenado_G.getFilas_k();

					 int[][] mat_c1=ordenado_G.generarMatrizAdyacencias();

					 List<linea> ListArista = new ArrayList<linea>();

					 int	suma=0;
					 for (int i = 0; i < l1; i++) {
						 suma=suma+mat_A[i][0];
						 for (int j = 0; j < 3; j++) {
							 circulo a = ListLKruskal.get(mat_A[i][1]);
							 circulo b= ListLKruskal.get(mat_A[i][2]);

							 linea lineas = new linea (a,b ,mat_A[i][0]);	
							 ListArista.add(lineas);
						 }
					 }	

					 Dibujo_Kruskal dg=new Dibujo_Kruskal(ListLKruskal,ListArista);

					 dg.setLocation(1, 1);

					 dg.setSize(500, 400);
					 Dimension dlgSize = dg.getPreferredSize(); 
					 Dimension pantalla = getSize(); 
					 Dimension ventana = dg.getSize() ; 

					 jTextField1.setText(""+suma/2);

					 dg.repaint();
					 jDesktopPane2.add(dg);

					 dg.setVisible(true) ; // ESTO HACE QUE SE VEA EL SEGUNDO JFRAME..
				 }
				 else
					 JOptionPane.showMessageDialog(null,"El Grafo no es Conexo no se puede aplicar kruskal", "informe de estado",
							 JOptionPane.ERROR_MESSAGE);


			 }


			 catch (Exception e6) {
				 // TODO Auto-generated catch block
				 e6.printStackTrace();
			 }


				
	        }
	        
	        //lee y ejecuta la opcion de dijktra
	        if (e.getSource()==mDjtr) {
	            //f.setBackground(new Color(0,255,0));
	        	Dijkstra dj = new Dijkstra(); 
				try {
					gr = dj.dijkstra(0, l.getObjGrafo());
					String titulo = gr.imprimirMatriz(gr.generarMatrizAdyacencias());
					System.out.println(titulo);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
	        }
	        
	        //lector opcion matriz Adyacencia
	        if(e.getSource() == mMatm){
	        	grafoAux = l.getObjGrafo();
				try {
					String titulo = grafoAux.matrizMM();
					titulo = "Matriz de adyacencia m*m\n"+titulo  ;
					jTar_Informacion.setText(titulo);
				} catch (Exception a) {
					// TODO Auto-generated catch block
					a.printStackTrace();
				}
	        }
	        
	        //lee y ejecuta la opcion para mostrar la matriz m*m
	        if(e.getSource() == mMat){
	        	grafoAux = l.getObjGrafo();
				try {
					String titulo = grafoAux.imprimirMatriz(grafoAux.generarMatrizAdyacencias());
					titulo = "Matriz de adyacencia\n"+titulo  ;
					jTar_Informacion.setText(titulo);
					
				} catch (Exception s) {
					// TODO Auto-generated catch block
					s.printStackTrace();
				}
	        	
	        }
	        
	        //ejecuta la opcion de ver el grado de un vertice
	        if(e.getSource() == mGrd){
	        	grafoAux = l.getObjGrafo();
				String valor = JOptionPane.showInputDialog(null, "Digite el nombre del vertice", "Solicitud dato",JOptionPane.QUESTION_MESSAGE );
				try {
					grafoAux.gradoVertice(valor);
				} catch (Exception q) {
					// TODO Auto-generated catch block
					q.printStackTrace();
				}
	        }
	    }
	 
	 
	 
	
	

	
}