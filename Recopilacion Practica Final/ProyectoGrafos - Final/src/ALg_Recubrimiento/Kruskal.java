package ALg_Recubrimiento;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Clases_Grafo.Grafo;
import Clases_Grafo.NodoGrafo;

public class Kruskal
{
	//@SuppressWarnings("unchecked")
	int n=0;
	public Grafo<String>  aplicarKruskal(Grafo<String>  grafo, int Mat_k[][],int l) throws Exception
	{
		Grafo <String> Grafo_k = new Grafo<String> ();


		for (int i = 0; i < grafo.listaVertices.getTamanio(); i++) {
			NodoGrafo aux = grafo.listaVertices.getValor(i);
			Grafo_k .agregarVertices(grafo.listaVertices.getValor(i).getElemento().toString());
		}
		Grafo_k.imprimirVertices();

		for (int i = 0; i < l; i = i + 2) {
			System.out.println("es"+i);
			for (int j = 0; j < 3; j++) {
				if (j == 0 && i == 0) {
					Grafo_k.agregarAdyacencias(Mat_k[0][1], Mat_k[0][2],
							Mat_k[0][0]);
				}
				else{
					int[][] mat_k=Grafo_k.matrizCaminos();

					if(HayCiclo(Grafo_k,mat_k,Mat_k[i][1],Mat_k[i][2])==true)
					{
						Grafo_k.agregarAdyacencias(Mat_k[i][1], Mat_k[i][2],
								Mat_k[i][0]);
					}
				}
			}
		}
		return Grafo_k;
	}

	public boolean HayCiclo(Grafo g, int mat_k[][],int posi,int posf) throws Exception
	{
		boolean a=false;

		for(int i=0;i<g.listaVertices.getTamanio();i++)
		{
			for(int j=0;j<g.listaVertices.getTamanio();j++)
			{
				if(mat_k[posi][posf]==0)
				{
					a=true;
				}
			}
		}	
		return a;	
	}	
}	
