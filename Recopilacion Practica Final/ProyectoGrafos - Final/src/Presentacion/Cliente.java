package Presentacion;

import java.util.Scanner;

import Clases_Grafo.Grafo;

public class Cliente {

	public static void main(String[] args) throws Exception {
		
		Grafo <String> objGrafo = new Grafo<String> ();
		Scanner in = new Scanner(System.in);
		
		System.out.println("***** BIENVENIDO AL PROGRAMA DE CREACION DE GRAFOS");
		System.out.println("\n\t�Que desea hacer?");
		System.out.println("1) Crear un grafo desde cero");
		System.out.println("2) Crear un grafo con cinco vertices automaticamente con sus respectivas adyacencias");
	
		int opc = leerEntero();
		while(opc < 1 || opc > 2){
			System.out.println("Opcion incorrecta");
			opc = leerEntero();
		}
		
		if(opc == 2){
			objGrafo.agregarVertices("a");//  0
			objGrafo.agregarVertices("b");//  1
			objGrafo.agregarVertices("c");//  2
			objGrafo.agregarVertices("d");//  3
			objGrafo.agregarVertices("e");//  4
			
			//recive la posicion de los vertioces q le vamos a agregar las adyacencias
			objGrafo.agregarAdyacencias(0, 1,0);
			objGrafo.agregarAdyacencias(0, 4,0);
			objGrafo.agregarAdyacencias(1, 2,0);
			objGrafo.agregarAdyacencias(1, 3,0);
			objGrafo.agregarAdyacencias(4, 4,0);
			
			System.out.println("Grafo Automatico creado");
			System.out.println("Elementos del grafo y su posicion en la Lista");
			objGrafo.imprimirVertices();
			System.out.println("\n\tADYACENCIAS: ");
			objGrafo.estaVertice("a",true);
			objGrafo.estaVertice("b",true);
			objGrafo.estaVertice("c",true);
			objGrafo.estaVertice("d",true);
			objGrafo.estaVertice("e",true);			
			
		}
		
		int opcion;
		
		do{
			menuOpciones();
			opcion = leerEntero();
			
			switch(opcion) {
				
			case 1 :
				System.out.println("Digite el valor que desea agregar");
				objGrafo.agregarVertices(in.next());
				System.out.println("Vertice ingresado en el grafo \n\t Recuerde que aun no tiene ninguna adyacencia!");
				break;
				
			case 2:
				System.out.println("digite la posicion del vertice inicial: ");
				int ini = in.nextInt();
				System.out.println("Digite la posicion del vertice Final: ");
				objGrafo.agregarAdyacencias((ini-1), (in.nextInt()-1),0);
				System.out.println("Operacion concluida...");
				break;
				
			case 3:
				System.out.println("Digite el valor del vertice");
				String nombre = in.nextLine();
				if (objGrafo.estaVertice(nombre, false)) {
					System.out.println("El vertice si se encuentra en el grafo");
					objGrafo.estaVertice(nombre, true);
				}else
					System.out.println("El vertice ingresado no hace parte del Grafo");
				break;
				
			case 4:
				for (int i = 0; i < objGrafo.listaVertices.getTamanio(); i++) {
					System.out.print((i+1)+".  ");
					objGrafo.imprimirAdyacencias(i);
				}
				break;
				
			case 5:
				objGrafo.ordenGrafo();
				break;
				
				
			case 6:
				System.out.println("Digite el vertice que desea consultar: ");
				String ver = in.next();
				objGrafo.gradoVertice(ver);
				break;
				
				
			case 7:
				System.out.println();System.out.println();
				objGrafo.imprimirMatrizAdyacencias();
				System.out.println();System.out.println();
				break;
				
				
			case 8:
				objGrafo.matrizMM();
				break;
				
				
			case 9:
				objGrafo.pseudografo();
				break;
				
				
				
			default: 
				if(opcion != 5)
				System.out.println("Opcion incorrecta, escoja nuevamente...");
				else
					System.out.println("*********  ADIOS  ***********");
				break;
			}
			
		}while(opcion != 10 );

	}
	
	public static void menuOpciones (){
		System.out.println("\n\n*******************************************************");
		System.out.println("\t*** MENU OPCIONES ***");
		System.out.println("1. Agregar vertice al grafo");
		System.out.println("2. Agregar V�rtices adyacentes a un v�rtice existente.");
		System.out.println("3. Verificar si un vertice existe");
		System.out.println("4. Imprimir el grafo con sus elementos y sus adyacencias");
		
		
		System.out.println("5. Orden del grafo");
		System.out.println("6. Grado de un vertice");
		System.out.println("7. Visualizar la matriz de adyacencias");
		System.out.println("8. Visualizar la matriz de adyacencias m*m");
		System.out.println("9. Determinar si el grafo es un PSEUDOGRAFO");
		
		
		System.out.println("10. Salir");
	}

	public static int leerEntero() {
		Scanner in = new Scanner(System.in);
		System.out.println("\nDigite su opcion: ");
		int entero = in.nextInt();
		System.out.println();
		return entero;
	}

}
