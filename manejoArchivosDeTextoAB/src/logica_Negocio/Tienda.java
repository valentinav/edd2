
package logica_Negocio;

import java.io.IOException;



public class Tienda
{
	    //Atributos
		private Producto articulos[]; //vector de productos
		private Archivo objArchivo = new Archivo();
		
		//Constructor
		public Tienda()
		{			
		}
		public Tienda(String nombreArchivo) throws IOException
		{
			int tamano;
			tamano=objArchivo.contarLineas(nombreArchivo);
			articulos = new Producto [tamano];
		}
		
		public void setArticulos(Producto[] vector)
		{
			articulos=vector;
		}
		public Producto[] getArticulos()
		{
			return articulos;
		}
		
		public void obtenerListaProductos(String nombreArchivo) throws Exception
		{
			 
			int tamano;
			tamano=objArchivo.contarLineas(nombreArchivo);
			articulos = new Producto [tamano];
			
			objArchivo.abrirArchivo(nombreArchivo, false);
			 int aux = 0;
			

			 while(objArchivo.puedeLeer()){
				String linea = objArchivo.leerArchivo();
				String[] datos = linea.split(","); //obtiene cada dato del producto:
				int codigo = Integer.parseInt(datos[0]);
				String nombre = datos[1];
				double precio = Double.parseDouble(datos[2]);
				Producto prod = new Producto(codigo, nombre, precio); 
				articulos[aux] = prod;
				aux ++;
			 }
			 objArchivo.cerrarArchivo();
			
		 }
		 
		
		public void insertarProducto(String nombreArchivo, String datosProducto)
		{
			try {
				objArchivo.abrirArchivo(nombreArchivo, true);
				objArchivo.escribirArchivo(datosProducto);
				objArchivo.cerrarArchivo();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
		}

}
