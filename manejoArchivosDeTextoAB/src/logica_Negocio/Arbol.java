package logica_Negocio;


public class Arbol {

	//Atributo
	private Nodo raiz;
	
	//Variable
    Nodo m;
    int padreLllave;
    int sw=0;
    
	public String buscar(int nro)
	{
		if (buscar (this.raiz,nro))
		   {
			if(m.llave==raiz.llave)
			{System.out.println("El dato corresponde a la raiz por lo tanto no tiene primos!!");}
			else {return "Numero Encontrado: " + nro+"\nPADRE: "+m.p.izq.llave+" "+m.p.izq.valor+"\nPRIMOS: "+m.p.der.der.llave+" "+m.p.der.der.valor+" "+m.p.der.izq.llave+" "+m.p.der.izq.valor;}
		   }
		return "Numero no encontrado " + nro;
	}
	
	
	
	
    private boolean buscar(Nodo n, int nro){
	
	if(n != null)
	{
		m=n;
		
		if (n.getLlave() == nro)
			{
                 sw++;
			     return true;
			}else
			 {
				m=n;
		        
		     }
		  
		   
            boolean ok1 = buscar(n.getIzq(),nro);
            //hijoIzq=n.getIzq();
            //m=n;
			boolean ok2 = buscar(n.getDer(),nro);
			//hijoDer=n.getDer();
			if(ok1 || ok2){
				          // m=n;
				            return true;
				          }//m=n;
			
	}
	m=n;
	
	return false;
}

public void imprimir()
{
	
	//System.out.println("HIJOS: "+m.p.getIzq().llave+" "+m.p.getDer().llave);
	
	
	//System.out.println("PRIMOS: "+sw1.der.llave+" "+sw1.izq.llave+" "+sw2.der+" "+sw2.izq);
}

	public void insertar(int key, Object valor)
	{
		Nodo n = new Nodo(key);
		
		n.valor = valor;
		if(getRaiz()==null)
		{
			setRaiz(n);
		}
		else
		    {
			Nodo temporal = getRaiz();
			while(temporal != null)
			{
				n.p = temporal;
				if(n.llave>=temporal.llave)
				{
					temporal = temporal.der;
					//m.der=temporal;
				}
				else
				    {
					temporal = temporal.izq;
					//m.izq=temporal;
				    }
			}
			if(n.llave<n.p.llave)
			{
				n.p.izq=n;
			}
			else
			    {
				n.p.der = n;
				}
		    }
	}
	
	/*********************************** GET AND SET ************************************************/
	public Nodo getM()
	{
	  return m;
	}

	public void setM(Nodo m) {
		this.m = m;
	}

	public Arbol()
	{
		setRaiz(null);
	}
	
	/************************************************************************************************/
	
	public void recorrerArbol(Nodo x)
	{
		if(x!=null)
		{
			recorrerArbol(x.izq);
			System.out.println(x.llave);
			System.out.println(x.valor);
			recorrerArbol(x.der);
		}
	}
	


	public Nodo getRaiz() {
		return raiz;
	}

	public void setRaiz(Nodo raiz) {
		this.raiz = raiz;
	}
}
