package logica_Negocio;

public class Nodo {
	
	// Atributos Nodo --> "PUBLICOS"
	public Nodo p;
	public Nodo der;
	public Nodo izq;
	public int llave;
	public Object valor;

	// Constructor Por Defecto.
	public Nodo(int key)
	{
		llave = key;
		der = null;
		izq = null;
		p = null;
		valor = null;
	}
	

	/****************************************+GET AND SET***********************************/
	public Nodo getP() {
		return p;
	}

	public void setP(Nodo p) {
		this.p = p;
	}

	public Nodo getDer() {
		return der;
	}

	public void setDer(Nodo der) {
		this.der = der;
	}

	public Nodo getIzq() {
		return izq;
	}

	public void setIzq(Nodo izq) {
		this.izq = izq;
	}

	public int getLlave() {
		return llave;
	}

	public void setLlave(int llave) {
		this.llave = llave;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

	/***************************************************************************************/
	
}
