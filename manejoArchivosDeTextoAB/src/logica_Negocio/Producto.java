package logica_Negocio;

public class Producto
{
	private int codigo;
	private String nombre;
	private double precio;
	
	public Producto()
	{
		this.codigo = 0;
		this.nombre = "";
		this.precio=0;
	}
	public Producto(int codigo, String nombre, double precio) throws Exception{
		this.codigo = codigo;
		this.nombre = nombre;
		this.setPrecio(precio);
	}
	
	public void setCodigo(int c){
		this.codigo = c;
	}
	
	public void setNombre(String n){
		this.nombre = n;
	}
	
	public void setPrecio(double p) throws Exception{
		if(p < 0) throw new Exception("El precio debe ser mayor o igual a cero");
		this.precio = p;
	}
	
	public int getCodigo(){
		return codigo;
	}
	
	public String getNombre(){
		return nombre;
	}

	public double getPrecio() {
		return precio;
	}
	
	public String toString() {
		return "COD: "+ codigo + " " + nombre + " V/u: $" + precio ;
 }
	
}
