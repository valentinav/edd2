package logica;

public class Calculadora 
{
	private int atrValor1;
	private int atrValor2;
	
	public Calculadora(){}
	public Calculadora(int parValor1, int parValor2)
	{
		atrValor1 = parValor1;
		atrValor2 = parValor2;
	}
	public int suma()
	{
		return this.atrValor1 + this.atrValor2;
	}
	public int resta()
	{
		return this.atrValor1 - this.atrValor2;
	}
}
