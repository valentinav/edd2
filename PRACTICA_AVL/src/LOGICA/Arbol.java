package LOGICA;

import java.io.IOException;

public class Arbol {
    	 
	 private Nodo raiz;
	 public String[] niveles;
	 private int altura=0;
	 private int cantidadHojas=0;
	 public Arbol() {
	 this.raiz = null;
	 }
	 
	 public Nodo getRaiz() {
	 return raiz;
	 }
	 
	 public void insertar(Comparable elemento ) {
		 raiz = insertar(raiz,elemento );
	 }
	 
	 private Object elementosNodo(Nodo arbol )
	 {
		 if (arbol==null)
		 return null;
		 else
		 return arbol.getElemento();
	 }
	 public void recorridoEnAnchura() {
	        niveles = new String[altura + 1];

	        recorridoEnAnchura(getRaiz(), 0);
	        for (int i = 0; i < niveles.length; i++) {
	            System.out.print(niveles[i]);
	        }
	    }
	 public void recorridoEnAnchura(Nodo pNodo, int nivel2) {
	        if (pNodo != null) {
	            niveles[nivel2] = pNodo.getElemento() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
	            recorridoEnAnchura(pNodo.getDer(), nivel2 + 1);
	            recorridoEnAnchura(pNodo.getIzq(), nivel2 + 1);
	        }
	    }
	 public static int factorEquilibrio(Nodo arbol){
		 if (arbol==null)
		 return -1;
		 else
		 return arbol.getFactorEquilibrio();
	 }
	 
	 private static int maxFB( int alturaIzquierdo, int alturaDerecho ) {
		 if (alturaIzquierdo > alturaDerecho)
		 return alturaIzquierdo;
		 else
		 return alturaDerecho;
	 }
	 
	 private Nodo insertar(Nodo arbol,Comparable elemento){
			
		     if( arbol == null )
			 arbol = new Nodo(elemento, null, null);
			 else if (elemento.compareTo(arbol.getElemento()) < 0 )
			 {
			 arbol.setIzq(insertar(arbol.getIzq(), elemento));
			 if (factorEquilibrio(arbol.getIzq()) -
			 factorEquilibrio(arbol.getDer()) == 2 )
			 if (elemento.compareTo(arbol.getIzq().getElemento()) < 0 )
			 arbol = RotacionSimpleIzq(arbol);
			 else
			 arbol = RotacionDobleIzq_Der(arbol);
			 }
			 else if (elemento.compareTo(arbol.getElemento()) > 0 )
			 {
			 arbol.setDer(insertar(arbol.getDer(), elemento));
			 if( factorEquilibrio(arbol.getDer()) -
			 factorEquilibrio(arbol.getIzq()) == 2)
			 if( elemento.compareTo(arbol.getDer().getElemento()) > 0 )
			 arbol = RotacionSimpleDer(arbol);
			 else
			 arbol = RotacionDobleDer_Izq(arbol);
			 }
			 else; // Duplicados; no hace nada
			 arbol.setFactorEquilibrio(maxFB(factorEquilibrio(arbol.getIzq()),
			 factorEquilibrio(arbol.getDer())) + 1);
			 return arbol;
		
	     }
	 
	 private static Nodo RotacionSimpleIzq(Nodo arbol) {
			 
			 Nodo subArbol = arbol.getIzq();
			 arbol.setIzq(subArbol.getDer());
			 subArbol.setDer(arbol);
			 arbol.setFactorEquilibrio(maxFB(factorEquilibrio(arbol.getIzq()),
			 factorEquilibrio(arbol.getDer())) + 1);
	
			 subArbol.setFactorEquilibrio(maxFB(factorEquilibrio(subArbol.getIzq()),
			 arbol.getFactorEquilibrio()) + 1);
			 return subArbol;
		 }
		 
	 private static Nodo RotacionSimpleDer(Nodo subArbol){
			 Nodo arbol = subArbol.getDer();
			 subArbol.setDer(arbol.getIzq());
			 arbol.setIzq(subArbol);
			 subArbol.setFactorEquilibrio(maxFB(factorEquilibrio(subArbol.getIzq()),
			 factorEquilibrio(subArbol.getDer())) + 1);
			 arbol.setFactorEquilibrio(maxFB(factorEquilibrio(arbol.getDer()),
			 subArbol.getFactorEquilibrio()) + 1);
			 return arbol;
		 }
	 private static Nodo RotacionDobleIzq_Der(Nodo arbol)
		 {
			 arbol.setIzq(RotacionSimpleDer(arbol.getIzq()));
			 return RotacionSimpleIzq( arbol );
			 }
			 private static Nodo RotacionDobleDer_Izq(Nodo subArbol)
			 {
			 subArbol.setDer(RotacionSimpleIzq(subArbol.getDer()));
			 return RotacionSimpleDer(subArbol);
		 }
	 public void leerArchivo() throws IOException{
			        Archivo archivo = new Archivo();
			        archivo.abrirArchivo("Arbol.txt", false);
			        while(archivo.puedeLeer()){
			            String linea = archivo.leerArchivo();
			            String[] arbol = linea.split(" ");
			        for(int i = 0; i < arbol.length; i++)
//			            insertar(Integer.parseInt(arbol[i]));
			            insertar(arbol[i]);
			        }
			    archivo.cerrarArchivo();
			    System.out.println("Archivo cargado.");
	   }
	  public void preorden(Nodo arbol){
		        if (arbol != null) {
		            System.out.print(arbol.getElemento() + " ");
		            preorden(arbol.getIzq());
		            preorden(arbol.getDer());
		        }  
		    }    
		    //Recorrido Inorden
	  public void inorden(Nodo arbol){
		        if (arbol != null) {
		           inorden(arbol.getIzq());
		           System.out.print(arbol.getElemento()+ " ");
		           inorden(arbol.getDer());
		        }  
		    }
		    //Recorrido Postorden
	  public void postorden(Nodo arbol){
		        if (arbol != null) {
		           postorden(arbol.getIzq());
		           postorden(arbol.getDer());
		           System.out.print(arbol.getElemento() + " ");
		        }  
		    }
	  public void imprimir(Nodo arbol, int nivel){
		        if (arbol != null) {
		            imprimir(arbol.getIzq(),nivel + 1);
		            imprimir(arbol.getDer(),nivel + 1);
		            System.out.println("Nivel: " + nivel + " - Dato: " + arbol.getElemento());
		        }
		    }
	 private void cantidadNodosHoja(Nodo pNodo) {
		        if (pNodo != null) {
		            if (pNodo.getIzq() == null && pNodo.getDer() == null) {
		            	
		                cantidadHojas++;
		                System.out.print(" " + pNodo.getElemento());
		            }
		            cantidadNodosHoja(pNodo.getIzq());
		            cantidadNodosHoja(pNodo.getDer());
		        }
		    }
	public int cantidadNodosHoja() {
		        cantidadHojas = 0;
		        cantidadNodosHoja(getRaiz());
		        return cantidadHojas;
		    }
	 public boolean eliminarNodo(Comparable dato) {
	        Nodo subArb = raiz;
	        Nodo aux = raiz;
	        Nodo padre = raiz;
	        boolean esHijoIzq = true;
	        
	        while(aux.getElemento() != dato){
	            padre = aux;
	            if (dato.compareTo(aux.getElemento())<0)
	            	aux = aux.getIzq();
	            else{
	                esHijoIzq = false;
	                aux = aux.getDer();
	            }
	            if (aux == null) return false;
	        }
	        //Nodo Hoja
	        if (aux.getIzq() == null && aux.getDer() == null) {
	            if (aux.equals(raiz)) raiz = null;
	            else {
	                if (esHijoIzq) padre.setIzq(null);
	                else padre.setDer(null);
	                if ((factorEquilibrio(subArb.getIzq()) - factorEquilibrio(subArb.getDer())) == 2) {}
	                 
	                    if (!(aux.getElemento()).equals(subArb.getIzq().getElemento()) ) 
	                        padre = RotacionSimpleIzq(subArb);
	                    else 
	                        padre = RotacionDobleIzq_Der(subArb);
	                }
	            }
	               else if (aux.getDer().equals(null)) {
	                 if (aux.equals(raiz)) raiz = aux.getIzq();
	            else 
	                if (esHijoIzq) padre.setIzq(aux.getIzq());
	                else padre.setDer(aux.getIzq());
	        }else if (aux.getIzq() == null) {
	                 if (aux.equals(raiz)) raiz = aux.getDer();
	            else 
	                if (esHijoIzq) padre.setIzq(aux.getDer());
	                else padre.setDer(aux.getDer());
	        }else{
	            Nodo reemplazo = obtenerNodoReemplazo(aux);
	            if (aux.equals(raiz)) raiz = reemplazo;
	            else if (esHijoIzq)
	                    padre.setIzq(reemplazo);
	                else
	                    padre.setDer(reemplazo);
	            reemplazo.setIzq(aux.getIzq());
	            }
	        return true;
	    }
	    
	    //Metodo de Reemplazo del nodo a Eliminar
	    public Nodo obtenerNodoReemplazo(Nodo nodoReemp){
	        Nodo reemplazarPadre = nodoReemp;
	        Nodo reemplazo = nodoReemp;
	        Nodo aux = nodoReemp.getDer();
	        while(aux != null){
	            reemplazarPadre = reemplazo;
	            reemplazo = aux;
	            aux = aux.getIzq();
	        }
	        if (reemplazo != nodoReemp.getDer()) {
	            reemplazarPadre.setIzq(reemplazo.getDer());
	            reemplazo.setDer(nodoReemp.getDer());
	        }
	        System.out.println("Nodo reemplazo " + reemplazo);
	        return reemplazo;
	    }
	
}//Fin clase Arbol

