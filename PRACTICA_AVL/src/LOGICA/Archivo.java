package LOGICA;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Archivo {
	BufferedWriter archivoEscritura = null;
	 BufferedReader archivoLectura = null;

	    public void abrirArchivo(String nombre, boolean escritura) throws IOException {
		if(escritura)
	            setArchivoEscritura(new BufferedWriter(new FileWriter(nombre)));
		else
	            setArchivoLectura(new BufferedReader(new FileReader(nombre)));	
	    }
	    public void cerrarArchivo() throws IOException{
	        if(getArchivoEscritura() != null)
	            getArchivoEscritura().close();
	        if(getArchivoLectura() != null)
	            getArchivoLectura().close();
	    }
	    public int contarLineas(String nombre) throws IOException {
	        abrirArchivo(nombre, false);
	        int lineas = 0;
	        while(puedeLeer()){
	            leerArchivo();
	            lineas ++; 
	        }
	        cerrarArchivo();
		return lineas;
	    } 
	    public void limpiarArchivo() throws IOException{
	        getArchivoEscritura().write("");
	    }
	    public String leerArchivo() throws IOException{
	        return getArchivoLectura().readLine();
	    }
	    public void escribirArchivo(String datos) throws IOException {
	        getArchivoEscritura().write(datos);
	    }
	    public boolean puedeLeer() throws IOException{
		return getArchivoLectura().ready();
	    }

	    public BufferedWriter getArchivoEscritura() {
	        return archivoEscritura;
	    }
	    public BufferedReader getArchivoLectura() {
	        return archivoLectura;
	    }

	    public void setArchivoEscritura(BufferedWriter archivoEscritura) {
	        this.archivoEscritura = archivoEscritura;
	    }
	    public void setArchivoLectura(BufferedReader archivoLectura) {
	        this.archivoLectura = archivoLectura;
	    }
	    public void guardarLineasDeConsola(String nombre)
	    {
	    	try {
	    	      //create a buffered reader that connects to the console, we use it so we can read lines
	    	      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

	    	      //read a line from the console
	    	      String lineFromInput = in.readLine();

	    	      //create an print writer for writing to a file
	    	      PrintWriter out = new PrintWriter(new FileWriter(nombre));

	    	      //output to the file a line
	    	      out.println(lineFromInput);

	    	      //close the file (VERY IMPORTANT!)
	    	      out.close();
	    	   }
	    	      catch(IOException e1) {
	    	        System.out.println("Error during reading/writing");
	    	   }
	    }
}
