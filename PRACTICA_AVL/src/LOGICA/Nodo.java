package LOGICA;

public class Nodo<T> {
	 private Nodo izq; // hijo izquierdo
	 private Nodo der; // hijo derecho
	 private int factorEquilibrio; // factor de equilibrio
	 private T elemento; // los datos como elementos del arbol avl
	// Constructores
	 public Nodo(T elemento) {
	 this(elemento, null, null );
	 }
	 public Nodo( T elemento, Nodo izq, Nodo der )
	 {
	 this.elemento = elemento;
	 this.izq = izq;
	 this.der = der;
	 factorEquilibrio = 0;
	 }
	 public Nodo getIzq() {
	 return izq;
	 }
	 public void setIzq(Nodo izq) {
	 this.izq = izq;
	 }
	 public Nodo getDer() {
	 return der;
	 }
	 public void setDer(Nodo der) {
	 this.der = der;
	 }
	 public int getFactorEquilibrio() {
	 return factorEquilibrio;
	 }
	 public void setFactorEquilibrio(int factorEquilibrio) {
	 this.factorEquilibrio = factorEquilibrio;
	 }
	 public T getElemento() {
	 return elemento;
	 }
	 public void setElemento(T elemento) {
	 this.elemento = elemento;
	 }
}//Fin clase Nodo

