package PRESENTACION;

import java.io.IOException;
import java.util.Scanner;

import LOGICA.Arbol;

public class TEST {

	private static Scanner entrada= new Scanner(System.in);
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
    Arbol objArbol = new Arbol();
    int opcionMenu;
    int elemento;
    do {
    	System.out.println("1: Crear Arbol \n"
    			         + "2: Imprimir Arbol \n"
    			          +"3: Recorridos \n"
    			          +"4: Eliminar \n"
  			              + "5: Cantidad Nodos Hoja"
    			          +"0: Salir \n");
    	opcionMenu= entrada.nextInt();
		switch (opcionMenu) {
		case 1:
		    objArbol.leerArchivo();
			break;
        case 2:
			objArbol.imprimir(objArbol.getRaiz(), 0);
			break;
        case 3:
        	System.out.println();
        	objArbol.inorden(objArbol.getRaiz());
        	System.out.println();
        	objArbol.postorden(objArbol.getRaiz());
        	System.out.println();
        	objArbol.preorden(objArbol.getRaiz());
        	//objArbol.recorridoEnAnchura();
        	break;
        case 4:
        	System.out.println("Ingrese el elemento a eliminar: ");
        	elemento= entrada.nextInt();
        	objArbol.eliminarNodo(elemento);
        	break;
        case 5:
        	objArbol.cantidadNodosHoja();
        	break;

		
		}
	} while (opcionMenu!=0);
    

    
    
    
	}

}
