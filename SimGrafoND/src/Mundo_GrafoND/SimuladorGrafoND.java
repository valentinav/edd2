/**
 * ---------------------------------------------------------------------
 * $Id: SimuladorGrafoND.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */


package Mundo_GrafoND;

import Colecciones_SEED.GrafoND;
import Colecciones_SEED.ListaCD;
import Colecciones_SEED.Vertice;

/**
 * Clase que conecta la capa de presentación del Simulador con las Estructuras de Datos.
 * @author Uriel Garcia - Yulieth Pabon
 * @version 1.0
 */

public class SimuladorGrafoND {
    
    private GrafoND<Character> miGrafo;

    public SimuladorGrafoND() {
        this.miGrafo = new GrafoND<Character>();
    }

    public boolean insertarVertice(char val) {
        return this.miGrafo.insertarVertice(val);
    }

    public boolean insertarArista(String x, int p) {
        char v1 = x.charAt(0);
        char v2 = x.charAt(1);
        return this.miGrafo.insertarAristaP(v1, v2, p);
    }
    
    public boolean eliminarArista(String x) {
        char v1 = x.charAt(0);
        char v2 = x.charAt(1);
        return this.miGrafo.eliminarArista(v1, v2);
    }

    public boolean eliminarVertice(char valEli) {
        return (this.miGrafo.eliminarVertice(valEli));
    }

    public String getMatrizAdyacencia() {
        Object m[][] = this.miGrafo.getMatrizAdyacencia();
        String cad = " \n  ---------- Matriz de Adyacencia ---------- \n\n";
        for(int i=0; i<m[0].length; i++){
            for(int j=0; j<m.length; j++){
                if(!(i==0 && j==0)){
                    String dato = m[i][j].toString();
                    cad+=dato+"\t";
                }else
                    cad+="\t";
            }
            cad+="\n";
        }
        return (cad);
    }

    public String getMatrizIncidencia() {
        Object m[][] = this.miGrafo.getMatrizIncidencia();
        String cad = " \n  ---------- Matriz de Incidencia ---------- \n\n";
        for(int i=0; i<m.length; i++){
            for(int j=0; j<m[0].length; j++){
                if(!(i==0 && j==0)){
                    if(m[i][j]!=null){
                        String dato = m[i][j].toString();
                        cad+=dato+"\t";
                    }
                }else
                cad+="\t";
            }
            cad+="\n";
        }
        return (cad);
    }
    
    public boolean estaVacioGrafo(){
        return this.miGrafo.esVacio();
    }

    public boolean estaVertice(char v1) {
        return this.miGrafo.esta(v1);
    }

    public String getRutaEntre(char v1, char v2) {
        ListaCD<Vertice> ruta = this.miGrafo.getRutaEntre(v1, v2);
        if(ruta.esVacia())
            return ("No existe ruta posible estre los Nodos!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }
    
    public String getDijkstra(char v1, char v2) {
       ListaCD<Vertice> ruta = this.miGrafo.rutaMinimaDijkstra(v1, v2);
        if(ruta.esVacia())
            return ("No existe ruta posible estre los Nodos!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }

    public String getRutaMasCorta(char v1, char v2) {
        ListaCD<Vertice> ruta = this.miGrafo.rutaMinimaDijkstra(v1, v2);
        if(ruta.esVacia())
            return ("No existe ruta posible estre los Nodos!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }

    public String getCicloEuleriano() {
        ListaCD<Vertice> ruta = this.miGrafo.getCicloEuleriano();
        if(ruta.esVacia())
            return ("No existe ciclo Euleriano en el Grafo!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }
    
    public String getCicloHamiltoniano() {
        ListaCD<Vertice> ruta = this.miGrafo.getCicloHamiltoniano();
        if(ruta.esVacia())
            return ("No existe ciclo Hamiltoniano en el Grafo!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }

    public String getCaminoEuleriano() {
        ListaCD<Vertice> ruta = this.miGrafo.getCaminoEuleriano();
        if(ruta.esVacia())
            return ("No existe Camino Euleriano en el Grafo!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }
    
    public String getCaminoHamiltoniano() {
        ListaCD<Vertice> ruta = this.miGrafo.getCaminoHamiltoniano();
        if(ruta.esVacia())
            return ("No existe Camino Hamiltoniano en el Grafo!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }

    
    public String getBEP(char val){
        ListaCD<Vertice> ruta = this.miGrafo.getBEP(val);
        if(ruta.esVacia())
            return ("No se puede determinar la BEP del Grafo!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }
    
    public String getBEA(char val){
        ListaCD<Vertice> ruta = this.miGrafo.getBEA(val);
        if(ruta.esVacia())
            return ("No se puede determinar la BEA del Grafo!");
        String cad="";
        for(int i=0; i<ruta.getTamanio(); i++){
            cad+= ruta.get(i).getInfo().toString();
            if(i+1!=ruta.getTamanio())
                cad+="-";
        }
        return (cad);
    }

    public int getCostoRutaMinima(char v1, char v2) {
        return (this.miGrafo.longRutaMinimaDijkstra(v1, v2));
    }

    
}
