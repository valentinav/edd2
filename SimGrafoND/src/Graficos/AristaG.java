/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Graficos;

import javafx.scene.control.Label;
import javafx.scene.shape.Line;

/**
 *
 *  @author Wilfred Uriel García  cod.1150204
 *          Yulieth Pabón Sánchez cod.1150159
 * 
 * Clase que representa el concepto de Arista Gráfica
 */
public class AristaG {

    
    private VerticeG vertA;
    private VerticeG vertB;
    private int peso;
    private boolean esVisit;
    private Line arco;
    private Label p;

    public AristaG(VerticeG vertA, VerticeG vertB, Line arco, int peso, Label p) {
        this.vertA = vertA;
        this.vertB = vertB;
        this.arco = arco;
        this.peso = peso;
        this.esVisit = false;
        this.p = p;
    }

    public VerticeG getVertA() {
        return vertA;
    }

    public void setVertA(VerticeG vertA) {
        this.vertA = vertA;
    }

    public VerticeG getVertB() {
        return vertB;
    }

    public void setVertB(VerticeG vertB) {
        this.vertB = vertB;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public boolean isEsVisit() {
        return esVisit;
    }

    public void setEsVisit(boolean esVisit) {
        this.esVisit = esVisit;
    }

    public Line getArco() {
        return arco;
    }

    public void setArco(Line arco) {
        this.arco = arco;
    }

    public Label getP() {
        return p;
    }

    public void setP(Label p) {
        this.p = p;
    }
    
    
    
    
    
}
